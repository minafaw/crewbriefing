﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CB.Core.Common;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Keys;
using CB.Core.Dtos.Base;
using Xamarin.Forms;

namespace CB.Core.Dtos
{
	public class FlightDto : BaseDtoModel
	{
		public ImageSource AirPlaneImage => ImageSource.FromFile("aeroplane.png");

		public int Id { get; set; }

		public MessageDto[] Messages;
		public OverflightCostDto OverflightCost;
		public string FlightLogId { get; set; }
		public string PpsName { get; set; }
		public string AcfTail { get; set; }
		public string Dep { get; set; }
		public string Dest { get; set; }
		public string Alt1 { get; set; }
		public string Alt2 { get; set; }
		public DateTime Std { get => _std; set { _std = value; RaisePropertyChanged(); } }
		public int Pax { get; set; }
		public double Fuel { get; set; }
		public int Load { get; set; }
		public string ValidHrs { get; set; }
		public int MinFl { get; set; }
		public int MaxFl { get; set; }
		public string EropsAltApts { get; set; }
		public List<string> AdequateApt { get; set; }
		public List<string> Fir { get; set; }
		public List<string> AltApts { get; set; }
		public string Toa { get; set; }
		public string Fmdid { get; set; }
		public string DestStdAlt { get; set; }
		public string FuelComp { get; set; }
		public string TimeComp { get; set; }
		public string FuelCont { get; set; }
		public string TimeCont { get; set; }
		public string PctCont { get; set; }
		public string Fuelmin { get; set; }
		public string TimeMin { get; set; }
		public string FuelTaxi { get; set; }
		public string TimeTaxi { get; set; }
		public string FuelExtra { get; set; }
		public string TimeExtra { get; set; }
		public string FuelLdg { get; set; }
		public string TimeLdg { get; set; }
		public string Zfm { get; set; }
		public string Gcd { get; set; }
		public string Esad { get; set; }
		public string Gl { get; set; }
		public string FuelBias { get; set; }
		public DateTime Sta { get; set; }
		public int SchbLockTime { get; set; }
		public string Disp { get; set; }
		public DateTime LastEditDate {
			get => _lastEditDate;
			set { _lastEditDate = value;
				RaisePropertyChanged();
				RaisePropertyChanged(nameof(UploadeDate4Show));

			} }
		public string FuelMinto { get; set; }
		public string TimeMinto { get; set; }
		public string Aramp { get; set; }
		public string TimeAct { get; set; }
		public string FuelAct { get; set; }
		public string DestEra { get; set; }
		public string TrafficLoad { get; set; }
		public string WeightUnit { get; set; }
		public string WindComponent { get; set; }
		public string CustomerDataPps { get; set; }
		public string CustomerDataScheduled { get; set; }
		public int Fl { get; set; }
		public int RouteDistNm { get; set; }
		public string RouteName { get; set; }
		public string RouteType { get; set; }
		public int EmptyWeight { get; set; }
		public int TotalDistance { get; set; }
		public int AltDist { get; set; }
		public int DestTime { get; set; }
		public int AltTime { get; set; }
		public int AltFuel { get; set; }
		public int HoldTime { get; set; }
		public int ReserveTime { get; set; }
		public int Cargo { get; set; }
		public double ActTow { get; set; }
		public double TripFuel { get; set; }
		public double HoldFuel { get; set; }
		public double Elw { get; set; }
		public string FuelPolicy { get; set; }
		public int Alt2Time { get; set; }
		public int Alt2Fuel { get; set; }
		public double MaxTom { get; set; }
		public double MaxLm { get; set; }
		public double MaxZfm { get; set; }
		public DateTime WeatherObsTime { get; set; }
		public DateTime WeatherPlanTime { get; set; }
		public string Mfci { get; set; }
		public string CruiseProfile { get; set; }
		public int TempTopOfClimb { get; set; }
		public string Climb { get; set; }
		public string Descend { get; set; }
		public string FuelPl { get; set; }
		public string DescendWind { get; set; }
		public string ClimbProfile { get; set; }
		public string DescendProfile { get; set; }
		public string HoldProfile { get; set; }
		public string StepClimbProfile { get; set; }
		public string FuelContDef { get; set; }
		public string FuelAltDef { get; set; }
		public string AmexsyStatus { get; set; }
		public int AvgTrack { get; set; }
		public TafDto DepTaf;
		public string DepMetar { get; set; }
		public NotamDto[] DepNotam;
		public TafDto DestTaf;
		public string DestMetar { get; set; }
		public NotamDto[] DestNotam;
		public TafDto Alt1Taf;
		public TafDto Alt2Taf;
		public string Alt1Metar { get; set; }
		public string Alt2Metar { get; set; }
		public NotamDto[] Alt1Notam;
		public NotamDto[] Alt2Notam;
		public List<RoutePointDto> RoutePoints;
		public List<CrewDto> Crews;
		//public Response Responce;
		public AtcDto AtcData;
		//public NextLeg NextLeg;
		//public FlightLevel[] OptFlightLevels;
		public NotamDto[] AdequateNotam;
		public NotamDto[] FirNotam;
		public NotamDto[] AlternateNotam;
		//public AltAirport[] Airports;
		public string[] EnrouteAlternates;
		public RoutePointDto[] Alt1Points;
		public RoutePointDto[] Alt2Points;
		//public AlternateAirport[] StdAlternates;
		public string[] CustomerData;
		private DateTime _std;
		private string _airports4ShowDep;
		private string _airports4ShowDst;
		private DateTime _lastEditDate;

		//public RCFData RcfData;        
		public string ToAlt { get; set; }
		public string DepIata { get; set; }
		public string DestIata { get; set; }
		//public SID SidPlanned;
		//public SID[] SidAlternatives;
		public int FinalReserveMinutes { get; set; }
		public int FinalReserveFuel { get; set; }
		public int AddFuelMinutes { get; set; }
		public int AddFuel { get; set; }
		public string FlightSummary { get; set; }
		// height of image in details - trick for binding
		
		public LineBreakMode DepDstLineBreakMode { get; set; } = LineBreakMode.WordWrap;
		public string Eta4Show
		{
			get
			{
				if (string.IsNullOrEmpty(AtcData?.AtcEet) || AtcData.AtcEet.Length < 4)
				{
					return string.Empty;
				}

				return AtcData.AtcEet.Substring(0, 2) + ":" + AtcData.AtcEet.Substring(2, 2);
			}
		}
		public string AtcCtot => AtcData == null ? string.Empty : AtcData.AtcCtot;

		public string GetFlightIdToShow()
		{
			if (string.IsNullOrEmpty(FlightLogId))
			{
				return string.Empty;
			}
			var view01 = string.Empty;
			var flightId1 = FlightLogId;
			var flightId = flightId1.IndexOf("-", StringComparison.Ordinal) != -1 ? flightId1.Split('-') : new[] { flightId1 };
			var firstBit = flightId[0];
			if (firstBit.IndexOf("(", StringComparison.Ordinal) != -1)
			{
				//Step 2
				var fLogIDbits = firstBit.Split('(');
				firstBit = fLogIDbits[0];
				var secondBit = fLogIDbits[1];
				view01 = secondBit.TrimEnd(')');
			}
			var view1 = firstBit;
			var result = view1;
			if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(view01))
			{
				result += "\n";
			}
			return result + view01;
		}

		public string Std4Show
		{
			get
			{
				var std = CommonStatic.GetPreparedStd(Std);

				if (AtcCtot != null && !AtcCtot.Trim().Equals(""))
				{
					std += " / ";
				}
				return std;
			}
		}

		public string Sta4Show => DateTime.Compare(Sta, DateTime.MinValue.ToUniversalTime()) == 0 ? "No STA" : CommonStatic.GetPreparedStd(Sta);

		public bool IsRecalculated => !string.IsNullOrEmpty(FlightLogId) && FlightLogId.EndsWith("-R");
		public string Recalculated4Show => IsRecalculated ? Resource.recalculated : string.Empty;

		public string CrewCmd4Show => GetCrew4Show(Enums.CrewType.Cmd);

		public double SmallFontSize => CommonStatic.LabelFontSize(NamedSize.Small);

		public double MediumFontSize => CommonStatic.LabelFontSize(NamedSize.Medium);

		public string CrewCop4Show => GetCrew4Show(Enums.CrewType.Cop);

		public string UploadeDate4Show => CommonStatic.GetFormatedDateTime(LastEditDate);

		public string Airports4ShowDep { get => _airports4ShowDep; set { _airports4ShowDep = value; RaisePropertyChanged(); }
		}
		public string Airports4ShowDst { get => _airports4ShowDst; set { _airports4ShowDst = value; RaisePropertyChanged(); } }
		private string GetCrew4Show(Enums.CrewType crewType)
		{
			if (Crews == null)
			{
				return string.Empty;
			}			
			var selectedCrew =  Crews.Where(c => c.CrewType.ToLower() == crewType.ToString().ToLower() ).FirstOrDefault();
			if (selectedCrew != null) return selectedCrew.Initials;
			return "****";

		}
		public double DateTimeFormattedWidth => CommonStatic.Instance.DateTimeFormattedWidth;
		public double MediumFontBiggerForDetails => CommonStatic.LabelFontSize(NamedSize.Medium);

		public double SmallFontBiggerForDetails => CommonStatic.LabelFontSize(NamedSize.Small);
	}
}
