﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Dtos.Base;

namespace CB.Core.Dtos
{
    public class OverflightCostDto : BaseDtoModel
	{
	    public FirOverflightCostDto[] Cost { get; set; }
	    public string Currency { get; set; }
	}
}
