﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Common.HelperClasses;
using CB.Core.Dtos.Base;
using Xamarin.Forms;

namespace CB.Core.Dtos
{
    public class FlightListItemDto : BaseDtoModel
	{
		public int ID { get; set; }
		public List<CrewDto> CrewDto { get; set; }
	    public string Alt1 { get; set; }
	    public string Alt2 { get; set; }
	    public int FPLID { get; set; }
	    public string FlightlogId { get; set; }
	    public string DEP { get; set; }
	    public string DEST { get; set; }
	    public DateTime STD { get; set; }
	    public DateTime STA { get; set; }
	    public string ACFTAIL { get; set; }
	    //public ushort ATCTime { get; set; }	   
	    public string ATCCtot { get; set; }
	    public ushort ATCEET { get; set; }
	    public string TOA { get; set; }
	    public string ATCID { get; set; }
	    public DateTime LastEdit { get; set; }
		public string DestAirportFull { get; set; }
		public string DepAirportFull { get; set; }
		public bool IsRecalculated
		{
			get
			{
				//return ID % 2 != 0;
				if (string.IsNullOrEmpty(FlightlogId))
				{
					return false;
				}
				return FlightlogId.EndsWith("-R");
			}
		}
		public string FirstListColumn => GetFlightIdToShow(FlightlogId);
		public Thickness FlightListItemGridTopPadding => CommonStatic.Instance.FlightListItemGridVertPadding;
		public Thickness FlightListItemGridRightPadding => CommonStatic.Instance.FlightListItemGridRightPadding;
		public static string GetFlightIdToShow(string flightlogId)
		{
			if (string.IsNullOrEmpty(flightlogId))
			{
				return string.Empty;
			}
			var view01 = string.Empty;
			var flightId1 = flightlogId;
			var flightId = flightId1.IndexOf("-", StringComparison.Ordinal) != -1 ? flightId1.Split('-') : new[] { flightId1 };
			var firstBit = flightId[0];
			if (firstBit.IndexOf("(", StringComparison.Ordinal) != -1)
			{
				//Step 2
				var fLogIDbits = firstBit.Split('(');
				firstBit = fLogIDbits[0];
				var secondBit = fLogIDbits[1];
				view01 = secondBit.TrimEnd(')');
			}
			var view1 = firstBit;
			var result = view1;
			if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(view01))
			{
				result += "\n";
			}
			return result + view01;
		}
		public string Dep_Dest => (DEP ?? "") + "-" + (DEST ?? "");
		public Thickness FlightListColumn2Padding => CommonStatic.Instance.FlightListColumn2Padding;
		public string PreparedStd => CommonStatic.GetPreparedStd(STD);
		public string MarkImagePath { get; set; }
	    public double MarkImageHeight => CommonStatic.Instance.GetMediumHeight;
	    public double MarkImageWidth => CommonStatic.Instance.GetMediumHeight * 0.65;
	    public double RowMarkHeight => CommonStatic.Instance.GetMediumHeight * 2.5;
	    public bool IsMarkerVisible
	    {
		    get
		    {
			    if (string.IsNullOrEmpty(MarkImagePath))
			    {
				    return false;
			    }
			    return MarkImagePath.ToLower().Contains("vert_mark.png");
		    }
	    }
	    public bool IsImageVisible => !IsMarkerVisible;

	    private bool _isSelected;
	    public bool IsSelected
	    {
		    get => _isSelected;
		    set
		    {
			    _isSelected = value;
				RaisePropertyChanged();
		    }
	    }
		
	}
}
