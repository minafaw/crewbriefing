﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Dtos.Base;

namespace CB.Core.Dtos
{
    public class CrewDto : BaseDtoModel
	{
	    public string ID { get; set; }
	    public string CrewType { get; set; }
	    public string CrewName { get; set; }
		public string Initials { get; set; }
	    public string GSM { get; set; }
	    public int FlightId { get; set; }
	}
}
