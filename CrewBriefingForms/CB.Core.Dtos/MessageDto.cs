﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Dtos.Base;

namespace CB.Core.Dtos
{
    public class MessageDto: BaseDtoModel
	{
	    public string Subject;
	    public string Text;
	    public string SentFrom;
	    public System.DateTime SentTime;
	    public System.DateTime ValidFrom;
	    public System.DateTime ValidTo;
	    public bool HighPriority;
	    public MessageRecipientDto[] Recipients;
	}
}
