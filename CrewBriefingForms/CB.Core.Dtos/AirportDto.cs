﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CB.Core.Dtos
{
	public class AirportDto
	{
		public string Name { get; set; }
		public string ICAO { get; set; }
		public string IATA { get; set; }
		public string Country { get; set; }
		public string Metar { get; set; }
		public string VAR { get; set; }
		public string ELEV { get; set; }
		public string LAT { get; set; }
		public string LON { get; set; }
		public string RunWL { get; set; }
		public string FIR { get; set; }
	}
}
	

