﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Dtos.Base;

namespace CB.Core.Dtos
{
    public class FirOverflightCostDto : BaseDtoModel
	{
	    public string Fir;
	    public int Distance;
	    public int Cost;
	}
}
