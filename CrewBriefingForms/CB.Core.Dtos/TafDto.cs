﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Dtos.Base;

namespace CB.Core.Dtos
{
    public class TafDto : BaseDtoModel
	{
	    public string Type { get; set; }
	    public string Text { get; set; }
	}
}
