﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Dtos.Base;

namespace CB.Core.Dtos
{
    public class NotamDto : BaseDtoModel
	{
	    public string Number;
	    public string Text;
	    public DateTime FromDate;
	    public DateTime ToDate;
	    public int FromLevel;
	    public int ToLevel;
	    public string Fir;
	    public string QCode;
	    public string ECode;
	    public string Icao;
	    public string UniformAbbreviation;
	    public int Year;
	}
}
