﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Dtos.Base;

namespace CB.Core.Dtos
{
    public class MessageRecipientDto : BaseDtoModel
	{
	    public string RecipientType { get; set; }
	    public string Recipient { get; set; }
	}
}
