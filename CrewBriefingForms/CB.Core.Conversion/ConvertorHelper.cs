﻿using CB.Core.Dtos;
using CB.Core.Entities;
using CB.Core.Models.ResponseModels;
using System;
using System.Collections.Generic;
using CB.Core.Common.Extensions;

namespace CB.Core.Conversion
{
    public static class ConvertorHelper
	{

		#region FlightListItem Convertors
		public static List<Entities.FlightListItem> FlightListItemModelConvertor(List<Models.FlightListItem> flightListItems)
        {
            List<Entities.FlightListItem> flightLists = new List<FlightListItem>();
            foreach (var item in flightListItems){
                var flight = new FlightListItem
                {
                    ACFTAIL = item.ACFTAIL,
                    Alt1 = item.Alt1,
                    Alt2 = item.Alt2,
                    ATCCtot = item.ATCCtot,
                    ATCID = item.ATCID,
                    DEP = item.DEP,
                    DEST = item.DEST,
                    DepAirportFull = item.DepAirportFull,
                    DestAirportFull = item.DestAirportFull,
                    FlightlogID = item.FlightlogID,
                    FPLID = item.FPLID,
                    ID = item.ID,
                    LastEdit = item.LastEdit.ToDateTimeOffset(),
                    STA = item.STA.ToDateTimeOffset(),
                    STD = item.STD.ToDateTimeOffset(),
                    TOA = item.TOA,

                    CreatedDate = DateTime.UtcNow,
                    //ATCEET = item                         ,
                   //CrewItem = new List<Entities.Crew>(CrewConvertor(item.Crew , item.ID)),

                };
				var crewEntitiesList = new List<Entities.Crew>(CrewConvertor(item.Crew, item.ID));
				if (crewEntitiesList != null && crewEntitiesList.Count > 0)
				{
					foreach (var crewItem in crewEntitiesList)
					{
						flight.CrewItem.Add(crewItem);
					}
				}

				flightLists.Add(flight);
            }
            return flightLists;

        }


		public static Entities.FlightListItem FlightListItemModelConvertor(Models.FlightListItem item)
		{
			
				var flight = new FlightListItem
				{
					ACFTAIL = item.ACFTAIL,
					Alt1 = item.Alt1,
					Alt2 = item.Alt2,
					ATCCtot = item.ATCCtot,
					ATCID = item.ATCID,
					DEP = item.DEP,
					DEST = item.DEST,
					DepAirportFull = item.DepAirportFull,
					DestAirportFull = item.DestAirportFull,
					FlightlogID = item.FlightlogID,
					FPLID = item.FPLID,
					ID = item.ID,
					LastEdit = item.LastEdit.ToDateTimeOffset(),
					STA = item.STA.ToDateTimeOffset(),
					STD = item.STD.ToDateTimeOffset(),
					TOA = item.TOA,

					CreatedDate = DateTime.UtcNow,
					//ATCEET = item                         ,
					//CrewItem = new List<Entities.Crew>(CrewConvertor(item.Crew , item.ID)),

				};
			var crewEntitiesList = new List<Entities.Crew>(CrewConvertor(item.Crew, item.ID));
			if(crewEntitiesList != null && crewEntitiesList.Count > 0)
			{
				foreach (var crewItem in crewEntitiesList)
				{
					flight.CrewItem.Add(crewItem);
				}
			}
			return flight;

		}
		public static List<FlightListItemDto> FlightListItemConvertor(List<Entities.FlightListItem> flightListItems)
		{
			List<FlightListItemDto> itemList = new List<FlightListItemDto>();
			foreach (var item in flightListItems)
			{
				FlightListItemDto dto = new FlightListItemDto
				{
					ACFTAIL = item.ACFTAIL					,
					Alt1 = item.Alt1						,
					Alt2 = item.Alt2						,
					ATCCtot = item.ATCCtot					,
					ATCID = item.ATCID						,
					DEP = item.DEP							,
					DEST = item.DEST						,
					DepAirportFull = item.DepAirportFull    ,
					DestAirportFull = item.DestAirportFull  ,
					FlightlogId = item.FlightlogID          ,
					FPLID = item.FPLID						,
					ID = item.ID							,
					LastEdit = item.LastEdit.UtcDateTime	,
					STA = item.STA.UtcDateTime				,
					STD = item.STD.UtcDateTime				,
					TOA = item.TOA							,
					//ATCEET = item							,
					CrewDto = CrewDtoConvertor( new List<Entities.Crew>(item.CrewItem))   ,

				};
				itemList.Add(dto);
			}
			return itemList;

		}
		public static List<FlightListItemDto> FlightListItemConvertor(List<Models.FlightListItem> flightListItems)
		{
			List<FlightListItemDto> itemList = new List<FlightListItemDto>();
			foreach (var item in flightListItems)
			{
				FlightListItemDto dto = new FlightListItemDto
				{
					ACFTAIL = item.ACFTAIL,
					Alt1 = item.Alt1,
					Alt2 = item.Alt2,
					ATCCtot = item.ATCCtot,
					ATCID = item.ATCID,
					DEP = item.DEP,
					DEST = item.DEST,
					DepAirportFull = item.DepAirportFull,
					DestAirportFull = item.DestAirportFull,
					FlightlogId = item.FlightlogID,
					FPLID = item.FPLID,
					ID = item.ID,
					LastEdit = item.LastEdit,
					STA = item.STA,
					STD = item.STD,
					TOA = item.TOA,
					//ATCEET = item							,
					CrewDto = CrewDtoConvertor(new List<Models.Crew>(item.Crew) , item.ID),

				};
				itemList.Add(dto);
			}
			return itemList;
		}
		#endregion

		#region Flight 
		public static FlightDto FlightDtoConvertor(Entities.Flight flight)
		{
			FlightDto flightDto = new FlightDto()
			{
				AcfTail			= flight.AcfTail,
				ActTow			= flight.ActTow,
				AddFuel			= flight.AddFuel,
				AddFuelMinutes  = flight.AddFuelMinutes,
				AdequateApt     = flight.AdequateApt as List<string>,
				//AdequateNotam   = flight.AdequateNotam,
				Airports4ShowDep = flight.Airports4ShowDep,
				Airports4ShowDst = flight.Airports4ShowDst,
				Alt1             = flight.Alt1,
				Alt1Metar        = flight.Alt1Metar,
				Alt2             = flight.Alt2 ,
				Alt2Fuel         = flight.Alt2Fuel,
				Alt2Metar        = flight.Alt2Metar,
				Alt2Time         = flight.Alt2Time ,
				AltApts          = flight.AltApts as List<string>,
				AltDist          = flight.AltDist,
				AltFuel          = flight.AltFuel,
				Sta              = flight.Sta.UtcDateTime,
				Std              = flight.Std.UtcDateTime,
				Dep              = flight.Dep,
				Dest			 = flight.Dest,
				FlightSummary    = flight.FlightSummary,
				RoutePoints      = RoutePointDtoConvertor (flight.RoutePoints as List<Entities.RoutePoint>),
				LastEditDate     = flight.LastEditDate.UtcDateTime,
				Crews            = CrewDtoConvertor(flight.Crews as List<Entities.Crew>),
				FlightLogId      = flight.FlightLogId,
				AtcData          = AtcDtoConvertor(flight?.AtcData, flight.Id) ,

			};

			return flightDto;
		}

		private static AtcDto AtcDtoConvertor(Atc atcData, int id)
		{
			if (atcData == null) return null;
			AtcDto atcDto = new AtcDto
			{
				AtcCtot = atcData.AtcCtot,
				AtcEet = atcData.AtcEet,
				FlightId = id,
			};
			return atcDto;
		}

		public static FlightDto FlightDtoConvertor(GetFlightResult flight)
		{
			FlightDto flightDto = new FlightDto()
			{
				AcfTail = flight.ACFTAIL,
				ActTow = flight.ActTOW,
				AddFuel = flight.AddFuel,
				AddFuelMinutes = flight.AddFuelMinutes,
				AdequateApt = flight.AdequateApt.String ,
				Alt1 = flight.ALT1,
				Alt2 = flight.ALT2,
				Alt2Fuel = flight.Alt2Fuel,				
				Alt2Time = flight.Alt2Time,
				AltApts = flight.AltApts.String ,
				AltDist = flight.AltDist,
				AltFuel = flight.AltFuel,
				Sta = flight.STA,
				Std = flight.STD,
				Dep = flight.DEP,
				Dest = flight.DEST,
				Crews = CrewDtoConvertor(flight.Crews?.Crew , flight.ID),
                AtcData = AtcDtoConvertor(flight?.ATCData , flight.ID),
				LastEditDate = flight.LastEditDate,
				FlightLogId = flight.FlightLogID,
				FlightSummary = flight.FlightSummary,
                RoutePoints = RoutePointDtoConvertor(flight?.RoutePoints?.RoutePoint , flight.ID ),
				
			};
			

			return flightDto;

		}
		public static Entities.Flight FlightConvertor(GetFlightResult flight)
		{
			Entities.Flight flightDto = null;
			try {
				flightDto  = new Entities.Flight()
			{
                Id = flight.ID,
				AcfTail = flight.ACFTAIL,
				ActTow = flight.ActTOW,
				AddFuel = flight.AddFuel,
				AddFuelMinutes = flight.AddFuelMinutes,
				//AdequateApt = flight.AdequateApt.String,
				Alt1 = flight.ALT1,
				Alt2 = flight.ALT2,
				Alt2Fuel = flight.Alt2Fuel,
				Alt2Time = flight.Alt2Time,
				//AltApts = flight.AltApts.String,
				AltDist = flight.AltDist,
				AltFuel = flight.AltFuel,
				Sta = flight.STA,
				Std = flight.STD,
				Dep = flight.DEP,
				Dest = flight.DEST,
				Crews = CrewConvertor(flight.Crews.Crew),
                AtcData = AtcConvertor(flight.ATCData , flight.ID ),
				LastEditDate = flight.LastEditDate,
				FlightLogId = flight.FlightLogID,
				FlightSummary = flight.FlightSummary,
				RoutePoints = RoutePointConvertor(flight?.RoutePoints?.RoutePoint),


			};
		      }catch(Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e);
			}

			return flightDto;

		}
		#endregion

		#region Crew Convertors 
		public static List<CrewDto> CrewDtoConvertor(List<Models.Crew> crews, int flightId)
		{
			if (crews == null) return null;
			List<CrewDto> itemList = new List<CrewDto>();
			foreach (var item in crews)
			{
				CrewDto crewDto = new CrewDto
				{
					CrewName = item.CrewName,
					CrewType = item.CrewType,
					FlightId = flightId,
					GSM = item.Gsm,
					ID = item.Id,
					Initials = item.Initials
				};
				itemList.Add(crewDto);
			}
			return itemList;
		}

		public static CrewDto CrewDtoConvertor(Models.Crew item, int flightId)
		{
			if (item == null) return null;
			
				CrewDto crewDto = new CrewDto
				{
					CrewName = item.CrewName,
					CrewType = item.CrewType,
					FlightId = flightId,
					GSM = item.Gsm,
					ID = item.Id,
					Initials = item.Initials
				};
			return crewDto;
		}
		public static List<Entities.Crew> CrewConvertor(List<Models.Crew> crews, int flightId)
		{
			List<Entities.Crew> itemList = new List<Entities.Crew>();
			foreach (var item in crews)
			{
				Entities.Crew crew = new Entities.Crew
				{
					CrewName = item.CrewName,
					CrewType = item.CrewType,
					FlightId = flightId,
					Gsm = item.Gsm,
					Id = item.Id,
					Initials = item.Initials
				};
				itemList.Add(crew);
			}
			return itemList;
		}
		public static List<CrewDto> CrewDtoConvertor(List<Models.Crew> crews)
		{
			List<CrewDto> itemList = new List<CrewDto>();
			foreach (var item in crews)
			{
				CrewDto crewDto = new CrewDto
				{
					CrewName = item.CrewName,
					CrewType = item.CrewType,
					GSM = item.Gsm,
					ID = item.Id,
					Initials = item.Initials
				};
				itemList.Add(crewDto);
			}
			return itemList;
		}
		public static List<Entities.Crew> CrewConvertor(List<Models.Crew> crews)
		{
			List<Entities.Crew> itemList = new List<Entities.Crew>();
			foreach (var item in crews)
			{
				Entities.Crew crewDto = new Entities.Crew
				{
					CrewName = item.CrewName,
					CrewType = item.CrewType,
					Gsm = item.Gsm,
					Id = item.Id,
					Initials = item.Initials
				};
				itemList.Add(crewDto);
			}
			return itemList;
		}
		public static CrewDto CrewDtoConvertor(Models.Crew item)
		{

			CrewDto crewDto = new CrewDto
			{
				CrewName = item.CrewName,
				CrewType = item.CrewType,
				GSM = item.Gsm,
				ID = item.Id,
				Initials = item.Initials

			};
			return crewDto;
		}
		public static List<CrewDto> CrewDtoConvertor(List<Entities.Crew> crews)
		{
			List<CrewDto> itemList = new List<CrewDto>();
			foreach (var item in crews)
			{
				CrewDto crewDto = new CrewDto
				{
					CrewName = item.CrewName,
					CrewType = item.CrewType,
					FlightId = item.FlightId,
					GSM = item.Gsm,
					ID = item.Id,
					Initials = item.Initials
				};
				itemList.Add(crewDto);
			}
			return itemList;
		}

		#endregion

		#region Route Convertors 
		public static List<RoutePointDto> RoutePointDtoConvertor(List<Models.ResponseModels.RoutePoint> routePoint, int flightId)
		{
			List<RoutePointDto> itemList = new List<RoutePointDto>();
			if (routePoint == null) return itemList;

			foreach (var item in routePoint)
			{
				RoutePointDto crewDto = new RoutePointDto
				{
					AccDist = item.ACCDIST,
					AccTime  = int.Parse(item.ACCTIME),
					DistRemaining = item.DistRemaining,
					Lon = item.LON,
					Lat = item.LAT,
                    FlightId = flightId ,
					IdEnt = item.IDENT,
					
				};
				itemList.Add(crewDto);
			}
			return itemList;
		}
		public static List<Entities.RoutePoint> RoutePointConvertor(List<Models.ResponseModels.RoutePoint> routePoint)
		{
			List<Entities.RoutePoint> itemList = new List<Entities.RoutePoint>();
			foreach (var item in routePoint)
			{
				Entities.RoutePoint crewDto = new Entities.RoutePoint
				{
					AccDist = item.ACCDIST,
					AccTime = int.Parse(item.ACCTIME),
					DistRemaining = item.DistRemaining,
					Lon = item.LON,
					Lat = item.LAT,
					IdEnt = item.IDENT,

				};
				itemList.Add(crewDto);
			}
			return itemList;
		}
		public static List<RoutePointDto> RoutePointDtoConvertor(List<Entities.RoutePoint> routePoint)
		{
			List<RoutePointDto> itemList = new List<RoutePointDto>();
			foreach (var item in routePoint)
			{
				RoutePointDto crewDto = new RoutePointDto
				{
					AccDist = item.AccDist,
					AccTime = item.AccDist,
					DistRemaining = item.DistRemaining,
					Lon = item.Lon,
					Lat = item.Lat,
					IdEnt = item.IdEnt,

				};
				itemList.Add(crewDto);
			}
			return itemList;
		}
		#endregion

		#region Atc Convertors 
		public static AtcDto AtcDtoConvertor(ATCData aTCData , int flightId)
		{
			if(aTCData == null) return null;
			AtcDto atcDto = new AtcDto
			{
				AtcCtot = aTCData.ATCCtot,
				AtcEet = aTCData.ATCEET,
                FlightId = flightId,
			};
			return atcDto;
		}
        public static Entities.Atc AtcConvertor(ATCData aTCData , int flightId)
		{
			if (aTCData == null) return null;
			Entities.Atc atcDto = new Entities.Atc
			{
				AtcCtot = aTCData.ATCCtot,
				AtcEet = aTCData.ATCEET,
                FlightId = flightId,
			};
			return atcDto;
		}
		#endregion

		#region Airport Convertors 
		public static AirportDto AirportDtoConvertor(GetAirportResult airport)
		{
			AirportDto airportDto = new AirportDto
			{
			   ELEV = airport.ELEV,
			   FIR  = airport.FIR,
			   IATA  = airport.IATA,
			   ICAO = airport.ICAO,
			   LAT  = airport.LAT,
			   LON  = airport.LON,
			   Metar  =airport.Metar,
			   Name = airport.Name,
			   RunWL = airport.RunWL,
			   VAR = airport.VAR
			};

			return airportDto;
		}
		public static AirportDto AirportDtoConvertor(Models.ResponseModels.Airport airport)
		{
			AirportDto airportDto = new AirportDto
			{
				ELEV = airport.ELEV,
				FIR = airport.FIR,
				IATA = airport.IATA,
				ICAO = airport.ICAO,
				LAT = airport.LAT,
				LON = airport.LON,
				Metar = airport.Metar,
				Name = airport.Name,
				RunWL = airport.RunWL,
				VAR = airport.VAR
			};

			return airportDto;
		}
		public static Entities.Airport AirportConvertor(GetAirportResult airport)
		{
			Entities.Airport airportEntity = new Entities.Airport
			{
				Elev = airport.ELEV,
				Fir = airport.FIR,
				Iata = airport.IATA,
				Icao = airport.ICAO,
				Lat = airport.LAT,
				Lon = airport.LON,
				Metar = airport.Metar,
				Name = airport.Name,
				RunWl = airport.RunWL,
				Var = airport.VAR
			};

			return airportEntity;
		}
		public static AirportDto AirportDtoConvertor(Entities.Airport airport)
		{
			AirportDto airportEntity = new AirportDto
			{
				ELEV = airport.Elev,
				FIR = airport.Fir,
				IATA = airport.Iata,
				ICAO = airport.Icao,
				LAT = airport.Lat,
				LON = airport.Lon,
				Metar = airport.Metar,
				Name = airport.Name,
				RunWL = airport.RunWl,
				VAR = airport.Var
			};

			return airportEntity;
		}
		public static List<CB.Core.Entities.Airport> AirportModelConvertor(List<Models.ResponseModels.Airport> airportListItems)
		{

			List<Entities.Airport> airportList = new List<Entities.Airport>();
			foreach(var airport in airportListItems)
			{
				Entities.Airport airportEntity = new Entities.Airport
				{
					Elev = airport.ELEV,
					Fir = airport.FIR,
					Iata = airport.IATA,
					Icao = airport.ICAO,
					Lat = airport.LAT,
					Lon = airport.LON,
					Metar = airport.Metar,
					Name = airport.Name,
					RunWl = airport.RunWL,
					Var = airport.VAR
				};
				airportList.Add(airportEntity);
			}
			

			return airportList;
		}
		#endregion

	}
}
