﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.CustomControls.NavigationService;
using CrewBriefing.Standard.Views;
using CrewBriefing.Standard.Views.FlightDetials;
using CrewBriefingForms.Helper;
using Realms;
using Xamarin.Forms;
using CommonStatic = CrewBriefing.Standard.Helper.CommonStatic;
using FlightMapPage = CrewBriefing.Standard.Views.FlightMapPage;
using PlatformSpecialRequirment = CrewBriefingForms.Helper.PlatformSpecialRequirment;

namespace CrewBriefing.Standard
{
    public partial class App : Application
    {
		
		public static double ScreenHeight;
        public static double ScreenWidth;
		

		public static readonly string BinqMapApiKey = "3JF8CqrVCeBXPqggXdDE~wUrdyIKdGZ-VDUqgWnqZgg~AolBKlv38Abz3AQOQRixqNi7Tce_qFGxCY33OlLHhchnltKQHrXPVrkbwuJoRw90";
		public static readonly ulong RealmDataBaseVersion = 1;
        // see https://www.bingmapsportal.com/Application
        public static bool GoogleMapInitialized = false;
        public static INavigationService NavigationService { get; } = new ViewNavigationService();

		public App()
        {
			
			InitializeComponent();
			ThreadHelper.Init(SynchronizationContext.Current);
			ScreenWidth = CommonStatic.Instance.ScreenWidth;

			MessagingCenter.Subscribe<App, string>(this, "PushNotification", (arg1, arg2) =>
            {
                ToastUtils.ShowTemporayMessage(Enums.ToastType.Info, arg2);
            });
            IntiateApp();
			//InitRealm();
	        RegisterPages();
			
		}

		private void InitRealm()
		{
			// TODO Recreate database if its old 
			RealmConfiguration config = new RealmConfiguration()
			{
				ShouldDeleteIfMigrationNeeded = true  ,
				SchemaVersion  = RealmDataBaseVersion ,
				MigrationCallback = (migration, oldSchemaVersion) =>
				{
					
				}

			};

			var otherRealm = Realm.GetInstance(config);
		}

		protected async override void OnStart()
        {
            base.OnStart();
            await StartUp.Intialize();
            Current.Resources["Locator"] = new ViewModelLocator();

            IntiateRootPage();
        }

        private void IntiateRootPage()
        {
            string PageName = nameof(LoginPage);

            var quickLogin = AppSetting.Instance.GetValueOrDefault(CommanConstants.QuickLoginKey, false);
            if (quickLogin)
            {
                string username = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.UserNameKey, null),
                       password = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.PasswordKey, null);

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    PageName = nameof(MasterDetialMainPage);
                }
            }
            else
            {
                AppSetting.Instance.AddOrUpdateValue(CommanConstants.IsLoginedInKey, false);
            }


            SetRootPage(PageName);
        }

        private void RegisterPages()
	    {
		    NavigationService.Configure(nameof(FlightListPage)           , typeof(FlightListPage));
			NavigationService.Configure(nameof(MasterDetialMainPage)     , typeof(MasterDetialMainPage));
		    NavigationService.Configure(nameof(LoginPage)                , typeof(LoginPage));
			NavigationService.Configure(nameof(FlightDetialsMain)        , typeof(FlightDetialsMain));
			//NavigationService.Configure(nameof(FlightDetialContent)      , typeof(FlightDetialContent));
			NavigationService.Configure(nameof(HelpPage)                 , typeof(HelpPage));
			NavigationService.Configure(nameof(AboutPage)                , typeof(AboutPage));
			NavigationService.Configure(nameof(TabletMainPage)           , typeof(TabletMainPage));
			NavigationService.Configure(nameof(FlightMapPage)            , typeof(FlightMapPage));
			NavigationService.Configure(nameof(FlightSummaryPage)        , typeof(FlightSummaryPage));
			NavigationService.Configure(nameof(MenuPage)                 , typeof(MenuPage));
			
			


		}

	    public void SetRootPage(string rootPageName)
	    {
		    var mainPage = ((ViewNavigationService) NavigationService).SetRootPage(rootPageName);
		    MainPage = mainPage;
	    }

	    private static void IntiateApp()
        {
            var textSizes = DependencyService.Get<ITextMeasuring>();

            var coefHeight = PlatformSpecialRequirment.GetTextHightInGrid();

            CommonStatic.SmallHeight = textSizes.CalculateHeight("ASDFW", NamedSize.Small) * coefHeight;
            CommonStatic.Instance.MediumHeight = textSizes.CalculateHeight("ASDFW", NamedSize.Medium) * coefHeight;
            CommonStatic.Instance.PicTailWidth = textSizes.CalculateWidth("Tail:  ", NamedSize.Small);
            CommonStatic.Instance.DateTimeFormattedWidth = textSizes.CalculateWidth(CommonStatic.GetFormatedDateTime(DateTime.Now), NamedSize.Small);
            if (Device.Idiom == TargetIdiom.Tablet)
            {
                CommonStatic.Instance.GetDetailsHeight = (CommonStatic.Instance.MediumHeight + CommonStatic.LabelFontSize(NamedSize.Medium) * 1.3 + CommonStatic.SmallHeight * 3) * 1.2;
            }
            else
            {
                CommonStatic.Instance.GetDetailsHeight = (CommonStatic.Instance.MediumHeight + CommonStatic.LabelFontSize(NamedSize.Medium) * 1.3 + CommonStatic.SmallHeight * 3) * 1.6;
            }
        }


       
        public static async Task Sleep(int ms)
        {
            await Task.Delay(ms);
        }

	
	}
}
