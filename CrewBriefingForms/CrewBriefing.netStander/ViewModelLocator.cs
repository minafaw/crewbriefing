﻿using CB.Core.DAL;
using CB.Core.Services;
using CB.Core.ViewModels;
using CommonServiceLocator;
using CrewBriefing.Standard.Controls;
using CrewBriefing.Standard.ViewModels;
using CrewBriefing.Standard.Views;

namespace CrewBriefing.Standard
{
	public class ViewModelLocator
    {
		//static ViewModelLocator()
		//{
		//	 StartUp.Intialize();
		//}
		public static ApiServices ApiServices => ServiceLocator.Current.GetInstance<ApiServices>();
		public static DalServices DalServices => ServiceLocator.Current.GetInstance<DalServices>();
		public static LoginPage LoginPage => ServiceLocator.Current.GetInstance<LoginPage>();
		public static MenuPage MenuPage => ServiceLocator.Current.GetInstance<MenuPage>();
		
		public static StackLayoutFlightList StackLayoutFlightList => ServiceLocator.Current.GetInstance<StackLayoutFlightList>();
		public static LoginPageViewModel LoginPageViewModel => ServiceLocator.Current.GetInstance<LoginPageViewModel>();

		public static FlightListViewModel FlightListViewModel => ServiceLocator.Current.GetInstance<FlightListViewModel>();
		public static FlightDetailsViewModel FlightDetailsViewModel => ServiceLocator.Current.GetInstance<FlightDetailsViewModel>();
		public static FlightDetialListItem FlightDetialListItem => ServiceLocator.Current.GetInstance<FlightDetialListItem>();
		public static FilterPageViewModel FilterPageViewModel => ServiceLocator.Current.GetInstance<FilterPageViewModel>();

		

	}
}
