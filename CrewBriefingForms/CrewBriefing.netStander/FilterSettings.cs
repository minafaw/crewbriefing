﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.Conversion;
using CB.Core.DAL;
using CB.Core.DAL.Interfaces;
using CB.Core.Dtos;
using CB.Core.Entities;
using CB.Core.Models;
using CrewBriefing.Standard.Interfaces;
using static CB.Core.Common.Keys.Enums;

namespace CrewBriefing.Standard
{
    public class FilterSettings : IFilterSettings
	{
		private readonly IDalServices _dalServices;
		private readonly IFilterDbSettingsRepository filterDbSettingsRepository;
		private readonly IFlightListItemRepository flightListItemRepository;

		public FilterSettings(IDalServices dalServices , IFilterDbSettingsRepository filterDbSettingsRepository, IFlightListItemRepository flightListItemRepository)
		{
			_dalServices = dalServices;
			this.filterDbSettingsRepository = filterDbSettingsRepository;
			this.flightListItemRepository = flightListItemRepository;
		}

        private static readonly string[] TextFilterSeparator = { " " };
		
		private static List<string> _tail = new List<string>();
        private static List<string> _crew = new List<string>();
        private static List<string> _dep = new List<string>();
        private static List<string> _dest = new List<string>();
        private static List<string> _flightNo = new List<string>();

        // default value for STD
        public const int DefaultValueStd = 2;

        private static int _sortField = 2; // STD/CTOT default

        private static int _sortType = 0, // 0 = asc, 1 = desc
        _stdIndex1 = DefaultValueStd;


        private static string _filterOn;

        //private static bool stdEnabled = false;
        private static DateTime _stdDate1 = DateTime.MinValue, _stdDate2 = DateTime.MaxValue; // see Strings.filter_std[N]

        public async Task<List<string>> LoadAndParseTextFiler(SettingType type , string currentUserName)
        {
            var list = new List<string>();
            var settingType = await filterDbSettingsRepository.GetFilterSettingAsync(type , currentUserName);
            var dbValue = settingType.Trim().ToLower();
            if (!string.IsNullOrEmpty(dbValue))
            {
                list.AddRange(dbValue.Split(TextFilterSeparator, StringSplitOptions.RemoveEmptyEntries));
            }
            return list;
        }
        public async Task ClearFilter()
        {
            var currentUserName = AppSetting.Instance.GetValueOrDefault(CommanConstants.UserNameKey, "");

            _filterOn = "false";
	        var filterDbSettings = new FilterDbSettings()
	        {
		        Id = (int) SettingType.StdFilterOn,
				Value = "false",
		        UserName = currentUserName,
		        PrimKey = currentUserName + SettingType.StdFilterOn
			};

	        await filterDbSettingsRepository.InsertOrUpdateFilterSetting(filterDbSettings);

            _stdIndex1 = DefaultValueStd;
            GetDateTimeByStdCode(_stdIndex1 + 1, out _stdDate1, out _stdDate2);
        }
        //public bool SortByDefault(FlightListItemDto item)
        //{
        //    GetDateTimeByStdCode(DefaultValueStd + 1, out DateTime defstdDate1, out DateTime defstdDate2);
        //    return item.STD >= defstdDate1 && item.STD <= defstdDate2;
        //}

        public List<FlightListItemDto> SortByDefault()
        {
            var query = flightListItemRepository.GetAllFlightListItems();
            GetDateTimeByStdCode(DefaultValueStd + 1, out DateTime defstdDate1, out DateTime defstdDate2);
	        var flightEntites = query.Where(item => item.STD >= defstdDate1 && item.STD <= defstdDate2).ToList();
			return ConvertorHelper.FlightListItemConvertor(flightEntites);

        }
        public async Task LoadFilterFromDb()
        {
	        var currentUserName = AppSetting.Instance.GetValueOrDefault(CommanConstants.UserNameKey, "");
            _tail =  await LoadAndParseTextFiler(SettingType.TailFilter , currentUserName);
            _crew = await LoadAndParseTextFiler(SettingType.CrewFilter , currentUserName);
            _dep = await LoadAndParseTextFiler(SettingType.DepFilter , currentUserName);
            _dest = await LoadAndParseTextFiler(SettingType.DestFilter, currentUserName);
            _flightNo = await LoadAndParseTextFiler(SettingType.FlightNoFilter  , currentUserName);
            _filterOn = await filterDbSettingsRepository.GetFilterSettingAsync(SettingType.StdFilterOn , currentUserName);

           
			_sortField = int.TryParse(await filterDbSettingsRepository.GetFilterSettingAsync(SettingType.SortField , currentUserName) , out _sortField) ? _sortField : DefaultValueStd ;
                     
            _sortType = 0;
            int.TryParse(await filterDbSettingsRepository.GetFilterSettingAsync(SettingType.SortType , currentUserName), out _sortType);


            _stdIndex1 = DefaultValueStd;
            if (int.TryParse(await filterDbSettingsRepository.GetFilterSettingAsync(SettingType.StdFilter1 , currentUserName), out _stdIndex1))
            {
                _stdIndex1 += 1;
            }  // see Strings.filter_std[N], but it is zero-based in DB

            if (_stdIndex1 < 1 || _stdIndex1 > MenuPage.StdFilterCount)
            {
                _stdIndex1 = DefaultValueStd;
            }

            GetDateTimeByStdCode(_stdIndex1, out _stdDate1, out _stdDate2);

        }
        public  void GetDateTimeByStdCode(int code, out DateTime startDatetime, out DateTime endDatetime)
        {
            startDatetime = DateTime.UtcNow;
            endDatetime = DateTime.UtcNow;
            switch (code)
            {
                case 1:
                    //  6 hour
                    startDatetime = DateTime.UtcNow;
                    endDatetime = DateTime.UtcNow.AddHours(6);
                    break;
                case 2:
                    //  12 hour.
                    startDatetime = DateTime.UtcNow;
                    endDatetime = DateTime.UtcNow.AddHours(12);
                    break;
                case 3:
                    // 24 hour 
                    startDatetime = DateTime.UtcNow;
                    endDatetime = DateTime.UtcNow.AddDays(1);
                    break;
                case 4:
                    // Prev 24 hour / Next 24 Hour
                    startDatetime = DateTime.UtcNow.AddDays(-1);
                    endDatetime = DateTime.UtcNow.AddDays(1);
                    break;
                case 5:
                    // recent 
                    startDatetime = DateTime.MinValue;
                    endDatetime = DateTime.UtcNow;
                    break;
                case 6:
                    // any period 
                    startDatetime = DateTime.MinValue;
                    endDatetime = DateTime.MaxValue;
                    break;

            }

        }
        public  void SortList(ref List<FlightListItemDto> list)
        {
            if (list != null && list.Count == 0)
            {
                return;
            }
            switch (_sortField)
            {
                case 1: //dep-dest
                    if (_sortType == 0)
                    {
                        list.Sort(new CompareByDepDestAsc());
                    }
                    else
                    {
                        list.Sort(new CompareByDepDestDesc());
                    }
                    break;
                case 2: //STD
                    if (_sortType == 0)
                    {
                        list.Sort(new CompareBySTDAsc());
                    }
                    else
                    {
                        list.Sort(new CompareBySTDDesc());
                    }
                    break;
                default:  // or 0 - FlightNo
                    if (_sortType == 0)
                    {
                        list.Sort(new CompareByFlighNoAsc());
                    }
                    else
                    {
                        list.Sort(new CompareByFlighNoDesc());
                    }
                    break;

            }
        }

		public bool IsFilterNotEMpty()
		{
			return (_tail.Count > 0 || _crew.Count > 0 || _dep.Count > 0 || _dest.Count > 0 || _flightNo.Count > 0 ||
												_stdIndex1 - 1 != DefaultValueStd) && _filterOn.Equals("true");
		}
		

        public  async Task<List<FlightListItemDto>> HandleFilterData()
        {
            var listDb = IsFilterNotEMpty() ? FilterData() :  SortByDefault();
            SortList(ref listDb);
            return listDb;
        }

		public List<FlightListItemDto> GetAllFlights()
		{
			var listTempDb = flightListItemRepository.GetAllFlightListItems();
			List<FlightListItemDto> items = ConvertorHelper.FlightListItemConvertor(listTempDb);
			return items;
		}

		public  List<FlightListItemDto> FilterData()
        {
            var query = flightListItemRepository.GetAllFlightListItems();

            if (_tail.Count > 0)
            {
                query = query.FindAll(item => !string.IsNullOrEmpty(item.ACFTAIL) &&
                                              _tail.Any(item.ACFTAIL.ToLower().Contains));
            }
			////AND contitions
			if (_crew.Count > 0)  // crew filter
            {
                query = query.FindAll(item => item.CrewItem.Any(
				  x => !string.IsNullOrWhiteSpace(x.Initials) && _crew.Any(z => x.Initials.ToLower().Contains(z))));

				//query = query.FindAll(item => item.Crew.Any(
				//x => x.Initials != null && _crew.Contains(x.Initials.ToLower())));
			}

            if (_dep.Count > 0)
            {
                query = query.FindAll(item => item.DEP != null &&
                                              _dep.Contains(item.DEP.ToLower()) ||
                                              item.DepAirportFull != null &&
                                              _dep.Any(item.DepAirportFull.ToLower().Contains));
            }

            if (_dest.Count > 0)
            {
                query = query.FindAll(item => item.DEST != null &&
                                              _dest.Any(item.DEST.ToLower().Contains) ||
                                              item.DestAirportFull != null &&
                                              _dest.Any(item.DestAirportFull
                                                  .ToLower().Contains));
            }

			if (_flightNo.Count > 0)
			{
				query = query.FindAll(item => FlightListItemDto.GetFlightIdToShow(item.FlightlogID) != null
											  && _flightNo.Any(FlightListItemDto.GetFlightIdToShow(item.FlightlogID).ToLower().Contains));
			}

			if(_stdIndex1 != 6) // mean Choose Any Period
			{
				query = query.Where(item => item.STD >= _stdDate1 && item.STD <= _stdDate2).ToList();
			}
			

            return ConvertorHelper.FlightListItemConvertor(query);
		}

   //     public  bool ItemPassedFilter(FlightListItemDto item)
   //     {

   //         if (_tail.Count > 0)
   //         {
   //             if (string.IsNullOrWhiteSpace(item.ACFTAIL) || !_tail.Contains(item.ACFTAIL.ToLower()))
   //             {
   //                 return false;
   //             }
   //         }
   //         //AND contitions
   //         if (_crew.Count > 0)  // crew filter
   //         {
			//	var matched = item.CrewDto.Any(x => !string.IsNullOrWhiteSpace(x.Initials) && _crew.Any(z => x.Initials.ToLower().Contains(z)));
			//	if (!matched) return false;
			//}
   //         if (_dep.Count > 0)
   //         {

   //             if (string.IsNullOrWhiteSpace(item.DEP))
   //             {
   //                 return false;
   //             }
   //             var depLow = item.DEP.ToLower();
   //             var depAirportLow = string.IsNullOrEmpty(item.DepAirportFull) ? string.Empty : item.DepAirportFull.ToLower();
   //             var i = 0;
   //             foreach (var depItem in _dep)
   //             {
   //                 var depItemLow = depItem.ToLower();
   //                 if (depLow.Contains(depItemLow) || depAirportLow.Contains(depItemLow))
   //                 {
   //                     i++;
   //                 }
   //             }
   //             if (i != _dep.Count)
   //             {
   //                 return false;
   //             }

   //         }
   //         if (_dest.Count > 0)
   //         {
   //             if (string.IsNullOrEmpty(item.DEST))
   //             {
   //                 return false;
   //             }
   //             var destLow = item.DEST.ToLower();
   //             var destAirportLow = string.IsNullOrEmpty(item.DestAirportFull) ? string.Empty : item.DestAirportFull.ToLower();
   //             int i = 0;
   //             foreach (var destItem in _dest)
   //             {
   //                 var destItemLow = destItem.ToLower();
   //                 if (destLow.Contains(destItemLow) || destAirportLow.Contains(destItemLow))
   //                 {
   //                     i++;
   //                 }
   //             }
   //             if (i != _dest.Count)
   //             {
   //                 return false;
   //             }

   //         }
   //         if (_flightNo.Count > 0)
   //         {
			//	var itemFnLow = FlightListItemDto.GetFlightIdToShow(item.FlightlogId) ?? string.Empty;
   //             itemFnLow = itemFnLow.ToLower();
   //             foreach (var fn in _flightNo)
   //             {
   //                 if (!itemFnLow.Contains(fn.ToLower()))
   //                 {
   //                     return false;
   //                 }
   //             }
   //         }
   //         return item.STD >= _stdDate1 && item.STD <= _stdDate2;
   //     }
        private class CompareByFlighNoAsc : IComparer<FlightListItemDto>
        {
            public int Compare(FlightListItemDto x, FlightListItemDto y)
            {
                return string.Compare(x.FlightlogId, y.FlightlogId, StringComparison.CurrentCultureIgnoreCase);
            }
        }
        private class CompareByFlighNoDesc : IComparer<FlightListItemDto>
        {
            public int Compare(FlightListItemDto x, FlightListItemDto y)
            {
                return string.Compare(y.FlightlogId, x.FlightlogId, StringComparison.CurrentCultureIgnoreCase);
            }
        }
        private class CompareByDepDestAsc : IComparer<FlightListItemDto>
        {
            public int Compare(FlightListItemDto x, FlightListItemDto y)
            {
                return string.Compare(x.Dep_Dest, y.Dep_Dest, StringComparison.CurrentCultureIgnoreCase);
			}
		}
        private class CompareByDepDestDesc : IComparer<FlightListItemDto>
        {
            public int Compare(FlightListItemDto x, FlightListItemDto y)
            {
                return string.Compare(y.Dep_Dest, x.Dep_Dest, StringComparison.CurrentCultureIgnoreCase);

			}
		}
        private class CompareBySTDAsc : IComparer<FlightListItemDto>
        {
            public int Compare(FlightListItemDto x, FlightListItemDto y)
            {
                return DateTime.Compare(x.STD, y.STD);
            }
        }
        private class CompareBySTDDesc : IComparer<FlightListItemDto>
        {
            public int Compare(FlightListItemDto x, FlightListItemDto y)
            {
                return DateTime.Compare(y.STD, x.STD);
            }
        }
    }
}
