﻿using CB.Core.Common.Keys;

namespace CrewBriefing.Standard.Helper
{

   public  class AppConstants
    {

        public static readonly Enums.PdfDocType[] DocsOrder =
        {
            Enums.PdfDocType.Messages,   //0
            Enums.PdfDocType.Logstring,  //1
            Enums.PdfDocType.Wx,         //2
            Enums.PdfDocType.NotaMs,     //3
            Enums.PdfDocType.Atc,        //4
            Enums.PdfDocType.WindT,      //5
            Enums.PdfDocType.Swx,        //6
            Enums.PdfDocType.CrossSection,  // 7

            Enums.PdfDocType.UploadedDocuments,  //8
            Enums.PdfDocType.CombineDocuments    //9
        };

	    public static readonly string PartnerUserName = "TeoIntl";
	    public static readonly string PartnerPassword = "V5N9K5YH7V";

	    public static readonly string ClientId = "CB6173";
	    public static readonly string ClientKey = "7dba38bbfffbf974b6f7976ece8f43f8";

	    public static readonly string ApiKey = "62FA4115-90FB-4908-910D-DDDA4A94E433";
	    public static readonly string AppName = "CB_MOBILE";
	    // public static readonly string IOsAppVersion = "1.7.3";

	    //public static readonly string UserNameKey = "UserNameKey";
	    //public static readonly string PasswordKey = "PasswordKey";
	    //public static readonly string QuickLoginKey = "QuickLoginKey";

	    //Timer is set to go off one time after 120 seconds
	    public static readonly int TimeOutWebService = 120000; //60000,  120000

	    public static readonly int PopOverWidthIpad = 289;
	    public static readonly int PopOverHeightForIos6 = 640; //Because it's navigation bar height is small
	    public static readonly int PopOverHeightForIos7 = 395;

	    public static readonly int MainMenuHeightIpad = 430;
	    public static readonly string ServiceUrlKey = "serviceURL";
	}
}
