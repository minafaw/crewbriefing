﻿using System;
using System.Threading.Tasks;
using CB.Core.Dtos;
using CrewBriefing.Standard.ViewModels;
using CrewBriefing.Standard.Views.FlightDetials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Controls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StackLayoutFlightList : StackLayout
    {
	    
		private int _selectedFlightId ;
        private double _listViewWidth ;
		private FlightListViewModel _flightListViewModel;

		public StackLayoutFlightList()
        {
			InitializeComponent();
			_flightListViewModel = ViewModelLocator.FlightListViewModel;
             BindingContext = _flightListViewModel;

			HandleRefreshClicked(true);

			listViewFlights.SizeChanged += ListViewFlights_SizeChanged;
        }

		public void HandleRefreshClicked(bool forceRefresh)
		{
			Task.Run(async () =>
		   {
			   await _flightListViewModel.UpdateList(forceRefresh);
		   });
			
		}
		

		private void ListViewFlights_SizeChanged(object sender, EventArgs e)
        {
            if (listViewFlights.Width > 0)
            {
                _listViewWidth = listViewFlights.Width;
            }
        }
              
        /// <summary>
        /// Get full airports names
        /// </summary>
        /// <param name="flightList"></param>
        /// <returns>List of comma-separated icao, which are not found</returns>
	    private  void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
			_flightListViewModel.HandleItemSelected(e.Item);
		}

      
        private void ListViewFlights_Refreshing(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await CrewBriefing.Standard.App.Sleep(500);
                listViewFlights.IsRefreshing = true;
				await _flightListViewModel.UpdateList(true);
				listViewFlights.IsRefreshing = false;
            });
            
         }
    }
}
