﻿using CrewBriefing.Standard.Helper;
using Xamarin.Forms;

namespace CrewBriefing.Standard
{
    public class MenuCell : ViewCell
    {
        public string Text
        {
            get => label.Text;
	        set => label.Text = value;
        }
        public string ImagePath { get; set; }

	    private Label label;

	    public MenuPage Host { get; set; }

        public MenuCell()
        {
            label = new Label
            {
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium)
            };

            var layout = new StackLayout
            {
                BackgroundColor = (Color)Application.Current.Resources["menu_background"],
                Padding = new Thickness(20, 0, 0, 0),
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center,
                Children = { label }
            };
            View = layout;
        }
      
    }
}

