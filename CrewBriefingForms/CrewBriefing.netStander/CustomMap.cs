using CB.Core.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace CrewBriefing.Standard
{
	public class CustomMap : Map
	{
//        public List<CustomPin> CustomPins { get; set; }
		public FlightDto FlightInfo { get; set; }
		public ContentPage parentPage { get; set; }
	}
}