﻿using CB.Core.Common.HelperClasses;
using System;
using Xamarin.Forms;

namespace CrewBriefing.Standard.MenuItems
{
    public class MasterPageItem 
    {
		public double ImageDesiredSize => CommonStatic.Instance.MediumHeight * 1.2;
		public double LableFontSize => CommonStatic.LabelFontSize(NamedSize.Medium);
		public string ImagePath { get; set; }
		public string Text { get; set; }
		public Type TargetType { get; set; }

	}
}
