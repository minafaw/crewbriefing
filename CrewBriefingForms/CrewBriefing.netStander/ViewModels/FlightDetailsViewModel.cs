﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using CB.Core.Common;
using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.Conversion;
using CB.Core.DAL.Interfaces;
using CB.Core.Dtos;
using CB.Core.Models;
using CB.Core.Models.ResponseModels;
using CB.Core.Services;
using CB.Core.Services.Interfaces;
using CrewBriefing.Standard;
using CrewBriefing.Standard.Helper;
using CrewBriefing.Standard.Views;
using Xamarin.Forms;
using static CB.Core.Common.Keys.Enums;

namespace CB.Core.ViewModels
{
	public class FlightDetailsViewModel : BaseViewModel
	{
		public ICommand PressItemCommand { get; private set; }
		private readonly IDalServices DalServices;
		private readonly IApiServices ApiServices;
		private readonly IPdfRepository pdfRepository;
		private readonly IFlightRepository flightRepository;
		private readonly IAirportRepository airportRepository;
		private List<FlightDetialListItem> _myItems = new List<FlightDetialListItem>();
		public List<FlightDetialListItem> MyItems
		{
			get => _myItems;
			set
			{
				_myItems = value;
				RaisePropertyChanged();
			}
		}

		public FlightDetailsViewModel(IDalServices dalServices, IApiServices apiServices , IPdfRepository pdfRepository , IFlightRepository flightRepository , IAirportRepository airportRepository)
		{
			PressItemCommand = new Command(async obj =>
			{
				await HandleItemPressAction(obj);
			});
			DalServices = dalServices;
			ApiServices = apiServices;
			this.pdfRepository = pdfRepository;
			this.flightRepository = flightRepository;
			this.airportRepository = airportRepository;
		}

		FlightDetialListItem lastItemSelected = null;
		private async Task HandleItemPressAction(object obj)
		{
			var currentItem = obj as FlightDetialListItem;
			if (currentItem == null) return;
			if (currentItem.Title.Equals(Resource.route_map))
			{
                if(FlightDetial != null)
				{
					if(Device.Idiom == TargetIdiom.Tablet)
					{
						NavigateToTabletViews(false);
						if(lastItemSelected != null)
						{
							lastItemSelected.IsSelected = false;
						}
						currentItem.IsSelected = true;
						lastItemSelected = currentItem;
					}
					else
					{
						await App.NavigationService.NavigateAsync(nameof(FlightMapPage), FlightDetial);
					}
				}
				
				return;
			}
			else if (currentItem.Title.Equals(Resource.flight_summary))
			{
				if (FlightDetial != null)
				{
					if (Device.Idiom == TargetIdiom.Tablet)
					{
						NavigateToTabletViews(true);
						if (lastItemSelected != null)
						{
							lastItemSelected.IsSelected = false;
						}
						currentItem.IsSelected = true;
						lastItemSelected = currentItem;
					}
					else
					{
						await App.NavigationService.NavigateAsync(nameof(FlightSummaryPage), FlightDetial);
					}
				}
				return;
			}
			if (currentItem.DocType == null) return;

			

			var storedPath = await pdfRepository.GetStoredPdfPath(currentItem.FlightId, (PdfDocType)currentItem.DocType);
			if (!string.IsNullOrEmpty(storedPath))
			{
				try
				{
					var SaveAndLoad = DependencyService.Get<ISaveAndLoad>();
					await SaveAndLoad.OpenFileWithDefaultOsViewer(storedPath, currentItem.Title);
				}
				catch (Exception exc)
				{
					ToastUtils.ShowTemporayMessage(ToastType.Error, "Error open file. " + exc.Message);
				}
			}
		}

		private void NavigateToTabletViews(bool isSummary)
		{
			var currentNav = (Application.Current.MainPage as NavigationPage);
			var currentPage = (currentNav.CurrentPage as MasterDetialMainPage);
			currentPage.IsPresented = false;
			var childNavPage = (currentPage.Detail as NavigationPage);

			var tabletPage = childNavPage.CurrentPage as TabletMainPage;
			if (tabletPage != null)
			{
				tabletPage.ShowMapOrSummary(FlightDetial, isSummary);

			}
		}

		public async Task SetUserId(int flightId)
		{
			Device.BeginInvokeOnMainThread(async () =>
			{
				HeaderVisibility = true;
				await GetFlightDetialAsync(flightId);
				HeaderVisibility = false;
			});

			await CreatelDetialList(flightId);
		}



		private bool _headerVisibility;
		public bool HeaderVisibility
		{
			get => _headerVisibility;
			set
			{
				_headerVisibility = value;
				RaisePropertyChanged();
			}
		}
		private FlightDto _flightDetial;
		public FlightDto FlightDetial
		{
			get => _flightDetial;
			set
			{
				_flightDetial = value;
				RaisePropertyChanged();
			}
		}

		private async Task GetFlightDetialAsync(int flightId)
		{
			// check if flight exists in database
			var FlightEntity = await flightRepository.GetFilghtDb(flightId);
            // if not get the data from webservice 
            if (FlightEntity == null)
            {
                // in this status we need to check if session still live
                FlightResponseEnvelope flightModelResult = await ApiServices.GetFlight(await CreateFlightRequest(flightId));
                var getFlightResult = flightModelResult?.Body?.GetFlightResponse?.GetFlightResult;
                if (getFlightResult == null || getFlightResult?.Responce?.Succeed == false)
				{
					ToastUtils.ShowTemporayMessage(ToastType.Error, "Cant load flight");
					await App.NavigationService.GoBack();
					return;
				}
				FlightDetial = ConvertorHelper.FlightDtoConvertor(getFlightResult);
				var flightEntity = ConvertorHelper.FlightConvertor(getFlightResult);
				await flightRepository.InsertOrUpdateFlight(flightEntity);
			}
			else
			{
				FlightDetial = ConvertorHelper.FlightDtoConvertor(FlightEntity);
			}

			if (FlightDetial == null)
			{
				// if flight detial still null 
				return;
			}

			if(Device.Idiom == TargetIdiom.Tablet)
			{
				// handle select map at first opening 
				await HandleItemPressAction(MyItems.First());
			}

			var i = 0;
			foreach (var icao in new[] { FlightDetial.Dep, FlightDetial.Dest })
			{
				if (string.IsNullOrEmpty(icao)) continue;
				AirportDto airportDto = null;
				var airport = await airportRepository.GetAirportDb(icao);
				if(airport != null) airportDto = ConvertorHelper.AirportDtoConvertor(airport);
				if (airport == null)
				{
					var airportrequest = new GetAirportRequest
					{
						SessionID = AppSetting.Instance.GetValueOrDefault(CommanConstants.CurrentSessionKey, ""),
						ICAO = icao
					};

					var result = await ApiServices.GetAirport<AirportDetialResponseEnvelope>(airportrequest);
                    var wsResult =  result?.Body?.GetAirportResponse?.GetAirportResult;
                    if ( wsResult != null )
					{
                        var airportEntity = ConvertorHelper.AirportConvertor(wsResult);
                        airportDto = ConvertorHelper.AirportDtoConvertor(wsResult);
						await airportRepository.InsertOrUpdateAirport(airportEntity);
					}
				}
				

				if (airportDto != null)
				{

					var extraString = "";
					if (Device.RuntimePlatform == Device.UWP)
						extraString = Environment.NewLine;
					var airportName = (airportDto.Name ?? string.Empty).Replace("/", "/ " + extraString);
					if (i == 0)
					{
						FlightDetial.Airports4ShowDep = airportName;
					}
					else
					{
						FlightDetial.Airports4ShowDst = airportName;
					}
				}
				else
				{
					ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Error receive Airport data for '" + icao + "'");
				}

				i++;

			}


		}


        async Task<FlightRequest> CreateFlightRequest(int passedItemId)
        {
            if (!await ApiServices.CheckSessionAndRefreshIfRequired())
                return null;

            var fr = new FlightRequest()
            {
                SessionID = AppSetting.Instance.GetValueOrDefault(CommanConstants.CurrentSessionKey, ""),
                FlightID = passedItemId,
                Messages = false,
                Metar = false,
                Notams = false,
                Taf = false
            };
            return fr;
        }
        private async Task CreatelDetialList(int passedItemId)
        {

            var items = new List<FlightDetialListItem>();

            var routeItem = new FlightDetialListItem(DalServices , ApiServices , pdfRepository)
            {
                Title = Resource.route_map,
				Image1Path = CrewBriefing.Standard.Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false

            };
            var summeryItem = new  FlightDetialListItem(DalServices, ApiServices, pdfRepository)
			{
                Title = Resource.flight_summary,
                Image1Path = CrewBriefing.Standard.Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false

            };
            items.Add(routeItem);
            items.Add(summeryItem);

			foreach (PdfDocType itemDocType in AppConstants.DocsOrder)
			{
                var result = await pdfRepository.CheckDocExists(itemDocType, passedItemId);
				bool downloadedExists = result.Item1;
				DateTime? downloadDate = result.Item2;

				var item = new FlightDetialListItem(DalServices, ApiServices , pdfRepository)
				{
					Title = AppResource.ResourceManager.GetString("doc_title_" + itemDocType),
					Image1Path = CrewBriefing.Standard.Utils.ImageFolder + (downloadedExists ? "redownload_doc.png" : "icon_download.png"),
					DownloadDate = downloadDate,
					ItemDownloaded = downloadedExists,
					DocType = itemDocType,
					FlightId = passedItemId
				};
				if (itemDocType == PdfDocType.UploadedDocuments)
				{
					item.Title = downloadedExists
						? AppResource.doc_title_UploadedDocumentsShort
						: AppResource.doc_title_UploadedDocuments;
				}
				if (itemDocType == PdfDocType.CombineDocuments)
				{
					item.Title = downloadedExists
						? AppResource.doc_title_CombineDocumentsShort
						: AppResource.doc_title_CombineDocuments;
				}
				items.Add(item);
			}


			MyItems =  items; 
        }
       
    }
}
