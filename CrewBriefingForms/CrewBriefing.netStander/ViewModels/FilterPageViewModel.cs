﻿using CB.Core.Common.Base;
using CB.Core.DAL.Interfaces;
using CrewBriefing.Standard.Interfaces;
using System;
using System.Collections.Generic;
using System.Resources;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CrewBriefing.Standard.ViewModels
{
    public class FilterPageViewModel : BaseViewModel
	{
		public static readonly int StdFilterCount = 6;
		public readonly IFilterSettings FilterSettings;
        public readonly IDalServices Dalservices;
		public readonly IFilterDbSettingsRepository filterDbSettingsRepository;
		string[] stdPickerItems;
		public EventHandler donePressed;
		public ICommand DoneClicked { get;  set; }

		public string[] StdPickerItems
		{
			get { return stdPickerItems; }
			set {
				stdPickerItems = value;
				RaisePropertyChanged();
			}
		}

		private List<string> sortPickerItems;

		public List<string> SortPickerItems
		{
			get { return sortPickerItems; }
			set
			{
				sortPickerItems = value;
				RaisePropertyChanged();
			}
		}

        private List<string> sortTypePickerItems= new List<string>();

		public List<string> SortTypePickerItems
		{
			get { return sortTypePickerItems; }
			set
			{
				sortTypePickerItems = value;
				RaisePropertyChanged();
			}
		}


        public FilterPageViewModel(IFilterSettings filterSettings , IDalServices dalservices , IFilterDbSettingsRepository filterDbSettingsRepository )
		{
			DoneClicked = new Command(HandleDoneClicked);
			StdPickerItems = FillStdPickerData();
			SortPickerItems = FillSortPickerData();
			SortTypePickerItems.Add(AppResource.asc);
			SortTypePickerItems.Add(AppResource.desc);
			FilterSettings = filterSettings;
			Dalservices = dalservices;
			this.filterDbSettingsRepository = filterDbSettingsRepository;
			FilterSettings.LoadFilterFromDb();
		}

		private void HandleDoneClicked(object obj)
		{
			donePressed?.Invoke(null , EventArgs.Empty);
			
		}

		private List<string> FillSortPickerData()
		{
			var sortArray = new List<string>();
			for (int i = 1; i <= 3; i++)
			{
				sortArray.Add(AppResource.ResourceManager.GetString("sort_field" + i));
			}
			return sortArray;
		}

		private string[] FillStdPickerData()
		{

			var arrayStrings = new string[StdFilterCount];

			for (var i = 1; i <= StdFilterCount; i++)
			{
				arrayStrings[i - 1] = AppResource.ResourceManager.GetString("filter_std" + i);
			}
			return arrayStrings;
		}

		public int GetStdIndex(string value)
		{
			for (var i = 0; i < stdPickerItems.Length; i++)
			{
				if (value == stdPickerItems[i])
					return i;
			}
			throw new Exception("getStdIndex - invalid value " + value);
		}

		public async void ProcessDone()
        {
            //_stDone.BackgroundColor = Color.Gray;
            //_labelDone.BackgroundColor = Color.Gray;
            //await CrewBriefing.Standard.App.Sleep(250);
            //_stDone.BackgroundColor = Color.Transparent;
            //_labelDone.BackgroundColor = _bkColor;

            Device.BeginInvokeOnMainThread(async () =>
            {
                
            });

        }
	}
}
