﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.DAL.Interfaces;
using CB.Core.Models.Base;
using CB.Core.Models.RequestModels;
using CB.Core.Models.ResponseModels;
using CB.Core.Services.Interfaces;
using CrewBriefing.Standard;
using CrewBriefing.Standard.Views;
using Xamarin.Forms;

namespace CB.Core.ViewModels
{
	public class LoginPageViewModel : BaseViewModel
	{
		public ICommand quickLoginCommand;
		public ICommand helpCommand;
		private bool _quickLoginStatus;
		private string _userNameTxt;
		private IApiServices _apiServices;
		private readonly IDalServices _dalservices;
		double coef = 1.0; // iso & UWP
		public LoginPageViewModel(IApiServices apiServices , IDalServices dalservices)
		{
			_apiServices = apiServices;
			_dalservices = dalservices;
			quickLoginCommand = new Command(HandleQuickCommand);
			helpCommand = new Command(HandleHelpClicked);
			if (Device.RuntimePlatform == Device.Android)
				coef = 1.5;

			if (LoadCredentials)
			{
				UserNameTxt = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.UserNameKey, null);
				PasswordTxt = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.PasswordKey, null);
				QuickLoginStatus = AppSetting.Instance.GetValueOrDefault(CommanConstants.QuickLoginKey, false);
			}

		}

		private void HandleHelpClicked(object obj)
		{
			App.NavigationService.NavigateAsync(nameof(HelpPage));
		}

		public string UserNameTxt
		{
			get => _userNameTxt;
			set
			{
				_userNameTxt = value;
				RaisePropertyChanged();
			}
		}

		public bool LoadCredentials => AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.UserNameKey, null) != null &&
                                       AppSetting.Instance.GetValueOrDefault(CommanConstants.IsLoginedInKey, false) != false ;


		private string _passwordTxt;
		public string PasswordTxt
		{
			get { return _passwordTxt; }
			set
			{
				_passwordTxt = value;
				RaisePropertyChanged();
			}
		}

		private bool _loginBusy;
		public bool LoginBusy
		{
			get { return _loginBusy; }
			set { _loginBusy = value;
				RaisePropertyChanged();
			}
		}


		public string LogoImageSouce { get => CrewBriefing.Standard.Utils.ImageFolder + "icon_launcher.png"; }
		public string IconInfoSouce { get => CrewBriefing.Standard.Utils.ImageFolder + "icon_info.png"; }
		public double LogoHeight { get => CommonStatic.Instance.GetMediumHeight * coef; }
		public double LogoWidth { get => CommonStatic.Instance.GetMediumHeight * coef; }
		public double MediumHeight { get => CommonStatic.Instance.GetMediumHeight ; }


		public double smallSize { get => CommonStatic.Instance.GetSmallHeight; }
		public double LabelFontSize
		{
			get => CommonStatic.LabelFontSize(NamedSize.Large) * (Device.Idiom == TargetIdiom.Tablet ? 1.3 : 1.15);
		}


		public LoginRequestModel LoginRequestModel { get; set; }
		public bool QuickLoginStatus
		{
			get => _quickLoginStatus;
			set
			{
				_quickLoginStatus = value;
				RaisePropertyChanged();
			}
		}
	
		private void HandleQuickCommand(object obj)
		{
			QuickLoginStatus = !QuickLoginStatus;

		}

		public async Task ProcessLoginButton(string username, string password)
		{
			if (string.IsNullOrWhiteSpace(username) || !username.Equals("AAADEMO"))
			{
				if (!Validate(out string errorMsg))
				{
					ToastUtils.ShowTemporayMessage(Enums.ToastType.Warning, errorMsg);
					return;
				}
			}
			LoginBusy = true;
			//SetBusy(true);
			LoginResponseEnvelope response = null;
			if (IsConnected)
			{
				response = await _apiServices.Login(username, password);
			}
			else
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, AppResource.not_net);
				LoginBusy = false;
				return;
			}


			LoginBusy = false;
			//SetBusy(false);
			// that mean credential is wrong

			if (response == null)
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Server Error ");
				
            } else if (response.Message != null && response.Message.Contains("Invalid Username or Password"))
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, response.Message);
				
			}else if (response.Code == Enums.WebApiErrorCode.Success)
			{
				var oldUserName = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.UserNameKey, null);
			
				if (string.IsNullOrEmpty(oldUserName) || (!string.IsNullOrEmpty(oldUserName) && !oldUserName.Equals(username.ToLower())))
				{
					// if user changed delete all old files
					 _dalservices.DeleteAllDateBase();

					var saveLoad = DependencyService.Get<ISaveAndLoad>();
					saveLoad.RemoveAllFilesInLocalStorage(string.Empty);
					AppSetting.Instance.Remove(CommanConstants.FlightListRetrievedKey);
				}
              

                AppSetting.Instance.AddOrUpdateValue(CommanConstants.UserNameKey, username.ToLower());
				AppSetting.Instance.AddOrUpdateValue(CommanConstants.PasswordKey, password);
                AppSetting.Instance.AddOrUpdateValue(CommanConstants.QuickLoginKey, QuickLoginStatus);
                // this condition to check user logged in
                AppSetting.Instance.AddOrUpdateValue(CommanConstants.IsLoginedInKey, true);
				AppSetting.Instance.AddOrUpdateValue(CommanConstants.SessionReceivedKey, DateTime.UtcNow);

				string pageName = nameof(MasterDetialMainPage);
				
				 ((App)Application.Current).SetRootPage(pageName);
            }else if(response.Code != Enums.WebApiErrorCode.Success && !string.IsNullOrWhiteSpace(response.Message))
            {
                ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, response.Message);
            }

		}

		private bool Validate(out string errorMsg)
		{
			errorMsg = null;
			if (string.IsNullOrEmpty(UserNameTxt))
			{
				errorMsg = Common.Resource.username_empty_error;
				return false;
			}
			if (string.IsNullOrEmpty(PasswordTxt))
			{
				errorMsg = Common.Resource.password_empty_error;
				return false;
			}
			return true;
		}
	}
}
