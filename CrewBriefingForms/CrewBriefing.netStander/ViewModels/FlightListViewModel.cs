﻿using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.Conversion;
using CB.Core.CustomControls.NavigationService;
using CB.Core.DAL.Interfaces;
using CB.Core.Dtos;
using CB.Core.Models;
using CB.Core.Services;
using CB.Core.Services.Interfaces;
using CrewBriefing.Standard.Interfaces;
using CrewBriefing.Standard.Views;
using CrewBriefing.Standard.Views.FlightDetials;
using CrewBriefingForms.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CrewBriefing.Standard.ViewModels
{
    public class FlightListViewModel : BaseViewModel
	{
		private readonly IApiServices _apiServices;
		private readonly ICrewRepository crewRepository;
		private readonly IDalServices _dalServices;
		private readonly IFilterSettings _filterSettings;
		private readonly IFlightListItemRepository flightListItemRepository;
		private readonly IAirportRepository airportRepository;
	
		public ICommand BackToDefault => new Command(async (obj) =>
									{ await HandleBackToDefault(obj); });
		

		public FlightListViewModel(IApiServices apiServices, ICrewRepository crewRepository  , IDalServices dalServices, IFilterSettings filterSettings ,  IFlightListItemRepository flightListItemRepository , IAirportRepository airportRepository)
		{
			_apiServices = apiServices;
			this.crewRepository = crewRepository;
			_dalServices = dalServices;
			_filterSettings = filterSettings;
			
			this.flightListItemRepository = flightListItemRepository;
			this.airportRepository = airportRepository;
			 

		}

		private async Task HandleBackToDefault(object obj)
		{
			await _filterSettings.ClearFilter();
			await UpdateList(false);
		}

		public static FlightListItemDto lastItemSelected = null;
		public void HandleItemSelected(object obj , bool dontSelect = false)
		{
			var item = obj as FlightListItemDto;
			if (Device.Idiom == TargetIdiom.Phone)
			{
				
				App.NavigationService.NavigateAsync(nameof(FlightDetialsMain), item);
			}
			else
			{
				if(lastItemSelected != null && dontSelect == true)
				{
					// here dont go any where
					var iteminList =  FlightListItems.Where(c => c.ID == lastItemSelected.ID ).FirstOrDefault();
					if(iteminList != null)
					{
						iteminList.IsSelected = true;
						lastItemSelected = iteminList;
					}
					return;
				}
				if (lastItemSelected != null ) lastItemSelected.IsSelected = false;
				item.IsSelected = true;
				lastItemSelected = item;

				var currentNav = (Application.Current.MainPage as NavigationPage);
				var currentPage = (currentNav.CurrentPage as MasterDetialMainPage);
				currentPage.IsPresented = false;
				var childNavPage = (currentPage.Detail as NavigationPage);

				if (childNavPage.CurrentPage is TabletMainPage tabletPage)
				{
					tabletPage.SetItem(item.ID);

				}

			}
			
		}

		private bool _flightListBusy;

		public bool FlightListBusy
		{
			get { return _flightListBusy; }
			set { _flightListBusy = value;
				RaisePropertyChanged();
			}
		}

		private bool _filterPanelVisibilty;

		public bool FilterPanelVisibilty
		{
			get { return _filterPanelVisibilty; }
			set { _filterPanelVisibilty = value;
				RaisePropertyChanged();
			}
		}

		private List<FlightListItemDto> _flightListItems;

		public List<FlightListItemDto> FlightListItems
		{
			get { return _flightListItems; }
			set { _flightListItems = value;
				RaisePropertyChanged();
			}
		}

		private bool _isRefreshing;

		public bool IsRefreshing
		{
			get { return _isRefreshing; }
			set { _isRefreshing = value;
				RaisePropertyChanged();
			}
		}
		public double AssignedWidth {  get; set; }

		public async Task CallFilterButtonClicked()
		{
			Device.BeginInvokeOnMainThread(() =>
			{
				SetBusy(AppResource.loading);
				//Todo comment
				//ForceLayout();

			});
			await App.Sleep(500);
			await _filterSettings.ClearFilter();
			await UpdateList(false);
			FilterPanelVisibilty = false;
			//SetFilterVisible(false);
		}

		public double ImageSize => Device.Idiom == TargetIdiom.Tablet ?
		  CommonStatic.Instance.MediumHeight * 0.8 :
		  37 * CommonStatic.Instance.ScreenCoef;

		public bool ForceNoBusy;

		public void SetBusy(string text)
		{
			Device.BeginInvokeOnMainThread(async () => {

				await CrewBriefing.Standard.App.Sleep(700);

				if (string.IsNullOrEmpty(text))
				{
					FlightListBusy = false;

					//busyPanel.IsVisible = false;
					//busyIndicator.IsRunning = false;
					//listViewFlights.IsRefreshing = false;
				}
				else
				{
					FlightListBusy = true;
					//labelBusy.Text = text;
					//busyIndicator.IsRunning = true;
					//busyPanel.IsVisible = true;
				}

			});

		}

		
		public async Task UpdateList(bool forceRefresh)
		{
			SetBusy(AppResource.loading);
			_filterSettings.LoadFilterFromDb();

			if (!forceRefresh)
			{
				var flightListIsValid = flightListItemRepository.FlightListIsValidAsync();
				if (flightListIsValid)
				{
					var listNotEmpty = flightListItemRepository.IsFlightListNotEmpty() ;
					if (listNotEmpty)
					{
						await SetListSource();

						return;
					}
				}
			}
			
			if (!Utils.IsInternet())
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Info, AppResource.not_net);
				SetBusy(string.Empty);
				return;
			}
			
			// if forceRefresh equal True 
			var checkSessionOk = await _apiServices.CheckSessionAndRefreshIfRequired();
			
			if (!checkSessionOk)
			{
				SetBusy(string.Empty);
				return;
			}
			if (!IsRefreshing)
			{
				SetBusy(AppResource.send_request);				
			}

			var sessionId = AppSetting.Instance.GetValueOrDefault(CommanConstants.CurrentSessionKey, "");
			var response = await _apiServices.GetFlightList(CreateFlightSearchRequest(), sessionId);
			var itemsList = response?.Body?.GetFlightListSearchResponse?.GetFlightListSearchResult?.Items;
			if (itemsList != null)
			{
				try
				{
					// Store in the database 
					//var flightCrewsItems = itemsList.Where(c => c.Crew.Any()).ToList();
					var flightEntity = ConvertorHelper.FlightListItemModelConvertor(itemsList);
					///var flightIsEmpty = flightListItemRepository.IsFlightListNotEmpty();
					flightListItemRepository.DeleteAllFlightTable();
					// flightIsEmpty = flightListItemRepository.IsFlightListNotEmpty();

					await BeginInvokeOnMainThreadAsync(async () => { await flightListItemRepository.UpdateFlights(flightEntity, true); });
					
					//Comment this line to get all flight list
					//AppSetting.Instance.AddOrUpdateValue(CommanConstants.FlightListRetrievedKey, DateTime.UtcNow);
				}
				catch (Exception exc)
				{
					SetBusy(string.Empty);
					ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Error parse server response, probably transmission error: " + exc.Message);
					return;
				}
				if (itemsList == null || itemsList.Count == 0)
				{
					SetBusy(string.Empty);
					return;
				}
				
				await SetListSource();
				var list = ConvertorHelper.FlightListItemConvertor(itemsList);
				var errorList = await SetFullAirportNames(list);
				if (!string.IsNullOrEmpty(errorList))
				{
					ToastUtils.ShowTemporayMessage(Enums.ToastType.Info, AppResource.cant_retrieve_airports + errorList);
				}

			}
			else
			{
				var result = itemsList.FirstOrDefault();
				if (itemsList.Any() )
				{
					ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, Utils.GetErrorMessage(result.Code) + " " + result.Message);
				}
			}
			SetBusy(string.Empty);

		}
		public static Task<T> BeginInvokeOnMainThreadAsync<T>(Func<T> a)
		{
			var tcs = new TaskCompletionSource<T>();
			Device.BeginInvokeOnMainThread(() =>
			{
				try
				{
					var result = a();
					tcs.SetResult(result);
				}
				catch (Exception ex)
				{
					tcs.SetException(ex);
				}
			});
			return tcs.Task;
		}

		private async Task SetListSource()
		{
			// this workaround to force refersh 
			
			List<FlightListItemDto> listDb = await _filterSettings.HandleFilterData();
			FlightListItems = listDb;

			FilterPanelVisibilty = _filterSettings.IsFilterNotEMpty();
			if (FilterPanelVisibilty && !FlightListItems.Any())
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Info, AppResource.no_records_filtered);
			}
			SetBusy(string.Empty);
			if (Device.Idiom == TargetIdiom.Tablet)
			{
				if (listDb?.Count > 0)
				{
					var firstItem = listDb.First();
					HandleItemSelected(firstItem , true);
				}

			}

			//if (Device.RuntimePlatform == Device.Android)
			//{
			//	CommonStatic.Instance.FlightListColumn2Padding = new Thickness(0);
			//}
			//else
			//{
			//	if (AssignedWidth > 0)
			//	{
			//		// 2do: it seems  calculateWidth works incorrect 
			//		double maxWidth2 = 0;

			//		var textSizes = DependencyService.Get<ITextMeasuring>();
			//		if (list != null)
			//			foreach (var item in list)
			//			{
			//				var d =  textSizes.CalculateWidth(item.Dep_Dest, NamedSize.Default);
			//				if (d > maxWidth2)
			//				{
			//					maxWidth2 = d;
			//				}
			//			}
			//		if (maxWidth2 == 0)
			//		{
			//			maxWidth2 = textSizes.CalculateWidth("WWWW-WWWW", NamedSize.Default);
			//		}
			//		maxWidth2 *= 0.8;

			//		if (maxWidth2 < AssignedWidth / 3)
			//		{
			//			CommonStatic.Instance.FlightListColumn2Padding = new Thickness(((AssignedWidth / 3.0) - maxWidth2) / 2.0, 0, 0, 0);
						
			//		}
			//	}
			//}
			
		}
		private FlightSearchRequest CreateFlightSearchRequest()
		{
			DateTime flighDateTime = AppSetting.Instance.GetValueOrDefault(CommanConstants.FlightListRetrievedKey, DateTime.MinValue);
			var fsr = new FlightSearchRequest()
			{
				ChangedAfter = flighDateTime.ToUniversalTime().ToString("s")

			};
			return fsr;
		}

		public async Task<List<string>> GetIcao4RetrieveAndFillExistingAsync(List<FlightListItemDto> flightList)
		{
			var icao4Retrieve = new List<string>();
			foreach (var item in flightList)
			{
				var i = 0;
				foreach (var icao in new[] { item.DEP, item.DEST })
				{
					i++;
					if (string.IsNullOrEmpty(icao))
					{
						continue;
					}
					var isFullEmpty = i == 1 ? string.IsNullOrEmpty(item.DepAirportFull) : string.IsNullOrEmpty(item.DestAirportFull);
					if (!isFullEmpty)
					{
						continue;
					}
					var airport = await airportRepository.GetAirportDb(icao);
					if (airport != null)
					{
						if (i == 1)
						{
							item.DepAirportFull = airport.Name;
						}
						else
						{
							item.DestAirportFull = airport.Name;
						}
					}
					else
					{
						if (!icao4Retrieve.Contains(icao))
						{
							icao4Retrieve.Add(icao);
						}
					}
				}
			}
			return icao4Retrieve;

		}
		private async Task<string> SetFullAirportNames(List<FlightListItemDto> flightList)
		{
			var result = string.Empty;
			var icao4Retrieve = await GetIcao4RetrieveAndFillExistingAsync(flightList);
			if (icao4Retrieve?.Count > 0)
			{
				var icaoString = string.Join(",", icao4Retrieve);
				var ar = new GetAirportListRequest
				{
					SessionID = AppSetting.Instance.GetValueOrDefault(CommanConstants.CurrentSessionKey, ""),
					ICAOList = icaoString
				};
				var responseAirportList = await _apiServices.GetAirportList<CB.Core.Models.ResponseModels.AirportResponseEnvelope>(ar);
				if (responseAirportList != null)
				{
					var airports = responseAirportList.Body.GetAirportListResponse.GetAirportListResult.Airports.Airport;
					await airportRepository.InsertOrUpdateAirport(ConvertorHelper.AirportModelConvertor(airports));
					//var airports = EdfService.ParseAirportList(responseAirportList.Message);
					//foreach (var airpot in airports)
					//{
					//   await new AirportDal().InsertOrUpdateAirport(airpot);
					//}
				}
				else
				{
					result = icaoString;
				}
				icao4Retrieve = await GetIcao4RetrieveAndFillExistingAsync(flightList);
				if (icao4Retrieve.Count > 0)
				{
					string.Join(",", icao4Retrieve);
				}
			}

			return result;

		}
	}
}
