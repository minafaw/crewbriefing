﻿using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CrewBriefing.Standard.MenuItems;
using CrewBriefing.Standard.Views;
using Plugin.Messaging;
using Plugin.Share;
using Plugin.Share.Abstractions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CrewBriefing.Standard.ViewModels
{
    public class MasterPageViewModel : BaseViewModel
	{
		private List<MasterPageItem> menuCells;
		public List<MasterPageItem> MenuList
		{
			get { return menuCells; }
			set {
				menuCells = value;
				RaisePropertyChanged();
			}
		}
		public ICommand ItemSelected { get; set; }
        public ICommand CloseButton { get; set; }
        public ICommand TailLogClickedButton { get; set; }
		public MasterPageViewModel()
		{
            ItemSelected = new Command(async (obj) => await HandleItemSelected(obj));
            TailLogClickedButton = new Command(  ()=> HandleTailLogClicked() );
			CloseButton = new Command(() =>
			{
				// to be tested 
				var masterPageNav = (Application.Current.MainPage as NavigationPage);
				(masterPageNav.CurrentPage as MasterDetialMainPage).IsPresented = false;
			});
			MenuList = CreateMenuList();
			var ver = DependencyService.Get<IAppVersion>();
			var version = ver.GetAppVersionBuild();
			AppVersion = "CrewBriefing for iOS " + AppResource.version + " " + version;
			if (Device.RuntimePlatform == Device.Android)
				AppVersion =  AppVersion.Replace("iOS", "Android");
			
		}


        public ImageSource TailLogSource { get => ImageSource.FromResource("CrewBriefing.Standard.Images.TL_banner_512T.png"); }

        public bool IsiPad { get => Device.Idiom == TargetIdiom.Tablet && Device.RuntimePlatform == Device.iOS; }
		private async Task HandleItemSelected(object obj)
		{
			MasterPageItem selecteditem = obj as MasterPageItem;
			if (selecteditem == null) return;
			if(selecteditem.Text == AppResource.login_details)
			{
				await App.NavigationService.NavigateAsync(nameof(LoginPage));
			}
			else if (selecteditem.Text == AppResource.about)
			{
				await App.NavigationService.NavigateAsync(nameof(AboutPage));
			} else if (selecteditem.Text == AppResource.help)
			{
				await App.NavigationService.NavigateAsync(nameof(HelpPage));

			} else if (selecteditem.Text == AppResource.share_app)
			{
				await HandleShareClicked();
			}else if (selecteditem.Text == AppResource.feedback)
			{
				HandleFeedBackClicked();
			}

		}

		private string _appVersion = string.Empty;
		public string AppVersion
		{
			get { return _appVersion; }
			set {
				 _appVersion = value;
				RaisePropertyChanged();
			}
		}

		public double ImageDesiredSize => CommonStatic.Instance.MediumHeight * 1.2;
		public string HomeImg => Utils.ImageFolder + "icon_launcher.png";
		public string CloseImg => Utils.ImageFolder + "icon_close_grey.png";
		public double LableFontSize => CommonStatic.LabelFontSize(NamedSize.Medium);

		string imageFolder => Utils.ImageFolder;
		private List<MasterPageItem> CreateMenuList()
		{
			
			var list = new List<MasterPageItem>
			{
//				new MasterPageItem {Text = AppResource.main_title, ImagePath = imageFolder + "icon_launcher.png"},
				new MasterPageItem { TargetType=typeof(LoginPage) , Text = AppResource.login_details, ImagePath = imageFolder + "tablet_login_details_icon.png"},
				new MasterPageItem { TargetType=typeof(AboutPage) , Text = AppResource.about, ImagePath = imageFolder + "icon_info_17.png"},
				new MasterPageItem { TargetType=typeof(HelpPage) , Text = AppResource.help, ImagePath = imageFolder + "icon_question.png"},
				new MasterPageItem { Text = AppResource.share_app, ImagePath = imageFolder + "tablet_share_popover_icon.png"},
				new MasterPageItem { Text = AppResource.feedback, ImagePath = imageFolder + "tablet_feedback_icon.png"},
			};
			return list;
		}

		private async Task HandleShareClicked()
		{
			string url = null;
			var shareTitle = AppResource.share_title;
			//Utils.ShowTemporayMessage(Interfaces.ToastType.Info, "Share");
			switch (Device.RuntimePlatform)
			{
				case Device.iOS:
					url = "https://itunes.apple.com/us/app/pps-crewbriefing/id886621885";
					shareTitle = url;
					break;
				case Device.Android:
					url = "https://play.google.com/store/apps/details?id=crewbriefing.droid";
					break;
				case Device.UWP:
					url = "https://www.microsoft.com/en-us/store/p/crewbriefing/9pmwthf9gskg";
					break;
			}
			await CrossShare.Current.Share(new ShareMessage()
			{
				Text = shareTitle,
				Title = shareTitle,
				Url = url
			});
		}

		private void HandleFeedBackClicked()
		{
			var ver = DependencyService.Get<IAppVersion>();

			string emailBody = " <br/><br/>CrewBriefing App " + ver.GetAppVersionBuild() +
				"<br/>" +
				"User: " + WebUtility.HtmlEncode(AppSetting.Instance.GetValueOrDefault(CommanConstants.UserNameKey, "")) +
				"<br/>Device: " + WebUtility.HtmlEncode(ver.GetDeviceName()) +
				"<br/>OS: " + WebUtility.HtmlEncode(ver.GetOs());

			if (!string.IsNullOrEmpty(ver.MailEol()))
			{
				emailBody = emailBody.Replace("<br/>", ver.MailEol());
			}

			var emailMessenger = CrossMessaging.Current?.EmailMessenger;
			if (emailMessenger != null && emailMessenger.CanSendEmail)
			{

				// Alternatively use EmailBuilder fluent interface to construct more complex e-mail with multiple recipients, bcc, attachments etc. 
				var email = new EmailMessageBuilder()
				  .To("feedback@airsupport.dk")
				  .Subject("Feedback")
				  .Body(emailBody)
				  .Build();

				emailMessenger.SendEmail(email);
			}
			else
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Cant send mail");
			}
		}

        private void HandleTailLogClicked(){
            //Handle image Tap
            const string urlToSend = "https://itunes.apple.com/dk/app/taillog/id947286350?l=da&mt=8";
            Device.OpenUri(new Uri(urlToSend));
        }

    }
}
