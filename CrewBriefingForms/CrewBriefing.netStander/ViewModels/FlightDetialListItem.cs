﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CB.Core.Common;
using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.DAL.Interfaces;
using CB.Core.Entities;
using CB.Core.Models;
using CB.Core.Models.Base;
using CB.Core.Models.RequestModels;
using CB.Core.Models.ResponseModels;
using CB.Core.Services;
using CB.Core.Services.Interfaces;
using CrewBriefing.Standard;
using Xamarin.Forms;

namespace CB.Core.ViewModels
{
	public class FlightDetialListItem : BaseViewModel
    {
        readonly IDalServices DalServices;
        readonly IApiServices ApiServices;
		private readonly IPdfRepository pdfRepository;
		public ICommand GetPdfCommand { get; private set; }
		

		public FlightDetialListItem(IDalServices dalServices , IApiServices apiServices, IPdfRepository pdfRepository)
	    {
			DalServices = dalServices;
			ApiServices = apiServices;
			this.pdfRepository = pdfRepository;
			GetPdfCommand = new Command( async () =>
			{
				await GetPdf();
			});
		}

		private async Task GetPdf()
		{
			if (DocType == null) return;
			var saveLoad = DependencyService.Get<ISaveAndLoad>();

			ItemDownloading = true;
			bool checkSessionOk = !DalServices.IsSessionExpired();
			if (!checkSessionOk)
			{
				 checkSessionOk = await ApiServices.CheckSessionAndRefreshIfRequired();
			}

			if (!checkSessionOk)
			{
				ItemDownloading = false;
				return;
			}
			var SessionId = AppSetting.Instance.GetValueOrDefault(CommanConstants.CurrentSessionKey, "");

			//var response = await ApiServices.GetPdfResponse((Enums.PdfDocType)DocType, FlightId);

				GetPDF_Result pdf = null;
				switch (DocType)
				{
					case Enums.PdfDocType.Logstring:
						var LogPdfResult = await ApiServices.GetPDF_Logstring( new GetPDF_LogstringRequest() { FlightID = FlightId , SessionID = SessionId });
						pdf = LogPdfResult?.Body?.GetPDF_LogstringResponse?.GetPDF_LogstringResult;
						break;
					 case Enums.PdfDocType.Messages:
						var MessagePdfResult = await ApiServices.GetPDF_Messages<MessagesPdfEnvelope>(new GetPDF_MessagesRequest() {FlightID = FlightId , SessionID=SessionId});
						pdf = MessagePdfResult?.Body?.GetPDF_MessagesResponse?.GetPDF_MessagesResult;
						break;
					 case Enums.PdfDocType.Wx:
						var wxPdfResult = await ApiServices.GetPDF_WX<WxPdfEnvelope>(new GetPDF_WXRequest() { FlightID=FlightId , SessionID= SessionId});
						pdf = wxPdfResult?.Body?.GetPDF_WXResponse?.GetPDF_WXResult;
						break;
					 case Enums.PdfDocType.NotaMs:
						var NotamPdf = await ApiServices.GetPDF_NOTAMs<NotamsEnvelope>(new GetPDF_WXRequest() { FlightID = FlightId, SessionID = SessionId });
						pdf = NotamPdf?.Body?.GetPDF_NOTAMsResponse?.GetPDF_NOTAMsResult;
					break;
					  case Enums.PdfDocType.Atc:
						var atcpdf = await ApiServices.GetPDF_ATC<AtcEnvelope>(new GetPDF_LogstringRequest() { FlightID = FlightId, SessionID = SessionId } );
						pdf = atcpdf?.Body?.GetPDF_ATCResponse?.GetPDF_ATCResult;
						break;
					case Enums.PdfDocType.WindT:
					case Enums.PdfDocType.Swx:
					case Enums.PdfDocType.CrossSection:
						var Docpdf = await ApiServices.GetFlightDoc<DocumentEnvelope>(new GetFlightDocumentRequest() { SessionID= SessionId , documentIdentifier = GetPrepareIdentifier() });
						var docPdfRes = Docpdf?.Body?.GetFlightDocumentResponse?.GetFlightDocumentResult;
						 pdf = new GetPDF_Result() { BArray= docPdfRes?.ByteArray  , Responce = docPdfRes.Response , FileName = docPdfRes.Meta.Title };
					break;
					case Enums.PdfDocType.UploadedDocuments:
					var frFupl = new GetPDF_FullPackageRequest()
					{
						SessionID = SessionId,
						FlightID = FlightId,
						ATC = false,
						Charts = false,
						FlightLog = false,
						Messages = false,
						Notams = false,
						Uploaded = true,
						WX = false
					};

					var Frpdf = await ApiServices.GetPDF_FullPackage<FullPdfEnvelope>(frFupl);
					pdf = Frpdf.Body.GetPDF_FullPackageResponse.GetPDF_FullPackageResult;
					break;
				case Enums.PdfDocType.CombineDocuments:

					var frFull = new GetPDF_FullPackageRequest()
					{
						SessionID = SessionId,
						FlightID = FlightId,
						ATC = true,
						Charts = true,
						FlightLog = true,
						Messages = true,
						Notams = true,
						Uploaded = true,
						WX = true
					};
					var Fullpdf = await ApiServices.GetPDF_FullPackage<FullPdfEnvelope>(frFull);
					pdf = Fullpdf.Body.GetPDF_FullPackageResponse.GetPDF_FullPackageResult;
					break;
				default:
					throw new Exception("GetPDF parse not implemented: " + DocType.ToString());
			}

			ItemDownloading = false;

			if (pdf == null || pdf.Responce.Succeed.Equals("false") || pdf.BArray == null)
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Info, AppResource.no_uploaded_doc, 3000);
				return;
			}

			// this equal to force to refresh
			if (ItemDownloaded)
				{
					await pdfRepository.DeletePdf(FlightId, (Enums.PdfDocType)DocType);
				}
				var dbPdf = new DbPdf
				{
					Id = FlightId + " " + (int)DocType,
					DocType = (int)DocType,
					DownloadTime = DateTime.UtcNow,
					FileName = pdf.FileName,
					FlightId = FlightId
				};
				await saveLoad.SaveFile(dbPdf.FilePath, pdf.BArray);

				await pdfRepository.InsertOrUpdatePdf(dbPdf);
				ItemDownloading = false;

				if (DocType != null)
					 await CheckDocExists();
			

			ItemDownloading = false;
		}

		private string GetPrepareIdentifier()
		{
			var identifier = "WeatherChart&" + FlightId;
			switch (DocType)
			{
				case Enums.PdfDocType.WindT:
					identifier += "&RS_WT_DefaultFL";
					break;
				case Enums.PdfDocType.Swx:
					identifier += "&RS_SWC_M";
					break;
				case Enums.PdfDocType.CrossSection:
					identifier += "&CS";
					break;
			}
			return identifier;
		}
		private async Task CheckDocExists()
		{
            var pdd = await pdfRepository.GetStoredPdf(FlightId , DocType.Value);
            if ( pdd != null)
			{
				ItemDownloaded = true;
				DownloadDate = pdd.LocalDate.UtcDateTime;
				Image1Path = CrewBriefing.Standard.Utils.ImageFolder + (ItemDownloaded ? "redownload_doc.png" : "icon_download.png");

			}

		}

		private int _flightId;
        public int FlightId
        {
            get => _flightId;
            set
            {
                _flightId = value;
                RaisePropertyChanged();
            }
        }
        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                RaisePropertyChanged();
            }
        }

        private bool _itemDownloading;
        public bool ItemDownloading
        {
            get => _itemDownloading;
            set
            {
                _itemDownloading = value;
                RaisePropertyChanged();
            }

        }
        private bool _itemDownloaded;
        public bool ItemDownloaded
        {
            get => _itemDownloaded;
            set
            {
                _itemDownloaded = value;
                RaisePropertyChanged();
	            RaisePropertyChanged(nameof(Title));
			}
        }
        private string _title;
        public string Title
        {
	        get
	        {
		        if (!ItemDownloaded) return _title;
		        switch (DocType)
		        {
			        case Enums.PdfDocType.UploadedDocuments:
				        return Resource.doc_title_UploadedDocumentsShort;
			        case Enums.PdfDocType.CombineDocuments:
				        return Resource.doc_title_CombineDocumentsShort;
		        }
		        return _title;

	        } 
            set
            {
                _title = value;
                RaisePropertyChanged();
            }
        }

	    public string Image1Path { get; set; }
	    
        private bool _isVisibleImage1 = true;
        public bool IsVisibleImage1
        {
            get => _isVisibleImage1;
            set
            {
                _isVisibleImage1 = value;
                RaisePropertyChanged();
            }
        }
        //public bool IsVisibleImage2 { get; set; }
        public string DownloadDateStr
        {
            get
            {
                string downloadDateStr = null;
                if (DownloadDate == null) return null;

                var diff = DateTime.UtcNow - (DateTime)DownloadDate;
                if (diff.Days != 0)
                {
                    if (diff.Days >= 7)
                    {
                        downloadDateStr += diff.Days / 7 + "w ";
                        if (diff.Days % 7 != 0)
                        {
                            downloadDateStr += diff.Days % 7 + "d ";
                        }
                    }
                    else
                    {
                        downloadDateStr += diff.Days + "d ";
                    }

                }
                if (diff.Hours != 0)
                {
                    downloadDateStr += diff.Hours + "h ";
                }
                if (string.IsNullOrEmpty(downloadDateStr) && diff.Minutes == 0)
                {
                    downloadDateStr = "Now";
                }
                else
                {
                    downloadDateStr += diff.Minutes + "min";
                }
                return downloadDateStr;
            }

        }
        private DateTime? _downloadDate;
        public DateTime? DownloadDate
        {
            get => _downloadDate;
            set
            {
                _downloadDate = value;
                RaisePropertyChanged();
                // notify that calculated property 
                RaisePropertyChanged(nameof(DownloadDateStr));
                // notify textcolor property 
                RaisePropertyChanged(nameof(DateTextColor));
	            // notify textcolor property 
	            RaisePropertyChanged(nameof(Image1Path));
			}
        }
        private Color _dateTextColor = Color.Black;
        public Color DateTextColor
        {
            get
            {
                _dateTextColor = Color.Black;
                if (DownloadDate == null)
                    return _dateTextColor;


                var ts = (System.TimeSpan)(DateTime.UtcNow - DownloadDate);
                if (Title.Equals(Resource.doc_title_WX))
                {
                    if (ts.TotalMinutes > 30)
                    {
                        _dateTextColor = Color.Red;  // WX - 30 min
                    }
                }
                if (Title.Equals(Resource.doc_title_NOTAMs))
                {
                    if (ts.TotalHours > 3)
                    {
                        _dateTextColor = Color.Red; // NOTAMs - 3 h
                    }
                }
                return _dateTextColor;
            }

            set
            {
                _dateTextColor = value;
                RaisePropertyChanged();
            }
        }
        

        public Thickness Image1WrapperPadding => new Thickness(0, 0, CommonStatic.Instance.MediumHeight * 2, 0);

        public Enums.PdfDocType? DocType
        {
            get; set;
        }
       
    }
}
