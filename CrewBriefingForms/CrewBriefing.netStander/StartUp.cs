﻿using System;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extras.CommonServiceLocator;
using CB.Core.DAL;
using CB.Core.DAL.AutoFacModule;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Services;
using CB.Core.Services.Interfaces;
using CB.Core.ViewModels;
using CommonServiceLocator;
using CrewBriefing.Standard.Controls;
using CrewBriefing.Standard.Interfaces;
using CrewBriefing.Standard.ViewModels;
using CrewBriefing.Standard.Views;


namespace CrewBriefing.Standard
{
    public class StartUp
    {
		public async static Task Intialize()
		{
			ContainerBuilder builder = new ContainerBuilder();

			#region register services
			builder.RegisterType<ApiManager>().As<IApiManager>();
			
            builder.RegisterType<FilterSettings>().As<IFilterSettings>();
			builder.RegisterModule<DalAutFacModule>();
			builder.RegisterType<ApiServices>().AsImplementedInterfaces().SingleInstance();

			#endregion

			#region Register Pages
			builder.RegisterType<LoginPage>();
			builder.RegisterType<StackLayoutFlightList>();
			builder.RegisterType<MenuPage>();

			

			#endregion

			#region Register ViewModels
			builder.RegisterType<LoginPageViewModel>();
			builder.RegisterType<FlightListViewModel>();
			builder.RegisterType<FlightDetailsViewModel>();
			builder.RegisterType<FlightDetialListItem>();
			builder.RegisterType<FilterPageViewModel>();
			

			#endregion

			IContainer container = builder.Build();
            try{
                var apiServices = container.Resolve<IApiServices>();
                // await apiServices.FetchUrlAsync();
            }catch(Exception e){
                System.Diagnostics.Debug.WriteLine(e);
                throw;
            }

			AutofacServiceLocator serviceLocator = new AutofacServiceLocator(container);
			ServiceLocator.SetLocatorProvider( ()=> serviceLocator);
		}

	}
}
