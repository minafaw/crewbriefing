﻿using CB.Core.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static CB.Core.Common.Keys.Enums;

namespace CrewBriefing.Standard.Interfaces
{
    public interface IFilterSettings
    {
		bool IsFilterNotEMpty();
		Task<List<string>> LoadAndParseTextFiler(SettingType type , string currentUserName);
		Task ClearFilter();
		//bool SortByDefault(FlightListItemDto item);
		List<FlightListItemDto> SortByDefault();
		Task LoadFilterFromDb();
		void GetDateTimeByStdCode(int code, out DateTime startDatetime, out DateTime endDatetime);
		void SortList(ref List<FlightListItemDto> list);

		Task<List<FlightListItemDto>> HandleFilterData();
		//List<FlightListItemDto> FilterDataWithCrew();
		List<FlightListItemDto> FilterData();
		//bool ItemPassedFilter(FlightListItemDto item);
	}
}
