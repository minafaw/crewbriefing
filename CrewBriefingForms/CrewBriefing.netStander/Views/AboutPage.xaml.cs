﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using CB.Core.Common.Interfaces;
using CB.Core.CustomControls.NavigationService;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views
{
    
    public partial class AboutPage 
    {
        //private readonly HybridWebView hybrid;
        public Func<string, Task<string>> EvaluateJavascript { get; set; }

        public AboutPage()
        {
            InitializeComponent();
	        string pageTitle = AppResource.about;
			Title = pageTitle;
			CustomNavigationPage.SetTitlePosition(this, CustomNavigationPage.TitleAlignment.Center);
			CustomNavigationPage.SetTitleColor(this, Color.White);
			
			var ver = DependencyService.Get<IAppVersion>();
            var versionBuild = ver.GetAppVersion();
            var javaScriptCode = "document.getElementById('versionCode').innerHTML = '" + "Version: " + versionBuild + "';";

            webView.Source = LoadHtmlFileFromResource();
            webView.Navigated += (sender, e) => {
                webView.Eval(javaScriptCode);
            };
        }


      
        private static HtmlWebViewSource LoadHtmlFileFromResource()
        {
            var source = new HtmlWebViewSource();
            var assembly = typeof(AboutPage).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("CrewBriefing.Standard.about_us.html");
            using (var reader = new StreamReader(stream))
            {
                source.Html = reader.ReadToEnd();
            }
            return source;
        }


    }
}
