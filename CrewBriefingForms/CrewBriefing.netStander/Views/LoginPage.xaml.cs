﻿using System;
using System.Threading.Tasks;
using Autofac;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.Models.Base;
using CB.Core.Services;
using CB.Core.Services.Interfaces;
using CB.Core.ViewModels;
using Xamarin.Forms;
using CommonStatic = CrewBriefing.Standard.Helper.CommonStatic;

namespace CrewBriefing.Standard.Views
{
  
    public partial class LoginPage
    {
		LoginPageViewModel loginViewModel;
		public LoginPage()
        {
            InitializeComponent();
			loginViewModel = ViewModelLocator.LoginPageViewModel;
			BindingContext = loginViewModel;
			

			txtUsername.Completed += TxtUsername_Completed;
            txtPassword.Completed += TxtPassword_Completed;

            

            if (loginViewModel.LoadCredentials)
            {
                HandleShowToolBar();
            }
            else
            {
                BackgroundColor = Color.White;
                ToolbarItems.Clear();
            }
     
            var ver = DependencyService.Get<IAppVersion>();
           

            AbsoluteLayout.SetLayoutBounds(labelVersion, new Rectangle(0.5, 0.5, 0.7, 1));
            AbsoluteLayout.SetLayoutFlags(labelVersion, AbsoluteLayoutFlags.All);

            imageInfoPanel.Padding = new Thickness(0, 0, CommonStatic.Instance.GetMediumHeight, 0);  // CA-10

            AbsoluteLayout.SetLayoutBounds(imageInfoPanel, new Rectangle(0.5, 0.5, 300 + CommonStatic.Instance.GetMediumHeight * 2, 1)); //width = EditboxAlphabet + width of I image - CA-10
            AbsoluteLayout.SetLayoutFlags(imageInfoPanel, AbsoluteLayoutFlags.PositionProportional | AbsoluteLayoutFlags.HeightProportional);

            labelVersion.Text = AppResource.version + " " + ver.GetAppVersionBuild();
            NavigationPage.SetHasNavigationBar(this, false);

        }


        private void HandleShowToolBar()
        {
            BackgroundColor = (Color)Application.Current.Resources["nav_bar_color"];
                var cancelPanel = new StackLayout
                {
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    Padding = new Thickness(20, 0, 0, 0),
                    Orientation = StackOrientation.Horizontal
                };
                var labelCancel = new Label
                {
                    Text = "Cancel",
                    FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                    HorizontalTextAlignment = TextAlignment.Start,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    TextColor = Color.White
                };
                var tapCancel = new TapGestureRecognizer();
                tapCancel.Tapped += (sender, e) =>
                {
                    try
                    {
                    App.NavigationService.GoBack();
                    }
                    catch (Exception exc)
                    {
                        ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, exc.Message);
                    }
                };

                cancelPanel.Children.Add(labelCancel);
                cancelPanel.GestureRecognizers.Add(tapCancel);
                AbsoluteLayout.SetLayoutBounds(cancelPanel, new Rectangle(0, 0, 0.4, 1));
                AbsoluteLayout.SetLayoutFlags(cancelPanel, AbsoluteLayoutFlags.All);
                panelLogout.Children.Add(cancelPanel);


                var logoutPanel = new StackLayout
                {
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Padding = new Thickness(0, 0, 20, 0),
                    Orientation = StackOrientation.Horizontal,
                };
                var labelLogout = new Label
                {
                    Text = "Logout",
                    FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                    HorizontalTextAlignment = TextAlignment.End,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    TextColor = Color.White
                };
                var tapLogout = new TapGestureRecognizer();
                tapLogout.Tapped += (sender, e) =>
                {
                    AppSetting.Instance.Remove(CommanConstants.PasswordKey);
                    AppSetting.Instance.Remove(CommanConstants.QuickLoginKey);
					AppSetting.Instance.Remove(CommanConstants.IsLoginedInKey);
					AppSetting.Instance.Remove(CommanConstants.FlightListRetrievedKey);

					ToolbarItems.Clear();
					
                   ((App) Application.Current).SetRootPage(nameof(LoginPage));
                };

                logoutPanel.Children.Add(labelLogout);
                logoutPanel.GestureRecognizers.Add(tapLogout);
                AbsoluteLayout.SetLayoutBounds(logoutPanel, new Rectangle(1, 0, 0.4, 1));
                AbsoluteLayout.SetLayoutFlags(logoutPanel, AbsoluteLayoutFlags.All);

                panelLogout.HeightRequest = CommonStatic.Instance.GetMediumHeight * 1.8;

                panelLogout.Children.Add(logoutPanel); 
        }
        private async void TxtPassword_Completed(object sender, EventArgs e)
        {
            await loginViewModel.ProcessLoginButton(txtUsername.Text , txtPassword.Text);
        }

        private void TxtUsername_Completed(object sender, EventArgs e)
        {
            txtPassword.Focus();
        }
       

        public async void ButtonDemo_Click(object sender, EventArgs e)
        {
            const string username = "AAADEMO";
            const string password = "BLL111";

            await loginViewModel.ProcessLoginButton(username, password);
        }
        
        public async void button_Click(object sender, EventArgs e)
        {
            await loginViewModel.ProcessLoginButton(txtUsername.Text, txtPassword.Text);
        }
        }
}
