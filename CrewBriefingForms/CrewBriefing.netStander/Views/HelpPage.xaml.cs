﻿using System;
using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HelpPage 
    {
        private int _currentPage = 0;
        private Image[] dotImages = new Image[4];
        private string[] txtHeader = new string[4];
        private string[] txtContent = new string[4];
        private double imageSize = 20;
        public HelpPage()
        {
            InitializeComponent();
            //Utils.SetBackNavBar(this, panelNav, "Help");

            if (Device.RuntimePlatform == Device.iOS)
            {
                Padding = new Thickness(0, 20, 0, 0);
            }
            mainStackSwipe.OnSwipeRight += panel_SwipeRightustom;
            mainStackSwipe.OnSwipeLeft += panel_SwipeLeftCustom;

            var tapEmail = new TapGestureRecognizer();
            tapEmail.Tapped += async (sender, e) =>
            {
                labelEmail.BackgroundColor = Color.Gray;
                await App.Sleep(250);
                labelEmail.BackgroundColor = Color.Transparent;
                Device.OpenUri(new Uri("mailto:" + labelEmail.Text));
            };
            labelEmail.GestureRecognizers.Add(tapEmail);

            if (Device.Idiom == TargetIdiom.Tablet)
            {
                mainGrid.RowDefinitions[1].Height = new GridLength(45, GridUnitType.Star);
                mainGrid.RowDefinitions[2].Height = new GridLength(50, GridUnitType.Star);

                mainGrid.ColumnDefinitions[0].Width = new GridLength(35, GridUnitType.Star);
                mainGrid.ColumnDefinitions[1].Width = new GridLength(30, GridUnitType.Star);
                mainGrid.ColumnDefinitions[2].Width = new GridLength(35, GridUnitType.Star);
            }
            else
            {
                mainGrid.RowDefinitions[0].Height = new GridLength(3, GridUnitType.Star);
                mainGrid.RowDefinitions[1].Height = new GridLength(45, GridUnitType.Star);
                mainGrid.RowDefinitions[2].Height = new GridLength(50, GridUnitType.Star);

                mainGrid.ColumnDefinitions[0].Width = new GridLength(5, GridUnitType.Star);
                mainGrid.ColumnDefinitions[1].Width = new GridLength(90, GridUnitType.Star);
                mainGrid.ColumnDefinitions[2].Width = new GridLength(5, GridUnitType.Star);

            }

            imageSize = 10;
            if (Device.Idiom != TargetIdiom.Tablet)
            {
                imageSize = 6;
            }


            panelDots.Spacing = imageSize * 1.4;
            for (int i=0; i<4; i++)
            {
	            dotImages[i] = new Image
	            {
		            Aspect = Aspect.AspectFit,
		            HeightRequest = imageSize,
		            WidthRequest = imageSize,
		            Source = Utils.ImageFolder + "dot_normal.png"
	            };
	            panelDots.Children.Add(dotImages[i]);
                // selected size = normal * 1.25
            }
            var assembly = typeof(AppResource).GetTypeInfo().Assembly;
            var manager = new ResourceManager("CrewBriefing.Standard.AppResource", assembly);
            //panelDotsWrap.

            var tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) =>
            {
                _currentPage++;
                if (_currentPage > 3)
                {
                    _currentPage = 0;
                }
                setSelected();
            };
            panelDotsWrap.GestureRecognizers.Add(tap);
            panelDots.GestureRecognizers.Add(tap);

            for (int i = 1; i <= 4; i++)
            {
                txtHeader[i - 1] = manager.GetString("help_head" + i);
                txtContent[i - 1] = manager.GetString("help_text" + i);
            }
            setSelected();

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (Utils.md != null)
            {
                Utils.md.IsGestureEnabled = false;
            }
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (Utils.md != null)
            {
                Utils.md.IsGestureEnabled = true;
            }
        }

        private void panel_SwipeLeftCustom(object sender, EventArgs e)
        {
            _currentPage++;
            if (_currentPage > 3)
            {
                _currentPage = 0;
            }
            setSelected();
        }

        private void panel_SwipeRightustom(object sender, EventArgs e)
        {
            _currentPage--;
            if (_currentPage < 0)
            {
                _currentPage = 3;
            }
            setSelected();
        }

        private void setSelected()
        {
            panelPage1_1.IsVisible = _currentPage == 0;
            //panelPage1_2.IsVisible = panelPage1_1.IsVisible;
            //labelOr.IsVisible = panelPage1_1.IsVisible;

            image.Source = Utils.ImageFolder + "image_help_" + (_currentPage + 1) + ".png";
            
            for (int i=0; i<4; i++)
            {
                if (i == _currentPage)
                {
                    dotImages[i].HeightRequest = imageSize * 1.25;
                    dotImages[i].WidthRequest = imageSize * 1.25;
                    dotImages[i].Source = Utils.ImageFolder + "dot_selected.png";
                }
                else
                {
                    dotImages[i].HeightRequest = imageSize ;
                    dotImages[i].WidthRequest = imageSize;
                    dotImages[i].Source = Utils.ImageFolder + "dot_normal.png";
                }
            }
            labelHeader.Text = txtHeader[_currentPage];
            labelText.Text = txtContent[_currentPage];
            //currentPage
        }
    }
}