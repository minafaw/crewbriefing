﻿using System.Threading.Tasks;
using CB.Core.Common.Keys;
using CB.Core.CustomControls.NavigationService;
using CrewBriefing.Standard.Helper;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlightListPage 
    {
        
        public FlightListPage()
        {
            InitializeComponent();
			Title = "Flights";
			CustomNavigationPage.SetTitlePosition(this, CustomNavigationPage.TitleAlignment.Center);
			CustomNavigationPage.SetTitleColor(this, Color.White);
			CustomNavigationPage.SetBarBackground(this, Color.FromHex("#445C74"));


		}

		private void Refresh_Clicked(object sender, System.EventArgs e)
		{
			panelFlightList.HandleRefreshClicked(true);
		}
		public void HandleFilterDone()
		{
			panelFlightList.HandleRefreshClicked(false);
		}
		private void Filter_Clicked(object sender, System.EventArgs e)
		{
			var masterPageNav = (Application.Current.MainPage as NavigationPage);
			var masterPage = (masterPageNav.CurrentPage as MasterDetialMainPage);
			masterPage.OpenFilerPage();
		}
	}
}
