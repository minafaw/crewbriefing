﻿using CB.Core.CustomControls.NavigationService;
using CB.Core.Dtos;
using CB.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlightSummaryPage 
    {
        public FlightSummaryPage(FlightDto flightDto)
        {
            InitializeComponent();
			Title = "Flight Summary";
			CustomNavigationPage.SetTitlePosition(this, CustomNavigationPage.TitleAlignment.Center);
			CustomNavigationPage.SetTitleColor(this, Color.White);

			labelInfo.IsVisible = !string.IsNullOrEmpty(flightDto.FlightSummary);
            labelInfo.Text = flightDto.FlightSummary;
            panelNoInfo.IsVisible = !labelInfo.IsVisible;
            // https://developer.xamarin.com/guides/xamarin-forms/user-interface/text/fonts/

            string fontFamily = Utils.SummaryFontFamily;
            string fontFamilyBold = Utils.SummaryFontFamilyBold;

            labelNoinfoTitle.FontFamily = fontFamilyBold;
            labelNoinfoText.FontFamily = fontFamily;
            labelInfo.FontFamily = fontFamily;

            //Utils.SetBackNavBar(this, panelNav, flightNo);
        }
    }
}
