﻿using CrewBriefing.Standard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterPage 
	{
		public MasterPage ()
		{
			InitializeComponent ();
			
			Title = AppResource.main_title;
			Icon = Utils.ImageFolder + "menu.png";
			BindingContext = new MasterPageViewModel();
		}

		private void ExtListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			menuList.SelectedItem = null;
		}
	}
}