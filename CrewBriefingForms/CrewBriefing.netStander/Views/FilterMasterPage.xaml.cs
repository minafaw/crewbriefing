﻿using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.DAL;
using CB.Core.DAL.Interfaces;
using CB.Core.Entities;
using CrewBriefing.Standard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FilterMasterPage 
	{
        FilterPageViewModel viewModel;
		IDalServices _dalServices;
		public readonly IFilterDbSettingsRepository filterDbSettingsRepository;
		public FilterMasterPage ()
		{
			InitializeComponent ();
			Title = AppResource.main_title;
			Icon = Utils.ImageFolder + "menu.png";
            viewModel = ViewModelLocator.FilterPageViewModel;
            BindingContext = viewModel;
			_dalServices = viewModel.Dalservices;
			filterDbSettingsRepository = viewModel.filterDbSettingsRepository;

			viewModel.donePressed += async delegate
			{
				await SaveFilterSettings();
				HandleApplyFilter();
			};


			_txtTailFilter.TextChanged += TxtTailFilter_TextChanged;
            _switchTailFilter.Toggled += SwitchTailFilter_Toggled;
            _txtTailFilter.Completed += TxtTailFilter_Completed;
            _txtCrewFilter.TextChanged += TxtCrewFilter_TextChanged;
            _txtCrewFilter.Completed += TxtTailFilter_Completed;
            _switchCrewFilter.Toggled += SwitchCrewFilter_Toggled;
            _txtDepFilter.TextChanged += TxtDepFilter_TextChanged;
            _txtDepFilter.Completed += TxtTailFilter_Completed;
            _switchDepFilter.Toggled += SwitchDepFilter_Toggled;
            _txtDestFilter.TextChanged += TxtDestFilter_TextChanged;
            _txtDestFilter.Completed += TxtTailFilter_Completed;
            _switchDestFilter.Toggled += SwitchDestFilter_Toggled;
            _txtFlightNoFilter.TextChanged += TxtFlightNoFilter_TextChanged;
            _txtFlightNoFilter.Completed += TxtTailFilter_Completed;
            _switchFlightNoFilter.Toggled += SwitchFlightNoFilter_Toggled;

		}

		private void HandleApplyFilter()
		{
			var currentNav = (Application.Current.MainPage as NavigationPage);
			var currentPage = (currentNav.CurrentPage as MasterDetialMainPage);
			currentPage.IsPresented = false;
			var childNavPage = (currentPage.Detail as NavigationPage);

			var flightPage = childNavPage.CurrentPage as FlightListPage;
			if (flightPage != null) { 
				flightPage.HandleFilterDone();
			}
			else
			{
				var tabletPage = childNavPage.CurrentPage as TabletMainPage;
				if(tabletPage != null)
				{
					tabletPage.UpdateList(false);

				}
			}
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			 LoadFilterSettings();
		}

		private void TxtTailFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtTailFilter, _switchTailFilter);
        }

        private void TxtCrewFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtCrewFilter, _switchCrewFilter);
        }

        private void CheckToggleOff(Switch switchControl, Entry entryControl)
        {
            if (!switchControl.IsToggled)
            {
                entryControl.Text = string.Empty;
            }

        }
        private void SwitchDepFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchDepFilter, _txtDepFilter);
        }
        private void SwitchDestFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchDestFilter, _txtDestFilter);
        }
        private void SwitchFlightNoFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchFlightNoFilter, _txtFlightNoFilter);
        }
        private void TxtDepFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtDepFilter, _switchDepFilter);
        }
        private void TxtDestFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtDestFilter, _switchDestFilter);
        }
        private void TxtFlightNoFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtFlightNoFilter, _switchFlightNoFilter);
        }
        private void SwitchCrewFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchCrewFilter, _txtCrewFilter);
        }
        private void SwitchTailFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchTailFilter, _txtTailFilter);
        }
        private void TxtTailFilter_Completed(object sender, EventArgs e)
        {
            SaveFilterSettings();
			HandleApplyFilter();
		}


        public async Task SaveFilterSettings()
        {

            var currentUserName = AppSetting.Instance.GetValueOrDefault(CommanConstants.UserNameKey, "");
           
            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.StdFilterOn,
                Value = "true",
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.StdFilterOn
            });

            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.TailFilter,
                Value = _switchTailFilter.IsToggled ? _txtTailFilter.Text.Trim() : string.Empty,
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.TailFilter

            });
            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.CrewFilter,
                Value = _switchCrewFilter.IsToggled ? _txtCrewFilter.Text.Trim() : string.Empty,
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.CrewFilter
            });
            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.DepFilter,
                Value = _switchDepFilter.IsToggled ? _txtDepFilter.Text.Trim() : string.Empty,
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.DepFilter
            });
            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.DestFilter,
                Value = _switchDestFilter.IsToggled ? _txtDestFilter.Text.Trim() : string.Empty,
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.DestFilter
            });
            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.FlightNoFilter,
                Value = _switchFlightNoFilter.IsToggled ? _txtFlightNoFilter.Text.Trim() : string.Empty,
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.FlightNoFilter
            });

            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.StdFilter1,
                Value = viewModel.GetStdIndex(_stdStart.Items[_stdStart.SelectedIndex]).ToString(),
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.StdFilter1
            });

            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.SortField,
                Value = _sortField.SelectedIndex.ToString(),
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.SortField
            });

            await filterDbSettingsRepository.InsertOrUpdateFilterSetting(new FilterDbSettings()
            {
                Id = (int)Enums.SettingType.SortType,
                Value = _sortType.SelectedIndex.ToString(),
                UserName = currentUserName,
                PrimKey = currentUserName + Enums.SettingType.SortType
            });
        }

        public async Task LoadFilterSettings()
        {
            var currentUserName = AppSetting.Instance.GetValueOrDefault(CommanConstants.UserNameKey, "");

            _txtTailFilter.Text = await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.TailFilter, currentUserName);
            _txtCrewFilter.Text = await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.CrewFilter, currentUserName);
            _txtDepFilter.Text = await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.DepFilter, currentUserName);
            _txtDestFilter.Text = await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.DestFilter, currentUserName);
            _txtFlightNoFilter.Text = await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.FlightNoFilter, currentUserName);
            SetTextSwitch(_txtTailFilter, _switchTailFilter);
            SetTextSwitch(_txtCrewFilter, _switchCrewFilter);
            SetTextSwitch(_txtDepFilter, _switchDepFilter);
            SetTextSwitch(_txtDestFilter, _switchDestFilter);
            SetTextSwitch(_txtFlightNoFilter, _switchFlightNoFilter);
            try
            {
                var stdIndex = int.Parse(await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.StdFilter1, currentUserName));
                SelectPickerItem(_stdStart, viewModel.StdPickerItems[stdIndex]);
            }
            catch
            {
                _stdStart.SelectedIndex = FilterSettings.DefaultValueStd;
            }

            try
            {
                _sortField.SelectedIndex = int.Parse(await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.SortField, currentUserName));
            }
            catch
            {
                _sortField.SelectedIndex = 2;
            }
            try
            {
                _sortType.SelectedIndex = int.Parse(await filterDbSettingsRepository.GetFilterSettingAsync(Enums.SettingType.SortType, currentUserName));
            }
            catch
            {
                _sortType.SelectedIndex = 0;
            }
        }

        static void SetTextSwitch(Entry entry, Switch switchControl)
        {
            var bNotEmpty = !string.IsNullOrEmpty(entry.Text.Trim());
            if (switchControl.IsToggled != bNotEmpty)
            {
                switchControl.IsToggled = bNotEmpty;
            }
        }

		private bool SelectPickerItem(Picker picker, string value)
		{
			for (var i = 0; i < picker.Items.Count; i++)
			{
				if (picker.Items[i] != value) continue;
				picker.SelectedIndex = i;
				return true;
			}
			return false;
		}

		private void BtnClearFilter_Clicked(object sender, EventArgs e)
		{
			_switchTailFilter.IsToggled = false;
			_switchCrewFilter.IsToggled = false;
			_switchDepFilter.IsToggled = false;
			_switchDestFilter.IsToggled = false;
			_switchFlightNoFilter.IsToggled = false;
			_txtCrewFilter.Text = string.Empty;
			_txtTailFilter.Text = string.Empty;
			_txtDepFilter.Text = string.Empty;
			_txtDestFilter.Text = string.Empty;
			_txtFlightNoFilter.Text = string.Empty;
			_stdStart.SelectedIndex = FilterSettings.DefaultValueStd;
			//_stdEnd.SelectedIndex = stdFilterCount;
			_sortField.SelectedIndex = 2;
			_sortType.SelectedIndex = 0;
			//FilterSettings._filterOn = "false";
			viewModel.FilterSettings.ClearFilter();
		}
	}
}