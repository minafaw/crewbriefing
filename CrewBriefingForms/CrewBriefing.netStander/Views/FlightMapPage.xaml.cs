﻿using System;
using System.Drawing;
using CB.Core.CustomControls.NavigationService;
using CB.Core.Dtos;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlightMapPage 
    {
        public FlightMapPage(FlightDto flightDetails)
        {
            InitializeComponent();
			CustomNavigationPage.SetTitleColor(this, Color.White);
			//Utils.SetBackNavBar(this, panelNav, FlightListItemDto.GetFlightIdToShow(flightDetails.FlightLogId));
			Title = FlightListItemDto.GetFlightIdToShow(flightDetails.FlightLogId);

			flightMap.FlightInfo = flightDetails;
            flightMap.parentPage = this;

		

			if (flightDetails.RoutePoints?.Count > 0)
	        {
				System.Diagnostics.Debug.WriteLine("Number of points " + flightDetails.RoutePoints?.Count);
		        double minLat = flightDetails.RoutePoints[0].Lat,
					minLon = flightDetails.RoutePoints[0].Lon,
					maxLat = minLat, maxLon = minLon;
		        foreach (var p in flightDetails.RoutePoints)
		        {
			        minLat = Math.Min(minLat, p.Lat);
			        minLon = Math.Min(minLon, p.Lon);
			        maxLat = Math.Max(maxLat, p.Lat);
			        maxLon = Math.Max(maxLon, p.Lon);
		        }

				var distance = Math.Max(
								Utils.DistanceTo(minLat, minLon, maxLat, minLon),
								Utils.DistanceTo(minLat, minLon, minLat, maxLon)
								       ) / 2.0;
				var zoomPos = new Position((minLat + maxLat) * 0.5, (minLon + maxLon) * 0.5);
				
				flightMap.MoveToRegion(MapSpan.FromCenterAndRadius(zoomPos , Distance.FromMiles( distance ))  );
			}
        }
        
    }

    public class CustomPin
    {
        public Pin Pin { get; set; }

        public string Id { get; set; }

        public string Url { get; set; }
    }
}
