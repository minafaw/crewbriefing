﻿using CB.Core.ViewModels;

namespace CrewBriefing.Standard.Views.FlightDetials
{
    public partial class DetialFlightCell 
    {

        public DetialFlightCell()
        {
            InitializeComponent();
            BindingContext = ViewModelLocator.FlightDetialListItem;
        }
    }
}
