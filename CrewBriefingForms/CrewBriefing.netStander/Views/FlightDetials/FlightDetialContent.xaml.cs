﻿using CB.Core.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views.FlightDetials
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FlightDetialContent 
    {
		FlightDetailsViewModel FlightDetailsViewModel = ViewModelLocator.FlightDetailsViewModel;
		public FlightDetialContent(int? flightId)
        {

			Init(flightId);
		}

		public FlightDetialContent()
		{
			Init(null);
		}

		void Init(int? flightId)
		{
			InitializeComponent();

			BindingContext = FlightDetailsViewModel;
			if (flightId != null)
				FlightDetailsViewModel.SetUserId(flightId.Value);
		}

        public void SetItem(int flightId)
        {
			FlightDetailsViewModel.SetUserId(flightId);
		}

		private void ExtListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			detialList.SelectedItem = null;
		}
	}
}