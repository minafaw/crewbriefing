﻿using CB.Core.Common.Base;
using CB.Core.CustomControls.NavigationService;
using CB.Core.Dtos;
using Xamarin.Forms;

namespace CrewBriefing.Standard.Views.FlightDetials
{
    public partial class FlightDetialsMain
    {
		public FlightDetialsMain()
		{
			InitializeComponent();
			
		}

		public FlightDetialsMain(FlightListItemDto item)
        {
            InitializeComponent();
	        Title = item.FlightlogId;

			CustomNavigationPage.SetTitlePosition(this, CustomNavigationPage.TitleAlignment.Center);
			CustomNavigationPage.SetTitleColor(this, Color.White);

			panelFlightDetails.SetItem(item.ID);
        }

		public void SetItem(int flightId)
		{
			panelFlightDetails.SetItem(flightId);
		}

	}
}