﻿using CB.Core.CustomControls.NavigationService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefing.Standard.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MasterDetialMainPage : MasterDetailPage
	{
		public MasterDetialMainPage ()
		{
			InitializeComponent ();
			NavigationPage.SetHasNavigationBar(this, false);
			IsPresentedChanged += MasterDetialMainPage_IsPresentedChanged;

			if(Device.Idiom == TargetIdiom.Tablet)
			{
				Detail = new CustomNavigationPage(new TabletMainPage());
			}
			else
			{
				Detail = new CustomNavigationPage(new FlightListPage());
			}
			
			MasterBehavior = MasterBehavior.Popover;
		}

		public void OpenFilerPage()
		{
			Master = new FilterMasterPage();
			IsPresented = true;
		}

		private void MasterDetialMainPage_IsPresentedChanged(object sender, EventArgs e)
		{
			if(IsPresented == false && Master.GetType() != typeof(MasterPage ))
			Master = new MasterPage();
		}
	}
}