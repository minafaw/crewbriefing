﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace CrewBriefing.UITests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class LoginUiTests
    {
        IApp app;
        Platform platform;

        public LoginUiTests(Platform platform)
        {
            this.platform = platform;
        }

        [SetUp]
        public void BeforeEachTest()
        {
            app = AppInitializer.StartApp(platform);
        }

        [Test]
        public void AppLaunches()
        {
            app.Screenshot("First screen."); 
        }

        [Test]
        public void WrongCredentialTest()
        {
            // to make test wait till screen loaded 
            Func<AppQuery, AppQuery> DemoFlightsButton = x => x.Marked("demoBtn").Text("Show me some demo flights"); 
            app.WaitForElement(DemoFlightsButton , "time out waiting view loaded");
             
            var UserNameEntry = app.Query(c => c.Marked("userNameId"))[0].Id;
            var PassEntry = app.Query(c => c.Marked("passwordId"))[0].Id;

            app.Tap(UserNameEntry);
            app.EnterText(UserNameEntry, "rtgffkk");
            app.Screenshot("fill username entry");
            app.Tap(PassEntry);
            app.Screenshot("press password entry");
            app.EnterText(PassEntry, "fggff");
            app.TouchAndHold(PassEntry);
            app.Screenshot("fill password entry");
            app.TouchAndHold(PassEntry);
            app.Screenshot("HighLighted Entry Input");

            // Dismiss Keyboard
            app.DismissKeyboard();
            app.Tap(x => x.Text("Log in"));
            app.Screenshot("Wrong Cendentail error screen");
        }

        [Test]
        public void WrongCredentialFromSlide()
        {
            Func<AppQuery, AppQuery> DemoFlightsButton = x => x.Text("Show me some demo flights");

            app.WaitForElement(DemoFlightsButton, "time out Demo flight ");
            app.Tap(DemoFlightsButton);
            app.Screenshot("Showing Demo Flight authenticating ");

            // this condition to make sure page loaded 
            Func<AppQuery, AppQuery> FlightsLabel = x => x.Text("Flights");
            app.WaitForElement(FlightsLabel, "time out flight list ");
            app.Screenshot("Showing Flight List ");

            var SideMenuButton = app.Query(c => c.Class("AppCompatImageButton"))[0].Rect;
           
            //app.WaitForElement(SideMenuButton);
            app.TapCoordinates(SideMenuButton.CenterX , SideMenuButton.CenterY);
            app.Screenshot("SideMenu is opening ");

            app.Tap(x => x.Text("Login details"));

            // to make sure view loaded
            Func<AppQuery, AppQuery> LoginButton = x => x.Text("Log in");
            app.WaitForElement(LoginButton);

            var UserNameEntry = app.Query(c => c.Marked("userNameId"))[0].Id;
            app.WaitForElement(UserNameEntry);
            app.Tap(UserNameEntry);

            app.Screenshot("old login crendetial");
            app.EnterText(UserNameEntry, "2");
            app.Tap(x => x.Text("Log in"));
            app.Screenshot("Wrong Credential inside slide menu");
            
        }

    }
}

