﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.iOS;
using Xamarin.UITest.Queries;


namespace CrewBriefing.UITests
{
    [TestFixture(Platform.Android)]
    [TestFixture(Platform.iOS)]
    public class AppOverallTest
    {
            IApp app;
            Platform platform;
            public static bool IsPhone;
            public AppOverallTest(Platform platform)
            {
                this.platform = platform;
            }

            [SetUp]
            public void BeforeEachTest()
            {
                app = AppInitializer.StartApp(platform);

            }
        [Test]
        public void WriteTest(){
            app.Repl();
        }

        [Test]
        public void AppTestOverAll(){
            Func<AppQuery, AppQuery> DemoFlightsButton = x => x.Text("Show me some demo flights");
            app.WaitForElement(DemoFlightsButton, "time out Demo flight ");
            app.Tap(DemoFlightsButton);
            app.Screenshot("Tapped on view Demo Flights");


            // this condition to make sure page loaded 
            Func<AppQuery, AppQuery> FlightsLabel = x => x.Text("Flights");
            app.WaitForElement(FlightsLabel, "time out flight list ");
            app.Screenshot("Showing Flight List ");

            var FilterButton = app.Query("MenuItem1")[0].Rect;

            //app.WaitForElement(FilterButton, "time out waiting filter button ");
            app.TapCoordinates(FilterButton.CenterX , FilterButton.CenterY);
            app.Screenshot("click Filter Button ");

            app.ScrollDown();
            app.Screenshot("Swiped up Filter ");
            app.ScrollUp();
            app.Screenshot("Swipped down Filter");

            Func<AppQuery, AppQuery> DoneButton = x => x.Text("Done");
            app.WaitForElement(DoneButton , "TimeOut Done");
            app.Tap(DoneButton);
            app.Screenshot("Tapped on view Done");

            var SideMenuButton = app.Query(c => c.Class("AppCompatImageButton"))[0].Rect;
            //app.WaitForElement(SideMenuButton, "time out Demo flight ");
            app.TapCoordinates(SideMenuButton.CenterX , SideMenuButton.CenterY);
            app.Screenshot("Side Menu Screen");

            var loginItem = app.Query(c => c.Text("Login details"))[0].Id;
            app.WaitForElement(loginItem, "time out waiting login");

            app.Tap(c => c.Text("Login details"));
            app.Screenshot("Tapped on view with Login Screen");
            app.Tap(x => x.Text("Cancel"));
            app.Screenshot("Tapped on view after cancel login");

            app.TapCoordinates(SideMenuButton.CenterX, SideMenuButton.CenterY);
            app.Tap(x => x.Text("About"));
            app.Screenshot("About ScreenShot");
            app.Back();


            app.TapCoordinates(SideMenuButton.CenterX, SideMenuButton.CenterY);
            app.Tap(x => x.Text("Help"));
            app.Screenshot("Help Screen");
            app.Back();


            app.TapCoordinates(SideMenuButton.CenterX, SideMenuButton.CenterY);
            app.Tap(x => x.Text("Share App"));
            app.Screenshot("Share App Screen");
        }



        [Test]
        public void DetialTest()
        {
            Func<AppQuery, AppQuery> DemoFlightsButton = x => x.Text("Show me some demo flights");
            app.WaitForElement(DemoFlightsButton, "time out Demo flight ");
            app.Tap(DemoFlightsButton);
            app.Screenshot("Tapped on view Demo Flights");

            // this condition to make sure page loaded 
            Func<AppQuery, AppQuery> FlightsLabel = x => x.Text("Flights");
            app.WaitForElement(FlightsLabel, "time out flight list ");
            app.Screenshot("Showing Flight List ");

            Thread.Sleep(7000);
            var firstItem = app.Query(c => c.Class("ViewCellRenderer_ViewCellContainer")).First();
            app.TapCoordinates(firstItem.Rect.CenterX, firstItem.Rect.CenterY);
            app.Screenshot("Press first Item ");

            if (DeviceIsTablet()){
              Func<AppQuery, AppQuery> hideList = x => x.Marked("hideListId");
              app.Tap(hideList);

            }


            var  MessagesItemButton = app.Query(c => c.Class("ImageRenderer"))[3].Id;
            app.Screenshot("Detial page");
            app.WaitForElement(MessagesItemButton);
            // page loaded 

            app.Tap(x => x.Text("Route Map"));
            Thread.Sleep(5000);
            app.Screenshot("Map Loaded ");
            if(!DeviceIsTablet()){
                app.Back();   
            }


            app.Tap(x => x.Text("Flight Summary"));
            Thread.Sleep(1000);
            app.Screenshot("Summary Loaded ");
            if (!DeviceIsTablet())
            {
                app.Back();
            }



            app.Tap(MessagesItemButton);
            app.Screenshot("download Pdf" );
            Thread.Sleep(2000);
            app.Tap(MessagesItemButton);
            app.Screenshot("Pdf opened");

           
            var shareId = app.Query(c => c.Class("ImageRenderer"))[1].Id;
            app.Tap(shareId);
            app.Screenshot("open in external reader Pdf");
          
          
        }

        public bool DeviceIsTablet()
        {

            AppResult[] queryResult = app.Query(x => x.Marked("hideListId"));

            if(queryResult.Any()){
                return true;
            }
            return false;

            //var screen = app.Query(x => x.Id("content"));
            //var height = screen.FirstOrDefault().Rect.Height;
            //var width = screen.FirstOrDefault().Rect.Width;
            //if (width < 1600 || height > 1850)
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
        }

     

    }
}
