﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Xamarin.UITest;
using Xamarin.UITest.Android;
using Xamarin.UITest.iOS;
using Xamarin.UITest.Queries;

namespace CrewBriefing.UITests
{
    public class AppInitializer
    {
        
        public static IApp StartApp(Platform platform)
        {

            if (platform == Platform.Android)
            {
                var currentFile = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                var fileInfo = new FileInfo(currentFile);
                if (fileInfo.Directory != null)
                {
                    var dir = fileInfo.Directory.Parent.Parent.Parent.Parent.FullName;
                    var pathToApk = Path.Combine(dir, "CrewBriefingForms//CrewBriefingForms.Droid", "bin", "Debug", "crewbriefing.droid-Signed.apk");
                    return ConfigureApp.Android.ApkFile(pathToApk).StartApp();
                }


                return ConfigureApp
                    .Android
                    .StartApp();
            }
            var iosPath = "/Users/mina/Desktop/ipabuild/Payload/CrewBriefingFormsiOS.app";

            if(iosPath != null){
                return ConfigureApp
                    .iOS.InstalledApp("com.airsupport.crewbriefing").DeviceIp("192.168.137.124").StartApp();  
            }
           
                
                return ConfigureApp
                .iOS
               .StartApp();
               
           
        }


    }
}

