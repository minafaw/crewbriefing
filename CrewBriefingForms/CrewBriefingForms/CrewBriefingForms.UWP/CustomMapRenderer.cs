﻿using CrewBriefingForms;
using CustomRenderer.UWP;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Devices.Geolocation;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Media;
using Xamarin.Forms.Maps;

using CrewBriefingForms.Models;
using Xamarin.Forms.Platform.UWP;
using Xamarin.Forms.Maps.UWP;
using CrewBriefingForms.Controls;
using CrewBriefingForms.UWP;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace CustomRenderer.UWP
{
    public class CustomMapRenderer : MapRenderer
    {
        private MapControl nativeMap = null;
        private Flight flightInfo = null;
        private Xamarin.Forms.ContentPage page = null;
        //XamarinMapOverlay mapOverlay;
        private SolidColorBrush brushRoutePoint = new SolidColorBrush(Windows.UI.Colors.Blue);
        private SolidColorBrush brushRoutePointSelected = new SolidColorBrush(Windows.UI.Colors.Red);
        private bool mapOverlayShown = false;
        XamarinMapOverlay mapOverlay = null;
        private int markerScale = 1;

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                nativeMap.MapElementClick -= OnMapElementClick;
                nativeMap.Children.Clear();
                mapOverlay = null;
                nativeMap = null;
                mapOverlayShown = false;
            }

            if (e.NewElement != null)
            {
                nativeMap = Control as MapControl;

                var formsMap = (CustomMap)e.NewElement;
                nativeMap = Control as MapControl;
                flightInfo = formsMap.FlightInfo;
                page = formsMap.parentPage;
                nativeMap.Children.Clear();
                mapOverlayShown = false;
                nativeMap.MapElementClick += OnMapElementClick;
                TextMeasuring tm = new TextMeasuring();
                try
                {
                    if (tm.Xdpi > 0 && tm.Ydpi > 0 && !double.IsInfinity(tm.Xdpi) && !double.IsInfinity(tm.Ydpi))
                    {
                        double dpi = tm.Xdpi > tm.Ydpi ? tm.Xdpi : tm.Ydpi;
                        int i = (int)Math.Round(dpi / 96);
                        if (i >= 1 && i <= 4)
                        {
                            markerScale = i;
                        }
                        if (i < 1)
                        {
                            markerScale = 1;
                        }
                        if (i > 4)
                        {
                            markerScale = 4;
                        }
                    }
                    else
                    {
                        markerScale = 1;
                    }
                }
                catch
                {
                    markerScale = 1;
                }


                MyDrawMap();
            }
        }

        public static Windows.UI.Color GetColorFromHex(string hex)
        {
            hex = hex.Replace("#", string.Empty);
            byte a = (byte)(Convert.ToUInt32(hex.Substring(0, 2), 16));
            byte r = (byte)(Convert.ToUInt32(hex.Substring(2, 2), 16));
            byte g = (byte)(Convert.ToUInt32(hex.Substring(4, 2), 16));
            byte b = (byte)(Convert.ToUInt32(hex.Substring(6, 2), 16));
            return Windows.UI.Color.FromArgb(a, r, g, b);
        }

        private void MyDrawMap()
        {

            if (nativeMap == null)
            {
                return;
            }
            if (flightInfo == null)
            {
                return;
            }

            else if (flightInfo.RoutePoints == null)
            {
                return;
            }
            else if (flightInfo.RoutePoints.Length == 0)
            {
                return;
            }

            if (flightInfo != null)
            {
                // ??? Geodesic Line ???

                MapPolyline polyline = new MapPolyline();
                polyline.StrokeColor = GetColorFromHex("#FFE00000");
                polyline.StrokeThickness = 2;
                List<BasicGeoposition> geoPositions = new List<BasicGeoposition>();
                /*
                foreach (RoutePoint p in new[] { flightInfo.RoutePoints[0], flightInfo.RoutePoints[flightInfo.RoutePoints.Length - 1] } )
                {
                    geoPositions.Add(new BasicGeoposition() { Latitude = p.LAT, Longitude = p.LON });
                }
                */
                int n = 100;
                RoutePoint[] pointDepDst = new[] { flightInfo.RoutePoints[0], flightInfo.RoutePoints[flightInfo.RoutePoints.Length - 1] };
                for (int i = 0; i < pointDepDst.Length - 1; i++)
                {
                    // Convert coordinates from degrees to Radians
                    double lat1 = pointDepDst[i].LAT * (Math.PI / 180.0);
                    double lon1 = pointDepDst[i].LON * (Math.PI / 180.0);
                    double lat2 = pointDepDst[i + 1].LAT * (Math.PI / 180.0);
                    double lon2 = pointDepDst[i + 1].LON * (Math.PI / 180.0);
                    // Calculate the total extent of the route
                    double d = 2 * Math.Asin(Math.Sqrt(Math.Pow((Math.Sin((lat1 - lat2) / 2)), 2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Pow((Math.Sin((lon1 - lon2) / 2)), 2)));
                    // Calculate  positions at fixed intervals along the route
                    for (double k = 0; k <= n; k++)
                    {
                        double f = (k / (double)n);
                        double A = Math.Sin((1 - f) * d) / Math.Sin(d);
                        double B = Math.Sin(f * d) / Math.Sin(d);
                        // Obtain 3D Cartesian coordinates of each point
                        double x = A * Math.Cos(lat1) * Math.Cos(lon1) + B * Math.Cos(lat2) * Math.Cos(lon2);
                        double y = A * Math.Cos(lat1) * Math.Sin(lon1) + B * Math.Cos(lat2) * Math.Sin(lon2);
                        double z = A * Math.Sin(lat1) + B * Math.Sin(lat2);
                        // Convert these to latitude/longitude
                        double lat = Math.Atan2(z, Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)));
                        double lon = Math.Atan2(y, x);
                        // Create a Location (remember to convert back to degrees)
                        /*
                        var p = new Microsoft.Maps.Location(lat / (PI / 180), lon / (PI / 180));
                        // Add this to the array
                        locs.push(p);
                        */
                        geoPositions.Add(new BasicGeoposition()
                        {
                            Latitude = lat / (Math.PI / 180.0),
                            Longitude = lon / (Math.PI / 180.0)
                        }
                        );
                    }
                }
                // ??? Geodesic Line ???
                polyline.Path = new Geopath(geoPositions, AltitudeReferenceSystem.Surface);
                nativeMap.MapElements.Add(polyline);
                //?
                //http://nugetmusthaves.com/Package/Geodesy


                polyline = new MapPolyline();
                polyline.StrokeColor = GetColorFromHex("#FF0000E0");
                polyline.StrokeThickness = 3;
                List<BasicGeoposition> positions = new List<BasicGeoposition>();


                for (int i = 0; i < flightInfo.RoutePoints.Length; i++)
                {
                    RoutePoint p = flightInfo.RoutePoints[i];
                    positions.Add(new BasicGeoposition() { Latitude = p.LAT, Longitude = p.LON });

                    Geopoint gp = new Geopoint(new BasicGeoposition() { Latitude = p.LAT, Longitude = p.LON });

                    /*
                    MapIcon mapIcon = new MapIcon();
                    mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/pin.png"));  //ICAO_symbol_121_on_request_blue
                    mapIcon.Location = new Geopoint(new BasicGeoposition() { Latitude = p.LAT, Longitude = p.LON });
                    mapIcon.NormalizedAnchorPoint = new Windows.Foundation.Point(0.5, 0.5);
                    //mapIcon.ZIndex = int.MaxValue;
                    mapIcon.Title = p.IDENT;
                    mapIcon.Visible = true;
                    nativeMap.MapElements.Add(mapIcon);
                    */

                    var mapIcon = new MapIcon();
                    if (i == 0 || i == flightInfo.RoutePoints.Length - 1)  //DEP and DEST
                    {
                        mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/" + (i == 0 ? "ICAO_symbol_84_blue_" : "ICAO_symbol_84_orange_") + markerScale.ToString() + ".png"));
                        string _title = i == 0 ? flightInfo.Arports4ShowDEP : flightInfo.Arports4ShowDST;
                        if (!string.IsNullOrEmpty(_title))
                        {
                            TitleDepDest tdd = new TitleDepDest();
                            tdd.Title = _title;
                            MapControl.SetLocation(tdd, gp);
                            MapControl.SetNormalizedAnchorPoint(tdd, new Windows.Foundation.Point(0.5, 0));
                            nativeMap.Children.Add(tdd);
                        }
                    }
                    else {
                        mapIcon.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/ICAO_symbol_121_on_request_blue_"+markerScale.ToString()+".png"));
                    }
                    mapIcon.CollisionBehaviorDesired = MapElementCollisionBehavior.RemainVisible;
                    mapIcon.Location = new Geopoint(new BasicGeoposition() { Latitude = p.LAT, Longitude = p.LON });
                    mapIcon.NormalizedAnchorPoint = new Windows.Foundation.Point(0.5, 0.5);
                    mapIcon.Title = p.IDENT;

                    nativeMap.MapElements.Add(mapIcon);
                }
                polyline.Path = new Geopath(positions);
                nativeMap.MapElements.Add(polyline);
            }
        }

        private void OnMapElementClick(MapControl sender, MapElementClickEventArgs args)
        {

            var mapIcon = args.MapElements.FirstOrDefault(x => x is MapIcon) as MapIcon;

            if (mapIcon != null)
            {
                if (mapOverlay == null)
                {
                    mapOverlay = new XamarinMapOverlay(string.Empty);
                }
                if (flightInfo != null)
                {
                    RoutePoint routePoint = null;
                    foreach (RoutePoint p in flightInfo.RoutePoints)
                    {
                        if (p.IDENT == mapIcon.Title)
                        {
                            routePoint = p;
                            break;
                        }
                    }
                    if (routePoint != null)
                    {
                        if (mapOverlayShown && mapOverlay.Title == routePoint.IDENT)
                        {
                            nativeMap.Children.Remove(mapOverlay);
                            mapOverlayShown = false;
                            return;
                        }
                        mapOverlay.Title = routePoint.IDENT;
                        var snPosition = new BasicGeoposition { Latitude = routePoint.LAT, Longitude = routePoint.LON };
                        var snPoint = new Geopoint(snPosition);
                        if (!mapOverlayShown)
                        {
                            nativeMap.Children.Add(mapOverlay);
                        }
                        MapControl.SetLocation(mapOverlay, snPoint);
                        MapControl.SetNormalizedAnchorPoint(mapOverlay, new Windows.Foundation.Point(0.5, 1));
                        mapOverlayShown = true;
                        return;
                    }
                }
                if (mapOverlayShown)
                {
                    nativeMap.Children.Remove(mapOverlay);
                }
                mapOverlayShown = false;
            }
            else
            {
                if (mapOverlayShown)
                {
                    nativeMap.Children.Remove(mapOverlay);
                }
                mapOverlayShown = false;
            }
        }
    }
}
