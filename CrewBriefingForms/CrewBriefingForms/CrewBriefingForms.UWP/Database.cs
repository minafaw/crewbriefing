﻿using System.IO;
using Xamarin.Forms;
using Windows.Storage;

using CrewBriefingForms.Database;
using CrewBriefingForms.UWP;
using System.Threading.Tasks;
using SQLite;


[assembly: Dependency(typeof(CrewBriefingDB))]
namespace CrewBriefingForms.UWP
{
    public class CrewBriefingDB : ISQLite
    {
        const string sqliteFilename = "crew_briefing_net.db3";
        string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
        public string GetConnectionPath()
        {
            return path;
        }

        public SQLiteAsyncConnection GetConnection()
        {
            
            var conn = new SQLiteAsyncConnection( path);

            return conn;
        }

        public bool IsDbExists()
        {
            if (File.Exists(path)) return true;
            return false;
        }
    }
}
