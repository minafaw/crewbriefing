﻿using Microsoft.HockeyApp;
using Plugin.Share;
using Windows.UI.ViewManagement;

namespace CrewBriefingForms.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            Xamarin.FormsMaps.Init(CrewBriefingForms.App.BinqMapApiKey);
            LoadApplication(new CrewBriefingForms.App());

            //ShareImplementation.

            GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Startup", "UWP Main", "no label", 0);
/*
#if !DEBUG
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.FullScreen;
#else
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.FullScreen;
#endif
*/
            ApplicationView.PreferredLaunchWindowingMode = ApplicationViewWindowingMode.Auto;

        }
    }
}
