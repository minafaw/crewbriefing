﻿using Xamarin.Forms;
using CrewBriefingForms.UWP;
using Windows.Graphics.Display;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using System;

[assembly: Dependency(typeof(TextMeasuring))]
namespace CrewBriefingForms.UWP
{
    public class TextMeasuring : ITextMeasuring
    {
        /// <summary>
        /// Height for Label
        /// </summary>
        /// <param name="text"></param>
        /// <param name="fontSize"></param>
        /// <returns></returns>
        public double calculateHeight(string text, NamedSize fontSize)
        {
            return calculateSize(text, fontSize).Height;
        }

        /// <summary>
        /// Width for Label
        /// </summary>
        /// <param name="text"></param>
        /// <param name="fontSize"></param>
        /// <returns></returns>
        public double calculateWidth(string text, NamedSize fontSize)
        {
            return calculateSize(text, fontSize).Width;
        }
        private Windows.Foundation.Size calculateSize(string text, NamedSize fontSize)
        {
            TextBlock textBlock = new TextBlock() { FontSize = Device.GetNamedSize(fontSize, typeof(Label)) };
            textBlock.Text = text;
            var parentBorder = new Border { Child = textBlock, HorizontalAlignment = HorizontalAlignment.Left };
            textBlock.MaxHeight = double.PositiveInfinity;
            textBlock.MaxWidth = double.PositiveInfinity;
            parentBorder.Measure(new Windows.Foundation.Size(textBlock.MaxWidth, textBlock.MaxHeight));
            parentBorder.Child = null;
            return parentBorder.DesiredSize;
        }
        private static double sizeDetected = 0;
        public double ScreenSizeInInches()
        {
            //Windows.Graphics.Display.DisplayInformation.GetForCurrentView().
            //DisplayInformation.DiagonalSizeInInches
            //https://msdn.microsoft.com/library/windows/apps/windows.graphics.display.displayinformation.diagonalsizeininches.aspx?cs-save-lang=1&cs-lang=csharp#code-snippet-1

            if (sizeDetected != 0)
            {
                return sizeDetected;
            }

            var rawpixelperview = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;

            double height = Window.Current.Bounds.Height * rawpixelperview;
            double width = Window.Current.Bounds.Width * rawpixelperview;


            //            double screenWidth = width / Xdpi;
            //            double screenHeight = height / Ydpi;

            double screenWidthInch = width / (Info.RawDpiX == 0 ? Info.LogicalDpi : Info.RawDpiX);
            double screenHeightInch = height / (Info.RawDpiY == 0 ? Info.LogicalDpi : Info.RawDpiY);


            double size = Math.Sqrt(Math.Pow(screenWidthInch, 2) +
                                    Math.Pow(screenHeightInch, 2));
            sizeDetected = size;
            return size;
        }

        public Size ScreenSizeInFormPixels()
        {
            var rawpixelperview = Windows.Graphics.Display.DisplayInformation.GetForCurrentView().RawPixelsPerViewPixel;
            //A value representing the number of raw (physical) pixels for each view (layout) pixel.

            double height = Window.Current.Bounds.Height; //* rawpixelperview;
            double width = Window.Current.Bounds.Width; // * rawpixelperview;
            return new Size(width, height);
        }

        public double Xdpi { get { return Info.RawDpiY; } }

        public double Ydpi { get { return Info.RawDpiY; } }

        public double Scale
        {
            get { return ((int)Info.RawPixelsPerViewPixel) / 100; }
        }
        private static DisplayInformation Info
        {
            get { return DisplayInformation.GetForCurrentView(); }
        }
    }
}
