﻿using System;
using System.IO;
using System.Linq;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Threading.Tasks;

using CrewBriefingForms.Models;
using Xamarin.Forms;
using CrewBriefingForms.UWP;
using System.Collections.Generic;

[assembly: Dependency(typeof(SaveAndLoad_WinApp))]
namespace CrewBriefingForms.UWP
{
    // https://msdn.microsoft.com/en-us/library/windows/apps/xaml/hh758325.aspx
    public class SaveAndLoad_WinApp : ISaveAndLoad
    {
        public async Task SaveFile(string fileName, byte[] data)
        {
            StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            StorageFile sampleFile = null;
            try
            {
                sampleFile = await localFolder.CreateFileAsync(fileName, CreationCollisionOption.FailIfExists);
            }
            catch
            {
                try
                {
                    RemoveFile(fileName);
                }
                catch {
                }
                sampleFile = null;
            }
            if (sampleFile == null)
            {
                sampleFile = await localFolder.CreateFileAsync(fileName, CreationCollisionOption.FailIfExists);
            }
            
            await FileIO.WriteBytesAsync(sampleFile, data);
        }
        public async Task<byte[]> LoadFile(string fileName)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile file = await storageFolder.GetFileAsync(fileName);
            
            Stream stream = (await file.OpenReadAsync()).AsStreamForRead();
            int length = (int)stream.Length;
            using (BinaryReader reader = new BinaryReader(stream))
            {
                return reader.ReadBytes(length);
            }
        }
        public async void RemoveFile(string fileName)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile file = await storageFolder.GetFileAsync(fileName);
            await file.DeleteAsync();
        }
        public async Task OpenFileWithDefaultOSViewer(string fileName)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile fileToLaunch = await storageFolder.GetFileAsync(fileName);
            await Windows.System.Launcher.LaunchFileAsync(fileToLaunch, new Windows.System.LauncherOptions { DisplayApplicationPicker = false });
        }
        public void RemoveAllFilesInLocalStorage()
        {
            RemoveAllFilesInLocalStorage(string.Empty);
        }
        public async Task<FileTextAndPath> LoadTextFile(string fileName)
        {
            FileTextAndPath result = new FileTextAndPath();
            StorageFolder storageFolder = Windows.ApplicationModel.Package.Current.InstalledLocation;
                //ApplicationData.Current.LocalFolder;
            StorageFile file = await storageFolder.GetFileAsync(fileName);
            result.FileName = file.Path;

            Stream stream = (await file.OpenReadAsync()).AsStreamForRead();
            int length = (int)stream.Length;
            using (var reader = new StreamReader(stream))
            {
                result.TextContent = reader.ReadToEnd();
            }
            return result;
        }

        public async void RemoveAllFilesInLocalStorage(string fileNameMaskStart)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            IReadOnlyList<StorageFile> files = await storageFolder.GetFilesAsync();
            List<string> path = new List<string>();
            IEnumerator<StorageFile> filesReader = files.GetEnumerator();
            while (filesReader.MoveNext())
            {
                if (!string.IsNullOrEmpty(fileNameMaskStart))
                {
                    if (!filesReader.Current.Name.ToLower().StartsWith(fileNameMaskStart))
                    {
                        continue;
                    }
                }
                if (filesReader.Current.Name.ToLower().EndsWith(".db3"))  // do not remove database
                {
                    continue;
                }
                try
                {
                    await filesReader.Current.DeleteAsync();
                }
                catch { }
            }
        }

        public Task OpenFileWithDefaultOSViewer(string fileName, string title)
        {
            return OpenFileWithDefaultOSViewer(fileName);
        }

        public string AppPath(string pathFile)
        {
            throw new NotImplementedException();
        }
    }
}
