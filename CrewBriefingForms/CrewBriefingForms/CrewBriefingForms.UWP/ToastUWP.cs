﻿using CrewBriefingForms.Interfaces;
using CrewBriefingForms.UWP;
using System;
using System.Threading.Tasks;
using Windows.Data.Xml.Dom;
using Windows.UI.Notifications;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastWP))]
namespace CrewBriefingForms.UWP
{
    public class ToastWP : IToast
    {
        public async Task<int> ShowToast(ToastType toastType, string message)
        {
            if (toastType == ToastType.Error || toastType == ToastType.Warning)
            {
                await ((CrewBriefingForms.App)Application.Current).MainPage.DisplayAlert(
                    toastType.ToString(), message, "OK");
                return 1;
            }
                
               

            ToastTemplateType toastTemplType = ToastTemplateType.ToastText01;

            switch (toastType)
            {
                case ToastType.Info:
                    toastTemplType = ToastTemplateType.ToastText01;
                    break;
                case ToastType.Error:
                    toastTemplType = ToastTemplateType.ToastText02;
                    break;
                case ToastType.Warning:
                    toastTemplType = ToastTemplateType.ToastText02;
                    break;
            }
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplType);
            XmlNodeList toastTextElement = toastXml.GetElementsByTagName("text");
            if (toastType != ToastType.Info)
            {
                toastTextElement[0].AppendChild(toastXml.CreateTextNode(toastType.ToString()));
                toastTextElement[1].AppendChild(toastXml.CreateTextNode(message));
            }
            else
            {
                toastTextElement[0].AppendChild(toastXml.CreateTextNode(message));
                /*
                XmlNodeList toastImageAttributes = toastXml.GetElementsByTagName("image");
                ((XmlElement)toastImageAttributes[0]).SetAttribute("src", "Assets/m1enu.png"); 
                ((XmlElement)toastImageAttributes[0]).SetAttribute("alt", "test");
                */
            }

            IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
            ((XmlElement)toastNode).SetAttribute("duration", "short");

            ToastNotification toast = new ToastNotification(toastXml);  //
            ToastNotificationManager.CreateToastNotifier().Show(toast);               /*
            if (!CommonStatic.ToastsInitialized) {
                ToastNotification.Init();
                CommonStatic.ToastsInitialized = false;
            }
            
            var notificator = DependencyService.Get<IToastNotificator>();
            var options = new NotificationOptions()
            {
                Title = toastType.ToString(),
                Description = message
            };
            notificator.Notify(options);
            */
            //bool tapped = await notificator.Notify(ToastNotificationType.Info, "Title", "Description", TimeSpan.FromSeconds(2));
            //notificator.Notify(ToastNotificationType.Info, "Title", "Description", TimeSpan.FromSeconds(2));
            return 0;
        }

        public Task<int> ShowToast(ToastType type, string message, uint timeMSec)
        {
            return ShowToast(type, message);
        }
    }
}
