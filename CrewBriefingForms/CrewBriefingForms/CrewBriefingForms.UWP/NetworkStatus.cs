﻿using CrewBriefingForms.Interfaces;
using CrewBriefingForms.UWP;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Networking;
using Windows.Networking.Connectivity;
using Windows.Networking.Sockets;
using Xamarin.Forms;

[assembly: Dependency(typeof(NetworkStatus))]
namespace CrewBriefingForms.UWP
{
    public class NetworkStatus : INetworkStatus
    {
        private static int checkTimeoutMsec = 5000; 
        public bool IsInternet()
        {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            bool isConnected;
            if (profile == null)
                isConnected = false;
            else
                isConnected = profile.GetNetworkConnectivityLevel() != NetworkConnectivityLevel.None;

            return isConnected;
        }
        /// <summary>
        /// Checks if remote is reachable. RT apps cannot do loopback so this will alway return false.
        /// You can use it to check remote calls though.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="msTimeout"></param>
        /// <returns></returns>
        public bool IsReachable1(string host)
        {
            if (string.IsNullOrEmpty(host))
                throw new ArgumentNullException("host");

            /*
            if (!IsConnected)
                return false;
                */
            try
            {
                var serverHost = new HostName(host);
                using (var client = new StreamSocket())
                {

                    var task = client.ConnectAsync(serverHost, "http").AsTask();
                    if (task.Wait(checkTimeoutMsec))
                    {
                        client.Dispose();
                        return true;
                    }
                    else {
                        return false;
                    }
                }


            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Tests if a remote host name is reachable 
        /// </summary>
        /// <param name="host">Host name can be a remote IP or URL of website</param>
        /// <param name="port">Port to attempt to check is reachable.</param>
        /// <param name="msTimeout">Timeout in milliseconds.</param>
        /// <returns></returns>
        public async Task<bool> IsRemoteReachable1(string host, int port = 80, int msTimeout = 5000)
        {
            if (string.IsNullOrEmpty(host))
                throw new ArgumentNullException("host");

            host = host.Replace("http://www.", string.Empty).
              Replace("http://", string.Empty).
              Replace("https://www.", string.Empty).
              Replace("https://", string.Empty).
              TrimEnd('/');

            try
            {
                using (var tcpClient = new StreamSocket())
                {
                    
                    await tcpClient.ConnectAsync(
                        new Windows.Networking.HostName(host),
                        port.ToString(),
                        SocketProtectionLevel.PlainSocket);

                    var localIp = tcpClient.Information.LocalAddress.DisplayName;
                    var remoteIp = tcpClient.Information.RemoteAddress.DisplayName;

                    tcpClient.Dispose();

                    return true;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                //Debug.WriteLine("Unable to reach: " + host + " Error: " + ex);
                return false;
            }
        }
        public async Task<bool> IsReachable(string host)
        {
            var task = Task.Factory.StartNew<bool>(
                () =>
                {
                    if (NetworkInformation.GetInternetConnectionProfile()?.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.None)
                    {
                        return false;
                    }

                    try
                    {
                        var endPointPairListTask = DatagramSocket.GetEndpointPairsAsync(new HostName(host), "0");
                        
                        var endPointPairList = endPointPairListTask.GetResults();

                        var endPointPair = endPointPairList.First();

                        return true;
                    }
                    catch (Exception)
                    {
                    }

                    return false;
                });

            return await task;
        }
    }
}
