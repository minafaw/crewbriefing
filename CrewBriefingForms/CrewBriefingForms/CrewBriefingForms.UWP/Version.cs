﻿using CrewBriefingForms.UWP;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Security.ExchangeActiveSyncProvisioning;
using Windows.ApplicationModel;
using Windows.Devices.Enumeration.Pnp;
using Windows.System;
using Windows.System.Profile;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppVersion))]
namespace CrewBriefingForms.UWP
{
    public class AppVersion : IAppVersion
    {
        public string GetAppVersion()
        {
            return Package.Current.Id.Version.Major.ToString() + "." +
                Package.Current.Id.Version.Minor.ToString() + "." +
                Package.Current.Id.Version.Build.ToString() + "." +
            Package.Current.Id.Version.Revision.ToString();
        }

        public string GetAppVersionBuild()
        {
            return Package.Current.Id.Version.Major + "." +
                   Package.Current.Id.Version.Minor + "." +
                   Package.Current.Id.Version.Build + "." +
                   Package.Current.Id.Version.Revision;
        }

        public string GetDeviceName()
        {
            EasClientDeviceInformation deviceInfo = new EasClientDeviceInformation();
            return deviceInfo.SystemProductName;
        }

        public string GetOS()
        {
            string deviceFamilyVersion = AnalyticsInfo.VersionInfo.DeviceFamilyVersion;
            ulong version = ulong.Parse(deviceFamilyVersion);
            ulong major = (version & 0xFFFF000000000000L) >> 48;
            ulong minor = (version & 0x0000FFFF00000000L) >> 32;
            ulong build = (version & 0x00000000FFFF0000L) >> 16;
            ulong revision = (version & 0x000000000000FFFFL);
            return $"{major}.{minor}.{build}.{revision}";

        }

        public int GetOSMajorVersion()
        {
            throw new NotImplementedException();
        }

        public string MailEOL()
        {
            return "%0D%0A"; //Environment.NewLine;
        }
    }
}
