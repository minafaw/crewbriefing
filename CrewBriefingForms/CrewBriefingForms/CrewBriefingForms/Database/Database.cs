﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

using CrewBriefingForms.Models;
using CrewBriefingForms.ModelsAirport;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;
using System.Globalization;
using CrewBriefingForms.Interfaces;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CrewBriefingForms.Database;
using SQLite;
using SQLiteNetExtensionsAsync.Extensions;

namespace CrewBriefingForms.Database
{
    // Note: using of FK/delete cascade could be be more easy. But it is unclear/unstable on platforms, and we have simple DB - so can delete all manually. But every record has NotNull datetime to condider it expired. 24h usually.
    public class DB_PCL
    {
        public static readonly string DateTimeFormat = "yyyyMMddHHmmss";
        public static readonly string DateTimeSFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss";

        public static readonly string DatavaseVersion = "1.4"; // auto-clear DB on first launch, if DB has new version

        public static readonly CultureInfo CultureProvider = CultureInfo.InvariantCulture;

        private static SQLiteAsyncConnection _db;
        public static SQLiteAsyncConnection DataBase => _db ?? (_db = GetConnection());

        private static readonly SemaphoreSlim SemaphoreSlim = new SemaphoreSlim(initialCount: 1);

        public static string[] FlightTables = new string[] { "Pdf", "Airport", "Flight", "RoutePoint", "ATC", "Crew", "CrewBriefingSettings","FlightList" , "CrewBriefingDBVer" };

        private static readonly Type[] TableTypes = {
            typeof (DBPdf),
            typeof(Airport),
            typeof(Flight),
            typeof(RoutePoint),
            typeof(ATC),
            typeof(Crew),
            typeof(DBSettings),
            typeof(FilterDBSettings),
            typeof(FlightListItem),
            typeof(DBVer)
        };



		public static async Task ReCreateDatabase()
        {
            await DropDatabase();
            await CreateDatabase();
        }

        public static async Task CreateDatabase()
        {
            // Create all database tables 
            await DataBase.CreateTablesAsync(CreateFlags.None , TableTypes);
            UpdateDbVer();
        }

        /// <summary>
        /// Remove all data DB, when Login button presseed or Loguot. See DeleteAllDataFromDataBase in old Android code.
        /// </summary>
        public static async Task DropDatabase()
        {
            var filteredTables = FlightTables.Where(x => !x.Equals("FilterSettings") ).ToList();
            foreach(var tableName in filteredTables){
                await  DataBase.ExecuteAsync("DROP TABLE IF EXISTS " + tableName);  
            }
            var saveLoad = DependencyService.Get<ISaveAndLoad>();
            saveLoad.RemoveAllFilesInLocalStorage(string.Empty);

        }
        public static async Task UpdateFlights(List<FlightListItem> flights)
        {
            if(await IsFlightTabelEmpty())
            {
                try{
                        await DataBase.InsertAllWithChildrenAsync(flights);

                }catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                    await InsertFlightItemByItem(flights);
                }
            }else{
                await InsertFlightItemByItem(flights);
            }
            // store time in UTC  
           await InsertOrUpdateSetting(new DBSettings { Id = SettingType.FlightListRetrieved, Value = DateTime.UtcNow.ToString("s") });
           
        }

        private static async Task InsertFlightItemByItem(IEnumerable<FlightListItem> flights)
        {
            try
            {
                    //insert crew - need for filters
              await DataBase.InsertOrReplaceAllWithChildrenAsync(flights);   
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }

        }

        public static async Task InsertCrewInDb(List<FlightListItem> flights)
        {
            var filteredList = flights.Where(c => c.Crew?.Count > 0).ToList();

            foreach (var f in filteredList)
            {
                foreach (var crew in f.Crew)
                {
                    try
                    {
                        await DataBase.InsertOrReplaceAsync(crew);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        throw;
                    }
                    
                  
                }
            }
        }
        public static async Task<bool> IsFlightTabelEmpty()
        {
            var count = await DataBase.Table<FlightListItem>().CountAsync();
            return count <= 0;
        }

        public static async Task<bool> IsCrewTabelEmpty()
        {
            var count = await DataBase.Table<Crew>().CountAsync();
            return count <= 0;
        }

        public static async Task<bool> FlightListIsValidAsync()
        {
            var tst = await GetSettingAsync(SettingType.FlightListRetrieved);
            if (string.IsNullOrEmpty(tst))
            {
                return false;
            }
            try
            {
                var ts = DateTime.UtcNow - DateTime.ParseExact(tst, DateTimeSFormat, CultureProvider);
                return ts.TotalHours <= 24;
            }
            catch // db Error?
            {
                return false;
            }
        }
        public static async Task<List<FlightListItem>> GetFlightsList()
        {
            var list = await DataBase.GetAllWithChildrenAsync<FlightListItem>();
            foreach (var t in list)
            {
                t.Crew = await LoadCrew( t.ID);
            }
            return list;
        }
        public static void UpdateDbVer()
        {
            DataBase.InsertOrReplaceAsync(new DBVer()
            {
                Id = DatavaseVersion
            }
           );
        }
        public static async Task<bool> IsOldDb()
        {
            DBVer record;
            try
            {
                record =  await DataBase.Table<DBVer>().FirstOrDefaultAsync();
            }
            catch
            {
                return true;
            }
            return record == null || (record.Id != DatavaseVersion);
        }

        public static async Task<bool> CheckProfiletableExists(){
            try
            {
                if (await DataBase.Table<FilterDBSettings>().CountAsync() > 0)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
            

            return false;
        }
        public static async Task InsertOrUpdatePdf(DBPdf dbPdf)
        {
            try
            {
                if (await DataBase.Table<DBPdf>().CountAsync() > 0)
                {
                    await DeletePdf(dbPdf.FlightID, dbPdf.DocType);
                }

                await DataBase.InsertAsync(dbPdf);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
            
        }
        public static async Task UpdateSessionTimeAsync(string sessionId)
        {
            await InsertOrUpdateSetting(new DBSettings
            {
                Id = SettingType.SessionID,
                Value = sessionId
            });

            await InsertOrUpdateSetting(new DBSettings
            {
                Id = SettingType.SessionReceived,
                Value = DateTime.UtcNow.ToString(DateTimeFormat , CultureProvider )
            });
        }
      
        public static async Task<bool> IsSessionExpired()
        {
            var datestring = await GetSettingAsync(SettingType.SessionReceived);
            if (string.IsNullOrEmpty(datestring))
            {
                return true;
            }
            try
            {
                var ts = DateTime.UtcNow - DateTime.ParseExact(datestring, DateTimeFormat, CultureProvider);
                return ts.TotalMinutes > 30; 
            }
            catch  // invalid read from DB?
            {
                Utils.ShowTemporayMessage(ToastType.Warning, "Invalid read data from DB IsSessionExpired");
                return true;
            }
        }
        public static async Task InsertOrUpdateSetting(DBSettings value)
        {
            await DataBase.InsertOrReplaceAsync(value);
        }

        public static async Task InsertOrUpdateFilterSetting(FilterDBSettings value)
        {
            await DataBase.InsertOrReplaceAsync(value);
        }

        public static async Task LogoutMethod(){
            await DB_PCL.InsertOrUpdateSetting(new DBSettings { Id = SettingType.QuickLogin, Value = "0" });
            //await DB_PCL.InsertOrUpdateSetting(new DBSettings { Id = SettingType.Username, Value = string.Empty });
            await DB_PCL.InsertOrUpdateSetting(new DBSettings { Id = SettingType.Password, Value = string.Empty });
            await DB_PCL.InsertOrUpdateSetting(new DBSettings { Id = SettingType.SessionReceived, Value = string.Empty });
        }

        public static async Task<string> GetSettingAsync(SettingType id)
        {
           var taskSettings = await DataBase.Table<DBSettings>().Where(r => r.Id == id).FirstOrDefaultAsync();
            return taskSettings== null ? string.Empty : taskSettings.Value;
        }

        public static async Task<string> GetFilterSettingAsync(SettingType id )
        {
            string currentUserName = await DB_PCL.GetSettingAsync(Database.SettingType.Username);
            var taskSettings = await DataBase.Table<FilterDBSettings>().Where(r => r.Id == id && r.userName == currentUserName).FirstOrDefaultAsync();
            return taskSettings == null ? string.Empty : taskSettings.Value;
        }

        //public static async Task<DateTime> GetLastTimeStamp()
        //{
        //    await DataBase.Table<FlightListItem>().Where(x=> x.)
        //}

        public static bool CheckDbExists()
        {
               return DependencyService.Get<ISQLite>().IsDbExists();
         
        }
        public static SQLiteAsyncConnection GetConnection()
        {
            var connection = DependencyService.Get<ISQLite>().GetConnection();
            //var connection = new SQLiteAsyncConnection(dataConnectionPath);
            return connection;
        }

        public static async Task<string> GetStoredPdfPath(int flightId, PdfDocType docType)
        {
            try
            {
                var record = await DataBase.Table<DBPdf>().Where(r => r.FlightID == flightId && r.DocType == docType).FirstOrDefaultAsync();
                return record == null ? string.Empty : record.FilePath;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }
        public static async Task<string> GetStoredPdfPathMaskDelete(int flightId, PdfDocType docType)
        {
            DBPdf record = await DataBase.Table<DBPdf>().Where(r => r.FlightID == flightId && r.DocType == docType).FirstOrDefaultAsync();
            return record == null ? string.Empty : record.FilePathMaskDoDelete;
        }
        public static async Task DeletePdf(int flightId, PdfDocType docType)
        {
            //string storedPath = await GetStoredPdfPathMaskDelete(flightId, docType);
            var existingPdf =  await DataBase.FindAsync<DBPdf>(r => r.FlightID == flightId && r.DocType == docType);

            if (existingPdf == null)
            {
                return;
            }
            await DataBase.DeleteAsync(existingPdf);

        }

        public static async Task<List<PdfDocTypeWithDate>> GetStoredPdf(int flightId)
        {
            List<PdfDocTypeWithDate> list;
            try
            {
                 list = new List<PdfDocTypeWithDate>();
                var reader = await DataBase.Table<DBPdf>().Where(r => r.FlightID == flightId).ToListAsync();
                foreach (var item in reader)
                {
                    list.Add(new PdfDocTypeWithDate { docType = item.DocType, localDate = item.DownloadTime });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
            return list;
        }
        public static async Task InsertOrUpdateAirport(Airport value)
        {
            try
            {
                await DataBase.InsertOrReplaceAsync(value);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
            
        }
        public static async Task InsertOrUpdateAirport(Airport[] value)
        {
            await DataBase.InsertAllAsync(value);
        }

        public static async Task<Airport> GetAirportDb(string icao)
        {
            try
            {
                if (string.IsNullOrEmpty(icao))
                {
                    return null;
                }

                return await DataBase.Table<Airport>().Where(r => r.ICAO == icao).FirstOrDefaultAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                throw;
            }
        }
        public static async Task InsertOrUpdateFlight(Flight flight)
        {
            try
            {
                await DataBase.InsertOrReplaceAsync(flight);
                // save child records
                flight.ATCData.FlightID = flight.ID;
                await DataBase.InsertAsync(flight.ATCData);
                await SaveRoutesDb(flight.ID, flight.RoutePoints);
                await SaveCrewDb(flight.ID, flight.Crews);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
            }
         
        }
        public static async Task<Flight> GetFilghtDb(int id)
        {
            Flight flightResult;
            try
            {
                flightResult = await DataBase.Table<Flight>().Where(r => r.ID == id).FirstOrDefaultAsync();
                if (flightResult != null)
                {
                    flightResult.RoutePoints = (await DataBase.Table<RoutePoint>().Where(r => r.FlightID == id).ToListAsync())?.ToArray();
                    flightResult.Crews = await LoadCrew(id);
                    flightResult.ATCData = await DataBase.Table<ATC>().Where(r => r.FlightID == id).FirstOrDefaultAsync();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine("DataBase "+ " GetFilghtDb " + e);
                throw;
            }
            return flightResult;

        }
        private static async Task<List<Crew>> LoadCrew( int flightId)
        {
            try
            {
                return await DataBase.Table<Crew>().Where(r => r.FlightId == flightId).ToListAsync();
            }
            catch (Exception e)
            {
                Debug.WriteLine("DataBase " + " LoadCrew " + e);
                throw;
            }

        }
        private static async Task SaveRoutesDb(int flightId, RoutePoint[] points)
        {
            //     var existingRoutePoint = await DataBase.FindAsync<RoutePoint>(r => r.FlightID == flightID);
            //     await DataBase.DeleteAsync(existingRoutePoint);

            if (points == null) return;

            await SemaphoreSlim.WaitAsync().ConfigureAwait(false);
            try
            {


                foreach (var r in points)
                {
                    r.FlightID = flightId;
                    await DataBase.InsertOrReplaceAsync(r);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                throw;
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }
        private static async Task SaveCrewDb(int flightId, List<Crew> crews)
        {
            await SemaphoreSlim.WaitAsync().ConfigureAwait(false);
            try
            {
                if (crews == null) return;

                foreach (var crew in crews)
                {
                    crew.FlightId = flightId;
                    await DataBase.InsertOrReplaceAsync(crew);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(nameof(DataBase) + " SaveCrewDb " + e);
                throw;
            }
            finally
            {
                SemaphoreSlim.Release();
            }
            
        }

        public static async Task<int> CountOfRecordsPdf()
        {
            return await DataBase.Table<DBPdf>().CountAsync();
        }
    }
}
