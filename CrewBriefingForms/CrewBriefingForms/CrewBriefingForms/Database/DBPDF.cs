using CrewBriefingForms.Models;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace CrewBriefingForms.Database
{
    [Table("CrewBriefingSettings")]
    public class DBSettings
    {
        [PrimaryKey]
        public SettingType Id { get; set; }
        public string Value { get; set; }
    }


    [Table("FilterSettings")]
    public class FilterDBSettings  
    {
        [Indexed]
        public string userName { get; set; }
        [Indexed (Unique = false)]
        public SettingType Id { get; set; }
        public string Value { get; set; }

        [PrimaryKey]
        public string PrimKey { get; set; }
       

    }
    public enum SettingType {
        QuickLogin = 1,
        Username = 2,
        Password = 3,
        SessionReceived = 4,
        FlightListRetrieved = 5,
        TailFilter = 6,
        CrewFilter = 7,
        StdFilter1 = 8,
        StdFilter2 = 9,
        SortField = 10,
        SortType = 11,
        StdFilterOn = 12,
        DepFilter = 14,
        DestFilter = 15,
        FlightNoFilter = 16,
        SessionID = 20
    };



    [Table("CrewBriefingDBVer")]
    public class DBVer
    {
        [PrimaryKey]
        public string Id { get; set; }
    }


    [Table("Pdf")]
    public class DBPdf
	{
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        /// <summary>
        /// ID from DBFlight
        /// </summary>
        [NotNull]
        public int FlightID { get; set; }

        [NotNull]
        /// <summary>
        /// Document Type
        /// </summary>
        public PdfDocType DocType { get; set; }


        [NotNull]
        /// <summary>
        /// Filename from ProxyClient PDF's Attribute
        /// </summary>
        public string FileName { set; get;}

        /// <summary>
        /// State of Downloading for this file
        /// </summary>
        public Constants.PdfDocDownloadingState DownloadingState { set; get; }

        [NotNull]
        public System.DateTime DownloadTime { get; set; }

        [Ignore]
        public string FilePath
        {
            get
            {
                return FilePathMaskDoDelete + DownloadTime.ToString(Database.DB_PCL.DateTimeFormat) + ".pdf";
            }
        }
        [Ignore]
        public string FilePathMaskDoDelete
        {
            get
            {
                return FlightID + "_" + ((int)DocType) + "_";
            }
        }

    }
}

