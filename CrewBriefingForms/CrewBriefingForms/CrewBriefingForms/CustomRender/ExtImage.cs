﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CrewBriefingForms.CustomRender
{
    public class ExtImage : Image 
    {
	    public ExtImage()
	    {
		    
		    
	    }

	    public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ExtImage), null, propertyChanged: (bo, o, n) => ((ExtImage)bo).OnCommandChanged());

	    public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ExtImage), null,
		    propertyChanged: (bindable, oldvalue, newvalue) => ((ExtImage)bindable).CommandCanExecuteChanged(bindable, EventArgs.Empty));

	    public ICommand Command
	    {
		    get => (ICommand)GetValue(CommandProperty);
		    set => SetValue(CommandProperty, value);
	    }

	    bool IsEnabledCore
	    {
		    set => SetValueCore(IsEnabledProperty, value);
	    }

		public object CommandParameter
	    {
		    get => GetValue(CommandParameterProperty);
			set => SetValue(CommandParameterProperty, value);
		}

	    private void OnCommandChanged()
	    {

		    if (Command != null)
		    {
			    var tapGesture = new TapGestureRecognizer()
			    {
				    NumberOfTapsRequired = 1,
				    Command = Command
			    };
			    GestureRecognizers.Add(tapGesture);
		    }

			if (Command != null)
		    {
			    Command.CanExecuteChanged += CommandCanExecuteChanged;
			    CommandCanExecuteChanged(this, EventArgs.Empty);
		    }
		    else
			    IsEnabledCore = true;
	    }

	    void CommandCanExecuteChanged(object sender, EventArgs eventArgs)
	    {
		    ICommand cmd = Command;
		    if (cmd != null)
			    IsEnabledCore = cmd.CanExecute(CommandParameter);
	    }


	}
}
