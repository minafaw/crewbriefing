﻿using Xamarin.Forms;

namespace CrewBriefingForms.CustomRender
{
	public class ExtLabel : Label 
    {
        public static readonly BindableProperty IsUnderlinedProperty = BindableProperty.Create(nameof(IsUnderlined), typeof(bool), typeof(ExtLabel), false);

        public bool IsUnderlined
        {
            get => (bool)GetValue(IsUnderlinedProperty);
            set => SetValue(IsUnderlinedProperty, value);
        }

        // set default value to handle RTL 
        public static readonly BindableProperty HandleArabicProperty = BindableProperty.Create(nameof(HandleArabic), typeof(bool), typeof(ExtLabel), false);

        public bool HandleArabic
        {
            get => (bool)GetValue(HandleArabicProperty);
            set => SetValue(HandleArabicProperty, value);
        }

        public static readonly BindableProperty PaddingProperty =
            BindableProperty.Create(nameof(Padding), typeof(Thickness), typeof(ExtLabel), new Thickness(0));

        public Thickness Padding
        {
            get => (Thickness)GetValue(PaddingProperty);
            set => SetValue(PaddingProperty, value);
        }

    }
}
