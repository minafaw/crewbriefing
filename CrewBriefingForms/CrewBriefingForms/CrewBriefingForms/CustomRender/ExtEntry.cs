﻿using System;
using Xamarin.Forms;

namespace CrewBriefingForms.CustomRender
{
    public class ExtEntry : Entry
    {
        public static readonly BindableProperty EntryBackgroundProperty =
            BindableProperty.Create(nameof(EntryBackground),
                typeof(Color), typeof(ExtEntry), Color.White); 
        public Color EntryBackground {
            get => (Color)GetValue(EntryBackgroundProperty);
            set => SetValue(EntryBackgroundProperty , value);
        }


        public static readonly BindableProperty HasBorderProperty =
            BindableProperty.Create(nameof(HasBorder), typeof(bool), typeof(ExtEntry), false);

        public bool HasBorder
        {
            get => (bool)GetValue(HasBorderProperty);
            set => SetValue(HasBorderProperty, value);
        }


        public static readonly BindableProperty BorderColorProperty =
            BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(ExtEntry), Color.Transparent);

        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(HasBorderProperty, value);
        }

        
    }
}
