﻿using CrewBriefingForms.WebServiceModels.GetFlightSearch;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlightDetailsPage 
    {

        public FlightDetailsPage(FlightListItem item)
        {
            InitializeComponent();
            panelFlightDetails.SetItem(item, panelNav, this, true);
            
        }
        public void SetBusy(bool value)
        {
            panelFlightDetails.SetLoadingExt(value);
        }

    }
}
