﻿using System;
using Xamarin.Forms;

namespace CrewBriefingForms
{
    public class MenuCell : ViewCell
    {
        public string Text
        {
            get { return label.Text; }
            set { label.Text = value; }
        }
        public string ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; }
        }

        private Label label;
        private string imagePath;

        public MenuPage Host { get; set; }

        public MenuCell()
        {
            label = new Label
            {
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium)
            };
            //image = new Image();

            var layout = new StackLayout
            {
                BackgroundColor = (Color)Application.Current.Resources["menu_background"],
                Padding = new Thickness(20, 0, 0, 0),
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center,
                Children = { label }
            };
            View = layout;
        }
        /*

        protected override void OnTapped()
        {
            base.OnTapped();

            Host.Selected(label.Text);
        }
        */
    }
}

