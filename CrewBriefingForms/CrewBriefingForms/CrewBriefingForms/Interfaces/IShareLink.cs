﻿using System;
namespace CrewBriefingForms.Interfaces
{
    public interface IShareLink
    {
        void OpenUri(string url); 
        bool CanOpenUri(string url); 
    }
}
