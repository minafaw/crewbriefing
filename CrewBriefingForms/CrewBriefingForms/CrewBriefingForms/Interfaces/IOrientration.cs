﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrewBriefingForms.Interfaces
{
    public enum DeviceOrientations
    {
        Undefined,
        Landscape,
        Portrait
    }

    public interface IOrientation
    {
        DeviceOrientations GetOrientation();
        //https://developer.xamarin.com/guides/xamarin-forms/dependency-service/device-orientation/
    }
}
