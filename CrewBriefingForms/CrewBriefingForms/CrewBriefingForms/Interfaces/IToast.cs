﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrewBriefingForms.Interfaces
{
    public interface IToast
    {
       
        Task<int> ShowToast(ToastType type, string message, uint timeMSec);
    }
    public enum ToastType
    {
        Info, Warning, Error
    }
}
