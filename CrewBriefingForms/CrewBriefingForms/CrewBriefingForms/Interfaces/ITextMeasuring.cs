﻿using Xamarin.Forms;

namespace CrewBriefingForms
{
    public interface ITextMeasuring
    {
         double calculateWidth(string text, NamedSize fontSize);
        double calculateHeight(string text, NamedSize fontSize);
        double ScreenSizeInInches();
        Size ScreenSizeInFormPixels();
    }
}
