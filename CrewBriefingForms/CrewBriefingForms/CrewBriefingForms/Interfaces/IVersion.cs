﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrewBriefingForms
{
    public interface IAppVersion
    {
        string GetAppVersion();
        string GetAppVersionBuild();
        string GetDeviceName();
        string GetOS();
        string MailEOL();
        int GetOSMajorVersion();
    }
}
