﻿using SQLite;
                         
namespace CrewBriefingForms.Database
{
    public interface ISQLite
    {
        string GetConnectionPath();
        SQLiteAsyncConnection GetConnection();
        bool IsDbExists();
    }
}
