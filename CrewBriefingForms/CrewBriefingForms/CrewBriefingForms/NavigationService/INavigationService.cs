﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CrewBriefingForms.NavigationService
{
	public interface INavigationService
	{
		string CurrentPageKey { get; }
		NavigationPage CurrentNavigationPage { get; }
		void Configure(string pageKey, Type pageType);
		Task GoBack();
		Task NavigateModalAsync(string pageKey, bool animated = true);
		Task NavigateModalAsync(string pageKey, object parameter, bool animated = true);
		Task NavigateAsync(string pageKey, bool animated = true);
		Task NavigateAsync(string pageKey, object parameter, bool animated = true);
	}
}
