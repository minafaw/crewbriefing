﻿using CrewBriefingForms.Models;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;
using System;
using System.Threading.Tasks;
using CrewBriefingForms.Base;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using CrewBriefingForms.Database;
using CrewBriefingForms.Controls;
using Plugin.Share;
using CrewBriefingForms.Interfaces;

namespace CrewBriefingForms
{
    public partial class TabletMainPage : ContentPage
    {
        private Label _labelFlightNo ;
        private StackLayout _panelMenuMap ;
        public static readonly double ImageCoef = 0.7;
        private bool _isMapShown = true;

        public TabletMainPage()
        {
            double imagePadding = 7; // buttons in headers
            CommonStatic.Instance.TabletPageHeaderHeight = CommonStatic.Instance.GetMediumHeight + imagePadding * 2;
            InitializeComponent();
            CustomIni();

            panelFlightList.ListViewFlights.IsVisible = false;
            panelFlightList.ParentPage = this;
            //AddRefresh();

            panelFlightList.SetBusy(Strings.start_update);
            Device.BeginInvokeOnMainThread(async () =>
            {
                await panelFlightList.UpdateList(false);
            });
        }
        public TabletMainPage(string username, string password)
        {
            double imagePadding = 7; // buttons in headers
            CommonStatic.Instance.TabletPageHeaderHeight = CommonStatic.Instance.GetMediumHeight + imagePadding * 2;
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
            CustomIni();
            panelFlightList.SetBusy(Strings.Attempting_login);
            panelFlightList.ParentPage = this;

            //AddRefresh();
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Login(username, password);
            });
        }
        private bool IsListVisible
        {
            get
            {
                return panelFlightListMain.IsVisible;
            }
            set
            {
                panelFlightListMain.IsVisible = value;
                if (value)
                {
                    Grid.SetColumn(panelMapMain, 1);
                    Grid.SetColumnSpan(panelMapMain, IsDetailsVisible ? 1 : 2);
                }
                else  // list hidden
                {
                    Grid.SetColumn(panelMapMain, 0);
                    Grid.SetColumnSpan(panelMapMain, IsDetailsVisible ? 2 : 3);
                }
                btnShowList.IsVisible = !value;
                _panelMenuMap.IsVisible = !value;
            }
        }
        private bool IsDetailsVisible
        {
            get
            {
                return panelFlightDetailsMain.IsVisible;
            }
            set
            {
                panelFlightDetailsMain.IsVisible = value;
                if (value)  // details visible
                {
                    Grid.SetColumn(panelMapMain, IsListVisible ? 1 : 0);
                    Grid.SetColumnSpan(panelMapMain, IsListVisible ? 1 : 2);
                }
                else  // details hidden
                {
                    Grid.SetColumn(panelMapMain, IsListVisible ? 1 : 0);
                    Grid.SetColumnSpan(panelMapMain, IsListVisible ? 2 : 3);
                }
                btnShowDetails.IsVisible = !value;
            }
        }
        private void CustomIni()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            var tapMenu = new TapGestureRecognizer();
            tapMenu.Tapped += (sender, e) =>
            {
                if (!Utils.md.IsPresented)
                {
                    Utils.ShowFilter = false;
                }
                Utils.md.IsPresented = !Utils.md.IsPresented;
            };

            var tapRefresh = new TapGestureRecognizer();
            tapRefresh.Tapped += async (sender, e) =>
            {
                await panelFlightList.UpdateList(true);
            };
            var tapSearch = new TapGestureRecognizer();
            tapSearch.Tapped += async (sender, e) =>
            {
                if (Utils.md != null)
                {
                    //Utils.ShowTemporayMessage(this, "Ok", "Filter");
                     await ((MenuPage)Utils.md.Master).LoadFilterSettings();
                    if (!Utils.md.IsPresented)
                    {
                        Utils.ShowFilter = true;
                    }
                    Utils.md.IsPresented = !Utils.md.IsPresented;
                }
            };
            var h = CommonStatic.Instance.GetMediumHeight;
            panelMap.SizeChanged += PanelMap_SizeChanged;
            panelFlightListMain.SizeChanged += PanelFlightListMain_SizeChanged;

            _labelFlightNo = Utils.SetCenterText(panelMapTop, " ", 0.7, null, null, tapMenu, true); 
            //panel with menu icon os last added child here
            _panelMenuMap = (StackLayout)panelMapTop.Children[panelMapTop.Children.Count - 1]; // created in setCenterText

            _panelMenuMap.IsVisible = false;

            Utils.SetCenterText(panelFlightDetailsTopNav, Strings.flight_info, 0.85, null, null, null, false);

            Utils.SetCenterText(panelFlightListTop, Strings.flights, 0.4, tapSearch, null, tapMenu, false); 
            var refreshAndSearchPanel = (StackLayout)panelFlightListTop.Children[panelFlightListTop.Children.Count - 2];

            AbsoluteLayout.SetLayoutBounds(refreshAndSearchPanel, new Rectangle(0, 0, 0.5, 1));
            refreshAndSearchPanel.Padding = new Thickness(h + 10, 0, 0, 0);

            // add refersh buttonay
            var refreshPanel = new StackLayout()
            {
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                Padding = new Thickness(0, 0, h + CommonStatic.Instance.GetMediumHeight * 0.4, 0),
                Orientation = StackOrientation.Horizontal,
            };
            var imReresh = new Image()
            {
                Source = ImageSource.FromFile(Utils.ImageFolder + "btn_refresh.png"),
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                HeightRequest = CommonStatic.Instance.GetMediumHeight * ImageCoef,
                Aspect = Aspect.AspectFit
            };
            refreshPanel.Children.Add(imReresh);
            imReresh.GestureRecognizers.Add(tapRefresh);
            AbsoluteLayout.SetLayoutBounds(refreshPanel, new Rectangle(1, 0, 0.3, 1));
            AbsoluteLayout.SetLayoutFlags(refreshPanel, AbsoluteLayoutFlags.All);
            panelFlightListTop.Children.Add(refreshPanel);


            //StackLayout refresPanel = (StackLayout)refreshAndSearchPanel.Children[refreshAndSearchPanel.Children.Count - 2];
            //refresPanel.IsVisible = false;

            var imagePadding = 7; // buttons in headers

            btnMapExpand.HeightRequest = h * ImageCoef;
            btnShowDetails.HeightRequest = h * ImageCoef;
            btnShowList.HeightRequest = h * ImageCoef;
            btnHideList.HeightRequest = h * ImageCoef;
            btnHideDetails.HeightRequest = h * ImageCoef;

            btnHideList.Source = Utils.ImageFolder + "arrow_left.png";
            btnShowList.Source = Utils.ImageFolder + "arrow_right.png";
            btnMapExpand.Source = Utils.ImageFolder + "icon_expand.png";
            btnShowDetails.Source = Utils.ImageFolder + "arrow_left.png";
            btnHideDetails.Source = Utils.ImageFolder + "arrow_right.png";
         


            panelMapTop.HeightRequest = CommonStatic.Instance.TabletPageHeaderHeight;
            panelFlightDetailsTop.HeightRequest = CommonStatic.Instance.TabletPageHeaderHeight;
            panelFlightListTop.HeightRequest = CommonStatic.Instance.TabletPageHeaderHeight;

            btnShowDetails.IsVisible = false;
            btnShowList.IsVisible = false;

            var tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) =>
            {
                if (Grid.GetColumnSpan(panelMapMain) == 3)
                {
                    IsDetailsVisible = true;
                    IsListVisible = true;
                }
                else
                {
                    IsDetailsVisible = false;
                    IsListVisible = false;
                }
            };
            btnMapExpand.GestureRecognizers.Add(tap);

            tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) =>
            {
                IsDetailsVisible = true;
            };
            btnShowDetails.GestureRecognizers.Add(tap);
            btnShowDetailsWrap.GestureRecognizers.Add(tap);

            //tap = new TapGestureRecognizer();
            //tap.Tapped += (sender, e) =>
            //{
               
            //};
            //btnShowListWrap.GestureRecognizers.Add(tap);

            tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) =>
            {
                IsDetailsVisible = false;
            };
            btnHideDetails.GestureRecognizers.Add(tap);
            btnHideDetailsWrap.GestureRecognizers.Add(tap);

            var tapGestureRecognizer = new TapGestureRecognizer()
            {
                NumberOfTapsRequired = 1
            };
            tapGestureRecognizer.Tapped += async (s, e) =>
            {
                await HandleTailLogClicked();

            };
            openIn.GestureRecognizers.Add(tapGestureRecognizer);

            if (Device.RuntimePlatform == Device.iOS && Device.Idiom == TargetIdiom.Tablet)
            {
                openIn.IsVisible = true;
            }   

        }
        protected void ChangeListTapped(object sender, EventArgs args)
        { 
            IsListVisible = !IsListVisible;
        }



        private static async Task HandleTailLogClicked()
        {
            //Handle image Tap
            var userName = await DB_PCL.GetSettingAsync(SettingType.Username);
            userName = userName.Trim();
            string urlToSend =
                $"taillog://DownloadPlan/?clientId={Constants.ClientId}&clientKey={Constants.ClientKey}&flightId={StackLayoutFlightList.SelectedItem?.ID}&username={userName}";

            if(! DependencyService.Get<IShareLink>().CanOpenUri(urlToSend)){
                urlToSend = "https://itunes.apple.com/dk/app/taillog/id947286350?l=da&mt=8";
            }
            Device.OpenUri(new Uri(urlToSend));
            System.Diagnostics.Debug.WriteLine("Url to Share is " + urlToSend);
        }

        public void SetBusy(bool value)
        {
            busyPanel.IsVisible = value;
            busyIndicator.IsRunning = value;
        }

        public void SetFlightListBusy(string value)
        {
            panelFlightList.SetBusy(value);  
        }

        public void SetFlightListVisibilty(bool value)
        {
            panelFlightList.IsEnabled = value;
        }
        public void ShowMapOrSummary(Flight flightDetailsExt, bool isSummary)
        {
            _isMapShown = !isSummary;
            SetMap(flightDetailsExt);
        }
        private void PanelFlightListMain_SizeChanged(object sender, EventArgs e)
        {
            if (panelFlightListMain.Width > 0)
            {
                var d = CommonStatic.Instance.GetMediumHeight;
                var hOffset = (CommonStatic.Instance.TabletPageHeaderHeight - d) / 2;
                var wOffset = d * 0.4;

                AbsoluteLayout.SetLayoutBounds(btnHideListWrap, new Rectangle(panelFlightListMain.Width - wOffset - d, hOffset, d + wOffset, d));
                AbsoluteLayout.SetLayoutFlags(btnHideListWrap, AbsoluteLayoutFlags.None);

                btnShowListWrap.HeightRequest = d;
                btnShowListWrap.Padding = new Thickness(wOffset, hOffset);

                btnMapExpandWrap.HeightRequest = CommonStatic.Instance.TabletPageHeaderHeight;
                btnMapExpandWrap.Padding = new Thickness(wOffset, hOffset);

                btnShowDetailsWrap.HeightRequest = CommonStatic.Instance.TabletPageHeaderHeight;
                btnShowDetailsWrap.Padding = new Thickness(0, hOffset, wOffset, hOffset);

                btnHideDetailsWrap.HeightRequest = CommonStatic.Instance.TabletPageHeaderHeight;
                btnHideDetailsWrap.Padding = new Thickness(wOffset, 0, 0, 0);
            }
        }

        private double _mapWidth = 100, _mapHeight = 100;
        private void PanelMap_SizeChanged(object sender, EventArgs e)
        {
            if (panelMap.Height > 0)
            {
                _mapWidth = panelMap.Width;
                _mapHeight = panelMap.Height;

                var d = CommonStatic.Instance.GetMediumHeight;
                //work AbsoluteLayout.SetLayoutBounds(panelMapRightButtons, new Rectangle(_d + 50, 0, _d * 2.5 , 50));
                AbsoluteLayout.SetLayoutBounds(panelMapRightButtons, new Rectangle(_mapWidth - d * 2.5, 0, d * 2.5, CommonStatic.Instance.TabletPageHeaderHeight));
                AbsoluteLayout.SetLayoutFlags(panelMapRightButtons, AbsoluteLayoutFlags.None);
            }
        }

        public async Task UpdateList(bool forceRefresh)
        {
            await panelFlightList.UpdateList(forceRefresh);
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasNavigationBar(this, false);
        }

      
        public void SetMap(Flight flightDetails)
        {
            panelMap?.Children?.Clear();
            if (flightDetails == null)
            {
                return;
            }
            if (_isMapShown)
            {
                var flightMap = new CustomMap
                {
                    FlightInfo = flightDetails,
                    parentPage = this,
                    HeightRequest = _mapHeight,
                    WidthRequest = _mapWidth
                };

                if (flightDetails.RoutePoints?.Length > 0)
                {
                    double minLat = flightDetails.RoutePoints[0].LAT, minLon = flightDetails.RoutePoints[0].LON, maxLat = minLat, maxLon = minLon;
                    foreach (var p in flightDetails.RoutePoints)
                    {
                        minLat = Math.Min(minLat, p.LAT);
                        minLon = Math.Min(minLon, p.LON);
                        maxLat = Math.Max(maxLat, p.LAT);
                        maxLon = Math.Max(maxLon, p.LON);
                    }
                    flightMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position((minLat + maxLat) * 0.5, (minLon + maxLon) * 0.5),
                            Distance.FromMiles(
                                Math.Max(
                                    Utils.DistanceTo(minLat, minLon, maxLat, minLon),
                                    Utils.DistanceTo(minLat, minLon, minLat, maxLon)
                                ) / 2.0
                            )
                        )
                    );
                }
                panelMap?.Children?.Add(flightMap);
            }
            else
            {
                var labelInfo = new Label()
                {
                    HorizontalTextAlignment = TextAlignment.Start,
                    VerticalTextAlignment = TextAlignment.Start,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    LineBreakMode = LineBreakMode.WordWrap,
                    TextColor = Color.Black
                };
                var fontFamily = Utils.SummaryFontFamily;
                var fontFamilyBold = Utils.SummaryFontFamilyBold;

                if (string.IsNullOrEmpty(flightDetails.FlightSummary))
                {
                    labelInfo.FontFamily = fontFamilyBold;
                    labelInfo.Text = Strings.no_flight_summary_text;
                }
                else
                {
                    labelInfo.FontFamily = fontFamily;
                    labelInfo.Text = flightDetails.FlightSummary;
                }
                var st = new StackLayout()
                {
                    Padding = new Thickness(20),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
                var sv = new ScrollView()
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Orientation = ScrollOrientation.Vertical,
                    BackgroundColor = Color.White
                };

                st.Children.Add(labelInfo);
                sv.Content = st;
                
                panelMap?.Children?.Add(sv);
            }

            /*
                            flightDetails.FlightSummary;
                            labelInfo.FontFamily = fontFamily;
                            no_flight_summary_text
                            */
        }
        public void OnFlightSelected(FlightListItem item)
        {
            if (item == null)
            {
                return;
            }
            panelMap?.Children?.Clear();
            panelFlightDetails.SetItem(item, null, this, false);
            
            if (_labelFlightNo != null)
            {
                _labelFlightNo.Text = item.FlightlogID;
            }
        }

        private async Task Login(string username, string password)
        {
            var response = await Utils.TryLogin(username, password, false, false);
            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                await panelFlightList.UpdateList(false);
            }
            else
            {
                var isWrongCredentials = response.Code == Constants.WebApiErrorCode.WRONG_CREDENTIALS;
                Utils.GoToPage(this, new LoginPage(!isWrongCredentials, false, false));
            }
        }

        private double _oldWidth, _oldHeight;

        // handle orientation change 
        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (_oldWidth != width || _oldHeight != height)
            {
                if (width > height)
                {
                    // Orientation Landscape
                    // do no thing here 

                    gridMain.ColumnDefinitions[0].Width = new GridLength(5, GridUnitType.Star);
                    gridMain.ColumnDefinitions[1].Width = new GridLength(5, GridUnitType.Star);
                    gridMain.ColumnDefinitions[2].Width = new GridLength(4, GridUnitType.Star);

                    panelFlightDetails?.ChangeFontSizeInTablet(false);
                }
                else
                {
                    //Orientation Portrait
                    gridMain.ColumnDefinitions[0].Width = new GridLength(5, GridUnitType.Star); 
                    gridMain.ColumnDefinitions[1].Width = new GridLength(1.5, GridUnitType.Star);
                    gridMain.ColumnDefinitions[2].Width = new GridLength(3.5 , GridUnitType.Star);
                    panelFlightDetails?.ChangeFontSizeInTablet(true);

                }
                _oldWidth = width;
                _oldHeight = height;

            }
        }
    }
}
