﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Reflection;
using System.Resources;

using CrewBriefingForms.Database;
using CrewBriefingForms.Models;
using System.Runtime.CompilerServices;
using System.Net;
using System.Threading.Tasks;
using CrewBriefingForms.Controls;
using Plugin.Share;
using SQLite;
using Plugin.Messaging;
using Plugin.Share.Abstractions;
using CrewBriefingForms.CustomRender;
using System.Diagnostics.Contracts;
using CrewBriefingForms.Base;
using CrewBriefingForms.Interfaces;

namespace CrewBriefingForms
{
    /// <summary>
    /// Required for PlatformRenderer
    /// </summary>
   

    public partial class MenuPage : BaseContentPage
    {
        private readonly MasterDetailPage _master;
        private readonly Grid _gridMenu;
        private readonly ExtEntry _txtTailFilter, _txtCrewFilter, _txtDepFilter, _txtDestFilter, _txtFlightNoFilter;
        private readonly Switch _switchTailFilter, _switchCrewFilter, _switchDepFilter, _switchDestFilter, _switchFlightNoFilter;
        private readonly StackLayout _layoutFilter;
        private readonly StackLayout _topPanel;
        private readonly Picker _stdStart;
        private readonly Picker _sortField, _sortType;
        private readonly string[] _stdStrings;
        public static readonly int StdFilterCount = 6;
        private readonly StackLayout _stDone;
        private readonly Label _labelDone;
        private readonly Color _bkColor;
        private readonly ResourceManager _manager;
        ScrollView scrollView;
        StackLayout mainPanel , footerStackLayout;

        public MenuPage(MasterDetailPage m)
        {
            _master = m;
           
            var assembly = typeof(CrewBriefingForms.Strings).GetTypeInfo().Assembly;
             _manager = new ResourceManager("CrewBriefingForms.Strings", assembly);

           Title = Strings.main_title;


            var imageFolder = Utils.ImageFolder;

            if (Device.RuntimePlatform != Device.Android)  // android?
            {
                Icon = imageFolder + "menu.png"; //
            }
            var ver = DependencyService.Get<IAppVersion>();
            var version = ver.GetAppVersionBuild();

            var list = new List<MenuCell>
            {
                new MenuCell {Text = Strings.main_title, Host = this, ImagePath = imageFolder + "icon_launcher.png"},
                new MenuCell {Text = Strings.login_details, Host = this, ImagePath = imageFolder + "tablet_login_details_icon.png"},
                new MenuCell {Text = Strings.about, Host = this, ImagePath = imageFolder + "icon_info_17.png"},
                new MenuCell {Text = Strings.help, Host = this, ImagePath = imageFolder + "icon_question.png"},
                new MenuCell {Text = Strings.share_app, Host = this, ImagePath = imageFolder + "tablet_share_popover_icon.png"},
                new MenuCell {Text = Strings.feedback, Host = this, ImagePath = imageFolder + "tablet_feedback_icon.png"},
            };
            _bkColor = (Color)Application.Current.Resources["menu_background"];
            BackgroundColor = _bkColor;
            _gridMenu = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill ,
                Margin = new Thickness(0,10,0,0)

               
            };

            var imageDesiredSize = CommonStatic.Instance.MediumHeight * 1.2;
            _gridMenu.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            var i = 0;
            foreach (var cell in list)
            {
                _gridMenu.RowDefinitions.Add(new RowDefinition { Height = new GridLength(imageDesiredSize * 1.5, GridUnitType.Absolute) });
                var im = new Image
                {
                    Source = ImageSource.FromFile(cell.ImagePath),
                    VerticalOptions = LayoutOptions.Center,
                    WidthRequest = imageDesiredSize,
                    HeightRequest = imageDesiredSize,
                    Aspect = Aspect.AspectFit
                };

                var s = new AbsoluteLayout();
                //s.BackgroundColor = _bkColor;
                //gridMenu.Children.Add(im, 0, i);
                s.Children.Add(im);
                AbsoluteLayout.SetLayoutBounds(im, new Rectangle(0, imageDesiredSize * 0.25, imageDesiredSize, imageDesiredSize));
                AbsoluteLayout.SetLayoutFlags(im, AbsoluteLayoutFlags.None);

                var l = new Label
                {
                    Text = cell.Text,
                    VerticalTextAlignment = TextAlignment.Center,
                    HorizontalTextAlignment = TextAlignment.Start,
                    TextColor = Color.White,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    FontSize = CommonStatic.LabelFontSize(NamedSize.Medium)
                };
                AbsoluteLayout.SetLayoutBounds(l, new Rectangle(imageDesiredSize + 12, 0, -1, 1));
                AbsoluteLayout.SetLayoutFlags(l, AbsoluteLayoutFlags.HeightProportional);
                s.Children.Add(l);

                if (i > 0)
                {
                    var tap = new TapGestureRecognizer();
                    tap.Tapped += async (sender, e) =>
                    {
                        s.BackgroundColor = Color.Black;
                        await App.Sleep(150);
                        s.BackgroundColor = _bkColor;

                        //cell.View.BackgroundColor = Color.Black; // 2do: not good way show reaction on tap, but it does not show default one somewhy
                        SelectedItem(cell);
                    };
                    s.GestureRecognizers.Add(tap);
                }
                else
                {
                    var imClose = new Image
                    {
                        Source = ImageSource.FromFile(imageFolder + "icon_close_grey.png"),
                        VerticalOptions = LayoutOptions.Center,
                        WidthRequest = imageDesiredSize * .8,
                        HeightRequest = imageDesiredSize * .8,
                        Margin = new Thickness(0,10,0,0),
                        Aspect = Aspect.AspectFit
                    };
                    AbsoluteLayout.SetLayoutBounds(imClose, new Rectangle(1, 0, -1, 1));
                    AbsoluteLayout.SetLayoutFlags(imClose, AbsoluteLayoutFlags.HeightProportional | AbsoluteLayoutFlags.XProportional);
                    var tapClose = new TapGestureRecognizer();
                    tapClose.Tapped += (sender, e) =>
                    {
                        if(_master != null){
                            _master.IsPresented = false; 
                        }
                       
                    };
                    imClose.GestureRecognizers.Add(tapClose);

                    s.Children.Add(imClose);
                }
                //gridMenu.Children.Add(cell.View, 1, i++);
                _gridMenu.Children.Add(s, 0, i++);
            }

           

            var gridFilter = new Grid
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Fill,
            };

            for (i = 0; i < 5; i++)
            {
                gridFilter.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            }
           
            gridFilter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(20, GridUnitType.Star) });
            gridFilter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60, GridUnitType.Star) });
            gridFilter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(20, GridUnitType.Star) });
            

            _labelDone = new Label
            {
                Text = Strings.done,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.EndAndExpand,
                HorizontalTextAlignment = TextAlignment.End

            };
           
            var tapDone = new TapGestureRecognizer();
            _stDone = new StackLayout()
            {
                Padding = new Thickness(15),
                HorizontalOptions = LayoutOptions.EndAndExpand
            };

            tapDone.Tapped += (sender, e) =>
            {
                ProcessDone();
            };

            //_labelDone.GestureRecognizers.Add(tapDone);
            _stDone.Children.Add(_labelDone);
            _stDone.GestureRecognizers.Add(tapDone);

            _topPanel = new StackLayout
            {
                
                Padding = new Thickness(0, 0, 0, 20),
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
               
                VerticalOptions = LayoutOptions.Start,
                Children = {
                    new ExtLabel
                    {
                        Text = Strings.filter_settings,
                        FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                        TextColor = Color.White,
                        HorizontalOptions = LayoutOptions.Start , 
                        Margin =new Thickness(0,15,0,0)
                    },
                    _stDone
                }
            };

            _txtTailFilter = new ExtEntry
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"]

            };
            _txtTailFilter.TextChanged += TxtTailFilter_TextChanged;
            _switchTailFilter = new Switch
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            
            };
            _switchTailFilter.Toggled += SwitchTailFilter_Toggled;
            _txtTailFilter.Completed += TxtTailFilter_Completed;
            var iGridRow = 0;
            gridFilter.Children.Add(new Label
            {
                Text = Strings.tail,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Center

            }, 0, iGridRow);
            gridFilter.Children.Add(_txtTailFilter, 1, iGridRow);
            gridFilter.Children.Add(_switchTailFilter, 2, iGridRow++);
            // crew filter
            _txtCrewFilter = new ExtEntry()
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"]
            };
            _txtCrewFilter.TextChanged += TxtCrewFilter_TextChanged;
            _txtCrewFilter.Completed += TxtTailFilter_Completed;
            _switchCrewFilter = new Switch { VerticalOptions = LayoutOptions.Center };
            _switchCrewFilter.Toggled += SwitchCrewFilter_Toggled;

            gridFilter.Children.Add(new Label
            {
                Text = Strings.crew,
                TextColor = Color.White,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                VerticalOptions = LayoutOptions.Center
            }, 0, iGridRow);
            gridFilter.Children.Add(_txtCrewFilter, 1, iGridRow);
            gridFilter.Children.Add(_switchCrewFilter, 2, iGridRow++);
            // dep filetr
            _txtDepFilter = new ExtEntry
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"]
            };
            _txtDepFilter.TextChanged += TxtDepFilter_TextChanged;
            _txtDepFilter.Completed += TxtTailFilter_Completed;
            _switchDepFilter = new Switch { VerticalOptions = LayoutOptions.Center };
            _switchDepFilter.Toggled += SwitchDepFilter_Toggled;

            gridFilter.Children.Add(new Label
            {
                Text = Strings.dep_label,
                TextColor = Color.White,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                VerticalOptions = LayoutOptions.Center
            }, 0, iGridRow);
            gridFilter.Children.Add(_txtDepFilter, 1, iGridRow);
            gridFilter.Children.Add(_switchDepFilter, 2, iGridRow++);

            // dest filter
            _txtDestFilter = new ExtEntry
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"]
            };
            _txtDestFilter.TextChanged += TxtDestFilter_TextChanged;
            _txtDestFilter.Completed += TxtTailFilter_Completed;
            _switchDestFilter = new Switch { VerticalOptions = LayoutOptions.Center };
            _switchDestFilter.Toggled += SwitchDestFilter_Toggled;

            gridFilter.Children.Add(new Label
            {
                Text = Strings.dest_label,
                TextColor = Color.White,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                VerticalOptions = LayoutOptions.Center
            }, 0, iGridRow);
            gridFilter.Children.Add(_txtDestFilter, 1, iGridRow);
            gridFilter.Children.Add(_switchDestFilter, 2, iGridRow++);


            // FLight No filter
            _txtFlightNoFilter = new ExtEntry
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"]
            };
            _txtFlightNoFilter.TextChanged += TxtFlightNoFilter_TextChanged;
            _txtFlightNoFilter.Completed += TxtTailFilter_Completed;
            _switchFlightNoFilter = new Switch { VerticalOptions = LayoutOptions.Center };
            _switchFlightNoFilter.Toggled += SwitchFlightNoFilter_Toggled;

            gridFilter.Children.Add(new Label
            {
                Text = Strings.flight_num,
                TextColor = Color.White,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Small),
                VerticalOptions = LayoutOptions.Center
            }, 0, iGridRow);
            gridFilter.Children.Add(_txtFlightNoFilter, 1, iGridRow);
            gridFilter.Children.Add(_switchFlightNoFilter, 2, iGridRow++);
            // end Flight No filter
            gridFilter.Children.Add(new Label
            {
                Text = Strings.std,
                TextColor = Color.White,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                VerticalOptions = LayoutOptions.Center
            }, 0, iGridRow++);

            _stdStart = new Picker
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"]
            };

            _stdStrings = FillStdPickerData();
            for (i = 0; i < StdFilterCount; i++)
            {
                _stdStart.Items.Add(_stdStrings[i]);
            }
            _stdStart.SelectedIndex = FilterSettings.DefaultValueStd;

            var panelStd = new Grid
            {
                RowSpacing = 0,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start
                
            };
            panelStd.RowDefinitions.Add(new RowDefinition { Height = new GridLength(0.5, GridUnitType.Star) });
            gridFilter.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            panelStd.Children.Add(_stdStart, 0, 0);

            gridFilter.Children.Add(panelStd, 0, iGridRow++);
            Grid.SetColumnSpan(panelStd, 3);

            // sort
            var labelSort = new Label
            { Text = Strings.sort_by, HorizontalOptions = LayoutOptions.FillAndExpand,
                TextColor = Color.White, VerticalOptions = LayoutOptions.Start,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium) };

            // sort controls
            _sortField = new Picker
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"],
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            
            for (i = 1; i <= 3; i++)
            {
                _sortField.Items.Add(_manager.GetString("sort_field" + i));
            }
            _sortField.SelectedIndex = 0;

            _sortType = new Picker
            {
                Style = (Style)Application.Current.Resources["FilterEntryStyle"],
                HorizontalOptions = LayoutOptions.FillAndExpand
                                          
            };
            
            _sortType.Items.Add(Strings.asc);
            _sortType.Items.Add(Strings.desc);
            _sortType.SelectedIndex = 0;

            var panelSort = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Orientation = StackOrientation.Horizontal,
                Children = { _sortField, _sortType }
            };
            var panelSortMain = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.StartAndExpand,
                Orientation = StackOrientation.Vertical,
                Children = { labelSort, panelSort }
            };

            gridFilter.Children.Add(panelSortMain, 0, iGridRow);
            Grid.SetColumnSpan(panelSortMain, 3);

            _layoutFilter = new StackLayout
            {
              
                Padding = new Thickness(0, 0, 0, 0),
                Orientation = StackOrientation.Vertical,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start,
                Children = { gridFilter },
                Spacing = 25
            };
            var btnClearFilter = new Button
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Start,
                Text = "  " + Strings.clear_filter + "  ",
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                TextColor = Color.White,
                BackgroundColor = _bkColor,
                BorderColor = Color.Gray
            };
            btnClearFilter.Clicked += BtnClearFilter_Clicked;
            _layoutFilter.Children.Add(btnClearFilter);

            _layoutFilter.IsVisible = false;
            _topPanel.IsVisible = false;

            var tailLabel = new Label
            {
                Text = "Partner showcase app:",
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Start,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Small),
                Margin = new Thickness(0, 0, 0, 0) ,
                IsVisible = false

            };

            var tailLog = new Image
            {
                Source = ImageSource.FromResource("CrewBriefingForms.Images.TL_banner_512T.png"),
                VerticalOptions = LayoutOptions.Center,
                Aspect = Aspect.AspectFill , 
                IsVisible = false
            };

            var tapGestureRecognizer = new TapGestureRecognizer()
            {
                NumberOfTapsRequired = 1
            };
            tapGestureRecognizer.Tapped +=  (s, e) =>
            {
                 HandleTailLogClicked();

            };
            tailLog.GestureRecognizers.Add(tapGestureRecognizer);

       
        void HandleTailLogClicked()
        {
            //Handle image Tap
            const string urlToSend = "https://itunes.apple.com/dk/app/taillog/id947286350?l=da&mt=8";
            Device.OpenUri(new Uri(urlToSend));
        }

            var versionLabel = new Label
            {
                Text = "CrewBriefing for iOS " + Strings.version + " " + version,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
                TextColor = Color.White,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.Start,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Micro), 
                Margin = new Thickness(0,10,0,0)
               
            };

            if (Device.RuntimePlatform == Device.Android)
            {
                var replace = versionLabel.Text.Replace("iOS", "Android");
                versionLabel.Text = replace;
            }

            if (Device.RuntimePlatform == Device.iOS && Device.Idiom == TargetIdiom.Tablet)
            {
                tailLog.IsVisible = true;
                tailLabel.IsVisible = true;
            }

             footerStackLayout = new StackLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.EndAndExpand,
                Orientation = StackOrientation.Vertical , 
                Children = { tailLabel, tailLog , versionLabel }

            };
            
             mainPanel = new StackLayout
            {
                BackgroundColor = _bkColor,
                VerticalOptions = LayoutOptions.Fill,
                HorizontalOptions = LayoutOptions.Start,
                Padding = new Thickness(10, 5, 10, 10),
                Children = { _topPanel, _gridMenu, _layoutFilter , footerStackLayout }
            };
        
             scrollView = new ScrollView()
            {
                Content = mainPanel,
                VerticalOptions = LayoutOptions.FillAndExpand,
                IsClippedToBounds = false
            };

            Content = scrollView;

            Task.Run( async() =>
            {
                await LoadFilterSettings();
            });
            NavigationPage.SetHasNavigationBar(this, false);

        }

        private string[] FillStdPickerData()
        {
            var arrayStrings = new string[StdFilterCount];
          
            for (var i = 1; i <= StdFilterCount; i++)
            {
                arrayStrings[i - 1] = _manager.GetString("filter_std" + i);
            }
            return arrayStrings;
        }
        private void CheckToggleOff(Switch switchControl, Entry entryControl)
        {
            if (!switchControl.IsToggled)
            {
                entryControl.Text = string.Empty;
            }

        }
        private void SwitchDepFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchDepFilter, _txtDepFilter);
        }
        private void SwitchDestFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchDestFilter, _txtDestFilter);
        }
        private void SwitchFlightNoFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchFlightNoFilter, _txtFlightNoFilter);
        }
        private void TxtDepFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtDepFilter, _switchDepFilter);
        }
        private void TxtDestFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtDestFilter, _switchDestFilter);
        }
        private void TxtFlightNoFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtFlightNoFilter, _switchFlightNoFilter);
        }
        private void SwitchCrewFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchCrewFilter, _txtCrewFilter);
        }
        private void SwitchTailFilter_Toggled(object sender, ToggledEventArgs e)
        {
            CheckToggleOff(_switchTailFilter, _txtTailFilter);
        }
        public async void ProcessDone()
        {
            _stDone.BackgroundColor = Color.Gray;
            _labelDone.BackgroundColor = Color.Gray;
            await App.Sleep(250);
            _stDone.BackgroundColor = Color.Transparent;
            _labelDone.BackgroundColor = _bkColor;

            Device.BeginInvokeOnMainThread(async () =>
            {
                if(_master != null)
                _master.IsPresented = false;

                var p = Utils.GetPageFromNavigation(_master.Detail);
                await App.Sleep(350);
                var page = p as FlightListPage;
                if (page != null)
                {
                    page.SaveFilterAndUpdateListWait(this, false);
                }
                else
                {
                    var tabletMainPage = p as TabletMainPage;
                    if (tabletMainPage != null)
                    {
                        tabletMainPage.SetFlightListBusy(Strings.applying_filter);
                        tabletMainPage.SetFlightListVisibilty(false);
                        await SaveFilterSettings();
                        await tabletMainPage.UpdateList(false);
                        tabletMainPage.SetFlightListBusy(null);
                        tabletMainPage.SetFlightListVisibilty(true);
                    }  
                }
            });

        }
        private void TxtTailFilter_Completed(object sender, EventArgs e)
        {
            ProcessDone();
        }
        private void BtnClearFilter_Clicked(object sender, EventArgs e)
        {
            _switchTailFilter.IsToggled = false;
            _switchCrewFilter.IsToggled = false;
            _switchDepFilter.IsToggled = false;
            _switchDestFilter.IsToggled = false;
            _switchFlightNoFilter.IsToggled = false;
            _txtCrewFilter.Text = string.Empty;
            _txtTailFilter.Text = string.Empty;
            _txtDepFilter.Text = string.Empty;
            _txtDestFilter.Text = string.Empty;
            _txtFlightNoFilter.Text = string.Empty;
            _stdStart.SelectedIndex = FilterSettings.DefaultValueStd;
            //_stdEnd.SelectedIndex = stdFilterCount;
            _sortField.SelectedIndex = 2;
            _sortType.SelectedIndex = 0;
            //FilterSettings._filterOn = "false";
             FilterSettings.ClearFilter();
        }
        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

        }
        private void TxtCrewFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtCrewFilter, _switchCrewFilter);
        }
        private static void SetTextSwitch(Entry entry, Switch switchControl)
        {
            var bNotEmpty = !string.IsNullOrEmpty(entry.Text.Trim());
            if (switchControl.IsToggled != bNotEmpty)
            {
                switchControl.IsToggled = bNotEmpty;
            }
        }
        private void TxtTailFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTextSwitch(_txtTailFilter, _switchTailFilter);
        }
        public async Task SaveFilterSettings()
        {
            
            var currentUserName = await DB_PCL.GetSettingAsync(Database.SettingType.Username);

            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings()
            {
                Id = SettingType.StdFilterOn,
                Value = "true" ,
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.StdFilterOn
            });

            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings()
            {
                Id = SettingType.TailFilter,
                Value = _switchTailFilter.IsToggled ? _txtTailFilter.Text.Trim() : string.Empty,
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.TailFilter
                                         
            });
            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings()
            {
                Id = SettingType.CrewFilter,
                Value = _switchCrewFilter.IsToggled ? _txtCrewFilter.Text.Trim() : string.Empty,
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.CrewFilter
            });
            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings()
            {
                Id = SettingType.DepFilter,
                Value = _switchDepFilter.IsToggled ? _txtDepFilter.Text.Trim() : string.Empty,
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.DepFilter
            });
            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings()
            {
                Id = SettingType.DestFilter,
                Value = _switchDestFilter.IsToggled ? _txtDestFilter.Text.Trim() : string.Empty,
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.DestFilter
            });
            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings()
            {
                Id = SettingType.FlightNoFilter,
                Value = _switchFlightNoFilter.IsToggled ? _txtFlightNoFilter.Text.Trim() : string.Empty,
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.FlightNoFilter
            });

            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings() 
            { 
                Id = SettingType.StdFilter1,
                Value = GetStdIndex(_stdStart.Items[_stdStart.SelectedIndex]).ToString(),
                userName = currentUserName ,
                PrimKey = currentUserName + SettingType.StdFilter1 
            });

            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings() 
            { 
                Id = SettingType.SortField,
                Value = _sortField.SelectedIndex.ToString(),
                userName = currentUserName ,
                PrimKey = currentUserName + SettingType.SortField
            });

            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings() 
            { 
                Id = SettingType.SortType,
                Value = _sortType.SelectedIndex.ToString(),
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.SortType 
            });
        }
        public async Task LoadFilterSettings()
        {
            
            _txtTailFilter.Text = await DB_PCL.GetFilterSettingAsync(SettingType.TailFilter );
            _txtCrewFilter.Text = await DB_PCL.GetFilterSettingAsync(SettingType.CrewFilter );
            _txtDepFilter.Text = await DB_PCL.GetFilterSettingAsync(SettingType.DepFilter );
            _txtDestFilter.Text = await DB_PCL.GetFilterSettingAsync(SettingType.DestFilter );
            _txtFlightNoFilter.Text = await DB_PCL.GetFilterSettingAsync(SettingType.FlightNoFilter );
            SetTextSwitch(_txtTailFilter, _switchTailFilter);
            SetTextSwitch(_txtCrewFilter, _switchCrewFilter);
            SetTextSwitch(_txtDepFilter, _switchDepFilter);
            SetTextSwitch(_txtDestFilter, _switchDestFilter);
            SetTextSwitch(_txtFlightNoFilter, _switchFlightNoFilter);
            try
            {
                var stdIndex = int.Parse(await DB_PCL.GetFilterSettingAsync(SettingType.StdFilter1));
                SelectPickerItem(_stdStart, _stdStrings[stdIndex]);
            }
            catch
            {
                _stdStart.SelectedIndex = FilterSettings.DefaultValueStd;
            }

            try
            {
                _sortField.SelectedIndex = int.Parse(await DB_PCL.GetFilterSettingAsync(SettingType.SortField  ));
            }
            catch
            {
                _sortField.SelectedIndex = 2;
            }
            try
            {
                _sortType.SelectedIndex = int.Parse(await DB_PCL.GetFilterSettingAsync(SettingType.SortType ));
            }
            catch
            {
                _sortType.SelectedIndex = 0;
            }
        }
        private bool SelectPickerItem(Picker picker, string value)
        {
            for (var i = 0; i < picker.Items.Count; i++)
            {
                if (picker.Items[i] != value) continue;
                picker.SelectedIndex = i;
                return true;
            }
            return false;
        }
        private int GetStdIndex(string value)
        {
            for (var i = 0; i < _stdStrings.Length; i++)
            {
                if (value == _stdStrings[i])
                    return i;
            }
            throw new Exception("getStdIndex - invalid value " + value);
        }
        public void ShowItems()
        {
            IAppVersion ver = DependencyService.Get<IAppVersion>();
            if(Device.RuntimePlatform == Device.Android && (ver.GetOSMajorVersion() == 16 || ver.GetOSMajorVersion() == 17) ){
                if (Utils.ShowFilter)
                {
                    
                    scrollView.VerticalOptions = LayoutOptions.Center;

                }
                else
                {
                    scrollView.VerticalOptions = LayoutOptions.FillAndExpand;
                }
                ForceLayout();   
            }

            _topPanel.IsVisible = Utils.ShowFilter;
            _layoutFilter.IsVisible = Utils.ShowFilter;
            _gridMenu.IsVisible = !Utils.ShowFilter;

        }

        //NavigationPage aboutPage = null;
        public async void SelectedItem(MenuCell cell)
        {
            cell.View.BackgroundColor = (Color)Application.Current.Resources["menu_background"];
            Utils.ShowFilter = false;
            if (cell.Text == Strings.about)
            {
                Utils.GoToPage(_master.Detail, new AboutPage(Strings.about));
            }
            else if (cell.Text == Strings.help)
            {
                //Utils.GoToPage(master.Detail, new AboutPage(Strings.help, Utils.IsTablet() ? "help2tablet.html" : "help2.html"));
                Utils.GoToPage(_master.Detail, new HelpPage());
            }
            else if (cell.Text == Strings.share_app)
            {
                string url = null;
                var shareTitle = Strings.share_title;
                //Utils.ShowTemporayMessage(Interfaces.ToastType.Info, "Share");
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        url = "https://itunes.apple.com/us/app/pps-crewbriefing/id886621885";
                        shareTitle = url;
                        break;
                    case Device.Android:
                        url = "https://play.google.com/store/apps/details?id=crewbriefing.droid";
                        break;
					case Device.UWP:
                        url = "https://www.microsoft.com/en-us/store/p/crewbriefing/9pmwthf9gskg";
                        break;
                }
                await CrossShare.Current.Share(new ShareMessage()
                     {
                         Text = shareTitle,
                         Title = shareTitle,
                         Url = url
                     });
            }
            else if (cell.Text == Strings.login_details)
            {
                Utils.GoToPage(_master.Detail, new LoginPage(true, true, false));
            }
            else if (cell.Text == Strings.feedback)
            {
                IAppVersion ver = DependencyService.Get<IAppVersion>();

                string emailBody = " <br/><br/>CrewBriefing App " + ver.GetAppVersionBuild() +
                    "<br/>" +
                    "User: " + WebUtility.HtmlEncode(await DB_PCL.GetSettingAsync(SettingType.Username)) +
                    "<br/>Device: " + WebUtility.HtmlEncode(ver.GetDeviceName()) +
                    "<br/>OS: " + WebUtility.HtmlEncode(ver.GetOS());

                if (!string.IsNullOrEmpty(ver.MailEOL()))
                {
                    emailBody = emailBody.Replace("<br/>", ver.MailEOL());
                }

				var emailMessenger = CrossMessaging.Current?.EmailMessenger;
                if (emailMessenger!= null && emailMessenger.CanSendEmail)
				{
					
					// Alternatively use EmailBuilder fluent interface to construct more complex e-mail with multiple recipients, bcc, attachments etc. 
					var email = new EmailMessageBuilder()
					  .To("feedback@airsupport.dk")
					  .Subject("Feedback")
					  .Body(emailBody)
					  .Build();

					emailMessenger.SendEmail(email);
                }else{
                    Utils.ShowTemporayMessage(ToastType.Error,"Cant send mail");
                }

            }

            _master.IsPresented = false;  // close the slide-out
        }
        protected override void OnDisappearing()
        {
            Utils.ShowFilter = false;
            base.OnDisappearing();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasNavigationBar(this, false);
        }

    }
}