﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using CrewBriefingForms.Database;
using CrewBriefingForms.Interfaces;
using CrewBriefingForms.Setting;

namespace CrewBriefingForms
{
  
    public partial class LoginPage 
    {
        public LoginPage(bool loadSavedCredentials, bool showToolbar, bool notFirstLogin)
        {
            InitializeComponent();

            labelWaitAuth.Text = Strings.wait_auth;

            var coef = 1.0; // iso & UWP 
            if (Device.RuntimePlatform == Device.Android)
                coef = 1.5;

            imageTopLogo.HeightRequest = CommonStatic.Instance.GetMediumHeight * coef;
            imageTopLogo.WidthRequest = CommonStatic.Instance.GetMediumHeight * coef;
            labelTopLogo.FontSize = CommonStatic.LabelFontSize(NamedSize.Large) * 
                (Utils.IsTablet() ? 1.3 : 1.15);

            txtUsername.Completed += TxtUsername_Completed;
            txtPassword.Completed += TxtPassword_Completed;

            var tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) =>
            {
                cbQuickLogin.Checked = !cbQuickLogin.Checked;
            };
            labelQuickLogin.GestureRecognizers.Add(tap);
            panelLogout.IsVisible = showToolbar;
            panelTitle.IsVisible = !showToolbar;

            if (loadSavedCredentials)
            {
               
               var userNameTxt = AppSetting.Instance.GetValueOrDefault<string>(Constants.UserNameKey , null);
               var passwordTxt = AppSetting.Instance.GetValueOrDefault<string>(Constants.PasswordKey, null); 
               var quickLoginTxt = AppSetting.Instance.GetValueOrDefault(Constants.QuickLoginKey, false);                

               txtUsername.Text = userNameTxt;
               txtPassword.Text = passwordTxt;
               cbQuickLogin.Checked = quickLoginTxt;
                
            }

            if (showToolbar)
            {
                HandleShowToolBar();
            }
            else
            {
                BackgroundColor = Color.White;
                ToolbarItems.Clear();
            }
            bottomPanel.IsVisible = !showToolbar;
            var ver = DependencyService.Get<IAppVersion>();
            var tap1 = new TapGestureRecognizer();
            tap1.Tapped += (sender, e) =>
            {
                Navigation.PushModalAsync(new HelpPage(), true);  
            };
            imageInfo.GestureRecognizers.Add(tap1);

            AbsoluteLayout.SetLayoutBounds(labelVersion, new Rectangle(0.5, 0.5, 0.7, 1));
            AbsoluteLayout.SetLayoutFlags(labelVersion, AbsoluteLayoutFlags.All);


            imageInfo.Source = Utils.ImageFolder + "icon_info.png";
            imageInfoPanel.Padding = new Thickness(0, 0, CommonStatic.Instance.GetMediumHeight, 0);  // CA-10

            AbsoluteLayout.SetLayoutBounds(imageInfoPanel, new Rectangle(0.5, 0.5, 300 + CommonStatic.Instance.GetMediumHeight * 2, 1)); //width = EditboxAlphabet + width of I image - CA-10
            AbsoluteLayout.SetLayoutFlags(imageInfoPanel, AbsoluteLayoutFlags.PositionProportional | AbsoluteLayoutFlags.HeightProportional);

            labelVersion.Text = Strings.version + " " + ver.GetAppVersionBuild();
            NavigationPage.SetHasNavigationBar(this, false);

            //if (notFirstLogin)   // strange behavior when create this page again
            //{
            //    bottomPanel.Padding = new Thickness(0, 0, 0, 30);
            //}
            imageTopLogo.Source = Utils.ImageFolder  + "icon_launcher.png";
        }

        private void HandleShowToolBar()
        {
            BackgroundColor = (Color)Application.Current.Resources["nav_bar_color"];
                var cancelPanel = new StackLayout
                {
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    Padding = new Thickness(20, 0, 0, 0),
                    Orientation = StackOrientation.Horizontal
                };
                var labelCancel = new Label
                {
                    Text = "Cancel",
                    FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                    HorizontalTextAlignment = TextAlignment.Start,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    TextColor = Color.White
                };
                var tapCancel = new TapGestureRecognizer();
                tapCancel.Tapped += (sender, e) =>
                {
                    try
                    {
                        Utils.GoToPrevPage();
                    }
                    catch (Exception exc)
                    {
                        Utils.ShowTemporayMessage(ToastType.Error, exc.Message);
                    }
                };

                cancelPanel.Children.Add(labelCancel);
                cancelPanel.GestureRecognizers.Add(tapCancel);
                AbsoluteLayout.SetLayoutBounds(cancelPanel, new Rectangle(0, 0, 0.4, 1));
                AbsoluteLayout.SetLayoutFlags(cancelPanel, AbsoluteLayoutFlags.All);
                panelLogout.Children.Add(cancelPanel);


                var logoutPanel = new StackLayout
                {
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    Padding = new Thickness(0, 0, 20, 0),
                    Orientation = StackOrientation.Horizontal,
                };
                var labelLogout = new Label
                {
                    Text = "Logout",
                    FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                    HorizontalTextAlignment = TextAlignment.End,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    VerticalOptions = LayoutOptions.CenterAndExpand,
                    VerticalTextAlignment = TextAlignment.Center,
                    TextColor = Color.White
                };
                var tapLogout = new TapGestureRecognizer();
                tapLogout.Tapped += (sender, e) =>
                {
                    AppSetting.Instance.Clear();
                   
                    ToolbarItems.Clear();
                    Utils.ResetMasterPage();
                    ((App) Application.Current).MainPage = new LoginPage(false, false, true);
                };

                logoutPanel.Children.Add(labelLogout);
                logoutPanel.GestureRecognizers.Add(tapLogout);
                AbsoluteLayout.SetLayoutBounds(logoutPanel, new Rectangle(1, 0, 0.4, 1));
                AbsoluteLayout.SetLayoutFlags(logoutPanel, AbsoluteLayoutFlags.All);

                panelLogout.HeightRequest = CommonStatic.Instance.GetMediumHeight * 1.8;

                panelLogout.Children.Add(logoutPanel); 
        }
        private async void TxtPassword_Completed(object sender, EventArgs e)
        {
            await ProcessLoginButton(txtUsername.Text , txtPassword.Text);
        }

        private void TxtUsername_Completed(object sender, EventArgs e)
        {
            txtPassword.Focus();
        }
        private void SetBusy(bool value)
        {
            busyIndicator.IsRunning = value;
            busyPanel.IsVisible = value;
            btnDemo.IsEnabled = !value;
            btnLogin.IsEnabled = !value;
        }

        public async void ButtonDemo_Click(object sender, EventArgs e)
        {
            const string username = "AAADEMO";
            const string password = "BLL111";

            await ProcessLoginButton(username, password);
        }
        private async Task ProcessLoginButton(string username , string password)
        {
	        if (string.IsNullOrWhiteSpace(username) || !username.Equals("AAADEMO"))
	        {
		        string errorMsg;
		        if (!Validate(out errorMsg))
		        {
			        Utils.ShowTemporayMessage(ToastType.Warning, errorMsg);
			        return;
		        }
	        }
	       
	        SetBusy(true);

            var response = await Utils.TryLogin(username, password, true, true);
            SetBusy(false);
            // that mean credential is wrong
            if (response.Message != null && response.Message.Contains("Credentials are not valid for"))
            {
                Utils.ShowTemporayMessage(ToastType.Error, "Invalid Username or Password");
                return ;
            }

            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                var oldUserName = AppSetting.Instance.GetValueOrDefault<string>(Constants.UserNameKey , null);
                if (!string.IsNullOrEmpty(oldUserName) && !oldUserName.Equals(username))
                {
                    await DB_PCL.ReCreateDatabase(); //recreate
                }


                NavigationPage.SetHasNavigationBar(this, false);
	            AppSetting.Instance.AddOrUpdateValue(Constants.UserNameKey, username);
	            AppSetting.Instance.AddOrUpdateValue(Constants.PasswordKey, password);
	            AppSetting.Instance.AddOrUpdateValue(Constants.QuickLoginKey, cbQuickLogin.Checked);

                if (Utils.IsTablet())
                {
                    Utils.GoToPage(this, new TabletMainPage());
                }
                else
                {
                    Utils.GoToPage(this, new FlightListPage());
                }
            }

        }

        private bool Validate(out string errorMsg)
        {
            errorMsg = null;
            if (string.IsNullOrEmpty(txtUsername.Text))
            {
                errorMsg = Strings.username_empty_error;
                return false;
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                errorMsg = Strings.password_empty_error;
                return false;
            }
            return true;
        }

        public async void button_Click(object sender, EventArgs e)
        {
            await ProcessLoginButton(txtUsername.Text, txtPassword.Text);
        }
        }
}
