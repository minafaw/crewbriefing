﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CrewBriefingForms.Controls
{
    public class StackLayoutSwipe : StackLayout
    {
        public EventHandler OnSwipeLeft, OnSwipeRight, OnTap;

        public StackLayoutSwipe()
        {
            //this.BackgroundColor = Color.Green;
        }
        public void SwipeRightCustom()
        {
            OnSwipeRight?.Invoke(this, EventArgs.Empty);   
        }
        public void SwipeLeftCustom()
        {
			OnSwipeLeft?.Invoke(this, EventArgs.Empty);
		}
        public void TapCustom()
        {
			OnTap?.Invoke(this, EventArgs.Empty);
		}

    }
}
