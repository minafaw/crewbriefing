﻿using CrewBriefingForms.Database;
using CrewBriefingForms.Models;
using CrewBriefingForms.WebService;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using CrewBriefingForms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StackLayoutFlightList : StackLayout
    {
        private int _selectedFlightId ;
        private double _listViewWidth ;
        public StackLayoutFlightList()
        {
            InitializeComponent();
            panelFilterActive.IsVisible = false;
           
            var cleaGestureRecognizer = new TapGestureRecognizer();
            cleaGestureRecognizer.Tapped += async (sender, e) => { await CallFilterButtonClicked(); };
            panelFilterActive.GestureRecognizers.Add(cleaGestureRecognizer);
              
            busyIndicator.PropertyChanged += BusyIndicator_PropertyChanged;
            listViewFlights.SizeChanged += ListViewFlights_SizeChanged;
        }
        public async Task CallFilterButtonClicked()
        {
            Device.BeginInvokeOnMainThread( () =>
            {
                SetBusy(Strings.loading);
                ForceLayout();
                
            });
            await App.Sleep(500);
            await FilterSettings.ClearFilter();
            await UpdateList(false);
            SetFilterVisible(false);
        }
        private void ListViewFlights_SizeChanged(object sender, EventArgs e)
        {
            if (listViewFlights.Width > 0)
            {
                _listViewWidth = listViewFlights.Width;
            }

        }
        private void BusyIndicator_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e?.PropertyName))
            {
                if (e.PropertyName.ToLower() == "isrunning")
                {
                    ForceNoBusy = false;
                }
            }
        }
        public void SetFilterVisible(bool value)
        {
            panelFilterActive.IsVisible = value;
        }
        public Page ParentPage { get; set; }
        public double AssignedWidth
        {
            get; set;
        }
        public ListView ListViewFlights
        {
            get
            {
                return listViewFlights;
            }
        }
        public void SetBusy(string text)
        {
            Device.BeginInvokeOnMainThread(  async ()=> {

                await App.Sleep(500);

                if (string.IsNullOrEmpty(text))
                {
                    busyPanel.IsVisible = false;
                    busyIndicator.IsRunning = false;
                    listViewFlights.IsRefreshing = false;
                }
                else
                {
                    labelBusy.Text = text;
                    busyIndicator.IsRunning = true;
                    busyPanel.IsVisible = true;
                } 

            });

        }
        public bool ForceNoBusy;
        public bool IsBusy
        {
            get
            {
                if (busyPanel == null || busyIndicator == null)
                {
                    return false;
                }
                return busyPanel.IsVisible && busyIndicator.IsRunning && !ForceNoBusy;
            }
        }
        public async Task UpdateList(bool forceRefresh)
        {
            await FilterSettings.LoadFilterFromDb();
            var filterIsOn = FilterSettings.IsFilterNotEMpty;

            if (!forceRefresh)
            {
                var flightListIsValid = await DB_PCL.FlightListIsValidAsync();
                if (flightListIsValid)
                {
                   // var listDb = await DB_PCL.GetFlightsList();
                    if (!await DB_PCL.IsFlightTabelEmpty())
                    {
                        var listDb = await FilterSettings.HandleFilterData();
                        listViewFlights.ItemsSource = new ObservableCollection<FlightListItem>(listDb);
                        SetFilterVisible(filterIsOn);
                        if (listDb.Count == 0)
                        {
                            Utils.ShowTemporayMessage(Interfaces.ToastType.Info, Strings.no_records_filtered);
                            ProcessItemSelected(null);
                        }
                        SetListSource(listDb);
                        listViewFlights.IsVisible = true;
                        SetBusy(string.Empty);
                        return;
                    }
                }
            }
            else
            {
                if (!Utils.IsInternet())
                {
                    Utils.ShowTemporayMessage(Interfaces.ToastType.Info, Strings.not_net);
                    SetBusy(string.Empty);
                    return;
                }
            }
            // if forceRefresh equal True 


            var checkSessionOk = await Utils.CheckSessionAndRefreshIfRequired(true);
            if (!checkSessionOk)
            {
                SetBusy(string.Empty);
                return;
            }
            if (!listViewFlights.IsRefreshing)
            {
                SetBusy(Strings.send_request);
                (ParentPage as TabletMainPage)?.SetBusy(true);
            }


            var response = await EdfService.GetFlightListSearch(await CreateFlightSearchRequest() , App.SessionId);
            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                    List<FlightListItem> list;
                    try
                    {
                    list = EdfService.ParseGetFlightListSearch(response.Message);
                       
                    }
                    catch (Exception exc)
                    {
                        SetBusy(string.Empty);
                        (ParentPage as TabletMainPage)?.SetBusy(false);
                        Utils.ShowTemporayMessage(Interfaces.ToastType.Error, "Error parse server response, probably transmission error: " + exc.Message);
                        return;
                    }
                     if (list == null || list.Count == 0)
                    {
                        //Utils.ShowTemporayMessage(Interfaces.ToastType.Info, Strings.no_flight_received);
                        (ParentPage as TabletMainPage)?.SetBusy(false);
                        SetBusy(string.Empty);
                        return;
                     }
                    (ParentPage as TabletMainPage)?.SetBusy(false);

                    await DB_PCL.UpdateFlights(list);
                  
                    var listDb = await FilterSettings.HandleFilterData();
                    SetListSource(listDb);

                    SetFilterVisible(filterIsOn);                  
                    listViewFlights.IsVisible = true;
                    SetBusy(string.Empty);
                    //await DB_PCL.InsertCrewInDb(list);

                    var errorList = await SetFullAirportNames(list);
                    if (!string.IsNullOrEmpty(errorList))
                    {
                        Utils.ShowTemporayMessage(Interfaces.ToastType.Info, Strings.cant_retrieve_airports + errorList);
                    }

            }
            else
            {
                Utils.ShowTemporayMessage(Interfaces.ToastType.Error, Utils.GetErrorMessage(response.Code) + " " + response.Message);
            }
                SetBusy(string.Empty);
            
        }
        private static async Task< FlightSearchRequest> CreateFlightSearchRequest()
        {
          DateTime flighDateTime;
           
           var tst = await DB_PCL.GetSettingAsync(SettingType.FlightListRetrieved);
            DateTime.TryParseExact(tst, DB_PCL.DateTimeSFormat, DB_PCL.CultureProvider , DateTimeStyles.None, out  flighDateTime);
           
            if(flighDateTime > DateTime.UtcNow  ){
                flighDateTime = DateTime.UtcNow;
            }
            var fsr = new FlightSearchRequest()
            {
                ChangedAfter = flighDateTime.ToString("s")

            };
            return fsr;
        }
        public async Task<List<string>> GetIcao4RetrieveAndFillExistingAsync(List<FlightListItem> flightList)
        {
            var icao4Retrieve = new List<string>();
            foreach (var item in flightList)
            {
                var i = 0;
                foreach (var icao in new[] { item.DEP, item.DEST })
                {
                    i++;
                    if (string.IsNullOrEmpty(icao))
                    {
                        continue;
                    }
                    var isFullEmpty = i == 1 ? string.IsNullOrEmpty(item.DepAirportFull) : string.IsNullOrEmpty(item.DestAirportFull);
                    if (!isFullEmpty)
                    {
                        continue;
                    }
                    var airport = await DB_PCL.GetAirportDb(icao);
                    if (airport != null)
                    {
                        if (i == 1)
                        {
                            item.DepAirportFull = airport.Name;
                        }
                        else
                        {
                            item.DestAirportFull = airport.Name;
                        }
                    }
                    else
                    {
                        if (!icao4Retrieve.Contains(icao))
                        {
                            icao4Retrieve.Add(icao);
                        }
                    }
                }
            }
            return icao4Retrieve;

        }
        /// <summary>
        /// Get full airports names
        /// </summary>
        /// <param name="flightList"></param>
        /// <returns>List of comma-separated icao, which are not found</returns>
        private async Task<string> SetFullAirportNames(List<FlightListItem> flightList)
        {
            var result = string.Empty;
            var icao4Retrieve = await GetIcao4RetrieveAndFillExistingAsync(flightList);
            if (icao4Retrieve?.Count > 0)
            {
                var icaoString = string.Join(",", icao4Retrieve);
                var ar = new GetAirportListRequest
                {
                    SessionID = App.SessionId,
                    ICAOList = icaoString
                };
                var responseAirportList = await EdfService.GetAirportList(ar);
                if (responseAirportList.Code == Constants.WebApiErrorCode.SUCCESS)
                {
                    var airports = EdfService.ParseAirportList(responseAirportList.Message);
                    foreach (var airpot in airports)
                    {
                       await DB_PCL.InsertOrUpdateAirport(airpot);
                    }
                }
                else
                {
                    result = icaoString;
                }
                icao4Retrieve = await GetIcao4RetrieveAndFillExistingAsync(flightList);
                if (icao4Retrieve.Count > 0)
                {
                    string.Join(",", icao4Retrieve);
                }
            }
       
            return result;

        }
        private void SetListSource(IReadOnlyCollection<FlightListItem> list)
        {
            if (ParentPage is TabletMainPage)
            {
                if (list?.Count > 0)
                {
                    var firstItem = list.First();
                    ProcessItemSelected(firstItem);
                }
                    
            }
            
            if (Device.RuntimePlatform == Device.Android)
            {
                //CommonStatic.Instance.FlightListColumn2Padding = new Thickness((CommonStatic.Instance.ScreenWidth / 3) * 0.1 , 0,0,0);
                CommonStatic.Instance.FlightListColumn2Padding = new Thickness(0);
                headerColumn2.Padding = CommonStatic.Instance.FlightListColumn2Padding;
            } else
            {
                if (AssignedWidth > 0)
                {
                    // 2do: it seems  calculateWidth works incorrect 
                    double maxWidth2 = 0;

                    var textSizes = DependencyService.Get<ITextMeasuring>();
                    if (list != null)
                        foreach (var item in list)
                        {
                            var d = textSizes.calculateWidth(item.Dep_Dest, NamedSize.Default);
                            if (d > maxWidth2)
                            {
                                maxWidth2 = d;
                            }
                        }
                    if (maxWidth2 == 0)
                    {
                        maxWidth2 = textSizes.calculateWidth("WWWW-WWWW", NamedSize.Default);
                    }
                    maxWidth2 *= 0.8;

                    if (maxWidth2 < AssignedWidth / 3)
                    {
                        CommonStatic.Instance.FlightListColumn2Padding = new Thickness(((AssignedWidth / 3.0) - maxWidth2) / 2.0, 0, 0, 0);
                        headerColumn2.Padding = CommonStatic.Instance.FlightListColumn2Padding;
                        //CommonStatic.Instance.FlightListColumn2Padding = new Thickness(40.0, 0, 0, 0);
                    }
                }
            }
          listViewFlights.ItemsSource = new ObservableCollection<FlightListItem>(list);
        }
        public double ImageSize => Utils.IsTablet() ?
	        CommonStatic.Instance.MediumHeight * 0.8 :
	        37 * CommonStatic.Instance.ScreenCoef;

	    private  void ListView_OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            ProcessItemSelected((FlightListItem)e.Item);
            listViewFlights.SelectedItem = null;
        }

        public static FlightListItem SelectedItem;

        private void ProcessItemSelected(FlightListItem item)
        {
           
            if (ParentPage is FlightListPage)
            {
                if (item != null)
                {
                    //TODO test new page
                    //Utils.GoToPage(ParentPage, new FlightDetailsPage(item));
                    Utils.GoToPage(ParentPage, new FlightDetials(item));
                }
            }
            else
            {

                if (SelectedItem != null)
                {
                    SelectedItem.IsSelected = false;
                }
                SelectedItem = item;
                if (SelectedItem != null) SelectedItem.IsSelected = true;

                _selectedFlightId = item?.ID ?? 0;
               // SetListMarkImages();
                ((TabletMainPage)ParentPage)?.OnFlightSelected(item);
            }
        }  
        private void ListViewFlights_Refreshing(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await App.Sleep(500);
                listViewFlights.IsRefreshing = true;
                await UpdateList(true);
                listViewFlights.IsRefreshing = false;
            });
            
         }
    }
}
