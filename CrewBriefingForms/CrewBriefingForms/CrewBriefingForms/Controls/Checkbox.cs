﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace CrewBriefingForms.Controls
{
    public class Checkbox : Image
    {
        private static readonly string ImageChecked = Utils.ImageFolder + "checkbox_checked.png";
        private static readonly string ImageUnchecked = Utils.ImageFolder + "checkbox_unchecked.png";

     
        public Checkbox()
        {
            var tap = new TapGestureRecognizer();
            tap.Tapped += (sender, e) =>
            {
                Checked = !Checked;
            };
            Source = ImageUnchecked;
            GestureRecognizers.Add(tap);
            SizeChanged += OnSizeChanged;
            BackgroundColor = Color.White;
            
        }

        private static void OnSizeChanged(object sender, EventArgs e)
        {
            //if (base.Height > 0)
            //{
            //    base.WidthRequest = base.Height;
            //}
        }

        public static BindableProperty CheckedProperty = BindableProperty.Create(
            propertyName: "Checked",
            returnType: typeof(bool?),
            declaringType: typeof(Checkbox),
            defaultValue: null,
            defaultBindingMode: BindingMode.TwoWay,
            propertyChanged: CheckedValueChanged);
        public bool Checked
        {
            get
            {
                if (GetValue(CheckedProperty) == null)
                {
                    return false;
                }
                return (bool)GetValue(CheckedProperty);
            }
            set
            {
                SetValue(CheckedProperty, value);
                OnPropertyChanged();
                RaiseCheckedChanged();
            }
        }

        private static void CheckedValueChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != null && (bool)newValue)
            {
                ((Checkbox)bindable).Source = ImageChecked;
            }
            else
            {
                ((Checkbox)bindable).Source = ImageUnchecked;
            }
        }

        public event EventHandler CheckedChanged;
        private void RaiseCheckedChanged()
        {
            CheckedChanged?.Invoke(this, EventArgs.Empty);
        }

        private Boolean _isEnabled = true;
        public new bool IsEnabled
        {
            get
            {
                return _isEnabled;
            }
            set
            {
                _isEnabled = value;
                OnPropertyChanged();
                if (value)
                {
                    Opacity = 1;
                }
                else
                {
                    Opacity = .5;
                }
                base.IsEnabled = value;
            }
        }

        public void OnEnabled_Changed()
        {

        }

        public void OnClicked(object sender, EventArgs e)
        {
            Checked = !Checked;

            // Call the base class event invocation method.
            //base.Clicked(sender, e);
        }

    }
}