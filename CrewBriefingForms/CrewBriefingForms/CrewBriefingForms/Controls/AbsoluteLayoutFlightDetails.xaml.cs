﻿using CrewBriefingForms.Database;
using CrewBriefingForms.Interfaces;
using CrewBriefingForms.Models;
using CrewBriefingForms.ModelsFlight;
using CrewBriefingForms.WebService;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using CrewBriefingForms.Annotations;
using Xamarin.Forms;

namespace CrewBriefingForms.Controls
{
    public partial class AbsoluteLayoutFlightDetails : AbsoluteLayout
    {
        //private FlightListItem _FlightListItem;
        //private Flight _flightDetails;
        private string _arports4ShowDst = string.Empty;
        private DateTime _lastTapProcessed = DateTime.Now.AddDays(-1);
        private bool _prevTapForceRefresh;
        private Page _parentPage;

        private ObservableCollection<DocumentsListItem> _listDocs = new ObservableCollection<DocumentsListItem>();

        public readonly PdfDocType[] DocsOrder =
        {
            PdfDocType.Messages,   //0
            PdfDocType.Logstring,  //1
            PdfDocType.WX,         //2
            PdfDocType.NOTAMs,     //3
            PdfDocType.ATC,        //4
            PdfDocType.WindT,      //5
            PdfDocType.SWX,        //6
            PdfDocType.CrossSection,  // 7

            PdfDocType.UploadedDocuments,  //8
            PdfDocType.CombineDocuments    //9
        };

        public AbsoluteLayoutFlightDetails()
        {
            InitializeComponent();

            mainGridDetails.IsVisible = false;
            //gridDetails.RowDefinitions[0].Height = new GridLength(CommonStatic.LabelFontSize(NamedSize.Medium) * 1.3);
            panelFlightInfo.HeightRequest = CommonStatic.LabelFontSize(NamedSize.Medium) * 1.3;
            gridDetails.RowDefinitions[1].Height = new GridLength(CommonStatic.LabelFontSize(NamedSize.Medium) * 3);

            image_aeroplane.Source = Utils.ImageFolder + "aeroplane.png";

        }

        public StackLayout ParentPanelNav;
        public void SetItem(FlightListItem passedItem, StackLayout panelNav, Page parent, bool goBackIfError)
        {
            if (passedItem == null)
            {
                IsVisible = false;
                return;
            }
            IsVisible = true;
            ParentPanelNav = panelNav;
            ParentPage = parent;
            mainGridDetails.IsVisible = false;
            // _FlightListItem = passedItem;
            IsLoading = true;
            Device.BeginInvokeOnMainThread(async () =>
            {
                try
                {
                    await GetDetails(goBackIfError, passedItem.ID);
                    IsLoading = false;
                }
                catch (Exception exc)
                {
                    IsLoading = false;
                    Utils.ShowTemporayMessage(ToastType.Error, exc.Message);
                }
            });
        }
        public Page ParentPage
        {
            get { return _parentPage; }
            set
            {
                _parentPage = value;
                var page = _parentPage as TabletMainPage;
                if (page != null)
                {  // remove header
                    //gridDetails.HeaderTemplate.

                    panelFlightInfo.IsVisible = false;
                    panelFlightInfo.Padding = new Thickness(0);
                    panelFlightInfo.BackgroundColor = (Color)Application.Current.Resources["white_color"];

                    gridDetails.RowDefinitions[0].Height = new GridLength(1);


                }
                //labelFlightInfo.IsVisible = !(parentPage is TabletMainPage);
            }
        }
        //        public void button_Click(object sender, EventArgs e)
        //        {
        //            Utils.GoToPage(_parentPage, new FlightMapPage(_item, _flightDetails));
        //        }
        private async Task<(bool downloadedExists, DateTime? downloadDate)> CheckDocExists(PdfDocType docType, int passedItemId)
        {
            var downloadedExists = false;
            DateTime? downloadDate = null;
            // var storedDocuments = new List<PdfDocTypeWithDate>();
            var storedDocuments = await DB_PCL.GetStoredPdf(passedItemId);
            foreach (var pdd in storedDocuments)
            {
                if (pdd.docType == docType)
                {
                    downloadedExists = true;
                    downloadDate = pdd.localDate;
                }
            }
            return (downloadedExists, downloadDate);
        }
        private async Task DrawDocList(PdfDocType? updateItemOnly, int passedItemId)
        {
            if (updateItemOnly.HasValue)
            {
                gridDetailsDoc.BeginRefresh();

                var item = _listDocs.First(s => s.DocType == updateItemOnly);
                await FillItemMethod(updateItemOnly, item);
                gridDetailsDoc.EndRefresh();
                return;
            }
            _listDocs = new ObservableCollection<DocumentsListItem>();


            var reterievedList = await CreatelDetialListDataSource(passedItemId);
            _listDocs = new ObservableCollection<DocumentsListItem>(reterievedList);
            gridDetailsDoc.ItemsSource = _listDocs;
            if (_firstLoad)
            {
                ChangeSelectedItem(_listDocs.First());
                _firstLoad = false;
            }
        }
        private async Task<List<DocumentsListItem>> CreatelDetialListDataSource(int passedItemId)
        {
            var assembly = typeof(Strings).GetTypeInfo().Assembly;
            var manager = new ResourceManager("CrewBriefingForms.Strings", assembly);

            var items = new List<DocumentsListItem>();

            var routeItem = new DocumentsListItem
            {
                Title = Strings.route_map,
                Image1Path = Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false,
                flightId = passedItemId
            };
            var summeryItem = new DocumentsListItem
            {
                Title = Strings.flight_summary,
                Image1Path = Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false,
                flightId = passedItemId
            };
            items.Add(routeItem);
            items.Add(summeryItem);

            foreach (PdfDocType itemDocType in DocsOrder)
            {
                var result = await CheckDocExists(itemDocType, passedItemId);
                bool downloadedExists = result.Item1;
                DateTime? downloadDate = result.Item2;

                var item = new DocumentsListItem
                {
                    Title = manager.GetString("doc_title_" + itemDocType),
                    Image1Path = Utils.ImageFolder + (downloadedExists ? "redownload_doc.png" : "icon_download.png"),
                    DownloadDate = downloadDate,
                    ItemDownloaded = downloadedExists,
                    DocType = itemDocType,
                    flightId = passedItemId
                };
                if (itemDocType == PdfDocType.UploadedDocuments)
                {
                    item.Title = downloadedExists
                        ? Strings.doc_title_UploadedDocumentsShort
                        : Strings.doc_title_UploadedDocuments;
                }
                if (itemDocType == PdfDocType.CombineDocuments)
                {
                    item.Title = downloadedExists
                        ? Strings.doc_title_CombineDocumentsShort
                        : Strings.doc_title_CombineDocuments;
                }
                items.Add(item);
            }

            return items;
        }

        private async Task FillItemMethod(PdfDocType? updateItemOnly, DocumentsListItem item)
        {

            var result = await CheckDocExists(updateItemOnly.Value, item.flightId);
            bool downloadedExists = result.Item1;
            DateTime? downloadDate = result.Item2;

            item.Image1Path = Utils.ImageFolder + (downloadedExists ? "redownload_doc.png" : "icon_download.png");
            //item.IsVisibleImage2 = downloadedExists;
            item.ItemDownloaded = downloadedExists;
            item.DownloadDate = downloadDate;
            if (updateItemOnly.Value == PdfDocType.UploadedDocuments) // list has 2 items except Documents
            {
                item.Title = downloadedExists ? Strings.doc_title_UploadedDocumentsShort : Strings.doc_title_UploadedDocuments;
            }
            if (updateItemOnly.Value == PdfDocType.CombineDocuments)
            {
                item.Title = downloadedExists ? Strings.doc_title_CombineDocumentsShort : Strings.doc_title_CombineDocuments;
            }
        }
        bool _firstLoad;

        private async Task GetDetails(bool goBackIfError, int passedItemId)
        {
            var flightDetails = await DB_PCL.GetFilghtDb(passedItemId);

            if (flightDetails == null) // no data in DB
            {
                var checkSessionOk = await Utils.CheckSessionAndRefreshIfRequired(true);
                if (!checkSessionOk || !Utils.IsInternet())
                {
                    //await this.Navigation.PopModalAsync();
                    if (_parentPage is TabletMainPage)
                    {
                        gridDetails.BindingContext = null;
                        await DrawDocList(null, passedItemId);
                    }
                    else
                    {
                        Utils.GoToPrevPage();
                    }

                    return;
                }

                var response = await EdfService.GetFlight(CreateFlightRequest(passedItemId));
                if (response.Code == Constants.WebApiErrorCode.SUCCESS)
                {
                    flightDetails = EdfService.ParseFlight(response.Message);
                    if (flightDetails != null)
                    {
                        await DB_PCL.InsertOrUpdateFlight(flightDetails);
                    }
                }
                else // no details in DB and can't get from Inet - go back
                {
                    Utils.CheckWebResponse(response, _parentPage, goBackIfError);
                    return;
                }
            }
            if (flightDetails != null)
            {
                var i = 0;
                foreach (var icao in new[] { flightDetails.DEP, flightDetails.DEST })
                {
                    if (string.IsNullOrEmpty(icao)) continue;

                    var airport = await DB_PCL.GetAirportDb(icao);
                    if (airport == null)
                    {
                        var ar = new GetAirportRequest
                        {
							SessionID = App.SessionId,
                            ICAO = icao
                        };

                        var responseAirport = await EdfService.GetAirport(ar);
                        if (responseAirport.Code == Constants.WebApiErrorCode.SUCCESS)
                        {
                            airport = EdfService.ParseAirport(responseAirport.Message);
                            await DB_PCL.InsertOrUpdateAirport(airport);
                        }
                        else
                        {
                            Utils.ShowTemporayMessage(ToastType.Error, "Error receive Airport data for '" + icao + "'");
                        }

                    }
                    if (airport != null)
                    {
                        var extraString = "";
						if (Device.RuntimePlatform == Device.UWP)
                            extraString = Environment.NewLine;
                        var airportName = (airport.Name ?? string.Empty).Replace("/", "/ " + extraString);
                        if (i == 0)
                        {
                            flightDetails.Arports4ShowDEP = airportName;
                            if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.iOS)
                            {
                                labelAirportDEP.LineBreakMode = LineBreakMode.WordWrap;
                                labelAirportDST.LineBreakMode = LineBreakMode.WordWrap;
                            }
                        }
                        else
                        {
                            flightDetails.Arports4ShowDST = airportName;
                        }
                    }

                    i++;
                }
                if (_parentPage is TabletMainPage)
                {
                    (_parentPage as TabletMainPage)?.ShowMapOrSummary(flightDetails, false);
                    _firstLoad = true;
                }

            }
            if (ParentPanelNav != null)
            {
                var documentTitle = flightDetails == null
                    ? string.Empty
                    : FlightListItem.GetFlightIdToShow(flightDetails.FlightLogID).Replace("\n", "-");
                Utils.SetBackNavBar(_parentPage, ParentPanelNav, documentTitle);
            }
            gridDetails.BindingContext = flightDetails;

            gridDetails.HeightRequest = CommonStatic.Instance.GetDetailsHeight * .95;
            await DrawDocList(null, passedItemId);

            gridDetailsDoc.ItemTapped += async (sender, e) =>
            {
                if (e == null) return; // has been set to null, do not 'process' tapped event
                var lisview = sender as ListView;
                if (lisview?.SelectedItem == null)
                {
                    return;
                }
                var item = e.Item as DocumentsListItem;

                if (_prevTapForceRefresh)
                {
                    _prevTapForceRefresh = false;
                    return;
                }

                var tc = DateTime.Now - _lastTapProcessed;
                _lastTapProcessed = DateTime.Now;
                if (tc.TotalSeconds > 0.2)
                {
                    // UtemSelected fires 2 times somewhy 
                    // https://forums.xamarin.com/discussion/20108/why-does-the-itemselected-method-execute-twice-when-touching-a-listview-item
                    // just workaround
                    // ItemTap does not work for disable Item selecteion.

                    await ProcessItemTap(item, false);
                    lisview.SelectedItem = null; // deselect row
                }

            };
        }
        private FlightRequest CreateFlightRequest(int passedItemId)
        {
            var fr = new FlightRequest()
            {
				SessionID = App.SessionId,
                FlightID = passedItemId,
                Messages = false,
                Metar = false,
                Notams = false,
                Taf = false
            };
            return fr;
        }
        private static string GetPrepareIdentifier(int flightId, PdfDocType docType)
        {
            var identifier = "WeatherChart&" + flightId;
            switch (docType)
            {
                case PdfDocType.WindT:
                    identifier += "&RS_WT_DefaultFL";
                    break;
                case PdfDocType.SWX:
                    identifier += "&RS_SWC_M";
                    break;
                case PdfDocType.CrossSection:
                    identifier += "&CS";
                    break;
            }
            return identifier;
        }
        private async Task GetPdf(PdfDocType pdfDocType, bool forceReload, DocumentsListItem itemPressed, int passedItemId)
        {
            var storedPath = await DB_PCL.GetStoredPdfPath(passedItemId, pdfDocType);
            var saveLoad = DependencyService.Get<ISaveAndLoad>();

            if (!string.IsNullOrEmpty(storedPath) && !forceReload)
            {
                // CommonStatic.ShowTemporayMessage(this, "debug info", "File exists: " + storedPath);
                try
                {

                    await saveLoad.OpenFileWithDefaultOSViewer(storedPath, itemPressed.Title);
                    return;
                }
                catch (Exception exc)
                {
                    Utils.ShowTemporayMessage(ToastType.Error, "Error open file. " + exc.Message);
                    return;
                }
            }
            // IsLoading = true;
            DownloadIconStatus(itemPressed, true);
            var checkSessionOk = await Utils.CheckSessionAndRefreshIfRequired(true);
            if (!checkSessionOk)
            {
                //  IsLoading = false;
                DownloadIconStatus(itemPressed, false);
                _prevTapForceRefresh = false;
                return;
            }
            var response = await GetPdfResponse(pdfDocType, passedItemId);

            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                GetPDF_Result pdf;
                switch (pdfDocType)
                {
                    case PdfDocType.Logstring:
                        pdf = EdfService.ParseGetPDF_Logstring(response.Message);
                        break;
                    case PdfDocType.Messages:
                        pdf = EdfService.ParseGetPDF_Messages(response.Message);
                        break;
                    case PdfDocType.WX:
                        pdf = EdfService.ParseGetPDF_WX(response.Message);
                        break;
                    case PdfDocType.NOTAMs:
                        pdf = EdfService.ParseGetPDF_NOTAMs(response.Message);
                        break;
                    case PdfDocType.ATC:
                        pdf = EdfService.ParseGetPDF_ATC(response.Message);
                        break;
                    case PdfDocType.WindT:
                    case PdfDocType.SWX:
                    case PdfDocType.CrossSection:
                        pdf = EdfService.ParseGetPDF_FlightDoc(response.Message);
                        break;
                    case PdfDocType.CombineDocuments:
                    case PdfDocType.UploadedDocuments:
                        pdf = EdfService.ParsePDF_FullPackage(response.Message);
                        break;

                    default:
                        throw new Exception("GetPDF parse not implemented: " + pdfDocType.ToString());
                }

                if (pdf.Responce.Succeed == false || pdf.bArray == null)
                {
                    Utils.ShowTemporayMessage(ToastType.Info, Strings.no_uploaded_doc, 3000);
                    IsLoading = false;
                    DownloadIconStatus(itemPressed, false);
                    _prevTapForceRefresh = false;
                    return;
                }
                if (forceReload)
                {
                    await DB_PCL.DeletePdf(passedItemId, pdfDocType);
                }
                var dbPdf = new DBPdf
                {
                    DocType = pdfDocType,
                    DownloadTime = DateTime.UtcNow,
                    FileName = pdf.FileName,
                    FlightID = passedItemId
                };
                await saveLoad.SaveFile(dbPdf.FilePath, pdf.bArray);

                await DB_PCL.InsertOrUpdatePdf(dbPdf);
                // IsLoading = false;
                DownloadIconStatus(itemPressed, false);
                _prevTapForceRefresh = false;

                if (itemPressed.DocType != null) await FillItemMethod((PdfDocType)itemPressed.DocType, itemPressed);
            }
            else
            {
                Utils.CheckWebResponse(response, null, false);
            }
            _prevTapForceRefresh = false;
            // IsLoading = false;
            DownloadIconStatus(itemPressed, false);
        }
        private static void DownloadIconStatus(DocumentsListItem itemPressed, bool showProgress)
        {
            itemPressed.ItemDownloading = showProgress;
            itemPressed.IsVisibleImage1 = !showProgress;
        }
        private async Task<WebServiceResponse> GetPdfResponse(PdfDocType pdfDocType, int passedItemId)
        {
            WebServiceResponse response;
            switch (pdfDocType)
            {
                case PdfDocType.Logstring:
                    var fr = new GetPDF_LogstringRequest()
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId
                    };
                    response = await EdfService.GetPDF_Logstring(fr);
                    break;
                case PdfDocType.Messages:
                    var fr1 = new GetPDF_MessagesRequest()
                    {
                        SessionID = App.SessionId,
                        FlightID = passedItemId,
                        importantOnly = true // harcoded yet
                    };
                    response = await EdfService.GetPDF_Messages(fr1);
                    break;
                case PdfDocType.WX:
                    var frWX = new GetPDF_WXRequest()
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId,
                        halfSizePages = false // harcoded yet
                    };
                    response = await EdfService.GetPDF_WX(frWX);
                    break;
                case PdfDocType.NOTAMs:
                    var frNOTAMs = new GetPDF_WXRequest() // same structure
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId,
                        halfSizePages = false // harcoded yet
                    };
                    response = await EdfService.GetPDF_NOTAMs(frNOTAMs);
                    break;
                case PdfDocType.ATC:
                    var frATC = new GetPDF_LogstringRequest() // same structure
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId
                    };
                    response = await EdfService.GetPDF_ATC(frATC);
                    break;
                case PdfDocType.WindT:
                case PdfDocType.SWX:
                case PdfDocType.CrossSection:
                    var frFlightDoc = new GetFlightDocumentRequest()
                    {
						SessionID = App.SessionId,
                        documentIdentifier = GetPrepareIdentifier(passedItemId, pdfDocType)
                    };
                    response = await EdfService.GetFlightDoc(frFlightDoc);
                    break;
                case PdfDocType.UploadedDocuments:
                    var frFupl = new GetPDF_FullPackageRequest()
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId,
                        ATC = false,
                        Charts = false,
                        FlightLog = false,
                        Messages = false,
                        Notams = false,
                        Uploaded = true,
                        WX = false
                    };
                    response = await EdfService.GetPDF_FullPackage(frFupl);
                    break;
                case PdfDocType.CombineDocuments:
                    var frFull = new GetPDF_FullPackageRequest()
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId,
                        ATC = true,
                        Charts = true,
                        FlightLog = true,
                        Messages = true,
                        Notams = true,
                        Uploaded = true,
                        WX = true
                    };
                    response = await EdfService.GetPDF_FullPackage(frFull);
                    break;
                default:
                    IsLoading = false;
                    throw new Exception("Upsupported GetPDF doctype");
            }

            return response;
        }
        private DocumentsListItem _selectedItem;
        private async Task ProcessItemTap(DocumentsListItem itemTapped, bool isImage2)
        {
            ChangeSelectedItem(itemTapped);
            var flightDetails = await DB_PCL.GetFilghtDb(itemTapped.flightId);
            if (itemTapped.Title == Strings.route_map)
            {
                if (Device.Idiom == TargetIdiom.Tablet)
                {
                    if (_parentPage != null)
                    {
                        (_parentPage as TabletMainPage)?.ShowMapOrSummary(flightDetails, false);
                        return;
                    }
                    Utils.ShowTemporayMessage(ToastType.Error, "Table main page does not exits. Please inform administrator");
                }
                else
                {
                    Utils.GoToPage(_parentPage, new FlightMapPage(itemTapped.flightId, flightDetails));
                }
            }
            else if (itemTapped.Title == Strings.flight_summary)
            {
                if (Device.Idiom == TargetIdiom.Tablet)
                {
                    if (_parentPage != null)
                    {
                        (_parentPage as TabletMainPage)?.ShowMapOrSummary(flightDetails, true);
                        return;
                    }
                    Utils.ShowTemporayMessage(ToastType.Error, "Table main page does not exits. Please inform administrator");
                }
                else
                {
                    Utils.GoToPage(_parentPage, new FlightSummaryPage(flightDetails.FlightSummary, FlightListItem.GetFlightIdToShow(flightDetails.FlightLogID)));
                }
            }
            else
            {
                try
                {
                    var docType = DocsOrder.FirstOrDefault(x => x == itemTapped.DocType);
                    await GetPdf(docType, isImage2, itemTapped, itemTapped.flightId);

                    //if (Device.RuntimePlatform == Device.Android && _parentPage is FlightDetailsPage) // CA-22 
                    //{
                    //    App.FlightDetailsOnResumePage = _parentPage;
                    //    App.FlightDetailsOnResume = _FlightListItem;
                    //}

                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.Message);
                    Utils.ShowTemporayMessage(ToastType.Error, "Error get PDF - please inform support: " + itemTapped.Title);
                }
            }
        }
        private void ChangeSelectedItem(DocumentsListItem itemTapped)
        {
            if (Device.Idiom == TargetIdiom.Tablet && itemTapped.DocType == null)
            {
                if (_selectedItem != null)
                {
                    _selectedItem.IsSelected = false;
                }
                _selectedItem = itemTapped;
                if (_selectedItem != null) _selectedItem.IsSelected = true;
            }
        }
        protected void OnListImage1Tapped(object sender, EventArgs args)
        {
            DocumentsListItem item = null;
            var isDownloadedImage = false;
            if (sender != null)
            {
                if (sender is Label)  // not possible?
                {
                    Utils.ShowTemporayMessage(ToastType.Warning, "Invalid event detected - Please select other item in this line and contack support");
                }
                else
                {
                    var imageSender = (Image)sender;
                    item = (DocumentsListItem)imageSender.BindingContext;
                    ChangeSelectedItem(item);
                    isDownloadedImage = ((DocumentsListItem)imageSender.BindingContext).ItemDownloaded;
                }
            }
            gridDetailsDoc.SelectedItem = null;

            if (item != null)
            {
                if (isDownloadedImage)
                {
                    _prevTapForceRefresh = true;
                }
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await ProcessItemTap(item, isDownloadedImage);
                });
            }
            else
            {
                Utils.ShowTemporayMessage(ToastType.Error, "Image1 - Unknown command");
            }
        }
        /* protected void OnListImage2Tapped(object sender, EventArgs args)
         {
             var imageSender = (Image)sender;
             gridDetailsDoc.SelectedItem = null;

             var item = (DocumentsListItem)imageSender.BindingContext;

             if (item != null)
             {
                 Device.BeginInvokeOnMainThread(async () =>
                 {
                     await ProcessItemTap(item, true);
                 });
             }
             else
             {
                 Utils.ShowTemporayMessage(ToastType.Error, "Image2 - Unknown command");
             }

         }*/

        private bool _isLoading;
        private bool IsLoading
        {
            get => _isLoading;
            set
            {
                if (!mainGridDetails.IsVisible && !value)
                {
                    mainGridDetails.IsVisible = true;
                }
                _isLoading = value;

                if (Device.Idiom == TargetIdiom.Tablet)
                {
                    (_parentPage as TabletMainPage)?.SetBusy(value);
                    return;
                }
                if (!value)
                {
                    gridDetailsDoc.IsVisible = true;
                }
                busyPanel.IsVisible = value;
                busyIndicator.IsRunning = value;

            }
        }
        public void SetLoadingExt(bool value)
        {
            IsLoading = value;
            if (value)
            {
                gridDetailsDoc.IsVisible = false;
            }
        }
        public class DocumentsListItem : INotifyPropertyChanged
        {
            private int _flightId;

            public int flightId
            {
                get { return _flightId; }
                set
                {
                    _flightId = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(flightId)));
                }
            }
            private bool _isSelected;
            public bool IsSelected
            {
                get { return _isSelected; }
                set
                {
                    _isSelected = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSelected)));
                }
            }

            private bool itemDownloading;
            public bool ItemDownloading
            {
                get { return itemDownloading; }
                set
                {
                    itemDownloading = value;
                    OnPropertyChanged1(nameof(ItemDownloading));
                }

            }
            private bool _itemDownloaded;
            public bool ItemDownloaded
            {
                get { return _itemDownloaded; }
                set
                {
                    _itemDownloaded = value;
                    OnPropertyChanged1(nameof(ItemDownloaded));
                }
            }
            private string _title;
            public string Title
            {
                get { return _title; }
                set
                {
                    _title = value;
                    OnPropertyChanged1(nameof(Title));
                }
            }
            private string _image1Path;
            public string Image1Path
            {
                get { return _image1Path; }
                set
                {
                    _image1Path = value;
                    OnPropertyChanged1(nameof(Image1Path));
                }
            }
            public string Image2Path { get; set; }
            private bool _isVisibleImage1 = true;
            public bool IsVisibleImage1
            {
                get { return _isVisibleImage1; }
                set
                {
                    _isVisibleImage1 = value;
                    OnPropertyChanged1(nameof(IsVisibleImage1));
                }
            }
            //public bool IsVisibleImage2 { get; set; }
            public string DownloadDateStr
            {
                get
                {
                    string downloadDateStr = null;
                    if (DownloadDate == null) return null;

                    var diff = DateTime.UtcNow - (DateTime)DownloadDate;
                    if (diff.Days != 0)
                    {
                        if (diff.Days >= 7)
                        {
                            downloadDateStr += diff.Days / 7 + "w ";
                            if (diff.Days % 7 != 0)
                            {
                                downloadDateStr += diff.Days % 7 + "d ";
                            }
                        }
                        else
                        {
                            downloadDateStr += diff.Days + "d ";
                        }

                    }
                    if (diff.Hours != 0)
                    {
                        downloadDateStr += diff.Hours + "h ";
                    }
                    if (string.IsNullOrEmpty(downloadDateStr) && diff.Minutes == 0)
                    {
                        downloadDateStr = "Now";
                    }
                    else
                    {
                        downloadDateStr += diff.Minutes + "min";
                    }
                    return downloadDateStr;
                }

            }
            private DateTime? _downloadDate;
            public DateTime? DownloadDate
            {
                get { return _downloadDate; }
                set
                {
                    _downloadDate = value;
                    OnPropertyChanged1(nameof(DownloadDate));
                    // notify that calculated property 
                    OnPropertyChanged1(nameof(DownloadDateStr));
                    // notify textcolor property 
                    OnPropertyChanged1(nameof(DateTextColor));
                }
            }
            private Color _dateTextColor = Color.Black;
            public Color DateTextColor
            {
                get
                {
                    _dateTextColor = Color.Black;
                    if (DownloadDate == null)
                        return _dateTextColor;


                    TimeSpan ts = (System.TimeSpan)(DateTime.UtcNow - DownloadDate);
                    if (Title == Strings.doc_title_WX)
                    {
                        if (ts.TotalMinutes > 30)
                        {
                            _dateTextColor = Color.Red;  // WX - 30 min
                        }
                    }
                    if (Title == Strings.doc_title_NOTAMs)
                    {
                        if (ts.TotalHours > 3)
                        {
                            _dateTextColor = Color.Red; // NOTAMs - 3 h
                        }
                    }
                    return _dateTextColor;
                }

                set
                {
                    _dateTextColor = value;
                    OnPropertyChanged1(nameof(DateTextColor));
                }
            }
            public double ImageSize => Device.Idiom == TargetIdiom.Tablet ?
	            CommonStatic.Instance.MediumHeight * 0.8 :
	            37 * CommonStatic.Instance.ScreenCoef;

	        public Thickness Image1WrapperPadding => new Thickness(0, 0, CommonStatic.Instance.MediumHeight * 2, 0);

	        public PdfDocType? DocType
            {
                get; set;
            }
            public double SmallFontSize => CommonStatic.LabelFontSize(NamedSize.Small);
	        public event PropertyChangedEventHandler PropertyChanged;
            [NotifyPropertyChangedInvocator]
            protected virtual void OnPropertyChanged1([CallerMemberName] string propertyName = null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void DeleteMenuItem_OnClicked(object sender, EventArgs e)
        {
            var mi = (MenuItem)sender;

            var command = mi.CommandParameter as DocumentsListItem;
            if (command != null && command.ItemDownloaded)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    if (command.DocType == null) return;
                    await DB_PCL.DeletePdf(command.flightId, command.DocType.Value);
                    await DrawDocList(command.DocType.Value, command.flightId);
                });
            }
        }

        private void FlightDetialCell_OnBindingContextChanged(object sender, EventArgs e)
        {

            var theViewCell = (ViewCell)sender;
            var item = theViewCell?.BindingContext as DocumentsListItem;

            if (Device.RuntimePlatform == Device.Android)
            {
                theViewCell?.ContextActions.Clear();
                return;
            }

            //if (item == null) return;
            //if (item.Title == Strings.route_map || item.Title == Strings.flight_summary)
            //theViewCell.ContextActions.RemoveAt(0);

        }

        public void ChangeFontSizeInTablet(bool isPortrait)
        {
            if (Device.Idiom == TargetIdiom.Tablet && labelDep != null)
            {
                var landscapeSize = 11;
                var portraitSize = 9;
                double currentSize = 9;
                if (!isPortrait)
                {
                    // Orientation Landscape
                    // do no thing here 
                    currentSize = landscapeSize;
                    labelInfoBottom4t.FontSize = 11;

                }
                else
                {
                    //Orientation Portrait
                    currentSize = portraitSize;
                    labelInfoBottom4t.FontSize = 8;

                }

                labelDep.FontSize = currentSize;
                labelSTD.FontSize = currentSize;
                labelDest.FontSize = currentSize;
                labelSTA.FontSize = currentSize;
                labelAirportDEP.FontSize = currentSize;
                labelAirportDST.FontSize = currentSize;
                labelInfoBottom1t.FontSize = currentSize;
                labelInfoBottom1.FontSize = currentSize;
                labelInfoBottom2t.FontSize = currentSize;
                labelInfoBottom2.FontSize = currentSize;
                labelInfoBottom3t.FontSize = currentSize;
                labelInfoBottom3.FontSize = currentSize;
                //labelInfoBottom4t.FontSize = currentSize;
                labelInfoBottom4.FontSize = currentSize;

            }
        }


    }
}