﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrewBriefingForms.Setting
{
	public interface IAppSetting
	{
		/// <summary>
		/// Gets the current value or the default that you specify.
		/// </summary>
		/// <param name="key">Key for settings</param>
		/// <param name="defaultValue">default value if not set</param>
		/// <returns>Value or default</returns>
		T GetValueOrDefault<T>(string key, T defaultValue);
		
		/// <summary>
		/// Adds or updates the value 
		/// </summary>
		/// <param name="key">Key for settting</param>
		/// <param name="value">Value to set</param>
		/// <returns>True of was added or updated and you need to save it.</returns>
		bool AddOrUpdateValue<T>(string key, T value);
		/// <summary>
		/// Removes a desired key from the settings
		/// </summary>
		/// <param name="key">Key for setting</param>
		bool Remove(string key);

		/// <summary>
		/// Clear all keys from settings
		/// </summary>
		void Clear();

		/// <summary>
		/// Checks to see if the key has been added.
		/// </summary>
		/// <param name="key">Key to check</param> 
		/// <returns>True if contains key, else false</returns>
		bool Contains(string key);

	}
}
