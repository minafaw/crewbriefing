﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace CrewBriefingForms.Converters
{
    public class InverseBoolConverter : IValueConverter
    {
        public InverseBoolConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
