﻿using System;
using System.Globalization;
using System.Reflection;
using System.Resources;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms
{
    // You exclude the 'Extension' suffix when using in Xaml markup
    [ContentProperty ("Text")]
	public class TranslateExtension : IMarkupExtension
	{
        readonly CultureInfo ci = null;
		const string ResourceId = "CrewBriefingForms.Strings";

		public TranslateExtension() {

            if (Device.RuntimePlatform == Device.iOS || Device.RuntimePlatform == Device.Android)
            {
                ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            }
            else {
                ci = System.Globalization.CultureInfo.CurrentUICulture;
            }
        }

		public string Text { get; set; }

		public object ProvideValue (IServiceProvider serviceProvider)
		{
			if (Text == null)
				return "";

			ResourceManager temp = new ResourceManager(ResourceId, typeof(TranslateExtension).GetTypeInfo().Assembly);
            string translation = string.Empty;
            try {
                //2do android, IOS Platform-Specific Code
                // https://developer.xamarin.com/guides/xamarin-forms/advanced/localization/
                translation = temp.GetString(Text, ci);
                /*
                if (ci.TwoLetterISOLanguageName.ToLower().StartsWith("en"))
                {
                    translation = temp.GetString(Text);
                }
                else {
                    translation = temp.GetString(Text, ci);
                }
                */
            }
            catch (Exception exc)
            {
                translation = exc.Message;
            }
            if (string.IsNullOrEmpty(translation))
            {
#if DEBUG
                throw new ArgumentException(
                    String.Format("Key '{0}' was not found in resources '{1}' for culture '{2}'.", Text, ResourceId, ci.Name),
                    "Text");
#else
				translation = Text; // HACK: returns the key, which GETS DISPLAYED TO THE USER
#endif
            }
            return translation;
		}
	}
}
