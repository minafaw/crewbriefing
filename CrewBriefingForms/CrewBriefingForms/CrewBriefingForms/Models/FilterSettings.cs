﻿using CrewBriefingForms.Database;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLiteNetExtensionsAsync.Extensions;

namespace CrewBriefingForms.Models
{
    public class FilterSettings
    {
        private static readonly string[] TextFilterSeparator = new string[] { " " };
        private static List<string> _tail = new List<string>();
        private static List<string> _crew = new List<string>();
        private static List<string> _dep = new List<string>();
        private static List<string> _dest = new List<string>();
        private static List<string> _flightNo = new List<string>();

        // default value for STD
        public const int DefaultValueStd = 2;

        private static int _sortField = 2; // STD/CTOT default

        private static int _sortType = 0, // 0 = asc, 1 = desc
        _stdIndex1 = DefaultValueStd;


        private static string _filterOn;

        //private static bool stdEnabled = false;
        private static DateTime _stdDate1 = DateTime.MinValue, _stdDate2 = DateTime.MaxValue; // see Strings.filter_std[N]

        private static async Task<List<string>> LoadAndParseTextFiler(SettingType type)
        {
            var list = new List<string>();
            var settingType = await DB_PCL.GetFilterSettingAsync(type);
            var dbValue = settingType.Trim().ToLower();
            if (!string.IsNullOrEmpty(dbValue))
            {
                list.AddRange(dbValue.Split(TextFilterSeparator, StringSplitOptions.RemoveEmptyEntries));
            }
            return list;
        }
        public static async Task ClearFilter()
        {
            string currentUserName = await DB_PCL.GetSettingAsync(Database.SettingType.Username);

            _filterOn = "false";

            await DB_PCL.InsertOrUpdateFilterSetting(new FilterDBSettings()
            {
                Id = SettingType.StdFilterOn,
                Value = "false",
                userName = currentUserName,
                PrimKey = currentUserName + SettingType.StdFilterOn
            });

            _stdIndex1 = DefaultValueStd;
            GetDateTimeByStdCode(_stdIndex1 + 1, out _stdDate1, out _stdDate2);
        }
        public static bool SortByDefault(FlightListItem item)
        {
            GetDateTimeByStdCode(DefaultValueStd + 1, out DateTime defstdDate1, out DateTime defstdDate2);
            return item.STD >= defstdDate1 && item.STD <= defstdDate2;
        }

        public static async Task<List<FlightListItem>> SortByDefault()
        {
            var query = DB_PCL.DataBase.Table<FlightListItem>();
            GetDateTimeByStdCode(DefaultValueStd + 1, out DateTime defstdDate1, out DateTime defstdDate2);
            return await query.Where(item => item.STD >= defstdDate1 && item.STD <= defstdDate2).ToListAsync();

        }
        public static async Task LoadFilterFromDb()
        {
            _tail = await LoadAndParseTextFiler(SettingType.TailFilter);
            _crew = await LoadAndParseTextFiler(SettingType.CrewFilter);
            _dep = await LoadAndParseTextFiler(SettingType.DepFilter);
            _dest = await LoadAndParseTextFiler(SettingType.DestFilter);
            _flightNo = await LoadAndParseTextFiler(SettingType.FlightNoFilter);
            _filterOn = await DB_PCL.GetFilterSettingAsync(SettingType.StdFilterOn);

           
			_sortField = int.TryParse(await DB_PCL.GetFilterSettingAsync(SettingType.SortField) , out _sortField) ? _sortField : DefaultValueStd ;
                     
            _sortType = 0;
            int.TryParse(await DB_PCL.GetFilterSettingAsync(SettingType.SortType), out _sortType);


            _stdIndex1 = DefaultValueStd;
            if (int.TryParse(await DB_PCL.GetFilterSettingAsync(SettingType.StdFilter1), out _stdIndex1))
            {
                _stdIndex1 += 1;
            }  // see Strings.filter_std[N], but it is zero-based in DB

            if (_stdIndex1 < 1 || _stdIndex1 > MenuPage.StdFilterCount)
            {
                _stdIndex1 = DefaultValueStd;
            }

            GetDateTimeByStdCode(_stdIndex1, out _stdDate1, out _stdDate2);

        }
        public static void GetDateTimeByStdCode(int code, out DateTime startDatetime, out DateTime endDatetime)
        {
            startDatetime = DateTime.UtcNow;
            endDatetime = DateTime.UtcNow;
            switch (code)
            {
                case 1:
                    //  6 hour
                    startDatetime = DateTime.UtcNow;
                    endDatetime = DateTime.UtcNow.AddHours(6);
                    break;
                case 2:
                    //  12 hour.
                    startDatetime = DateTime.UtcNow;
                    endDatetime = DateTime.UtcNow.AddHours(12);
                    break;
                case 3:
                    // 24 hour 
                    startDatetime = DateTime.UtcNow;
                    endDatetime = DateTime.UtcNow.AddDays(1);
                    break;
                case 4:
                    // Prev 24 hour / Next 24 Hour
                    startDatetime = DateTime.UtcNow.AddDays(-1);
                    endDatetime = DateTime.UtcNow.AddDays(1);
                    break;
                case 5:
                    // recent 
                    startDatetime = DateTime.MinValue;
                    endDatetime = DateTime.UtcNow;
                    break;
                case 6:
                    // any period 
                    startDatetime = DateTime.MinValue;
                    endDatetime = DateTime.MaxValue;
                    break;

            }

        }
        public static void SortList(ref List<FlightListItem> list)
        {
            if (list != null && list.Count == 0)
            {
                return;
            }
            switch (_sortField)
            {
                case 1: //dep-dest
                    if (_sortType == 0)
                    {
                        list.Sort(new CompareByDepDestAsc());
                    }
                    else
                    {
                        list.Sort(new CompareByDepDestDesc());
                    }
                    break;
                case 2: //STD
                    if (_sortType == 0)
                    {
                        list.Sort(new CompareBySTDAsc());
                    }
                    else
                    {
                        list.Sort(new CompareBySTDDesc());
                    }
                    break;
                default:  // or 0 - FlightNo
                    if (_sortType == 0)
                    {
                        list.Sort(new CompareByFlighNoAsc());
                    }
                    else
                    {
                        list.Sort(new CompareByFlighNoDesc());
                    }
                    break;

            }
        }

        public static bool IsFilterNotEMpty => (_tail.Count > 0 || _crew.Count > 0 || _dep.Count > 0 || _dest.Count > 0 || _flightNo.Count > 0 ||
                                                _stdIndex1 - 1 != DefaultValueStd) && _filterOn.Equals("true");

        public static async Task<List<FlightListItem>> HandleFilterData()
        {
            var listDb = IsFilterNotEMpty ? await FilterSettings.FilterData() : await FilterSettings.SortByDefault();
            SortList(ref listDb);
            return listDb;
        }

        public static async Task<List<FlightListItem>> FilterDataWithCrew()
        {
            var query = await DB_PCL.DataBase.GetAllWithChildrenAsync<FlightListItem>();
            //var query = DB_PCL.DataBase.Table<FlightListItem>();

            if (_tail.Count > 0)
            {
                query = query.FindAll(item => !string.IsNullOrEmpty(item.ACFTAIL) &&
                                              _tail.Contains(item.ACFTAIL.ToLower()));
            }
            ////AND contitions
            if (_crew.Count > 0)  // crew filter
            {
                query = query.FindAll(item => item.Crew.Any(
                  x => !string.IsNullOrWhiteSpace(x.Initials) && _crew.Any(z => x.Initials.ToLower().Contains(z))));

                //query = query.FindAll(item => item.Crew.Any(
                    //x => x.Initials != null && _crew.Contains(x.Initials.ToLower())));
            }

            if (_dep.Count > 0)
            {
                query = query.FindAll(item => item.DEP != null &&
                                              _dep.Contains(item.DEP.ToLower()) ||
                                              item.DepAirportFull != null &&
                                              _dep.Contains(item.DepAirportFull.ToLower()));
            }

            if (_dest.Count > 0)
            {
                query = query.FindAll(item => item.DEST != null &&
                                              _dest.Any(item.DEST.ToLower().Contains) ||
                                              item.DestAirportFull != null &&
                                              _dest.Any(item.DestAirportFull
                                                  .ToLower().Contains));
            }

            if (_flightNo.Count > 0)
            {
                query = query.FindAll(item => FlightListItem.GetFlightIdToShow(item.FlightlogID) != null
                                              && _flightNo.Any(FlightListItem.GetFlightIdToShow(item.FlightlogID).ToLower().Contains));
            }
          
                                                 
            query = query.FindAll(item => item.STD >= _stdDate1 && item.STD <= _stdDate2);
            return query;
        }

        public static async Task<List<FlightListItem>> FilterDataWithOutCrew()
        {
            
            var query = DB_PCL.DataBase.Table<FlightListItem>();

            if (_tail.Count > 0)
            {
                query = query.Where(item => item.ACFTAIL != null &&
                                              _tail.Contains(item.ACFTAIL.ToLower()));
            }

            // if filter contain crew approach will change
            //////AND contitions
            //if (_crew.Count > 0)  // crew filter
            //{
            //    // query = query.FindAll(item => item.Crew != null && item.Crew.Exists(x=>x.FlightId==item.ID));
            //    query = query.Where(item => item.Crew.Any(
            //        x => x.Initials != null && _crew.Contains(x.Initials.ToLower())));
            //}

            if (_dep.Count > 0)
            {
                query = query.Where(item => item.DEP != null &&
                                              _dep.Contains(item.DEP.ToLower()) ||
                                              item.DepAirportFull != null &&
                                              _dep.Contains(item.DepAirportFull.ToLower()));
            }

            if (_dest.Count > 0)
            {
                query = query.Where(item => item.DEST != null &&
                                              _dest.Any(item.DEST.ToLower().Contains) ||
                                              item.DestAirportFull != null &&
                                              _dest.Any(item.DestAirportFull
                                                  .ToLower().Contains));
            }

            if (_flightNo.Count > 0)
            {

                query = query.Where(item => FlightListItem.GetFlightIdToShow(item.FlightlogID) != null
                                              && _flightNo.Any(FlightListItem.GetFlightIdToShow(item.FlightlogID).ToLower().Contains));
            }

            query = query.Where(item => item.STD >= _stdDate1 && item.STD <= _stdDate2);
            return await query.ToListAsync();
        }

        public static async Task<List<FlightListItem>> FilterData()
        {
            // bad method just to increase performace 
            return  await FilterDataWithCrew() ;
        }

        public static bool ItemPassedFilter(FlightListItem item)
        {

            if (_tail.Count > 0)
            {
                if (string.IsNullOrWhiteSpace(item.ACFTAIL) || !_tail.Contains(item.ACFTAIL.ToLower()))
                {
                    return false;
                }
            }
            //AND contitions
            if (_crew.Count > 0)  // crew filter
            {
                var matched = item.Crew.Any(x => !string.IsNullOrWhiteSpace(x.Initials) && _crew.Any(z => x.Initials.ToLower().Contains(z)));
                if (!matched) return false;
            }
            if (_dep.Count > 0)
            {

                if (string.IsNullOrWhiteSpace(item.DEP))
                {
                    return false;
                }
                var depLow = item.DEP.ToLower();
                var depAirportLow = string.IsNullOrEmpty(item.DepAirportFull) ? string.Empty : item.DepAirportFull.ToLower();
                var i = 0;
                foreach (var depItem in _dep)
                {
                    var depItemLow = depItem.ToLower();
                    if (depLow.Contains(depItemLow) || depAirportLow.Contains(depItemLow))
                    {
                        i++;
                    }
                }
                if (i != _dep.Count)
                {
                    return false;
                }

            }
            if (_dest.Count > 0)
            {
                if (string.IsNullOrEmpty(item.DEST))
                {
                    return false;
                }
                var destLow = item.DEST.ToLower();
                var destAirportLow = string.IsNullOrEmpty(item.DestAirportFull) ? string.Empty : item.DestAirportFull.ToLower();
                int i = 0;
                foreach (var destItem in _dest)
                {
                    var destItemLow = destItem.ToLower();
                    if (destLow.Contains(destItemLow) || destAirportLow.Contains(destItemLow))
                    {
                        i++;
                    }
                }
                if (i != _dest.Count)
                {
                    return false;
                }

            }
            if (_flightNo.Count > 0)
            {
                var itemFnLow = FlightListItem.GetFlightIdToShow(item.FlightlogID) ?? string.Empty;
                itemFnLow = itemFnLow.ToLower();
                foreach (var fn in _flightNo)
                {
                    if (!itemFnLow.Contains(fn.ToLower()))
                    {
                        return false;
                    }
                }
            }
            return item.STD >= _stdDate1 && item.STD <= _stdDate2;
        }
        private class CompareByFlighNoAsc : IComparer<FlightListItem>
        {
            public int Compare(FlightListItem x, FlightListItem y)
            {
                return string.Compare(x.FlightlogID, y.FlightlogID, StringComparison.CurrentCultureIgnoreCase);
            }
        }
        private class CompareByFlighNoDesc : IComparer<FlightListItem>
        {
            public int Compare(FlightListItem x, FlightListItem y)
            {
                return string.Compare(y.FlightlogID, x.FlightlogID, StringComparison.CurrentCultureIgnoreCase);
            }
        }
        private class CompareByDepDestAsc : IComparer<FlightListItem>
        {
            public int Compare(FlightListItem x, FlightListItem y)
            {
                return string.Compare(x.Dep_Dest, y.Dep_Dest, StringComparison.CurrentCultureIgnoreCase);
            }
        }
        private class CompareByDepDestDesc : IComparer<FlightListItem>
        {
            public int Compare(FlightListItem x, FlightListItem y)
            {
                return string.Compare(y.Dep_Dest, x.Dep_Dest, StringComparison.CurrentCultureIgnoreCase);
            }
        }
        private class CompareBySTDAsc : IComparer<FlightListItem>
        {
            public int Compare(FlightListItem x, FlightListItem y)
            {
                return DateTime.Compare(x.STD, y.STD);
            }
        }
        private class CompareBySTDDesc : IComparer<FlightListItem>
        {
            public int Compare(FlightListItem x, FlightListItem y)
            {
                return DateTime.Compare(y.STD, x.STD);
            }
        }
    }
}
