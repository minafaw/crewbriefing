﻿namespace CrewBriefingForms.Models
{
    public class WebServiceResponse
    {
        public Constants.WebApiErrorCode Code { get; set; }
        public string Message { get; set; }
    }
}
