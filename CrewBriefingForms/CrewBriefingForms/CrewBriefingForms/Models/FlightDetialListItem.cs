﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using CrewBriefingForms.Base;
using CrewBriefingForms.Database;
using CrewBriefingForms.Interfaces;
using CrewBriefingForms.WebService;
using Xamarin.Forms;

namespace CrewBriefingForms.Models
{
	public class FlightDetialListItem : BaseViewModel
    {
		public ICommand GetPdfCommand { get; private set; }
	   

	    public FlightDetialListItem()
	    {
			GetPdfCommand = new Command( async () =>
			{
				await GetPdf();
			});


		}

	    private async Task GetPdf()
	    {
		    if (DocType == null) return;
		    var saveLoad = DependencyService.Get<ISaveAndLoad>();
           
		    ItemDownloading = true;

			var checkSessionOk = await Utils.CheckSessionAndRefreshIfRequired(true);
		    if (!checkSessionOk)
		    {
				ItemDownloading = false;
			    return;
			}
				
			
			var response = await EdfService.GetPdfResponse((PdfDocType)DocType, FlightId);

			if (response.Code == Constants.WebApiErrorCode.SUCCESS)
			{
				GetPDF_Result pdf;
				switch (DocType)
				{
					case PdfDocType.Logstring:
						pdf = EdfService.ParseGetPDF_Logstring(response.Message);
						break;
					case PdfDocType.Messages:
						pdf = EdfService.ParseGetPDF_Messages(response.Message);
						break;
					case PdfDocType.WX:
						pdf = EdfService.ParseGetPDF_WX(response.Message);
						break;
					case PdfDocType.NOTAMs:
						pdf = EdfService.ParseGetPDF_NOTAMs(response.Message);
						break;
					case PdfDocType.ATC:
						pdf = EdfService.ParseGetPDF_ATC(response.Message);
						break;
					case PdfDocType.WindT:
					case PdfDocType.SWX:
					case PdfDocType.CrossSection:
						pdf = EdfService.ParseGetPDF_FlightDoc(response.Message);
						break;
					case PdfDocType.CombineDocuments:
					case PdfDocType.UploadedDocuments:
						pdf = EdfService.ParsePDF_FullPackage(response.Message);
						break;

					default:
						throw new Exception("GetPDF parse not implemented: " + DocType.ToString());
				}

				ItemDownloading = false;

				if (pdf.Responce.Succeed == false || pdf.bArray == null)
				{
					Utils.ShowTemporayMessage(ToastType.Info, Strings.no_uploaded_doc, 3000);
					return;
				}
				// this equal to force to refresh
				if (ItemDownloaded)
				{
					await DB_PCL.DeletePdf(FlightId, (PdfDocType)DocType);
				}
				var dbPdf = new DBPdf
				{
					DocType = (PdfDocType) DocType,
					DownloadTime = DateTime.UtcNow,
					FileName = pdf.FileName,
					FlightID = FlightId
				};
				await saveLoad.SaveFile(dbPdf.FilePath, pdf.bArray);

				await DB_PCL.InsertOrUpdatePdf(dbPdf);
				ItemDownloading = false;

				if (DocType != null)
					await CheckDocExists();
			}
			else
			{
				Utils.CheckWebResponse(response, null, false);
			}

		    ItemDownloading = false;
		}
	    private async Task CheckDocExists()
	    {
		    var storedDocuments = await DB_PCL.GetStoredPdf(FlightId);
		    foreach (var pdd in storedDocuments)
		    {
			    if (pdd.docType != DocType) continue;
			    ItemDownloaded = true;
			    DownloadDate = pdd.localDate;
			    Image1Path = Utils.ImageFolder + (ItemDownloaded ? "redownload_doc.png" : "icon_download.png");

		    }
		    
	    }

		private int _flightId;
        public int FlightId
        {
            get => _flightId;
            set
            {
                _flightId = value;
                RaisePropertyChanged();
            }
        }
        private bool _isSelected;
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                _isSelected = value;
                RaisePropertyChanged();
            }
        }

        private bool _itemDownloading;
        public bool ItemDownloading
        {
            get => _itemDownloading;
            set
            {
                _itemDownloading = value;
                RaisePropertyChanged();
            }

        }
        private bool _itemDownloaded;
        public bool ItemDownloaded
        {
            get => _itemDownloaded;
            set
            {
                _itemDownloaded = value;
                RaisePropertyChanged();
	            RaisePropertyChanged(nameof(Title));
			}
        }
        private string _title;
        public string Title
        {
	        get
	        {
		        if (!ItemDownloaded) return _title;
		        switch (DocType)
		        {
			        case PdfDocType.UploadedDocuments:
				        return Strings.doc_title_UploadedDocumentsShort;
			        case PdfDocType.CombineDocuments:
				        return Strings.doc_title_CombineDocumentsShort;
		        }
		        return _title;

	        } 
            set
            {
                _title = value;
                RaisePropertyChanged();
            }
        }

	    public string Image1Path { get; set; }
	    
        private bool _isVisibleImage1 = true;
        public bool IsVisibleImage1
        {
            get => _isVisibleImage1;
            set
            {
                _isVisibleImage1 = value;
                RaisePropertyChanged();
            }
        }
        //public bool IsVisibleImage2 { get; set; }
        public string DownloadDateStr
        {
            get
            {
                string downloadDateStr = null;
                if (DownloadDate == null) return null;

                var diff = DateTime.UtcNow - (DateTime)DownloadDate;
                if (diff.Days != 0)
                {
                    if (diff.Days >= 7)
                    {
                        downloadDateStr += diff.Days / 7 + "w ";
                        if (diff.Days % 7 != 0)
                        {
                            downloadDateStr += diff.Days % 7 + "d ";
                        }
                    }
                    else
                    {
                        downloadDateStr += diff.Days + "d ";
                    }

                }
                if (diff.Hours != 0)
                {
                    downloadDateStr += diff.Hours + "h ";
                }
                if (string.IsNullOrEmpty(downloadDateStr) && diff.Minutes == 0)
                {
                    downloadDateStr = "Now";
                }
                else
                {
                    downloadDateStr += diff.Minutes + "min";
                }
                return downloadDateStr;
            }

        }
        private DateTime? _downloadDate;
        public DateTime? DownloadDate
        {
            get => _downloadDate;
            set
            {
                _downloadDate = value;
                RaisePropertyChanged();
                // notify that calculated property 
                RaisePropertyChanged(nameof(DownloadDateStr));
                // notify textcolor property 
                RaisePropertyChanged(nameof(DateTextColor));
	            // notify textcolor property 
	            RaisePropertyChanged(nameof(Image1Path));
			}
        }
        private Color _dateTextColor = Color.Black;
        public Color DateTextColor
        {
            get
            {
                _dateTextColor = Color.Black;
                if (DownloadDate == null)
                    return _dateTextColor;


                var ts = (System.TimeSpan)(DateTime.UtcNow - DownloadDate);
                if (Title == Strings.doc_title_WX)
                {
                    if (ts.TotalMinutes > 30)
                    {
                        _dateTextColor = Color.Red;  // WX - 30 min
                    }
                }
                if (Title == Strings.doc_title_NOTAMs)
                {
                    if (ts.TotalHours > 3)
                    {
                        _dateTextColor = Color.Red; // NOTAMs - 3 h
                    }
                }
                return _dateTextColor;
            }

            set
            {
                _dateTextColor = value;
                RaisePropertyChanged();
            }
        }
        public double ImageSize => Device.Idiom == TargetIdiom.Tablet ?
            CommonStatic.Instance.MediumHeight * 0.8 :
            37 * CommonStatic.Instance.ScreenCoef;

        public Thickness Image1WrapperPadding => new Thickness(0, 0, CommonStatic.Instance.MediumHeight * 2, 0);

        public PdfDocType? DocType
        {
            get; set;
        }
       
    }
}
