﻿using System.Xml.Serialization;

using CrewBriefingForms.Models;

namespace CrewBriefingForms.ModelsFlight
{
    [XmlRoot(Namespace = "http://crewbriefing.com/")]
    public class FlightRequest : AuthentifiedRequest
    {
        public string SessionID { get; set; }
        public int FlightID { get; set; }
        public bool Taf { get; set; }
        public bool Metar { get; set; }
        public bool Notams { get; set; }
        public bool Messages { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
    public partial class Envelope
    {

        private EnvelopeBody bodyField;

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeBody
    {

        private GetFlightResponse getFlightResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetFlightResponse GetFlightResponse
        {
            get
            {
                return this.getFlightResponseField;
            }
            set
            {
                this.getFlightResponseField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetFlightResponse
    {

        private Flight getFlightResultField;

        /// <remarks/>
        public Flight GetFlightResult
        {
            get
            {
                return this.getFlightResultField;
            }
            set
            {
                this.getFlightResultField = value;
            }
        }
    }



}
