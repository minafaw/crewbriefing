﻿using System;

namespace CrewBriefingForms.Models
{
    public class GetPDF_LogstringRequest
    {
        public string SessionID { get; set; }
        public int FlightID { get; set; }
    }

    public class GetPDF_MessagesRequest
    {
        public string SessionID { get; set; }
        public int FlightID { get; set; }
        public bool importantOnly { get; set; }
    }
    public class GetFlightDocumentRequest
    {
        public string SessionID { get; set; }
        public string documentIdentifier { get; set; }
    }
    public class GetPDF_FullPackageRequest
    {
        public string SessionID { get; set; }
        public int FlightID { get; set; }
        public bool Messages { get; set; }
        public bool FlightLog { get; set; }
        public bool WX { get; set; }
        public bool Notams { get; set; }
        public bool ATC { get; set; }
        public bool Charts { get; set; }
        public bool Uploaded { get; set; }
    }
    

    public class GetPDF_WXRequest
    {
        public string SessionID { get; set; }
        public int FlightID { get; set; }
        public bool halfSizePages { get; set; }
    }


    public enum PdfDocType : int
    {
        ATC,       //0
        Logstring, //1
        Messages,  //2 
        NOTAMs,     //3
        WX,         //4
        WindT, //5
        SWX,  //6
        CrossSection, //7
        UploadedDocuments,  //8
        CombineDocuments,  //9
        Other,  //10, not used yet
    }
    public class PdfDocTypeWithDate
    {
        public PdfDocType docType { get; set; }
        public DateTime localDate { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
    public class Envelope
    {

        private EnvelopeBody bodyField;

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    //        [System.Xml.Serialization.XmlInclude(typeof(GetPDF_LogstringResponse))]

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeBody
    {

        private GetPDF_LogstringResponse getPDF_LogstringResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_LogstringResponse GetPDF_LogstringResponse
        {
            get
            {
                return this.getPDF_LogstringResponseField;
            }
            set
            {
                this.getPDF_LogstringResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false, ElementName = "GetPDF_LogstringResponse")]
    public partial class GetPDF_LogstringResponse
    {

        private GetPDF_Result getPDF_LogstringResultField;

        /// <remarks/>
        public GetPDF_Result GetPDF_LogstringResult
        {
            get
            {
                return this.getPDF_LogstringResultField;
            }
            set
            {
                this.getPDF_LogstringResultField = value;
            }
        }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetPDF_Result
    {

        public string FileName
        {
            get; set;
        }

        public byte[] bArray
        {
            get; set;
        }

        public Models.Response Responce
        {
            get; set;
        }
    }
    // -- GetMessages

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public class EnvelopeMessages
    {

        private EnvelopeMessagesBody bodyField;

        /// <remarks/>
        public EnvelopeMessagesBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    //        [System.Xml.Serialization.XmlInclude(typeof(GetPDF_LogstringResponse))]

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeMessagesBody
    {

        private GetPDF_MessagesResponse getPDF_LogstringResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_MessagesResponse GetPDF_MessagesResponse
        {
            get
            {
                return this.getPDF_LogstringResponseField;
            }
            set
            {
                this.getPDF_LogstringResponseField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetPDF_MessagesResponse
    {

        private GetPDF_Result getPDF_LogstringResultField;

        /// <remarks/>
        public GetPDF_Result GetPDF_MessagesResult
        {
            get
            {
                return this.getPDF_LogstringResultField;
            }
            set
            {
                this.getPDF_LogstringResultField = value;
            }
        }
    }
    //GetPDF_WX
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public class EnvelopeWX
    {

        private EnvelopeWXBody bodyField;

        /// <remarks/>
        public EnvelopeWXBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeWXBody
    {

        private GetPDF_WXResponse getPDF_LogstringResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_WXResponse GetPDF_WXResponse
        {
            get
            {
                return this.getPDF_LogstringResponseField;
            }
            set
            {
                this.getPDF_LogstringResponseField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetPDF_WXResponse
    {

        private GetPDF_Result getPDF_LogstringResultField;

        /// <remarks/>
        public GetPDF_Result GetPDF_WXResult
        {
            get
            {
                return this.getPDF_LogstringResultField;
            }
            set
            {
                this.getPDF_LogstringResultField = value;
            }
        }
    }
    //GetPDF_NOTAMs
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public class EnvelopeNOTAMs
    {
        public EnvelopeNOTAMsBody Body { get; set; }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeNOTAMsBody
    {
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_NOTAMsResponse GetPDF_NOTAMsResponse { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetPDF_NOTAMsResponse
    {
        public GetPDF_Result GetPDF_NOTAMsResult { get; set; }
    }
    //GetPDF_ATC
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public class EnvelopeATC
    {
        public EnvelopeATCBody Body { get; set; }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeATCBody
    {
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_ATCResponse GetPDF_ATCResponse { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetPDF_ATCResponse
    {
        public GetPDF_Result GetPDF_ATCResult { get; set; }
    }
    //--GetFlightDocument
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public partial class EnvelopeFlightDoc
    {

        private EnvelopeBodyFlightDoc bodyField;

        /// <remarks/>
        public EnvelopeBodyFlightDoc Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeBodyFlightDoc
    {

        private GetFlightDocumentResponse getFlightDocumentResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetFlightDocumentResponse GetFlightDocumentResponse
        {
            get
            {
                return this.getFlightDocumentResponseField;
            }
            set
            {
                this.getFlightDocumentResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetFlightDocumentResponse
    {

        private GetFlightDocumentResponseGetFlightDocumentResult getFlightDocumentResultField;

        /// <remarks/>
        public GetFlightDocumentResponseGetFlightDocumentResult GetFlightDocumentResult
        {
            get
            {
                return this.getFlightDocumentResultField;
            }
            set
            {
                this.getFlightDocumentResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetFlightDocumentResponseGetFlightDocumentResult
    {

        private GetFlightDocumentResponseGetFlightDocumentResultMeta metaField;

        private byte[] byteArrayField;

        private Models.Response responseField;

        /// <remarks/>
        public GetFlightDocumentResponseGetFlightDocumentResultMeta Meta
        {
            get
            {
                return this.metaField;
            }
            set
            {
                this.metaField = value;
            }
        }

        /// <remarks/>
        public byte[] ByteArray
        {
            get
            {
                return this.byteArrayField;
            }
            set
            {
                this.byteArrayField = value;
            }
        }

        /// <remarks/>
        public Models.Response Response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetFlightDocumentResponseGetFlightDocumentResultMeta
    {

        private string titleField;

        private System.DateTime validFromField;

        private System.DateTime validToField;

        private uint flightIdField;

        private string identifierField;

        private string typeField;

        private string categoryField;

        private string info1Field;

        private object info2Field;

        private System.DateTime createdField;

        private System.DateTime basedOnForecastField;

        /// <remarks/>
        public string Title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ValidFrom
        {
            get
            {
                return this.validFromField;
            }
            set
            {
                this.validFromField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ValidTo
        {
            get
            {
                return this.validToField;
            }
            set
            {
                this.validToField = value;
            }
        }

        /// <remarks/>
        public uint FlightId
        {
            get
            {
                return this.flightIdField;
            }
            set
            {
                this.flightIdField = value;
            }
        }

        /// <remarks/>
        public string Identifier
        {
            get
            {
                return this.identifierField;
            }
            set
            {
                this.identifierField = value;
            }
        }

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        /// <remarks/>
        public string Info1
        {
            get
            {
                return this.info1Field;
            }
            set
            {
                this.info1Field = value;
            }
        }

        /// <remarks/>
        public object Info2
        {
            get
            {
                return this.info2Field;
            }
            set
            {
                this.info2Field = value;
            }
        }

        /// <remarks/>
        public System.DateTime Created
        {
            get
            {
                return this.createdField;
            }
            set
            {
                this.createdField = value;
            }
        }

        /// <remarks/>
        public System.DateTime BasedOnForecast
        {
            get
            {
                return this.basedOnForecastField;
            }
            set
            {
                this.basedOnForecastField = value;
            }
        }
    }
    //GetPDF_FullPackage
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public class EnvelopeFullPackage
    {
        public EnvelopeFullPackageBody Body { get; set; }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeFullPackageBody
    {
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_FullPackageResponse GetPDF_FullPackageResponse { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetPDF_FullPackageResponse
    {
        public GetPDF_Result GetPDF_FullPackageResult { get; set; }
    }


}
