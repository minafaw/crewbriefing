﻿using System.Xml.Serialization;
using SQLite;

namespace CrewBriefingForms.ModelsAirport
{
    /// <remarks/>
    [XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
    public partial class Envelope
    {

        private EnvelopeBody bodyField;

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeBody
    {

        private GetAirportResponse getAirportResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetAirportResponse GetAirportResponse
        {
            get
            {
                return this.getAirportResponseField;
            }
            set
            {
                this.getAirportResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetAirportResponse
    {

        private Airport getAirportResultField;

        /// <remarks/>
        public Airport GetAirportResult
        {
            get
            {
                return this.getAirportResultField;
            }
            set
            {
                this.getAirportResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [Table("Airport")]
    public partial class Airport
    {
        private GetAirportResponseGetAirportResultRunway[] runwaysField;

        private string nameField;

        private string iCAOField;

        private string iATAField;

        private string countryField;

        private GetAirportResponseGetAirportResultTAF tAFField;

        private string metarField;

        private string vARField;

        private string eLEVField;

        private string lATField;

        private string lONField;

        private string runWLField;

        private string fIRField;

        [Ignore]
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Runway", IsNullable = false)]
        public GetAirportResponseGetAirportResultRunway[] Runways
        {
            get
            {
                return this.runwaysField;
            }
            set
            {
                this.runwaysField = value;
            }
        }
        [NotNull]
        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }
        [NotNull]
        /// <remarks/>
        public string ICAO
        {
            get
            {
                return this.iCAOField;
            }
            set
            {
                this.iCAOField = value;
            }
        }
        
        /// <remarks/>
        public string IATA
        {
            get
            {
                return this.iATAField;
            }
            set
            {
                this.iATAField = value;
            }
        }

        /// <remarks/>
        public string Country
        {
            get
            {
                return this.countryField;
            }
            set
            {
                this.countryField = value;
            }
        }
        [Ignore]
        /// <remarks/>
        public GetAirportResponseGetAirportResultTAF TAF
        {
            get
            {
                return this.tAFField;
            }
            set
            {
                this.tAFField = value;
            }
        }

        /// <remarks/>
        public string Metar
        {
            get
            {
                return this.metarField;
            }
            set
            {
                this.metarField = value;
            }
        }

        /// <remarks/>
        public string VAR
        {
            get
            {
                return this.vARField;
            }
            set
            {
                this.vARField = value;
            }
        }

        /// <remarks/>
        public string ELEV
        {
            get
            {
                return this.eLEVField;
            }
            set
            {
                this.eLEVField = value;
            }
        }

        /// <remarks/>
        public string LAT
        {
            get
            {
                return this.lATField;
            }
            set
            {
                this.lATField = value;
            }
        }

        /// <remarks/>
        public string LON
        {
            get
            {
                return this.lONField;
            }
            set
            {
                this.lONField = value;
            }
        }

        /// <remarks/>
        public string RunWL
        {
            get
            {
                return this.runWLField;
            }
            set
            {
                this.runWLField = value;
            }
        }

        /// <remarks/>
        public string FIR
        {
            get
            {
                return this.fIRField;
            }
            set
            {
                this.fIRField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetAirportResponseGetAirportResultRunway
    {

        private string nameField;

        private string lengthField;

        /// <remarks/>
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string Length
        {
            get
            {
                return this.lengthField;
            }
            set
            {
                this.lengthField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetAirportResponseGetAirportResultTAF
    {

        private string typeField;

        private string textField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Text
        {
            get
            {
                return this.textField;
            }
            set
            {
                this.textField = value;
            }
        }
    }

}
