﻿using System;
using System.Xml.Serialization;

namespace CrewBriefingForms.Models
{
    [XmlRoot(Namespace = "http://crewbriefing.com/")]
    public class FlightSearchRequest: AuthentifiedRequest
    {

       // public string FromSTD { get; set; }
       // public string ToSTD { get; set; }
        public string ChangedAfter { get; set; }
       // public string[] Crew { get; set; }
     //   public bool CrewAnded { get; set; }
     //   public string[] FlightNumber { get; set; }
     //   public string[] TailNumber { get; set; }
      //  public string Departure { get; set; }
      //  public string Destination { get; set; }
    }
}
