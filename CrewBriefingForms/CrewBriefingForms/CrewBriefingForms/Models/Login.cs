﻿namespace CrewBriefingForms.Models
{
    public class LoginRequest
    {
        public string PartnerUser { get; set; }
        public string PartnerPassword { get; set; }
        public string CustomerUser { get; set; }
        public string CustomerPassword { get; set; }
    }
    public class LoginResponse
    {
        public string GetUserFilteredSessionIDResult { get; set; }
    }
}
