﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using CrewBriefingForms.Models;
using SQLite;
using SQLiteNetExtensions.Attributes;
using Xamarin.Forms;

namespace CrewBriefingForms.WebServiceModels.GetFlightSearch
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false)]
    public partial class Envelope
    {

        private EnvelopeBody bodyField;

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeBody
    {

        private GetFlightListSearchResponse getFlightListSearchResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetFlightListSearchResponse GetFlightListSearchResponse
        {
            get
            {
                return this.getFlightListSearchResponseField;
            }
            set
            {
                this.getFlightListSearchResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetFlightListSearchResponse
    {

        private GetFlightListSearchResponseGetFlightListSearchResult getFlightListSearchResultField;

        /// <remarks/>
        public GetFlightListSearchResponseGetFlightListSearchResult GetFlightListSearchResult
        {
            get
            {
                return this.getFlightListSearchResultField;
            }
            set
            {
                this.getFlightListSearchResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetFlightListSearchResponseGetFlightListSearchResult
    {

        private FlightListItem[] itemsField;

        private GetFlightListSearchResponseGetFlightListSearchResultResponse responseField;

        private bool moreFlightsToGetField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("FlightListItem", IsNullable = false)]
        public FlightListItem[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        public GetFlightListSearchResponseGetFlightListSearchResultResponse Response
        {
            get
            {
                return this.responseField;
            }
            set
            {
                this.responseField = value;
            }
        }

        /// <remarks/>
        public bool MoreFlightsToGet
        {
            get => moreFlightsToGetField;
	        set => moreFlightsToGetField = value;
        }
    }

    [Table("FlightList")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class FlightListItem : INotifyPropertyChanged
    {
        [OneToMany(CascadeOperations = CascadeOperation.All)]
        [Ignore]
        [System.Xml.Serialization.XmlArrayItemAttribute("Crew", IsNullable = false)]
        public List<Crew> Crew { get; set; }
        /// <remarks/>
        public string Alt1 { get; set; }
        [Ignore]
        public object Alt2 { get; set; }
        /// <remarks/>
        [PrimaryKey]
        public int ID { get; set; }
        /// <remarks/>
        public int FPLID { get; set; }
        /// <remarks/>
        public string FlightlogID { get; set; }
        /// <remarks/>
        public string DEP { get; set; }
        /// <remarks/>
        public string DEST { get; set; }
        /// <remarks/>
        public System.DateTime STD { get; set; }
        /// <remarks/>
        public System.DateTime STA { get; set; }
        /// <remarks/>
        public string ACFTAIL { get; set; }
        /// <remarks/>
        public ushort ATCTime { get; set; }
        /// <remarks/>
        public string ATCCtot { get; set; }
        /// <remarks/>
        public ushort ATCEET { get; set; }
        /// <remarks/>
        public string TOA { get; set; }
        /// <remarks/>
        public string ATCID { get; set; }
        /// <remarks/>
        public System.DateTime LastEdit { get; set; }
        public bool IsRecalculated
        {
            get
            {
                //return this.ID % 2 != 0;
                if (string.IsNullOrEmpty(FlightlogID))
                {
                    return false;
                }
                return FlightlogID.EndsWith("-R");
                
            }
        }
        public string FirstListColumn
        {
            get
            {
                return GetFlightIdToShow(FlightlogID);
            }
        }
        public Thickness FlightListItemGridTopPadding
        {
            get
            {
                return CommonStatic.Instance.FlightListItemGridVertPadding;
            }
        }
        public Thickness FlightListItemGridRightPadding
        {
            get
            {
                return CommonStatic.Instance.FlightListItemGridRightPadding;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public static string GetFlightIdToShow(string flightlogId)
        {
            if (string.IsNullOrEmpty(flightlogId))
            {
                return string.Empty;
            }
            var view01 = string.Empty;
            var flightId1 = flightlogId;
            var flightId = flightId1.IndexOf("-", StringComparison.Ordinal) != -1 ? flightId1.Split('-') : new[] { flightId1 };
            var firstBit = flightId[0];
            if (firstBit.IndexOf("(", StringComparison.Ordinal) != -1)
            {
                //Step 2
                var fLogIDbits = firstBit.Split('(');

                firstBit = fLogIDbits[0];
                var secondBit = fLogIDbits[1];

                view01 = secondBit.TrimEnd(')');
            }
           
           
            var view1 = firstBit;
            var result = view1;
            if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(view01))
            {
                result += "\n";
            }
            return result + view01;
        }
        public string Dep_Dest
        {
            get
            {
                return (DEP ?? "") + "-" + (DEST ?? "");
            }

        }
        public Xamarin.Forms.Thickness FlightListColumn2Padding
        {
            get
            {
                return CommonStatic.Instance.FlightListColumn2Padding;
            }
        }
        public string PreparedStd
        {
            get
            {
                return CommonStatic.GetPreparedStd(STD);
            }
        }
        [Ignore]
        public string MarkImagePath { get; set; }
        [Ignore]
        public double MarkImageHeight
        {
            get
            {
                return CommonStatic.Instance.GetMediumHeight ;
            }
        }
        [Ignore]
        public double MarkImageWidth
        {
            get
            { // list_arrow: 15x23, so W = 0.65 * H

                return CommonStatic.Instance.GetMediumHeight * 0.65;
            }
        }
        [Ignore]
        public double RowMarkHeight
        {
            get
            {
                return CommonStatic.Instance.GetMediumHeight * 2.5;
            }
        }
        [Ignore]
        public bool IsMarkerVisible
        {
            get
            {
                if (string.IsNullOrEmpty(MarkImagePath))
                {
                    return false;
                }
                return MarkImagePath.ToLower().Contains("vert_mark.png");
            }
        }
        
        [Ignore]
        public bool IsImageVisible
        {
            get
            {
                return !IsMarkerVisible;
            }
        }
        public string DestAirportFull
        {
            get;set;
        }
        public string DepAirportFull
        {
            get; set;
        }

        private bool _isSelected ;
        [Ignore]
        public bool IsSelected
        {
             get { return _isSelected; }
            set
            {
                  _isSelected = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsSelected)));
            }
        } 
    }

    /// <remarks/>
    [Table("Crew")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public class Crew
    {
        /// <remarks/>
       [PrimaryKey]
        [AutoIncrement]
        public int CID{ get; set; }

        public string ID { get; set; }
        /// <remarks/>
        public string CrewType { get; set; }
        /// <remarks/>
        public string CrewName { get; set; }
        /// <remarks/>
        public string Initials { get; set; }
        /// <remarks/>
        public string GSM { get; set; }
        [ForeignKey(typeof(FlightListItem))] 
        public int FlightId { get; set; }
        //[ManyToOne]
        //FlightListItem flightListItem { get; set; }

    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetFlightListSearchResponseGetFlightListSearchResultResponse
    {
        /// <remarks/>
        public bool Succeed { get; set; }
    }

}
