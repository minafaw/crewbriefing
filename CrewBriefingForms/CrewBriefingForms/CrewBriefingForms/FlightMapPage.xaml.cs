﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;
using CrewBriefingForms.Models;
using Xamarin.Forms.Xaml;


namespace CrewBriefingForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlightMapPage 
    {
        public FlightMapPage(int passedItemId, Flight flightDetails)
        {
            InitializeComponent();
            Utils.SetBackNavBar(this, panelNav, FlightListItem.GetFlightIdToShow(flightDetails.FlightLogID));
			if (Device.RuntimePlatform == Device.UWP)
            {
                //Xamarin.Forms.Maps.Map.In .Init(CrewBriefingForms.App.GoogleMapApiKey);
            }
            /*
            CustomPin pin = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.Place,
                    Position = new Position(37.79752, -122.40183),
                    Label = "Xamarin San Francisco Office",
                    Address = "394 Pacific Ave, San Francisco CA"
                },
                Id = "Xamarin",
                Url = "http://xamarin.com/about/"
            };

            flightMap.CustomPins = new List<CustomPin> { pin };
            flightMap.Pins.Add(pin.Pin);
            flightMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(37.79752, -122.40183), Distance.FromMiles(1.0)));
            */
            CustomPin pin = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.Generic,
                    Label = "Test1",
                    Position = new Position(37.79752, -122.40183)
                },
                Id = "Xamarin",
                Url = "http://xamarin.com/about/"
            };
            
            CustomPin pin1 = new CustomPin
            {
                Pin = new Pin
                {
                    Type = PinType.Generic,
                    Label = "Test11",
                    Position = new Position(38.79752, -123.40183)
                },
                Id = "Xamarin1",
                Url = "http://xamarin.com/about/"
            };


            //PolylineOptions line = new PolylineOptions();
            //MKPolygonRenderer polygonRenderer;
            //var polygon = new MapPolygon();
            //MapPolyline polyline = new MapPolyline();
            /*
            foreach (var data in Datas)
            {
                PosList.Add(new BasicGeoposition()
                {
                    Latitude = Convert.ToDouble(data.latitude),
                    Longitude = Convert.ToDouble(data.longitude)
                });
            }

            line.Path = new Geopath(poslist);

            MapFlight.MapElements.Add(line);
            */
            //flightMap.Pins.Add(pin.Pin);

            
            //List<CustomPin> pins = new List<CustomPin>();
            /*
            foreach (RoutePoint p in flightDetails.RoutePoints)
            {
                Pin _p = new Pin
                {
                    Type = PinType.SavedPin,
                    Label = p.IDENT,
                    Position = new Position(p.LAT, p.LON)
                };
                flightMap.Pins.Add(_p);

            }
            */

            /*
            flightMap.CustomPins = pins; // new List <CustomPin> { pin, pin1 };
            */
            flightMap.FlightInfo = flightDetails;
            flightMap.parentPage = this;

            if (flightDetails.RoutePoints != null)
            {
                if (flightDetails.RoutePoints.Length > 0)
                {
                    double minLat = flightDetails.RoutePoints[0].LAT, minLon = flightDetails.RoutePoints[0].LON, maxLat = minLat, maxLon = minLon;
                    foreach (RoutePoint p in flightDetails.RoutePoints)
                    {
                        minLat = Math.Min(minLat, p.LAT);
                        minLon = Math.Min(minLon, p.LON);
                        maxLat = Math.Max(maxLat, p.LAT);
                        maxLon = Math.Max(maxLon, p.LON);
                    }
                    flightMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position((minLat + maxLat) * 0.5, (minLon + maxLon) * 0.5),
                        Distance.FromMiles(
                            Math.Max(
                                Utils.DistanceTo(minLat, minLon, maxLat, minLon),
                                Utils.DistanceTo(minLat, minLon, minLat, maxLon)
                            ) / 2.0
                            )
                           )
                        );
                }
            }

            //flightMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(37.79752, -122.40183), Distance.FromMiles(150.0)));
            //flightMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(0, 0), Distance.FromMiles(150.0)));
            //            flightMap.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(37.79752, -122.40183), Distance.FromMiles(1.0)));
        }
        /*
        private Geocoordinate MidPoint(Geocoordinate posA, Geocoordinate posB)
        {
            Geocoordinate midPoint = new Geocoordinate();

            double dLon = DegreesToRadians(posB.Longitude - posA.Longitude);
            double Bx = Math.Cos(DegreesToRadians(posB.Latitude)) * Math.Cos(dLon);
            double By = Math.Cos(DegreesToRadians(posB.Latitude)) * Math.Sin(dLon);

            midPoint.Latitude = RadiansToDegrees(Math.Atan2(
                         Math.Sin(DegreesToRadians(posA.Latitude)) + Math.Sin(DegreesToRadians(posB.Latitude)),
                         Math.Sqrt(
                             (Math.Cos(DegreesToRadians(posA.Latitude)) + Bx) *
                             (Math.Cos(DegreesToRadians(posA.Latitude)) + Bx) + By * By)));
            // (Math.Cos(DegreesToRadians(posA.Latitude))) + Bx) + By * By)); // Your Code

            midPoint.Longitude = posA.Longitude + RadiansToDegrees(Math.Atan2(By, Math.Cos(DegreesToRadians(posA.Latitude)) + Bx));

            return midPoint;
        }
        */
    }

    public class CustomPin
    {
        public Pin Pin { get; set; }

        public string Id { get; set; }

        public string Url { get; set; }
    }
    
    public class CustomMap : Map
    {
//        public List<CustomPin> CustomPins { get; set; }
        public Flight FlightInfo { get; set; }
        public ContentPage parentPage { get; set; }
    }
    


}
