﻿using CrewBriefingForms.Models;
using CrewBriefingForms.ViewModel;

namespace CrewBriefingForms.Views
{
    public partial class DetialFlightCell 
    {
        public DetialFlightCell()
        {
            InitializeComponent();
            BindingContext = new FlightDetialListItem();
        }
    }
}
