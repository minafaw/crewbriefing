﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms.Views
{
   
    public partial class FlightDetials
    {
        public FlightDetials(FlightListItem item)
        {
            InitializeComponent();
	        Title = item.FlightlogID;

			panelFlightDetails.SetItem(item.ID, panelNav);
        }
    }
}