﻿using CrewBriefingForms.NavigationService;
using CrewBriefingForms.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms.Views
{
    public partial class FlightDetialContent 
    {
        public FlightDetialContent()
        {
            InitializeComponent();
	       //NavigationPage.SetBarBackground(this , Color.Green);
        }

        public void SetItem(int flightId, StackLayout paneLayout)
        {
            BindingContext = new FlightDetailsViewModel(flightId);
        }
    }
}