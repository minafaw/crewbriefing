﻿using CrewBriefingForms.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage 
    {
        //private readonly HybridWebView hybrid;
        public Func<string, Task<string>> EvaluateJavascript { get; set; }

        public AboutPage(string pageTitle)
        {
            InitializeComponent();
            Utils.SetBackNavBar(this, panelNav, pageTitle);

            var ver = DependencyService.Get<IAppVersion>();
            var versionBuild = ver.GetAppVersionBuild();
            var javaScriptCode = "document.getElementById('versionCode').innerHTML = '" + "Version: " + versionBuild + "';";

            webView.Source = LoadHtmlFileFromResource();
            webView.Navigated += (sender, e) => {
                webView.Eval(javaScriptCode);
            };
        }

        private static HtmlWebViewSource LoadHtmlFileFromResource()
        {
            var source = new HtmlWebViewSource();
            var assembly = typeof(AboutPage).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("CrewBriefingForms.about_us.html");
            using (var reader = new StreamReader(stream))
            {
                source.Html = reader.ReadToEnd();
            }
            return source;
        }


    }
}
