﻿using System;
using System.Resources;
using System.Threading.Tasks;
using Xamarin.Forms;

using CrewBriefingForms.Database;
using CrewBriefingForms.Models;
using CrewBriefingForms.Controls;
using System.Runtime.CompilerServices;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlightListPage 
    {
        private TapGestureRecognizer _tapSearch;
        TapGestureRecognizer _tapRefresh;
        TapGestureRecognizer _tapMenu;
        public FlightListPage(string username, string password)
        {
            InitializeComponent();
            panelFlightList.SetBusy(Strings.Attempting_login);
            panelFlightList.ParentPage = this;
            CustomIni();
            Device.BeginInvokeOnMainThread(async () => {
                await Login(username, password);
            });
        }
        public FlightListPage()
        {
            InitializeComponent();
            CustomIni();

            panelFlightList.ListViewFlights.IsVisible = false;
            //AddRefresh();

            panelFlightList.SetBusy(Strings.start_update);
            panelFlightList.ParentPage = this;

            Device.BeginInvokeOnMainThread( async () => {
                await panelFlightList.UpdateList(false);
            });
        }
        private void CustomIni()
        { 
            NavigationPage.SetHasNavigationBar(this, false);
            _tapSearch = new TapGestureRecognizer();
            _tapSearch.Tapped += async (sender, e) =>
            {
                await ((MenuPage)Utils.md.Master).LoadFilterSettings();
                if (!Utils.md.IsPresented)
                {
                    Utils.ShowFilter = true;
                }
                Utils.md.IsPresented = !Utils.md.IsPresented;
            };

            _tapRefresh = new TapGestureRecognizer();
            _tapRefresh.Tapped += async (sender, e) =>
            {
                panelFlightList.SetBusy(Strings.loading);
                await panelFlightList.UpdateList(true);
            };

            _tapMenu = new TapGestureRecognizer();
            _tapMenu.Tapped += (sender, e) =>
            {
                if (!Utils.md.IsPresented)
                {
                    Utils.ShowFilter = false;
                }
                Utils.md.IsPresented = !Utils.md.IsPresented;
            };
            panelFlightListTop.HeightRequest = CommonStatic.Instance.MediumHeight * 2;
            Utils.SetCenterText(panelFlightListTop, Strings.flights, 0.7, _tapSearch, _tapRefresh, _tapMenu, false);
            panelFlightList.AssignedWidth = 750 * CommonStatic.Instance.ScreenCoef - 10;
        }
        public void SaveFilterAndUpdateListWait(MenuPage menuPage, bool forceRefresh)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                SetBusyExt(Strings.loading);
                await App.Sleep(500); // no other way to make sure that 'Busy' indication is on screen. ForceNoBusy / Onchage IsRunning does not help
                panelFlightList.ForceNoBusy = false;
                await menuPage.SaveFilterSettings();
                //await FilterSettings.LoadFilterFromDb();
                await panelFlightList.UpdateList(forceRefresh);
            });
        }
        public void SetBusyExt(string text)
        {
            
            Device.BeginInvokeOnMainThread(() =>
            {
               panelFlightList.ForceNoBusy = true;
                panelFlightList.SetBusy(text);
                ForceLayout();
            });
        }
        public async Task UpdateList(bool forceRefresh)
        {
            panelFlightList.SetBusy(Strings.loading);
            ForceLayout();
            await panelFlightList.UpdateList(forceRefresh);
        }
        private async Task Login(string username, string password)
        {
            var response = await Utils.TryLogin(username, password, false, false);
            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                await panelFlightList.UpdateList(false);
            }
            else
            {
                var isWrongCredentials = response.Code == Constants.WebApiErrorCode.WRONG_CREDENTIALS;
                Utils.GoToPage(this, new LoginPage(!isWrongCredentials, false, true));
            }
        }
    }
}
