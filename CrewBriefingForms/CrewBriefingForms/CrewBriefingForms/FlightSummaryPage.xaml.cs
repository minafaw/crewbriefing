﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FlightSummaryPage 
    {
        public FlightSummaryPage(string flightSummary, string flightNo)
        {
            InitializeComponent();
            labelInfo.IsVisible = !string.IsNullOrEmpty(flightSummary);
            labelInfo.Text = flightSummary;
            panelNoInfo.IsVisible = !labelInfo.IsVisible;
            // https://developer.xamarin.com/guides/xamarin-forms/user-interface/text/fonts/

            string fontFamily = Utils.SummaryFontFamily;
            string fontFamilyBold = Utils.SummaryFontFamilyBold;

            labelNoinfoTitle.FontFamily = fontFamilyBold;
            labelNoinfoText.FontFamily = fontFamily;
            labelInfo.FontFamily = fontFamily;

            Utils.SetBackNavBar(this, panelNav, flightNo);
        }
    }
}
