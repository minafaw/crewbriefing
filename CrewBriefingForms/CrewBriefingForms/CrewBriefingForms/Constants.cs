﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrewBriefingForms

{
    public static class Constants
    {
        public static readonly string PartnerUserName = "TeoIntl";
        public static readonly string PartnerPassword = "V5N9K5YH7V";

        public static readonly string ClientId = "CB6173";
        public static readonly string ClientKey = "7dba38bbfffbf974b6f7976ece8f43f8";

        public static readonly string ApiKey = "62FA4115-90FB-4908-910D-DDDA4A94E433";
        public static readonly string AppName = "CB_MOBILE";
       // public static readonly string IOsAppVersion = "1.7.3";

	    public static readonly string UserNameKey = "UserNameKey";
	    public static readonly string PasswordKey = "PasswordKey";
	    public static readonly string QuickLoginKey = "QuickLoginKey";

		//Timer is set to go off one time after 120 seconds
		public static readonly int TimeOutWebService = 120000; //60000,  120000

        public static readonly int PopOverWidthIpad = 289;
        public static readonly int PopOverHeightForIos6 = 640; //Because it's navigation bar height is small
        public static readonly int PopOverHeightForIos7 = 395;

        public static readonly int MainMenuHeightIpad = 430;
        public static readonly string ServiceUrlKey = "serviceURL";

        public enum WebApiErrorCode { SUCCESS = -1, WRONG_CREDENTIALS = 1, UNDEFINED = 2, NULL_ERROR = 3, TIME_OUT = 4,  NO_NET = 5, INVALID_SESSION = 6 };

        //Will be used for DBPdf
        public enum PdfDocDownloadingState : int
        {
            NotDownloaded,
            Downloaded,
            Downloading,
            NotAvailable,
        }

        //Will be used for Sort Filter configuration
        public enum SortFilterType
        {
            FlightName,
            DEP,
            STD,
        }

        //Will be used for Sort Filter configuration
        public enum SortFilterValue
        {
            AtoZ,
            ZtoA,
            EarliestFirst, //For STD Option
            LatestFirst,   //For STD Option
        }

        //Will be used for STD Filter configuration
        public enum STD : int
        {
            Yesterday = 1,
            Now = 2,
            NextSixHours = 3 ,
            NextTwelveHours = 4 ,
            Tomorrow = 5,
            OneWeek = 6,
            TwoWeeks = 7,
            OneMonth = 8,
            Any = 9
        }

        public enum ClassType
        {
            Tail,
            Crew,
            STD,
            Sort
        }

        public enum SwitchClassType
        {
            Tail = 0,
            Crew = 1,
            STD = 2,
        }

        public enum CrewType
        {
            CMD,
            COP,
            CA1,
            CA2,
            CA3,
            CA4,
        }

    }
}
