﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CrewBriefingForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HelpPage 
    {
        private int currentPage = 0;
        private Image[] dotImages = new Image[4];
        private string[] txtHeader = new string[4];
        private string[] txtContent = new string[4];
        private double imageSize = 20;
        public HelpPage()
        {
            InitializeComponent();
            Utils.SetBackNavBar(this, panelNav, "Help");

            if (Device.RuntimePlatform == Device.iOS)
            {
                this.Padding = new Thickness(0, 20, 0, 0);
            }
            mainStackSwipe.OnSwipeRight += panel_SwipeRightustom;
            mainStackSwipe.OnSwipeLeft += panel_SwipeLeftCustom;

            TapGestureRecognizer tapEmail = new TapGestureRecognizer();
            tapEmail.Tapped += async (object sender, EventArgs e) =>
            {
                labelEmail.BackgroundColor = Color.Gray;
                await App.Sleep(250);
                labelEmail.BackgroundColor = Color.Transparent;
                Device.OpenUri(new Uri("mailto:" + labelEmail.Text));
            };
            labelEmail.GestureRecognizers.Add(tapEmail);

            //TapGestureRecognizer tapPhone = new TapGestureRecognizer();
            //tapPhone.Tapped += async (object sender, EventArgs e) =>
            //{
            //    labelPhone.BackgroundColor = Color.Gray;
            //    await App.Sleep(250);
            //    labelPhone.BackgroundColor = Color.Transparent;

            //    Device.OpenUri(new Uri("tel:" + labelPhone.Text.Replace(" ", string.Empty)));
            //};
            //labelPhone.GestureRecognizers.Add(tapPhone);
            

            /*
            double imagePct = Utils.IsTablet() ? 0.10 : 0.65;
            AbsoluteLayout.SetLayoutBounds(panelImage, new Rectangle(0, 0, 1, imagePct));
            AbsoluteLayout.SetLayoutFlags(panelImage, AbsoluteLayoutFlags.All);

            AbsoluteLayout.SetLayoutBounds(panelContent, new Rectangle(0.5, 0, Utils.IsTablet() ? 0.30 : 0.65, 1));
            AbsoluteLayout.SetLayoutFlags(panelContent, AbsoluteLayoutFlags.All);
            */
            if (Utils.IsTablet())
            {
                mainGrid.RowDefinitions[1].Height = new GridLength(45, GridUnitType.Star);
                mainGrid.RowDefinitions[2].Height = new GridLength(50, GridUnitType.Star);

                mainGrid.ColumnDefinitions[0].Width = new GridLength(35, GridUnitType.Star);
                mainGrid.ColumnDefinitions[1].Width = new GridLength(30, GridUnitType.Star);
                mainGrid.ColumnDefinitions[2].Width = new GridLength(35, GridUnitType.Star);
            }
            else
            {
                mainGrid.RowDefinitions[0].Height = new GridLength(3, GridUnitType.Star);
                mainGrid.RowDefinitions[1].Height = new GridLength(45, GridUnitType.Star);
                mainGrid.RowDefinitions[2].Height = new GridLength(50, GridUnitType.Star);

                mainGrid.ColumnDefinitions[0].Width = new GridLength(5, GridUnitType.Star);
                mainGrid.ColumnDefinitions[1].Width = new GridLength(90, GridUnitType.Star);
                mainGrid.ColumnDefinitions[2].Width = new GridLength(5, GridUnitType.Star);

            }

            imageSize = 10;
            if (!Utils.IsTablet())
            {
                imageSize = 6;
            }


            panelDots.Spacing = imageSize * 1.4;
            for (int i=0; i<4; i++)
            {
                dotImages[i] = new Image()
                {
                    Aspect = Aspect.AspectFit,
                    HeightRequest = imageSize,
                    WidthRequest = imageSize
                };
                dotImages[i].Source = Utils.ImageFolder + "dot_normal.png";
                panelDots.Children.Add(dotImages[i]);
                // selected size = normal * 1.25
            }
            var assembly = typeof(CrewBriefingForms.Strings).GetTypeInfo().Assembly;
            ResourceManager manager = new ResourceManager("CrewBriefingForms.Strings", assembly);
            //panelDotsWrap.

            TapGestureRecognizer tap = new TapGestureRecognizer();
            tap.Tapped += (object sender, EventArgs e) =>
            {
                currentPage++;
                if (currentPage > 3)
                {
                    currentPage = 0;
                }
                setSelected();
            };
            panelDotsWrap.GestureRecognizers.Add(tap);
            panelDots.GestureRecognizers.Add(tap);

            for (int i = 1; i <= 4; i++)
            {
                txtHeader[i - 1] = manager.GetString("help_head" + i.ToString());
                txtContent[i - 1] = manager.GetString("help_text" + i.ToString());
            }
            setSelected();

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (Utils.md != null)
            {
                Utils.md.IsGestureEnabled = false;
            }
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (Utils.md != null)
            {
                Utils.md.IsGestureEnabled = true;
            }
        }

        private void panel_SwipeLeftCustom(object sender, EventArgs e)
        {
            currentPage++;
            if (currentPage > 3)
            {
                currentPage = 0;
            }
            setSelected();
        }

        private void panel_SwipeRightustom(object sender, EventArgs e)
        {
            currentPage--;
            if (currentPage < 0)
            {
                currentPage = 3;
            }
            setSelected();
        }

        private void setSelected()
        {
            panelPage1_1.IsVisible = currentPage == 0;
            //panelPage1_2.IsVisible = panelPage1_1.IsVisible;
            //labelOr.IsVisible = panelPage1_1.IsVisible;

            image.Source = Utils.ImageFolder + "image_help_" + ((currentPage + 1).ToString()) + ".png";
            
            for (int i=0; i<4; i++)
            {
                if (i == currentPage)
                {
                    dotImages[i].HeightRequest = imageSize * 1.25;
                    dotImages[i].WidthRequest = imageSize * 1.25;
                    dotImages[i].Source = Utils.ImageFolder + "dot_selected.png";
                }
                else
                {
                    dotImages[i].HeightRequest = imageSize ;
                    dotImages[i].WidthRequest = imageSize;
                    dotImages[i].Source = Utils.ImageFolder + "dot_normal.png";
                }
            }
            labelHeader.Text = txtHeader[currentPage];
            labelText.Text = txtContent[currentPage];
            //currentPage
        }
    }
}