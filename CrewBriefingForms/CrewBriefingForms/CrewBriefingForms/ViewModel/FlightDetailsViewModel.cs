﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;
using System.Windows.Input;
using CrewBriefingForms.Base;
using CrewBriefingForms.Database;
using CrewBriefingForms.Helper;
using CrewBriefingForms.Interfaces;
using CrewBriefingForms.Models;
using CrewBriefingForms.ModelsFlight;
using CrewBriefingForms.WebService;
using Xamarin.Forms;

namespace CrewBriefingForms.ViewModel
{
    public class FlightDetailsViewModel : BaseViewModel
    {
		public ICommand PressItemCommand { get; private set; }

		public FlightDetailsViewModel(int flightId)
        {
            _myItems = new List<FlightDetialListItem>();

            PressItemCommand = new Command(async (object obj) =>
            {
                await HandleItemPressAction(obj);
            });

            Device.BeginInvokeOnMainThread(async () =>
            {
                HeaderVisibility = true;
                await GetFlightDetialAsync(flightId);
                HeaderVisibility = false;
            });


            Task.Run(async () =>
            {
                await CreatelDetialList(flightId);
            });
        }

		async Task HandleItemPressAction(object obj)
		{
			var selectedItem = obj as FlightDetialListItem;
			if (selectedItem == null) return;

			if (selectedItem.Title.Equals(Strings.route_map)){
				
			}else if(selectedItem.Title.Equals(Strings.flight_summary)){
				//var summeryPage = new FlightSummaryPage(flightDetails.FlightSummary, FlightListItem.GetFlightIdToShow(flightDetails.FlightLogID));
				
			}
			if (selectedItem.DocType == null) return;
            var saveLoad = DependencyService.Get<ISaveAndLoad>();
			var storedPath = await DB_PCL.GetStoredPdfPath(selectedItem.FlightId, (PdfDocType)selectedItem.DocType);
            if (!string.IsNullOrEmpty(storedPath))
            {
                try
                {
					await saveLoad.OpenFileWithDefaultOSViewer(storedPath, selectedItem.Title);
                    return;
                }
                catch (Exception exc)
                {
                    Utils.ShowTemporayMessage(ToastType.Error, "Error open file. " + exc.Message);
                    return;
                }
            }
        }

        private bool _headerVisibility;
        public bool HeaderVisibility
        {
            get => _headerVisibility;
            set
            {
                _headerVisibility = value;
                RaisePropertyChanged();
            }
        }
        private Flight _flightDetial;
        public Flight FlightDetial
        {
            get => _flightDetial;
            set
            {
                _flightDetial = value;
                RaisePropertyChanged();
            }
        }
     
        private List<FlightDetialListItem> _myItems ;
        public List<FlightDetialListItem> MyItems
        {
            get => _myItems;
            set
            {
                _myItems = value;
                RaisePropertyChanged();
            } 
        }
        
        private async Task GetFlightDetialAsync(int flightId)
        {
            // check if flight exists in database
            FlightDetial = await DB_PCL.GetFilghtDb(flightId);
            // if not get the data from webservice 
            if (FlightDetial == null)
            {
                FlightDetial = await GeFlightFromWs(flightId);
            }

            if (FlightDetial == null )
            {
                // if flight detial still null 
                return;
            }

                var i = 0;
                foreach (var icao in new[] { FlightDetial.DEP, FlightDetial.DEST })
                {
                    if (string.IsNullOrEmpty(icao)) continue;

                    var airport = await DB_PCL.GetAirportDb(icao);
                    if (airport == null)
                    {
                        var ar = new GetAirportRequest
                        {
						    SessionID = App.SessionId,
                            ICAO = icao
                        };

                        var responseAirport = await EdfService.GetAirport(ar);
                        if (responseAirport.Code == Constants.WebApiErrorCode.SUCCESS)
                        {
                            airport = EdfService.ParseAirport(responseAirport.Message);
                            await DB_PCL.InsertOrUpdateAirport(airport);
                        }
                        else
                        {
                            Utils.ShowTemporayMessage(ToastType.Error, "Error receive Airport data for '" + icao + "'");
                        }

                    }
                    if (airport != null)
                    {
                        var extraString = "";
                        if (Device.RuntimePlatform == Device.UWP)
                            extraString = Environment.NewLine;
                        var airportName = (airport.Name ?? string.Empty).Replace("/", "/ " + extraString);
                        if (i == 0)
                        {
                            FlightDetial.Arports4ShowDEP = airportName;
                            if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.iOS)
                            {
							    FlightDetial.DepDstLineBreakMode = LineBreakMode.WordWrap;
                            }
                        }
                        else
                        {
                            FlightDetial.Arports4ShowDST = airportName;
                        }
                    }

                    i++;
                }
                // TODO Refine this part 
                //if (_parentPage is TabletMainPage)
                //{
                //    (_parentPage as TabletMainPage)?.ShowMapOrSummary(flightDetails, false);
                //    _firstLoad = true;
                //}
            

            
        }
        private async Task<Flight> GeFlightFromWs(int flightId)
        {
            Flight flightDetails;
            var checkSessionOk = await Utils.CheckSessionAndRefreshIfRequired(true);
            if (!checkSessionOk || !Utils.IsInternet())
            {
                //await this.Navigation.PopModalAsync();
                if (Device.Idiom == TargetIdiom.Tablet)
                {
                    //clear binding context 
                    // show error toast 
                    //gridDetails.BindingContext = null;
                    //await DrawDocList(null, passedItemId);
                }
                else
                {
                    Utils.GoToPrevPage();
                }

                return null;
            }
            var response = await EdfService.GetFlight(CreateFlightRequest(flightId));
            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                flightDetails = EdfService.ParseFlight(response.Message);
                if (flightDetails != null)
                {
                    await DB_PCL.InsertOrUpdateFlight(flightDetails);
                }
            }
            else // no details in DB and can't get from Inet - go back
            {
                Utils.CheckWebResponse(response, null, true);
                return null;
            }
            return flightDetails;
        }
        private FlightRequest CreateFlightRequest(int passedItemId)
        {
            var fr = new FlightRequest()
            {
                SessionID = App.SessionId,
                FlightID = passedItemId,
                Messages = false,
                Metar = false,
                Notams = false,
                Taf = false
            };
            return fr;
        }
        private async Task CreatelDetialList(int passedItemId)
        {
            var assembly = typeof(Strings).GetTypeInfo().Assembly;
            var manager = new ResourceManager("CrewBriefingForms.Strings", assembly);

            var items = new List<FlightDetialListItem>();

            var routeItem = new FlightDetialListItem
            {
                Title = Strings.route_map,
				Image1Path = Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false

            };
            var summeryItem = new FlightDetialListItem
            {
                Title = Strings.flight_summary,
                Image1Path = Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false

            };
            items.Add(routeItem);
            items.Add(summeryItem);

            foreach (var itemDocType in AppConstants.DocsOrder)
            {
                var result = await CheckDocExists(itemDocType, passedItemId);
                bool downloadedExists = result.Item1;
                DateTime? downloadDate = result.Item2;

                var item = new FlightDetialListItem
                {
                    Title = manager.GetString("doc_title_" + itemDocType),
                    Image1Path = Utils.ImageFolder + (downloadedExists ? "redownload_doc.png" : "icon_download.png"),
                    DownloadDate = downloadDate,
                    ItemDownloaded = downloadedExists,
                    DocType = itemDocType,
                    FlightId = passedItemId
                };
                if (itemDocType == PdfDocType.UploadedDocuments)
                {
                    item.Title = downloadedExists
                        ? Strings.doc_title_UploadedDocumentsShort
                        : Strings.doc_title_UploadedDocuments;
                }
                if (itemDocType == PdfDocType.CombineDocuments)
                {
                    item.Title = downloadedExists
                        ? Strings.doc_title_CombineDocumentsShort
                        : Strings.doc_title_CombineDocuments;
                }
                items.Add(item);
            }

            MyItems =  items; 
        }
        private async Task<(bool downloadedExists, DateTime? downloadDate)> CheckDocExists(PdfDocType docType, int passedItemId)
        {
            var downloadedExists = false;
            DateTime? downloadDate = null;
            // var storedDocuments = new List<PdfDocTypeWithDate>();
            var storedDocuments = await DB_PCL.GetStoredPdf(passedItemId);
            foreach (var pdd in storedDocuments)
            {
                if (pdd.docType == docType)
                {
                    downloadedExists = true;
                    downloadDate = pdd.localDate;
                }
            }
            return (downloadedExists, downloadDate);
        }
    }
}
