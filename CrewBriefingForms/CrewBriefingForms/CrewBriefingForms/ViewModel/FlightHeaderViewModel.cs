﻿using System;
using CrewBriefingForms.Base;
using Xamarin.Forms;

namespace CrewBriefingForms.ViewModel
{
	public class FlightHeaderViewModel : BaseModel
    {
		public ImageSource AirPlaneImage
        {
            get => ImageSource.FromFile("aeroplane.png");

        }

    }
}
