﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrewBriefingForms.Models;

namespace CrewBriefingForms.Helper
{

   public  class AppConstants
    {

        public static readonly PdfDocType[] DocsOrder =
        {
            PdfDocType.Messages,   //0
            PdfDocType.Logstring,  //1
            PdfDocType.WX,         //2
            PdfDocType.NOTAMs,     //3
            PdfDocType.ATC,        //4
            PdfDocType.WindT,      //5
            PdfDocType.SWX,        //6
            PdfDocType.CrossSection,  // 7

            PdfDocType.UploadedDocuments,  //8
            PdfDocType.CombineDocuments    //9
        };
    }
}
