﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CrewBriefingForms.Helper
{
    public static class PlatformSpecialRequirment
    {

        // how height of text can be applie to grid's row in different OS
        public static Double GetTextHightInGrid()
        {
            var coefHeight = 1.0;
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    coefHeight = 2;
                    break;
                case Device.iOS:
                    coefHeight = 1;
                    break;
				case Device.UWP:
                    coefHeight = 1.2;
                    break;
            }
            return coefHeight;
        }
    }
}
