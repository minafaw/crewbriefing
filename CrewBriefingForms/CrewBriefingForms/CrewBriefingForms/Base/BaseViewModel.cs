﻿using System;
using System.Globalization;
using GalaSoft.MvvmLight;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace CrewBriefingForms.Base
{
    public class BaseViewModel : ViewModelBase
    {
        public INavigation Navigation { get; set; }
        public virtual void OnAppearing() { }
        public virtual void OnDisappearing() { }
        public bool IsConnected => CrossConnectivity.Current.IsConnected;

        public double SmallFontSize => CommonStatic.LabelFontSize(NamedSize.Small);

    }
}
