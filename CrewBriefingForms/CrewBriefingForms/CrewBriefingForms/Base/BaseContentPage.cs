﻿using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.Xaml;
using NavigationPage = Xamarin.Forms.NavigationPage;

namespace CrewBriefingForms.Base
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public class BaseContentPage : ContentPage
    {
        public BaseContentPage()
        {
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
            BackgroundColor =  Color.FromHex("#445C74");
			
        }

        protected override void OnAppearing()
        {
	        var navPage = Application.Current.MainPage as NavigationPage;
	        if (navPage != null)
	        {
		         navPage.BarBackgroundColor = Color.Green;
	        }

			base.OnAppearing();
        }
        protected enum  OrientationValue
       {
           Portrait ,
           Landscape
       }

        private double _oldWidth, _oldHeight;

        protected virtual void OrientationChanged(OrientationValue orientation){}

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);

            if (_oldWidth != width || _oldHeight != height)
            {
                OrientationChanged(width > height ? OrientationValue.Landscape : OrientationValue.Portrait);
                _oldWidth = width;
                _oldHeight = height;

            }
        }
    }
}
