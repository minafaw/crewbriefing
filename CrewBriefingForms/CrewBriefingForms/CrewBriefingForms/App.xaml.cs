﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using CrewBriefingForms.Database;
using CrewBriefingForms.Helper;
using CrewBriefingForms.NavigationService;
using CrewBriefingForms.Views;
using CrewBriefingForms.Setting;

namespace CrewBriefingForms
{
	public partial class App : Application
    {
        public static double ScreenHeight;
        public static double ScreenWidth;
        public static readonly string BinqMapApiKey = "3JF8CqrVCeBXPqggXdDE~wUrdyIKdGZ-VDUqgWnqZgg~AolBKlv38Abz3AQOQRixqNi7Tce_qFGxCY33OlLHhchnltKQHrXPVrkbwuJoRw90";
        
        // see https://www.bingmapsportal.com/Application
        public static bool GoogleMapInitialized = false;
        public static string SessionId { get; set; }

	    public static INavigationService NavigationService { get; } = new ViewNavigationService();


		// private static SQLiteAsyncConnection _database;
		//public static SQLiteAsyncConnection DataBase => _database ?? (_database = DataBase.GetConnection());

		public App()
        {

            InitializeComponent();

            MessagingCenter.Subscribe<App, string>(this, "PushNotification", (arg1, arg2) =>
            {
                Utils.ShowTemporayMessage(Interfaces.ToastType.Info, arg2);
            });
            IntiateApp();
	        RegisterPages();
                      
            if (DB_PCL.CheckDbExists())
            {
                Task.Run(async () =>
                {
                    var isOldDb = await DB_PCL.IsOldDb();
                    if (isOldDb)
                    {
                        await DB_PCL.ReCreateDatabase();
                    }

					var quickLogin = AppSetting.Instance.GetValueOrDefault(Constants.QuickLoginKey , false);
                    if (quickLogin)
                    {
						string username = AppSetting.Instance.GetValueOrDefault<string>(Constants.UserNameKey, null),
                            password = AppSetting.Instance.GetValueOrDefault<string>(Constants.PasswordKey, null);

                        if (!string.IsNullOrEmpty(username) &&
                            !string.IsNullOrEmpty(password))
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                Utils.md = new MasterDetailPage();
                                Utils.md.Master = new MenuPage(Utils.md);
                                Utils.md.Detail = Device.Idiom == TargetIdiom.Tablet ? new NavigationPage(new TabletMainPage(username, password)) : new NavigationPage(new FlightListPage(username, password));
                                Utils.md.IsPresented = false;
                                Utils.md.IsPresentedChanged += Utils.Md_IsPresentedChanged;
                                Utils.md.MasterBehavior = MasterBehavior.Popover;

                                MainPage = Utils.md;
                               // Utils.FixiOsTopPadding();


                            });
                            //MainPage = new FlightListPage(username, password);
                        }
                    }

                });

               
            }else{
                 DB_PCL.CreateDatabase();

            }
            MainPage = new LoginPage(false, false, false);  
            
        }

	    private void RegisterPages()
	    {
		    NavigationService.Configure(nameof(FlightListPage)     , typeof(FlightListPage));
		    NavigationService.Configure(nameof(LoginPage)          , typeof(LoginPage));
			NavigationService.Configure(nameof(FlightDetials)      , typeof(FlightDetials));
			NavigationService.Configure(nameof(HelpPage)           , typeof(HelpPage));
			NavigationService.Configure(nameof(TabletMainPage)     , typeof(TabletMainPage));
			NavigationService.Configure(nameof(FlightMapPage)      , typeof(FlightMapPage));
			NavigationService.Configure(nameof(FlightDetailsPage)  , typeof(FlightDetailsPage));
			NavigationService.Configure(nameof(MenuPage)           , typeof(MenuPage));

	    }

	    public void SetRootPage(string rootPageName)
	    {
		    var mainPage = ((ViewNavigationService) NavigationService).SetRootPage(rootPageName);
		    MainPage = mainPage;
	    }

	    private static void IntiateApp()
        {
            var textSizes = DependencyService.Get<ITextMeasuring>();

            var coefHeight = PlatformSpecialRequirment.GetTextHightInGrid();

            CommonStatic.SmallHeight = textSizes.calculateHeight("ASDFW", NamedSize.Small) * coefHeight;
            CommonStatic.Instance.MediumHeight = textSizes.calculateHeight("ASDFW", NamedSize.Medium) * coefHeight;
            CommonStatic.Instance.PicTailWidth = textSizes.calculateWidth("Tail:  ", NamedSize.Small);
            CommonStatic.Instance.DateTimeFormattedWidth = textSizes.calculateWidth(CommonStatic.GetFormatedDateTime(DateTime.Now), NamedSize.Small);
            if (Device.Idiom == TargetIdiom.Tablet)
            {
                CommonStatic.Instance.GetDetailsHeight = (CommonStatic.Instance.MediumHeight + CommonStatic.LabelFontSize(NamedSize.Medium) * 1.3 + CommonStatic.SmallHeight * 3) * 1.2;
            }
            else
            {
                CommonStatic.Instance.GetDetailsHeight = (CommonStatic.Instance.MediumHeight + CommonStatic.LabelFontSize(NamedSize.Medium) * 1.3 + CommonStatic.SmallHeight * 3) * 1.6;
            }
        }

        static ContentPage CreateContentPage(string text)
        {
            return new ContentPage { Title = text, Content = Link(text + ".sub") };
        }

        public static MasterDetailPage _mdPage;

        static Button Link(string name)
        {
            var button = new Button
            {
                Text = name,
                BackgroundColor = Color.FromRgb(0.9, 0.9, 0.9)
            };
            button.Clicked += delegate
            {
                _mdPage.Detail = new ContentPage
                {
                    Title = name,
                    Content = new Label { Text = name }
                };
                _mdPage.IsPresented = false;
            };
            return button;
        }
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }


        //public static FlightListItem FlightDetailsOnResume;
        //public static Page FlightDetailsOnResumePage;

        //protected override async void OnResume()
        //{
        //    if (FlightDetailsOnResumePage == null)
        //    {
        //        return;
        //    }
        //    var _p = FlightDetailsOnResumePage;
        //    (_p as FlightDetailsPage)?.SetBusy(true);

        //    var item = FlightDetailsOnResume;
        //    FlightDetailsOnResumePage = null;
        //    FlightDetailsOnResume = null;
        //    await Sleep(500);
        //    Utils.GoToPage(_p, new FlightDetailsPage(item), true); // just for repaint - CA-22
        //    var flightDetailsPage = _p as FlightDetailsPage;
        //    if (flightDetailsPage != null)
        //    {
        //        await Sleep(500);
        //        flightDetailsPage.SetBusy(false);
        //    }

        //}
        public static async Task Sleep(int ms)
        {
            await Task.Delay(ms);
        }

        
    }
}
