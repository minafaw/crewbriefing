﻿using System.Diagnostics;
using System;
using System.Xml.Serialization;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using CrewBriefingForms.Models;
using Xamarin.Forms;
using CrewBriefingForms.Interfaces;

namespace CrewBriefingForms.WebService.FretchURL
{
    public class FetchUrlService
    {
        private static readonly string SoapUrl = "http://services.airsupport.dk/AutomaticEndpointResolverService/Service.svc";        
        public static readonly string API_KEY = "3A3F7419-A92A-496D-AD7E-657F2E6BCD12";
        public static readonly string APP_NAME = "CB_MOBILE_NEXTGEN";
        public static readonly string APP_VERSION = "1.8.0.0";
        public static async Task<string> FetchUrl()
        {
            var result = string.Empty;
            var clientName = string.Empty;

            string xmlData = string.Format(Strings.data_fetch_url_request, API_KEY, APP_NAME, APP_VERSION);
            var response = await GetDataFromWebService(xmlData);
            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(EnvelopeFetchUrl));
                    using (TextReader reader = new StringReader(response.Message))
                    {
                        var resultWrapper = (EnvelopeFetchUrl)serializer.Deserialize(reader);
                        return resultWrapper.Body.ResolveServiceSingleCustomerResponse.Services.ServiceInfo.Url;
                    }
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(exc.Message);
                    Utils.ShowTemporayMessage(ToastType.Error, "Error parse: FetchUrl " + response.Message);
                    return string.Empty;
                }
            }
           
                Utils.ShowTemporayMessage(ToastType.Error, response.Message);
                return string.Empty;
            
        }
        private static TimeSpan _requestTimeout = new TimeSpan(0, 0, 0, 15); // 15 sec timeout
        public static async Task<WebServiceResponse> GetDataFromWebService(string requestXmltData)
        {

            var client = new HttpClient {Timeout = _requestTimeout};
            var request = new HttpRequestMessage(HttpMethod.Post, SoapUrl)
            {
                Content = new StringContent(requestXmltData,
                    Encoding.UTF8,
                    "text/xml")
            };
            request.Headers.Add("SOAPAction", "http://www.airsupport.dk/urlfetcher/IAutomaticEndpointResolverService/ResolveServices");
            var result = new WebServiceResponse { Code = Constants.WebApiErrorCode.NULL_ERROR };
            var startRequest = DateTime.Now;

            try
            {
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    result.Message = await response.Content.ReadAsStringAsync();
                    result.Code = Constants.WebApiErrorCode.SUCCESS;
                    return result;
                }
                var err = await response.Content.ReadAsStringAsync();
                EdfService.ParseErrorMessage(err, ref result);
                return result;

            }
            catch (System.Net.WebException webExeption)
            {
                string errorMessage;
                if (webExeption.Response != null)
                {
                    try
                    {
                        using (Stream stream = webExeption.Response.GetResponseStream())
                        {
                            StreamReader responseStream = null;
                            responseStream = new StreamReader(stream);

                            errorMessage = await responseStream.ReadToEndAsync();
                            EdfService.ParseErrorMessage(errorMessage, ref result);
                            return result;
                        }
                    }
                    catch
                    {
                    }
                }
                result.Message = "Unkonwn error";

            }
            catch (Exception exc)
            {
                TimeSpan ts = DateTime.Now - startRequest;
                if (ts.TotalMilliseconds >= _requestTimeout.TotalMilliseconds)
                {
                    result.Code = Constants.WebApiErrorCode.TIME_OUT;
                    result.Message = Strings.errot_request_timeout;
                }
                else {
                    result.Message = exc.Message;
                }

                result.Message = exc.Message;
            }
            return result;
        }

    }
    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false, ElementName = "Envelope")]
    public partial class EnvelopeFetchUrl
    {

        private EnvelopeBody bodyField;

        /// <remarks/>
        public EnvelopeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class EnvelopeBody
    {

        private ResolveServiceSingleCustomerResponse resolveServiceSingleCustomerResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.airsupport.dk/urlfetcher")]
        public ResolveServiceSingleCustomerResponse ResolveServiceSingleCustomerResponse
        {
            get
            {
                return this.resolveServiceSingleCustomerResponseField;
            }
            set
            {
                this.resolveServiceSingleCustomerResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.airsupport.dk/urlfetcher")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.airsupport.dk/urlfetcher", IsNullable = false)]
    public partial class ResolveServiceSingleCustomerResponse
    {

        private ResolveServiceSingleCustomerResponseServices servicesField;

        /// <remarks/>
        public ResolveServiceSingleCustomerResponseServices Services
        {
            get
            {
                return this.servicesField;
            }
            set
            {
                this.servicesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.airsupport.dk/urlfetcher")]
    public partial class ResolveServiceSingleCustomerResponseServices
    {

        private ResolveServiceSingleCustomerResponseServicesServiceInfo serviceInfoField;

        /// <remarks/>
        public ResolveServiceSingleCustomerResponseServicesServiceInfo ServiceInfo
        {
            get
            {
                return this.serviceInfoField;
            }
            set
            {
                this.serviceInfoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.airsupport.dk/urlfetcher")]
    public partial class ResolveServiceSingleCustomerResponseServicesServiceInfo
    {

        private string nameField;

        private string versionField;

        private string urlField;

        private object propertiesField;

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        public string Url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }

        /// <remarks/>
        public object Properties
        {
            get
            {
                return this.propertiesField;
            }
            set
            {
                this.propertiesField = value;
            }
        }
    }



}
