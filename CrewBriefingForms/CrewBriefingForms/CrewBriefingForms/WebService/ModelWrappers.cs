﻿using System.Xml.Serialization;
using CrewBriefingForms.Models;
using CrewBriefingForms.ModelsFlight;

namespace CrewBriefingForms.WebService
{
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceLoginRootWrapper
    {
        public WebServiceLoginBodyWrapper Body { get; set; }
    }
    public class WebServiceLoginBodyWrapper
    {
        [XmlElement(ElementName = "GetUserFilteredSessionID", Namespace = "http://crewbriefing.com/")]
        public Models.LoginRequest LoginData { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceLoginResponseRootWrapper
    {
        public WebServiceLoginResponseBodyWrapper Body { get; set; }
    }

    public class WebServiceLoginResponseBodyWrapper
    {
        [XmlElement(ElementName = "GetUserFilteredSessionIDResponse", Namespace = "http://crewbriefing.com/")]
        public Models.LoginResponse ResponseData { get; set; }
    }

    // --- FlightListSearch method request
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetFlightListSearchRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetFlightListSearchBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetFlightListSearchBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetFlightListSearch") ]
        public Models.WebServiceAuthentifidRequestDataWrapper method { get; set; }
    }

    // --- GetFlight method request
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetFlightRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetFlightBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetFlightBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetFlight")]
        public FlightRequest method { get; set; }
    }

    //--GetPDF_Logstring
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetPDF_LogstringRootWrapper 
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetPDF_LogstringBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetPDF_LogstringBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetPDF_Logstring")]
        public GetPDF_LogstringRequest method { get; set; }
    }

    //--GetAirport 
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetAirportRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetAirportBodyWrapper Body { get; set; }
    }
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetAirportBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetAirport")]
        public GetAirportRequest method { get; set; }
    }

    //--GetAirportList
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetAirportListRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetAirportListBodyWrapper Body { get; set; }
    }
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetAirportListBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetAirportList")]
        public GetAirportListRequest method { get; set; }
    }

    //--GetPDF_Messages
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetPDF_MessagesRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetPDF_MessagesBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetPDF_MessagesBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetPDF_Messages")]
        public GetPDF_MessagesRequest method { get; set; }
    }

    //--GetPDF_WX
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetPDF_WXRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetPDF_WXBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetPDF_WXBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetPDF_WX")]
        public GetPDF_WXRequest method { get; set; }
    }

    //--GetPDF_NOTAMs
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetPDF_NOTAMsRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetPDF_NOTAMsBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetPDF_NOTAMsBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetPDF_NOTAMs")]
        public GetPDF_WXRequest method { get; set; }
    }
    //--GetPDF_ATC
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetPDF_ATCRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetPDF_ATCBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetPDF_ATCBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetPDF_ATC")]
        public GetPDF_LogstringRequest method { get; set; }
    }

    //--GetFlightDocument
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetFlightDocumentRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetFlightDocumentBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetFlightDocumentBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetFlightDocument")]
        public GetFlightDocumentRequest method { get; set; }
    }
    //--GetPDF_FullPackage
    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Envelope")]
    public class WebServiceGetPDF_FullPackageRootWrapper
    {
        [XmlElement(Namespace = "http://www.w3.org/2003/05/soap-envelope", ElementName = "Body")]
        public WebServiceGetPDF_FullPackageBodyWrapper Body { get; set; }
    }

    [XmlRoot(Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class WebServiceGetPDF_FullPackageBodyWrapper
    {
        [XmlElement(Namespace = "http://crewbriefing.com/", ElementName = "GetPDF_FullPackage")]
        public GetPDF_FullPackageRequest method { get; set; }
    }



    public class GetAirportRequest
    {
        public string SessionID { get; set; }
        public string ICAO { get; set; }
    }
    public class GetAirportListRequest
    {
        public string SessionID { get; set; }
        public string ICAOList { get; set; }
    }

}
