﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using CrewBriefingForms.Models;
using CrewBriefingForms.ModelsFlight;
using CrewBriefingForms.ModelsAirport;
using System.Net.Http;
using CrewBriefingForms.Database;
using Envelope = CrewBriefingForms.Models.Envelope;
using System.Collections.Generic;
using CrewBriefingForms.Helper;
using CrewBriefingForms.Setting;
using CrewBriefingForms.WebServiceModels.GetFlightSearch;

namespace CrewBriefingForms.WebService
{
    public class EdfService
    {
        private static string _soapUrl = "https://www.crewbriefing.aero/EFB/v1.7/EfbService.asmx";
        private static bool _urlFetched;
        public static string SerializeForWebService(Object value)
        {
            var xsSubmit = new XmlSerializer(value.GetType());

            var ns = new XmlSerializerNamespaces();

            ns.Add("soap", "http://www.w3.org/2003/05/soap-envelope");
            ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            ns.Add("xsd", "http://www.w3.org/2001/XMLSchema");
            //ns.Add("", "http://crewbriefing.com/");

            var ms = new MemoryStream();
            var sww = new StreamWriter(ms);
            var settings = new XmlWriterSettings {Encoding = Encoding.UTF8};
            using (var writer = XmlWriter.Create(sww, settings))
            {
                xsSubmit.Serialize(writer, value, ns);
                writer.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                using (var sr = new StreamReader(ms, Encoding.UTF8))
                {
                    return sr.ReadToEnd();
                }
                //xmlData = sww.ToString(); // Your XML
            }
        }

        public static async Task<WebServiceResponse> GetSessionID(string username, string password)
        {
            if (!Utils.IsInternet())
            {
                Utils.ShowTemporayMessage(Interfaces.ToastType.Info, Strings.not_net);
                return new WebServiceResponse { Code = Constants.WebApiErrorCode.NO_NET };
            }
            if (!_urlFetched)
            {
                var url = await FretchURL.FetchUrlService.FetchUrl();
                if (string.IsNullOrEmpty(url))
                {
                    return new WebServiceResponse { Code = Constants.WebApiErrorCode.UNDEFINED, Message = "Error get SessionID" };
                }
                _soapUrl = url;
                _urlFetched = true;
            }
            var logindata = new LoginRequest
            {
                PartnerUser = Constants.PartnerUserName,
                PartnerPassword = Constants.PartnerPassword,
                CustomerPassword = password,
                CustomerUser = username
            };
            var wrp = new WebServiceLoginBodyWrapper() { LoginData = logindata };
            var rootWrapper = new WebServiceLoginRootWrapper() { Body = wrp };

            var xmlData = SerializeForWebService(rootWrapper);
            return await GetDataFromWebService(xmlData);
        }
        public static string ParseSessionIdResponse(string xmlResponse)
        {
            var serializer = new XmlSerializer(typeof(WebServiceLoginResponseRootWrapper));

            using (TextReader reader = new StringReader(xmlResponse))
            {
                var result = (WebServiceLoginResponseRootWrapper)serializer.Deserialize(reader);
                return result.Body.ResponseData.GetUserFilteredSessionIDResult;
            }
            
        }

	    /// <summary>
	    /// Check current session
	    /// </summary>
	    /// <param name="needLive"></param>
	    /// <returns>true, if no problems - session is not expired, or session is updated</returns>
	    public static async Task<bool> CheckSessionAndRefreshIfRequired(bool needLive)
	    {
		    var sessionExpired = await DB_PCL.IsSessionExpired();

		    if (!sessionExpired) return true;
		    var username = AppSetting.Instance.GetValueOrDefault(AppConstants);
		    var password = await DB_PCL.GetSettingAsync(SettingType.Password);
		    var response = await TryLogin(username,
			    password,
			    false, needLive);
		    if (response.Code == Enums.WebApiErrorCode.Success)
		    {
			    // can 'success' update, if not Inet even
			    // ShowTemporayMessage(ToastType.Info, "Session updated");
			    return true;
		    }
		    ShowTemporayMessage(Enums.ToastType.Warning, "Error update session");
		    return false;
	    }
		private static TimeSpan _requestTimeout = new TimeSpan(0, 0, 0, 120); // 120 sec timeout
        public static void ParseErrorMessage(string errorMessage, ref WebServiceResponse result)
        {
            if (errorMessage.ToLower().Contains("credentials are not valid"))
            {
                result.Message = "Invalid Username or Password";
                result.Code = Constants.WebApiErrorCode.WRONG_CREDENTIALS;
                return;
            }
            else if (errorMessage.ToLower().Contains("web service timeout") || errorMessage.ToLower().Contains("aborted") || errorMessage.Contains("NameResolutionFailure"))
            {
                result.Message = "Please check your internet connection and try again.";
                result.Code = Constants.WebApiErrorCode.TIME_OUT;
                return;
            }
            else if (errorMessage.ToLower().Contains("uploaded documents not available"))
            {
                result.Message = "Uploaded documents not available";
                result.Code = Constants.WebApiErrorCode.TIME_OUT;
                return;
            }
            else if (errorMessage.ToLower().Contains("invalid sessionid")) // Invalid sessionid
            {
                result.Message = "Invalid session";
                result.Code = Constants.WebApiErrorCode.INVALID_SESSION;
                return;
            }
            else
            {
                result.Message = "Something went wrong. Please try again.";
                result.Code = Constants.WebApiErrorCode.UNDEFINED;
                return;
            }

        }
        public static async Task<WebServiceResponse> GetDataFromWebService(string requestXmltData)
        {
            var tryCount = 0;
            var result = new WebServiceResponse { Code = Constants.WebApiErrorCode.NULL_ERROR };

            while (tryCount < 2)
            {
                if (!Utils.IsInternet())
                {
                    result.Code = Constants.WebApiErrorCode.NO_NET;
                    return result;
                }
                var client = new HttpClient
                {
                    Timeout = _requestTimeout
                };
                var request = new HttpRequestMessage(HttpMethod.Post, _soapUrl)
                {
                    Content = new StringContent(requestXmltData,
                                                    Encoding.UTF8,
                                                    "application/soap+xml")
                };


                var startRequest = DateTime.UtcNow;

                try
                {
                    var response = await client.SendAsync(request);
                    if (response.IsSuccessStatusCode)
                    {
                        result.Message = await response.Content.ReadAsStringAsync();
                        result.Code = Constants.WebApiErrorCode.SUCCESS;
                        return result;
                    }
                    string err = await response.Content.ReadAsStringAsync();
                    ParseErrorMessage(err, ref result);
                    if (result.Code == Constants.WebApiErrorCode.INVALID_SESSION && tryCount == 0)
                    {
                        Utils.ShowTemporayMessage(Interfaces.ToastType.Info, "Session expired - trying to auto-retrieve..");
                        var sessionResponse = await Utils.TryLogin(await DB_PCL.GetSettingAsync(SettingType.Username), await DB_PCL.GetSettingAsync(SettingType.Password), false, true);
                        if (sessionResponse.Code == Constants.WebApiErrorCode.SUCCESS)
                        {
                            tryCount++; // try again with new session 
                            continue;
                        }
                        return sessionResponse;
                    }
                    return result;
                }
                catch (System.Net.WebException webExeption)
                {
                    string errorMessage;
                    tryCount = int.MaxValue;
                    if (webExeption.Response != null)
                    {
                        try
                        {
                            using (var stream = webExeption.Response.GetResponseStream())
                            {
                                StreamReader responseStream;
                                responseStream = new StreamReader(stream);

                                errorMessage = await responseStream.ReadToEndAsync();
                                ParseErrorMessage(errorMessage, ref result);
                                return result;
                            }
                        }
                        catch
                        {
                        }
                    }
                    result.Message = "Unkonwn error";
                }
                catch (Exception exc)
                {
                    tryCount = int.MaxValue;
                    var ts = DateTime.UtcNow - startRequest;
                    if (ts.TotalMilliseconds >= _requestTimeout.TotalMilliseconds)
                    {
                        result.Code = Constants.WebApiErrorCode.TIME_OUT;
                        result.Message = Strings.errot_request_timeout;
                    }
                    else {
                        result.Message = exc.Message;
                    }

                    result.Message = exc.Message;
                }
            }
            return result;
        }
        public static async Task<WebServiceResponse> GetFlightListSearch(FlightSearchRequest request, string sessionId)
        {

            var wrp = new WebServiceGetFlightListSearchBodyWrapper()
            {
                method =
                new WebServiceAuthentifidRequestDataWrapper()
                {
                    SessionID = sessionId,
                    ChangedAfter = request.ChangedAfter
                }
            };
            var rootWrapper = new WebServiceGetFlightListSearchRootWrapper() { Body = wrp };

            var xmlData = SerializeForWebService(rootWrapper);
            System.Diagnostics.Debug.WriteLine("sent_data" + xmlData);
            return await GetDataFromWebService(xmlData);
        }

        public static List<FlightListItem> ParseGetFlightListSearch(string xml)
        {

            WebServiceModels.GetFlightSearch.FlightListItem[] result;
            var serializer = new XmlSerializer(typeof(CrewBriefingForms.WebServiceModels.GetFlightSearch.Envelope));

            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (WebServiceModels.GetFlightSearch.Envelope)serializer.Deserialize(reader);
                result = resultWrapper.Body.GetFlightListSearchResponse.GetFlightListSearchResult.Items;

                //var filteredList = result.Where(c => c.Crew?.Length > 0).ToList();
                foreach (var item in result)
                {
                    if (!(item?.Crew?.Count > 0)) continue;
                    foreach (var crew in item.Crew)
                    {
                        crew.FlightId = item.ID;
                    }
                }
                

            }
            return result.ToList();
        }
        public static async Task<WebServiceResponse> GetFlight(FlightRequest request)
        {

            var wrp = new WebServiceGetFlightBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetFlightRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
			System.Diagnostics.Debug.WriteLine(xmlData);
            return await GetDataFromWebService(xmlData);
        }
        public static Flight ParseFlight(string xml)
        {
            var serializer = new XmlSerializer(typeof(ModelsFlight.Envelope));

            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (ModelsFlight.Envelope)serializer.Deserialize(reader);
                return resultWrapper.Body.GetFlightResponse.GetFlightResult;
            }

        }
        public static Airport ParseAirport(string xml)
        {
            var serializer = new XmlSerializer(typeof(ModelsAirport.Envelope));

            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (ModelsAirport.Envelope)serializer.Deserialize(reader);
                return resultWrapper.Body.GetAirportResponse.GetAirportResult;
            }

        }
        public static Airport[] ParseAirportList(string xml)
        {
            var serializer = new XmlSerializer(typeof(ModelsAirportList.Envelope));

            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (ModelsAirportList.Envelope)serializer.Deserialize(reader);
                var res1 = resultWrapper.Body.GetAirportListResponse.GetAirportListResult.Airports;
                if (res1 == null)
                {
                    return null;
                }
                var res = new Airport[res1.Length];
                for (int i=0; i< res1.Length; i++)
                {
                    res[i] = new Airport()
                    {
                        ICAO = res1[i].ICAO,
                        Name = res1[i].Name,
                        Country = res1[i].Country,
                        LAT = res1[i].LAT,
                        LON = res1[i].LON
                    };
                }
                return res;
            }

        }



		public static async Task<WebServiceResponse> GetPdfResponse(PdfDocType pdfDocType, int passedItemId)
        {
            WebServiceResponse response;
            switch (pdfDocType)
            {
                case PdfDocType.Logstring:
                    var fr = new GetPDF_LogstringRequest()
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId
                    };
                    response = await EdfService.GetPDF_Logstring(fr);
                    break;
                case PdfDocType.Messages:
                    var fr1 = new GetPDF_MessagesRequest()
                    {
                        SessionID = App.SessionId,
                        FlightID = passedItemId,
                        importantOnly = true // harcoded yet
                    };
                    response = await EdfService.GetPDF_Messages(fr1);
                    break;
                case PdfDocType.WX:
                    var frWX = new GetPDF_WXRequest()
                    {
                        SessionID = App.SessionId,
                        FlightID = passedItemId,
                        halfSizePages = false // harcoded yet
                    };
                    response = await EdfService.GetPDF_WX(frWX);
                    break;
                case PdfDocType.NOTAMs:
                    var frNOTAMs = new GetPDF_WXRequest() // same structure
                    {
                        SessionID = App.SessionId,
                        FlightID = passedItemId,
                        halfSizePages = false // harcoded yet
                    };
                    response = await EdfService.GetPDF_NOTAMs(frNOTAMs);
                    break;
                case PdfDocType.ATC:
                    var frATC = new GetPDF_LogstringRequest() // same structure
                    {
                        SessionID = App.SessionId,
                        FlightID = passedItemId
                    };
                    response = await EdfService.GetPDF_ATC(frATC);
                    break;
                case PdfDocType.WindT:
                case PdfDocType.SWX:
                case PdfDocType.CrossSection:
                    var frFlightDoc = new GetFlightDocumentRequest()
                    {
                        SessionID = App.SessionId,
                        documentIdentifier = GetPrepareIdentifier(passedItemId, pdfDocType)
                    };
                    response = await EdfService.GetFlightDoc(frFlightDoc);
                    break;
                case PdfDocType.UploadedDocuments:
                    var frFupl = new GetPDF_FullPackageRequest()
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId,
                        ATC = false,
                        Charts = false,
                        FlightLog = false,
                        Messages = false,
                        Notams = false,
                        Uploaded = true,
                        WX = false
                    };
                    response = await EdfService.GetPDF_FullPackage(frFupl);
                    break;
                case PdfDocType.CombineDocuments:
                    var frFull = new GetPDF_FullPackageRequest()
                    {
						SessionID = App.SessionId,
                        FlightID = passedItemId,
                        ATC = true,
                        Charts = true,
                        FlightLog = true,
                        Messages = true,
                        Notams = true,
                        Uploaded = true,
                        WX = true
                    };
                    response = await EdfService.GetPDF_FullPackage(frFull);
                    break;
                default:
                   
                    throw new Exception("Upsupported GetPDF doctype");
            }

            return response;
        }

		private static string GetPrepareIdentifier(int flightId, PdfDocType docType)
        {
            var identifier = "WeatherChart&" + flightId;
            switch (docType)
            {
                case PdfDocType.WindT:
                    identifier += "&RS_WT_DefaultFL";
                    break;
                case PdfDocType.SWX:
                    identifier += "&RS_SWC_M";
                    break;
                case PdfDocType.CrossSection:
                    identifier += "&CS";
                    break;
            }
            return identifier;
        }


        public static Task<WebServiceResponse> GetPDF_Logstring(GetPDF_LogstringRequest request)
        {
            var wrp = new WebServiceGetPDF_LogstringBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetPDF_LogstringRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return GetDataFromWebService(xmlData);
        }
        public static GetPDF_Result ParseGetPDF_Logstring(string xml)
        {
            var serializer = new XmlSerializer(typeof(Envelope));
            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (Envelope)serializer.Deserialize(reader);
                return resultWrapper.Body.GetPDF_LogstringResponse.GetPDF_LogstringResult;
            }
        }
        public static GetPDF_Result ParseGetPDF_Messages(string xml)
        {
            var serializer = new XmlSerializer(typeof(EnvelopeMessages));
            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (EnvelopeMessages)serializer.Deserialize(reader);
                return resultWrapper.Body.GetPDF_MessagesResponse.GetPDF_MessagesResult;
            }
        }
        public static GetPDF_Result ParseGetPDF_WX(string xml)
        {
            var serializer = new XmlSerializer(typeof(EnvelopeWX));
            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (EnvelopeWX)serializer.Deserialize(reader);
                return resultWrapper.Body.GetPDF_WXResponse.GetPDF_WXResult;
            }
        }
        public static GetPDF_Result ParseGetPDF_NOTAMs(string xml)
        {
            var serializer = new XmlSerializer(typeof(EnvelopeNOTAMs));
            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (EnvelopeNOTAMs)serializer.Deserialize(reader);
                return resultWrapper.Body.GetPDF_NOTAMsResponse.GetPDF_NOTAMsResult;
            }
        }
        public static GetPDF_Result ParseGetPDF_ATC(string xml)
        {
            var serializer = new XmlSerializer(typeof(EnvelopeATC));
            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (EnvelopeATC)serializer.Deserialize(reader);
                return resultWrapper.Body.GetPDF_ATCResponse.GetPDF_ATCResult;
            }
        }
        public static GetPDF_Result ParseGetPDF_FlightDoc(string xml)
        {
            var serializer = new XmlSerializer(typeof(EnvelopeFlightDoc));
            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (EnvelopeFlightDoc)serializer.Deserialize(reader);
                return
                    new GetPDF_Result()
                    {
                        bArray = resultWrapper.Body.GetFlightDocumentResponse.GetFlightDocumentResult.ByteArray,
                        FileName = "FlightDoc",
                        Responce = resultWrapper.Body.GetFlightDocumentResponse.GetFlightDocumentResult.Response
                    };
            }
        }
        public static GetPDF_Result ParsePDF_FullPackage(string xml)
        {
            var serializer = new XmlSerializer(typeof(EnvelopeFullPackage));
            using (TextReader reader = new StringReader(xml))
            {
                var resultWrapper = (EnvelopeFullPackage)serializer.Deserialize(reader);
                return resultWrapper.Body.GetPDF_FullPackageResponse.GetPDF_FullPackageResult;
            }
        }



        public static Task<WebServiceResponse> GetPDF_Messages(GetPDF_MessagesRequest request)
        {
            var wrp = new WebServiceGetPDF_MessagesBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetPDF_MessagesRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return GetDataFromWebService(xmlData);
        }
        public static Task<WebServiceResponse> GetPDF_WX(GetPDF_WXRequest request)
        {
            var wrp = new WebServiceGetPDF_WXBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetPDF_WXRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return GetDataFromWebService(xmlData);
        }
        public static Task<WebServiceResponse> GetPDF_NOTAMs(GetPDF_WXRequest request)
        {
            var wrp = new WebServiceGetPDF_NOTAMsBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetPDF_NOTAMsRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return GetDataFromWebService(xmlData);
        }
        public static Task<WebServiceResponse> GetPDF_ATC(GetPDF_LogstringRequest request)
        {
            var wrp = new WebServiceGetPDF_ATCBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetPDF_ATCRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return GetDataFromWebService(xmlData);
        }
        public static Task<WebServiceResponse> GetFlightDoc(GetFlightDocumentRequest request)
        {
            var wrp = new WebServiceGetFlightDocumentBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetFlightDocumentRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return GetDataFromWebService(xmlData);
        }
        public static Task<WebServiceResponse> GetPDF_FullPackage(GetPDF_FullPackageRequest request)
        {
            var wrp = new WebServiceGetPDF_FullPackageBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetPDF_FullPackageRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return GetDataFromWebService(xmlData);
        }

        public static async Task<WebServiceResponse> GetAirport(GetAirportRequest request)
        {
            var wrp = new WebServiceGetAirportBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetAirportRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return await GetDataFromWebService(xmlData);
        }
        public static async Task<WebServiceResponse> GetAirportList(GetAirportListRequest request)
        {
            var wrp = new WebServiceGetAirportListBodyWrapper()
            {
                method = request
            };
            var rootWrapper = new WebServiceGetAirportListRootWrapper() { Body = wrp };

            var xmlData = EdfService.SerializeForWebService(rootWrapper);
            return await GetDataFromWebService(xmlData);
        }
    }

}
