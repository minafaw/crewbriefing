using System;
using System.Xml.Serialization;

using CrewBriefingForms.WebServiceModels.GetFlightSearch;

using Xamarin.Forms;
using SQLite;
using System.Globalization;
using System.Collections.Generic;
using CrewBriefingForms.Base;

namespace CrewBriefingForms.Models
{

	/*
        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
        public class FlightListItem
        {

            /// <remarks/>
            public Crew[] Crew;

            /// <remarks/>
            public string Alt1;

            /// <remarks/>
            public string Alt2;

            /// <remarks/>
            public int ID;

            /// <remarks/>
            public int FPLID;

            /// <remarks/>
            public string FlightlogID;

            /// <remarks/>
            public string DEP;

            /// <remarks/>
            public string DEST;

            /// <remarks/>
            public System.DateTime STD;

            /// <remarks/>
            public System.DateTime STA;

            /// <remarks/>
            public string ACFTAIL;

            /// <remarks/>
            public string ATCTime;

            /// <remarks/>
            public string ATCCtot;

            /// <remarks/>
            public string ATCAddr;

            /// <remarks/>
            public string ATCDOF;

            /// <remarks/>
            public string Remarks;

            /// <remarks/>
            public string ATCEET;

            /// <remarks/>
            public string Cancelled;

            /// <remarks/>
            public string TOA;

            /// <remarks/>
            public string ATCID;

            /// <remarks/>
            public string DESTTIME;

            /// <remarks/>
            public System.DateTime LastEdit;
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
        public partial class Crew
        {

            /// <remarks/>
            public string ID;

            /// <remarks/>
            public string CrewType;

            /// <remarks/>
            public string CrewName;

            /// <remarks/>
            public string Initials;
        }
        */

	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
	[Table("Flight")]
	public class Flight : BaseModel   // Flight Details
	{

		private LineBreakMode _depDstLineBreakMode = LineBreakMode.NoWrap;
		[Ignore]
		public LineBreakMode DepDstLineBreakMode
		{
			get => _depDstLineBreakMode;
			set
			{
				_depDstLineBreakMode = value;
				RaisePropertyChanged();
			}
		}

		public ImageSource AirPlaneImage
		{
			get => ImageSource.FromFile("aeroplane.png");

		}

#if CREW_OLD_ENPOINT
        public int Version { get; set; }
#endif
		/// <remarks/>
		public Message[] Messages;

		/// <remarks/>
		public OverflightCost OverflightCost;

		/// <remarks/>
		public string FlightLogID { get; set; }

		[PrimaryKey]
		/// <remarks/>
		public int ID { get; set; }

		/// <remarks/>
		public string PPSName { get; set; }

		/// <remarks/>
#if CREW_OLD_ENPOINT
        public string ACFTID { get; set; }
#endif

		/// <remarks/>
		public string ACFTAIL { get; set; }

		/// <remarks/>
		public string DEP { get; set; }

		/// <remarks/>
		public string DEST { get; set; }

		/// <remarks/>
		public string ALT1 { get; set; }

		/// <remarks/>
		public string ALT2 { get; set; }

		/// <remarks/>
		public System.DateTime STD { get; set; }

		/// <remarks/>
#if CREW_OLD_ENPOINT
        public string TOT { get; set; }
#endif

		/// <remarks/>
		public int PAX { get; set; }

		/// <remarks/>
		public double FUEL { get; set; }

		/// <remarks/>
		public int LOAD { get; set; }

		/// <remarks/>
		public string ValidHrs { get; set; }

		/// <remarks/>
		public int MinFL { get; set; }

		/// <remarks/>
		public int MaxFL { get; set; }

		/// <remarks/>
#if CREW_OLD_ENPOINT
        public string Remark { get; set; }
#endif

		/// <remarks/>
		public string EROPSAltApts { get; set; }

		/// <remarks/>
		public string[] AdequateApt;

		/// <remarks/>
		public string[] FIR;

		/// <remarks/>
		public string[] AltApts;

		/// <remarks/>
		public string TOA { get; set; }

		/// <remarks/>
		public string FMDID { get; set; }

		/// <remarks/>
		public string DESTSTDALT { get; set; }

		/// <remarks/>
		public string FUELCOMP { get; set; }

		/// <remarks/>
		public string TIMECOMP { get; set; }

		/// <remarks/>
		public string FUELCONT { get; set; }

		/// <remarks/>
		public string TIMECONT { get; set; }

		/// <remarks/>
		public string PCTCONT { get; set; }

		/// <remarks/>
		public string FUELMIN { get; set; }

		/// <remarks/>
		public string TIMEMIN { get; set; }

		/// <remarks/>
		public string FUELTAXI { get; set; }

		/// <remarks/>
		public string TIMETAXI { get; set; }

		/// <remarks/>
		public string FUELEXTRA { get; set; }

		/// <remarks/>
		public string TIMEEXTRA { get; set; }

		/// <remarks/>
		public string FUELLDG { get; set; }

		/// <remarks/>
		public string TIMELDG { get; set; }

		/// <remarks/>
		public string ZFM { get; set; }

		/// <remarks/>
		public string GCD { get; set; }

		/// <remarks/>
		public string ESAD { get; set; }

		/// <remarks/>
		public string GL { get; set; }

		/// <remarks/>
		public string FUELBIAS { get; set; }

		/// <remarks/>
		public System.DateTime STA { get; set; }

		/// <remarks/>
		public int SCHBLOCKTIME { get; set; }

		/// <remarks/>
		public string DISP { get; set; }

		/// <remarks/>
		public System.DateTime LastEditDate { get; set; }

		/// <remarks/>
		public string FUELMINTO { get; set; }

		/// <remarks/>
		public string TIMEMINTO { get; set; }

		/// <remarks/>
		public string ARAMP { get; set; }

		/// <remarks/>
		public string TIMEACT { get; set; }

		/// <remarks/>
		public string FUELACT { get; set; }

		/// <remarks/>
		public string DestERA { get; set; }

		/// <remarks/>
#if CREW_OLD_ENPOINT
        public string BlockTime { get; set; }
#endif
		/// <remarks/>
		public string TrafficLoad { get; set; }

		/// <remarks/>
		public string WeightUnit { get; set; }

		/// <remarks/>
		public string WindComponent { get; set; }

		/// <remarks/>
		public string CustomerDataPPS { get; set; }

		/// <remarks/>
		public string CustomerDataScheduled { get; set; }

		/// <remarks/>
		public int Fl { get; set; }

		/// <remarks/>
		public int RouteDistNM { get; set; }

		/// <remarks/>
		public string RouteName { get; set; }

		/// <remarks/>
		public string RouteType { get; set; }

		/// <remarks/>
		public int EmptyWeight { get; set; }

		/// <remarks/>
		public int TotalDistance { get; set; }

		/// <remarks/>
		public int AltDist { get; set; }

		/// <remarks/>
		public int DestTime { get; set; }

		/// <remarks/>
		public int AltTime { get; set; }

		/// <remarks/>
		public int AltFuel { get; set; }

		/// <remarks/>
		public int HoldTime { get; set; }

		/// <remarks/>
		public int ReserveTime { get; set; }

		/// <remarks/>
		public int Cargo { get; set; }

		/// <remarks/>
		public double ActTOW { get; set; }

		/// <remarks/>
		public double TripFuel { get; set; }

		/// <remarks/>
		public double HoldFuel { get; set; }

		/// <remarks/>
		public double Elw { get; set; }

		/// <remarks/>
		public string FuelPolicy { get; set; }

#if CREW_OLD_ENPOINT
        public int CruisePolicy { get; set; }

        /// <remarks/>
        public int WeightBalancePolicy { get; set; }

        /// <remarks/>
        public string Route { get; set; }
#endif
		/// <remarks/>
		public int Alt2Time { get; set; }

		/// <remarks/>
		public int Alt2Fuel { get; set; }

		/// <remarks/>
		public double MaxTOM { get; set; }

		/// <remarks/>
		public double MaxLM { get; set; }

		/// <remarks/>
		public double MaxZFM { get; set; }

		/// <remarks/>
		public System.DateTime WeatherObsTime { get; set; }

		/// <remarks/>
		public System.DateTime WeatherPlanTime { get; set; }

		/// <remarks/>
		public string MFCI { get; set; }

		/// <remarks/>
		public string CruiseProfile { get; set; }

		/// <remarks/>
		public int TempTopOfClimb { get; set; }

		/// <remarks/>
		public string Climb { get; set; }

		/// <remarks/>
		public string Descend { get; set; }

		/// <remarks/>
		public string FuelPL { get; set; }

		/// <remarks/>
		public string DescendWind { get; set; }

		/// <remarks/>
		public string ClimbProfile { get; set; }

		/// <remarks/>
		public string DescendProfile { get; set; }

		/// <remarks/>
		public string HoldProfile { get; set; }

		/// <remarks/>
		public string StepClimbProfile { get; set; }

		/// <remarks/>
		public string FuelContDef { get; set; }

		/// <remarks/>
		public string FuelAltDef { get; set; }

		/// <remarks/>
		public string AmexsyStatus { get; set; }

		/// <remarks/>
		public int AvgTrack { get; set; }

		/// <remarks/>
		public Taf DEPTAF;

		/// <remarks/>
		public string DEPMetar { get; set; }

		/// <remarks/>
		public Notam[] DEPNotam;

		/// <remarks/>
		public Taf DESTTAF;

		/// <remarks/>
		public string DESTMetar { get; set; }

		/// <remarks/>
		public Notam[] DESTNotam;

		/// <remarks/>
		public Taf ALT1TAF;

		/// <remarks/>
		public Taf ALT2TAF;

		/// <remarks/>
		public string ALT1Metar { get; set; }

		/// <remarks/>
		public string ALT2Metar { get; set; }

		/// <remarks/>
		public Notam[] ALT1Notam;

		/// <remarks/>
		public Notam[] ALT2Notam;

		/// <remarks/>
		public RoutePoint[] RoutePoints;

		/// <remarks/>
		public List<Crew> Crews;

		/// <remarks/>
#if CREW_OLD_ENPOINT
        public Units Units;
#endif
		public Response Responce;

		/// <remarks/>
		public ATC ATCData;

		/// <remarks/>
		public NextLeg NextLeg;

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayItem(IsNullable = false)]
		public FlightLevel[] OptFlightLevels;

		/// <remarks/>
		public Notam[] AdequateNotam;

		/// <remarks/>
		public Notam[] FIRNotam;

		/// <remarks/>
		public Notam[] AlternateNotam;

		/// <remarks/>
		[System.Xml.Serialization.XmlArrayItem(IsNullable = false)]
		public AltAirport[] Airports;

		/// <remarks/>
		public string[] EnrouteAlternates;

		/// <remarks/>
		public RoutePoint[] Alt1Points;

		/// <remarks/>
		public RoutePoint[] Alt2Points;

		/// <remarks/>
		public AlternateAirport[] StdAlternates;

		/// <remarks/>
#if CREW_OLD_ENPOINT
        public string Alt1ATCRoute;

        /// <remarks/>
        public string Alt2ATCRoute;
#endif

		/// <remarks/>
		public string[] CustomerData;

		/// <remarks/>
		public RCFData RCFData;

		/// <remarks/>
		public string TOALT { get; set; }

		/// <remarks/>
#if CREW_OLD_ENPOINT
        public string RouteToDest { get; set; }
#endif
		/// <remarks/>
		public string DEPIATA { get; set; }

		/// <remarks/>
		public string DESTIATA { get; set; }

		/// <remarks/>
		public SID SIDPlanned;

		/// <remarks/>
		public SID[] SIDAlternatives;
		private string _arports4ShowDEP;
		private string _arports4ShowDST;

		/// <remarks/>
		public int FinalReserveMinutes { get; set; }

		/// <remarks/>
		public int FinalReserveFuel { get; set; }

		/// <remarks/>
		public int AddFuelMinutes { get; set; }

		/// <remarks/>
		public int AddFuel { get; set; }

		/// <remarks/>
		public string FlightSummary { get; set; }

		// height of image in details - trick for binding
		public double AeroplaneImageHeight
		{
			get
			{

				double result = Utils.IsTablet() ?
					 CommonStatic.Instance.MediumHeight * 0.8 :
					39 * CommonStatic.Instance.ScreenCoef;
				if (Device.RuntimePlatform == Device.Android)
				{
					result *= .4;
				}
				return result;
			}
		}
		public double AeroplaneImageWidth
		{
			get
			{
				return AeroplaneImageHeight * 7.4;
				//                    273 * CommonStatic.Instance.ScreenCoef;
			}
		}
		public string ETA4Show
		{
			get
			{
				if (ATCData == null)
				{
					return string.Empty;
				}
				if (string.IsNullOrEmpty(ATCData.ATCEET))
				{
					return string.Empty;
				}
				if (ATCData.ATCEET.Length < 4)
				{
					return string.Empty;
				}
				return ATCData.ATCEET.Substring(0, 2) + ":" + ATCData.ATCEET.Substring(2, 2);
			}
		}
		public string ATCCtot
		{
			get
			{
				if (this.ATCData == null)
				{
					return string.Empty;
				}
				return ATCData.ATCCtot;

			}
		}
		public string STD4Show
		{
			get
			{
				string std = CommonStatic.GetPreparedStd(STD);

				if (ATCCtot != null && !ATCCtot.Trim().Equals(""))
				{
					std += " / ";
				}
				return std;
			}
		}

		public string STA4Show
		{
			get
			{
				var staValue = STA;

				// TODO uncomment this line in case this mehtod right 
				//                if ( DateTime.Compare(staValue , DateTime.MinValue) == 0)
				//                {
				//                    TimeSpan.TryParseExact(ATCData?.ATCEET, "hhmm", CultureInfo.InvariantCulture, out TimeSpan atceet);
				//                    var _ETADate = STD.Add(atceet);
				//                    staValue = _ETADate;
				//                } 
				if (DateTime.Compare(staValue, DateTime.MinValue) == 0)
				{
					return "No STA";
				}
				return CommonStatic.GetPreparedStd(staValue);
			}
		}
		public bool IsRecalculated
		{
			get
			{
				if (string.IsNullOrEmpty(FlightLogID))
				{
					return false;
				}
				return FlightLogID.EndsWith("-R");
			}
		}
		public string Recalculated4Show
		{
			get
			{
				return IsRecalculated ? Strings.recalculated : string.Empty;
			}
		}
		public string CrewCMD4Show
		{
			get
			{
				return getCrew4Show(Constants.CrewType.CMD);
			}
		}
		public double SmallFontSize
		{
			get
			{
				return CommonStatic.LabelFontSize(Xamarin.Forms.NamedSize.Small);
			}
		}
		public double MediumFontSize
		{
			get
			{
				return CommonStatic.LabelFontSize(Xamarin.Forms.NamedSize.Medium);
			}
		}
		public string CrewCOP4Show
		{
			get
			{
				return getCrew4Show(Constants.CrewType.COP);
			}
		}
		public string UploadeDate4Show
		{
			get
			{
				return CommonStatic.GetFormatedDateTime(this.LastEditDate);
			}
		}
		[Ignore]
		public string Arports4ShowDEP
		{
			get => _arports4ShowDEP;
			set
			{
				_arports4ShowDEP = value;
				RaisePropertyChanged();
			}
		}



		[Ignore]
		public string Arports4ShowDST 
		{ 
			get => _arports4ShowDST; set
			{
				_arports4ShowDST = value;
				RaisePropertyChanged();
			} 
		}
		private string getCrew4Show(Constants.CrewType crewType)
		{
			if (Crews == null)
			{
				return string.Empty;
			}
			foreach (Crew c in Crews)
			{
				if (c.CrewType == crewType.ToString())
				{
					return c.Initials;
				}
			}
			return "****";

		}
		public double DateTimeFormattedWidth
		{
			get
			{
				//for binding with Flight
				return CommonStatic.Instance.DateTimeFormattedWidth;
			}

		}
		[Ignore]
		public double MediumFontBiggerForDetails
		{
			get
			{
				return CommonStatic.LabelFontSize(Xamarin.Forms.NamedSize.Medium);
			}
		}
		[Ignore]
		public double SmallFontBiggerForDetails
		{
			get
			{
				return CommonStatic.LabelFontSize(Xamarin.Forms.NamedSize.Small);
			}
		}

	}

	/// <remarks/>
	[System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class Message
    {

        /// <remarks/>
        public string Subject;

        /// <remarks/>
        public string Text;

        /// <remarks/>
        public string SentFrom;

        /// <remarks/>
        public System.DateTime SentTime;

        /// <remarks/>
        public System.DateTime ValidFrom;

        /// <remarks/>
        public System.DateTime ValidTo;

        /// <remarks/>
        public bool HighPriority;

        /// <remarks/>
        public MessageRecipient[] Recipients;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class MessageRecipient
    {

        /// <remarks/>
        public string RecipientType;

        /// <remarks/>
        public string Recipient;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class OverflightCost
    {

        /// <remarks/>
        public FIROverflightCost[] Cost;

        /// <remarks/>
        public string Currency;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class FIROverflightCost
    {

        /// <remarks/>
        public string FIR;

        /// <remarks/>
        public int Distance;

        /// <remarks/>
        public int Cost;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class Taf
    {

        /// <remarks/>
        public string Type;

        /// <remarks/>
        public string Text;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class Notam
    {

        /// <remarks/>
        public string Number;

        /// <remarks/>
        public string Text;

        /// <remarks/>
        public System.DateTime FromDate;

        /// <remarks/>
        public System.DateTime ToDate;

        /// <remarks/>
        public int FromLevel;

        /// <remarks/>
        public int ToLevel;

        /// <remarks/>
        public string Fir;

        /// <remarks/>
        public string QCode;

        /// <remarks/>
        public string ECode;

        /// <remarks/>
        public string ICAO;

        /// <remarks/>
        public string UniformAbbreviation;

        /// <remarks/>
        public int Year;
    }

    /// <remarks/>



    [Table("RoutePoint")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class RoutePoint : MainlogRow
    {

        /// <remarks/>
        public int ID { get; set; }

        /// <remarks/>
        public string IDENT { get; set; }

        /// <remarks/>
        public int FL { get; set; }

        /// <remarks/>
        public int Wind { get; set; }

        /// <remarks/>
        public int Vol { get; set; }

        /// <remarks/>
        public int ISA { get; set; }

        /// <remarks/>
        public int LegTime { get; set; }

        /// <remarks/>
        public double LegCourse { get; set; }

        /// <remarks/>
        public int LegDistance { get; set; }

        /// <remarks/>
        public int LegCAT { get; set; }

        /// <remarks/>
        public string LegName { get; set; }

        /// <remarks/>
#if CREW_OLD_ENPOINT
        public string LegIDENT { get; set; }

        /// <remarks/>
        public string LegFir { get; set; }

        /// <remarks/>
        public string LegFir2 { get; set; }

        /// <remarks/>
        public string LegType { get; set; }
#endif
        public string LegAWY { get; set; }

        /// <remarks/>

        /// <remarks/>
        public double FuelUsed { get; set; }

        /// <remarks/>
        public int FuelFlow { get; set; }

        /// <remarks/>
        public double LAT { get; set; }

        /// <remarks/>
        public double LON { get; set; }

        /// <remarks/>
        public int VARIATION { get; set; }

        /// <remarks/>
        public int ACCDIST { get; set; }

        /// <remarks/>
        public int ACCTIME { get; set; }

        /// <remarks/>
        public double MagCourse { get; set; }

        /// <remarks/>
        public int TrueAirSpeed { get; set; }

        /// <remarks/>
        public int GroundSpeed { get; set; }

        /// <remarks/>
        public double FuelRemaining { get; set; }

        /// <remarks/>
        public int DistRemaining { get; set; }

        /// <remarks/>
        public int TimeRemaining { get; set; }

        /// <remarks/>
        public double MinReqFuel { get; set; }

        /// <remarks/>
        public double FuelFlowPerEng { get; set; }

        /// <remarks/>
        public int Temperature { get; set; }

        /// <remarks/>
        public int MORA { get; set; }

        /// <remarks/>
        public double Frequency { get; set; }

        public int FlightID { get; set; }
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(RoutePoint))]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(AlternateAirport))]
    public partial class MainlogRow
    {
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class AlternateAirport : MainlogRow
    {

        /// <remarks/>
        public string ICAO;

        /// <remarks/>
        public string IATA;

        /// <remarks/>
        public string Name;

        /// <remarks/>
        public int Wind;

        /// <remarks/>
        public int WindVelocity;

        /// <remarks/>
        public int FlightLevel;

        /// <remarks/>
        public int Distance;

        /// <remarks/>
        public double Course;

        /// <remarks/>
        public int Time;

        /// <remarks/>
        public double Fuel;

        /// <remarks/>
        public int BlockTime;

        /// <remarks/>
        public double BlockFuel;

        /// <remarks/>
        public string ERA;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class Units
    {

        /// <remarks/>
        public string Speed;

        /// <remarks/>
        public string Distance;

        /// <remarks/>
        public string Fuel;

        /// <remarks/>
        public string Time;

        /// <remarks/>
        public string Position;
    }

    /// <remarks/>




    [Table("ATC")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ATCData))]
    public partial class ATC
    {

        /// <remarks/>
        public string ATCID { get; set; }

        /// <remarks/>
        public string ATCTOA { get; set; }

        /// <remarks/>
        public string ATCRule { get; set; }

        /// <remarks/>
        public string ATCType { get; set; }

        /// <remarks/>
        public string ATCNum { get; set; }

        /// <remarks/>
        public string ATCWake { get; set; }

        /// <remarks/>
        public string ATCEqui { get; set; }

        /// <remarks/>
        public string ATCSSR { get; set; }

        /// <remarks/>
        public string ATCDep { get; set; }

        /// <remarks/>
        public string ATCTime { get; set; }

        /// <remarks/>
        public string ATCSpeed { get; set; }

        /// <remarks/>
        public string ATCFL { get; set; }

        /// <remarks/>
        public string ATCRoute { get; set; }

        /// <remarks/>
        public string ATCDest { get; set; }

        /// <remarks/>
        public string ATCEET { get; set; }

        /// <remarks/>
        public string ATCAlt1 { get; set; }

        /// <remarks/>
        public string ATCAlt2 { get; set; }

        /// <remarks/>
        public string ATCInfo { get; set; }

        /// <remarks/>
        public string ATCEndu { get; set; }

        /// <remarks/>
        public string ATCPers { get; set; }

        /// <remarks/>
        public string ATCRadi { get; set; }

        /// <remarks/>
        public string ATCSurv { get; set; }

        /// <remarks/>
        public string ATCJack { get; set; }

        /// <remarks/>
        public string ATCDing { get; set; }

        /// <remarks/>
        public string ATCCap { get; set; }

        /// <remarks/>
        public string ATCCover { get; set; }

        /// <remarks/>
        public string ATCColo { get; set; }

        /// <remarks/>
        public string ATCAcco { get; set; }

        /// <remarks/>
        public string ATCRem { get; set; }

        /// <remarks/>
        public string ATCPIC { get; set; }

        /// <remarks/>
        public string ATCCtot { get; set; }

        public int FlightID { get; set; }
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class ATCData : ATC
    {

    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class NextLeg
    {

        /// <remarks/>
        public System.DateTime STD;

        /// <remarks/>
        public string DEP;

        /// <remarks/>
        public string DEST;

        /// <remarks/>
        public int MINFUEL;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class FlightLevel
    {

        /// <remarks/>
        public int Level;

        /// <remarks/>
        public int Cost;

        /// <remarks/>
        public int WC;

        /// <remarks/>
        public double TimeNCruise;

        /// <remarks/>
        public int FuelNCruise;

        /// <remarks/>
        public double TimeProfile2;

        /// <remarks/>
        public int FuelProfile2;

        /// <remarks/>
        public double TimeProfile3;

        /// <remarks/>
        public int FuelProfile3;

        /// <remarks/>
        public int FuelLower;

        /// <remarks/>
        public int CostDiff;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class AltAirport
    {

        /// <remarks/>
        public string Type;

        /// <remarks/>
        public string Icao;

        /// <remarks/>
        public double Freq;

        /// <remarks/>
        public double Freq2;

        /// <remarks/>
        public string Info;

        /// <remarks/>
        public string Info2;

        /// <remarks/>
        public int Dist;

        /// <remarks/>
        public int Time;

        /// <remarks/>
        public int Fuel;

        /// <remarks/>
        public int MAGCURS;

        /// <remarks/>
        public string ATC;

        /// <remarks/>
        public double Lat;

        /// <remarks/>
        public double Long;

        /// <remarks/>
        public int Rwyl;

        /// <remarks/>
        public int Elevation;

        /// <remarks/>
        public string Name;

        /// <remarks/>
        public string Iata;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class RCFData
    {

        /// <remarks/>
        public string RCFAirport;

        /// <remarks/>
        public string RCFAlternate;

        /// <remarks/>
        public string DecisionPoint;

        /// <remarks/>
        public string ATCRoute;

        /// <remarks/>
        public int MinReqFuel;

        /// <remarks/>
        public int TripFuel;

        /// <remarks/>
        public int AlternateFuel;

        /// <remarks/>
        public int ExtraFuel;

        /// <remarks/>
        public int HoldingFuel;

        /// <remarks/>
        public int ContFuel;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class SID
    {

        /// <remarks/>
        public string RunwayName;

        /// <remarks/>
        public string ProcedureName;

        /// <remarks/>
        public int Distance;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class Arinc633FlightLog
    {

        /// <remarks/>
        public string FlightLogXml;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class PDF
    {

        /// <remarks/>
        public string FileName;

        /// <remarks/>
        public byte[] bArray;

    }

    /// <remarks/>

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class WXData
    {

        /// <remarks/>
        public string DEP;

        /// <remarks/>
        public string DEST;

        /// <remarks/>
        public string ALT1;

        /// <remarks/>
        public string ALT2;

        /// <remarks/>
        public string[] AdeqAPT;

        /// <remarks/>
        public string[] FIR;

        /// <remarks/>
        public Taf DEPTAF;

        /// <remarks/>
        public string DEPMetar;

        /// <remarks/>
        public NotamList DEPNotam;

        /// <remarks/>
        public Taf DESTTAF;

        /// <remarks/>
        public string DESTMetar;

        /// <remarks/>
        public NotamList DESTNotam;

        /// <remarks/>
        public Taf ALT1TAF;

        /// <remarks/>
        public string ALT1Metar;

        /// <remarks/>
        public NotamList ALT1Notam;

        /// <remarks/>
        public Taf ALT2TAF;

        /// <remarks/>
        public string ALT2Metar;

        /// <remarks/>
        public NotamList ALT2Notam;

        /// <remarks/>
        public NotamList AdeqAPTNotam;

        /// <remarks/>
        public NotamList FIRNotam;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class NotamList
    {

        /// <remarks/>
        public Notam[] Notams;

    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class RSChart
    {
        /// <remarks/>
        public byte[] Content;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class SIGMETList
    {

        /// <remarks/>
        public SIGMET[] SIGMETs;

    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class SIGMET
    {

        /// <remarks/>
        public string FIR;

        /// <remarks/>
        public string Message;
    }

    /// <remarks/>


    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class FlighInfoList
    {

        /// <remarks/>
        public string FlightLogID;

        /// <remarks/>
        public string ATCID;

        /// <remarks/>
        public System.DateTime STD;

        /// <remarks/>
        public string DOF;

        /// <remarks/>
        public string DEP;

        /// <remarks/>
        public string DEST;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class PDFMetaList
    {

        /// <remarks/>
        public PDFMeta[] Documents;

    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class PDFMeta
    {

        /// <remarks/>
        public string Title;

        /// <remarks/>
        public System.DateTime ValidFrom;

        /// <remarks/>
        public System.DateTime ValidTo;

        /// <remarks/>
        public int FlightId;

        /// <remarks/>
        public string Identifier;

        /// <remarks/>
        public DocumentType Type;

        /// <remarks/>
        public string Category;

        /// <remarks/>
        public string Info1;

        /// <remarks/>
        public string Info2;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, DataType = "dateTime")]
        public System.Nullable<System.DateTime> Created;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, DataType = "dateTime")]
        public System.Nullable<System.DateTime> BasedOnForecast;
    }

    /// <remarks/>


    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public enum DocumentType
    {

        /// <remarks/>
        Uploaded,

        /// <remarks/>
        WeatherChart,

        /// <remarks/>
        RunwayAnalysis,

        /// <remarks/>
        Company,
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class PDFDocument
    {

        /// <remarks/>
        public PDFMeta Meta;

        /// <remarks/>
        public byte[] ByteArray;
    }

    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class RecalcProperties
    {

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, DataType = "short")]
        public System.Nullable<short> Pax;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, DataType = "int")]
        public System.Nullable<int> Cargo;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true, DataType = "int")]
        public System.Nullable<int> Fuel;

        /// <remarks/>
        public string Alt1;

        /// <remarks/>
        public string Alt2;

        /// <remarks/>
        public string CrewCMD;

        /// <remarks/>
        public string CrewCOP;

        /// <remarks/>
        public string CrewCA1;

        /// <remarks/>
        public string CrewCA2;

        /// <remarks/>
        public string CrewCA3;

        /// <remarks/>
        public string CrewCA4;

        /// <remarks/>
        public string CrewCA5;

        /// <remarks/>
        public string CrewACM1;

        /// <remarks/>
        public string CrewACM2;
    }

    /// <remarks/>




    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class RecalcResponse
    {

        /// <remarks/>
        public string Message;

        /// <remarks/>
        public bool Succeed;

        /// <remarks/>
        public int RecalcFlightId;
    }

    public partial class GetSessionIDCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal GetSessionIDCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }

    public delegate void GetSessionIDCompletedEventHandler(object sender, GetSessionIDCompletedEventArgs args);

    public partial class GetUserFilteredSessionIDCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal GetUserFilteredSessionIDCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public string Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }

    public delegate void GetUserFilteredSessionIDCompletedEventHandler(object sender, GetUserFilteredSessionIDCompletedEventArgs args);

    public partial class TestConectivityCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
    {

        private object[] results;

        internal TestConectivityCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) :
                base(exception, cancelled, userState)
        {
            this.results = results;
        }

        public bool Result
        {
            get
            {
                this.RaiseExceptionIfNecessary();
                return ((bool)(this.results[0]));
            }
        }
    }
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://crewbriefing.com/")]
    public partial class Response
    {

        /// <remarks/>
        public string Message;

        /// <remarks/>
        public bool Succeed;
    }

    /// <remarks/>

}
