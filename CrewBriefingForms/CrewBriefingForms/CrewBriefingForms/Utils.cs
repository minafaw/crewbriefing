﻿using CrewBriefingForms.Database;
using CrewBriefingForms.Interfaces;
using CrewBriefingForms.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace CrewBriefingForms
{
    public class Utils
    {
        public static MasterDetailPage md;

        public static bool ShowFilter { get; set; } 
        public static string SummaryFontFamily => SummaryFontFamilyBold;
        public static string SummaryFontFamilyBold
        {
            get
            {
                string fontName = null;
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        fontName = "MonospaceBold";
                            break;
                    case Device.Android:
                        fontName = @"MonospaceBold.ttf#Monospace";
                        break;
					case Device.UWP:
                        fontName = @"/Assets/Fonts/MonospaceBold.ttf#Monospace";
                        break;
                }
                return fontName;
            }
        }
        public static async Task<WebServiceResponse> TryLogin(string username, string password, bool forceLogin, bool needLiveSession)
        {
            var sessIdResponse = new WebServiceResponse() { Code = Constants.WebApiErrorCode.UNDEFINED };
            var sessionExpired = await DB_PCL.IsSessionExpired();
            if (!sessionExpired && !forceLogin && !needLiveSession)
            {
				App.SessionId = await DB_PCL.GetSettingAsync(SettingType.SessionID);
                sessIdResponse.Code = Constants.WebApiErrorCode.SUCCESS;
            }
            else
            {
                sessIdResponse = await WebService.EdfService.GetSessionID(username, password);
                 switch (sessIdResponse.Code)
                    {
                        case Constants.WebApiErrorCode.SUCCESS:
                            var sessionId = WebService.EdfService.ParseSessionIdResponse(sessIdResponse.Message);
                            if (!string.IsNullOrEmpty(sessionId))
                            {
							    App.SessionId = sessionId;
                                await DB_PCL.UpdateSessionTimeAsync(sessionId);
                            }
                            else
                            {
                                sessIdResponse.Code = Constants.WebApiErrorCode.UNDEFINED;
                                sessIdResponse.Message = "Can not retrieve SessionID";
                            }
                            break;
                        case Constants.WebApiErrorCode.NO_NET:
                            var usernameStr = await DB_PCL.GetSettingAsync(SettingType.Username);
                            var sessionReceived = await DB_PCL.GetSettingAsync(SettingType.SessionReceived);
                           if (!string.IsNullOrEmpty(usernameStr) && 
                            !string.IsNullOrEmpty(sessionReceived) && !needLiveSession)
                            {
                                sessIdResponse.Code = Constants.WebApiErrorCode.SUCCESS;
                            }
                            await FilterSettings.LoadFilterFromDb();
                            return sessIdResponse;
                        case Constants.WebApiErrorCode.NULL_ERROR:
                            ShowTemporayMessage(ToastType.Error, GetErrorMessage(sessIdResponse.Code) + " " + sessIdResponse.Message);
                            break;
                        default:
                            ShowTemporayMessage(ToastType.Error, sessIdResponse.Message);
                            break;
                    }
                }
            if (sessIdResponse.Code == Constants.WebApiErrorCode.SUCCESS)
            {
               // await FilterSettings.LoadFilterFromDb();
            }
            return sessIdResponse;
        }
        /// <summary>
        /// Check current session
        /// </summary>
        /// <param name="needLive"></param>
        /// <returns>true, if no problems - session is not expired, or session is updated</returns>
        public static async Task<bool> CheckSessionAndRefreshIfRequired(bool needLive)
        {
            var sessionExpired = await DB_PCL.IsSessionExpired();

            if (!sessionExpired) return true;
            var username = await DB_PCL.GetSettingAsync(SettingType.Username);
            var password = await DB_PCL.GetSettingAsync(SettingType.Password);
            var response = await TryLogin(username,
                password,
                false, needLive );
            if (response.Code == Constants.WebApiErrorCode.SUCCESS)
            {
                // can 'success' update, if not Inet even
               // ShowTemporayMessage(ToastType.Info, "Session updated");
                return true;
            }
            ShowTemporayMessage(ToastType.Warning, "Error update session");
            return false;
        }
        public static bool IsInternet()
        {
            return CrossConnectivity.Current.IsConnected ;
        }
        private static int _oMessage;
        public static void ShowTemporayMessage(ToastType toastType, string message)
        {
             ShowTemporayMessage(toastType, message, 0);
        }
        public static void ShowTemporayMessage(ToastType toastType, string message, uint timeMSec)
        {
            if (_oMessage != 0 && toastType != ToastType.Info)
            {
                return;
            }
            if (toastType != ToastType.Info)
            {
                _oMessage = 1;
            }
            //await page.DisplayAlert(title, message, "OK");
            Device.BeginInvokeOnMainThread( async () =>
            {
               await DependencyService.Get<IToast>().ShowToast(toastType, message, timeMSec);
            });
            
            _oMessage = 0;

        }
        public static Page GetPageFromNavigation(Page p)
        {
            var page = p as NavigationPage;
            if (page != null)
            {
                return page.Navigation.NavigationStack[0];
            }
            return p;
        }
        public static void GoToPage(Page currentPage, Page nextPage)
        {
            GoToPage(currentPage, nextPage, false);
        }
        public static async void GoToPage(Page currentPage, Page nextPage, bool removePrevPage) // Content
        {

            if (Device.RuntimePlatform == Device.iOS && nextPage != null)
            {
                nextPage.Padding = new Thickness(0, 20, 0, 0);
            }
            NavigationPage.SetHasNavigationBar(nextPage, false);

            if (currentPage == null &&  nextPage != null && nextPage.GetType().FullName.ToUpper().Contains("PAGEVIEWIOS"))
            {
                await md.Detail.Navigation.PushAsync(nextPage);
                //FixiOsTopPadding();
                return;
            }

            if (currentPage == null && nextPage != null && nextPage.GetType().FullName.ToUpper().Contains("PAGEVIEWANDROID"))
            {
                await md.Detail.Navigation.PushAsync(nextPage);
                return;
            }

            if (currentPage == null && nextPage != null && nextPage.GetType().FullName.ToUpper().Contains("PDFVIEWPAGE"))
            {
                await md.Detail.Navigation.PushAsync(nextPage);
                return;
            }
            // Warning! Problem to hide NavigationBar:
            // https://bugzilla.xamarin.com/show_bug.cgi?id=51509
            if (currentPage is LoginPage)  // goto from login
            {
                md = new MasterDetailPage();
                md.IsPresentedChanged += Md_IsPresentedChanged;
                md.Master = new MenuPage(md);
                md.MasterBehavior = MasterBehavior.Popover;
                if (nextPage is TabletMainPage)
                {
                    md.Detail = new NavigationPage(nextPage);
                    // disable gesture 
                    md.IsGestureEnabled = false;
                }
                else
                {
                    if (nextPage is FlightListPage)
                    {
                        md.Detail = new NavigationPage(nextPage){
                            BarBackgroundColor = Color.Transparent
                        };
                    }
                    else
                    {
                        md.Detail = nextPage;
                    }
                }
				((CrewBriefingForms.App)Application.Current).MainPage = md;
            }
            else
            {
                if (nextPage is AboutPage || nextPage is FlightDetailsPage ||
                    nextPage is FlightSummaryPage ||
                    nextPage is HelpPage ||
                    nextPage is FlightMapPage ||
                    nextPage is LoginPage
                    ) // hide main menu
                {
                    var pushAsync = currentPage?.Navigation.PushAsync(nextPage, true);
                    if (pushAsync != null)
                        await pushAsync;
                    if (removePrevPage && currentPage != null)
                    {
                            if (currentPage.Navigation.NavigationStack?.Count > 1)
                            {
                                currentPage.Navigation.RemovePage(currentPage.Navigation.NavigationStack[currentPage.Navigation.NavigationStack.Count - 2]);
                            }
                    }                  
                    //currentPage.Navigation.PushModalAsync(nextPage, true);
                }
                else
                {
                    md = new MasterDetailPage();
                    md.Master = new MenuPage(md);
                    md.MasterBehavior = MasterBehavior.Popover;
                    md.IsPresentedChanged += Md_IsPresentedChanged;
                    md.Detail = new NavigationPage(nextPage){BarBackgroundColor = Color.Crimson};
                    //currentPage.Navigation.PushModalAsync(md, true);
                    if(currentPage != null)
                    await currentPage.Navigation.PushAsync(md, true);
                }
            }
           // FixiOsTopPadding();

        }
        //public static void FixiOsTopPadding()
        //{
        //    if (Device.RuntimePlatform != Device.iOS) return;
        //    if (md == null)
        //    {
        //        return;
        //    }
        //    var p = GetPageFromVav(md.Detail);
        //    if (p != null)
        //    {
        //        p.Padding = new Thickness(0, 20, 0, 0);
        //    }
        //    p = GetPageFromVav(md.Master);
        //    if (p != null)
        //    {
        //        p.Padding = new Thickness(0, 20, 0, 0);
        //    }
        //}
        //public static Page GetPageFromVav(Page page)
        //{
        //    if (page == null)
        //    {
        //        return null;
        //    }
        //    var navigationPage = page as NavigationPage;
        //    Page contentPage = navigationPage != null ? navigationPage.CurrentPage : page;
        //    return contentPage;
        //}
        public static void Md_IsPresentedChanged(object sender, EventArgs e)
        {
            if (md.Master != null)
            {
                if (md.IsPresented)
                {
                    ((MenuPage)md.Master).ShowItems();
                }
                ShowFilter = false;
                if (Device.RuntimePlatform == Device.iOS && !md.IsPresented)
                {
                    ((MenuPage)md.Master).ShowItems(); // CA-47 Filter settings menu appears on a few seconds
                }
            }
        }
        public static void ResetMasterPage()
        {
            md = null;
        }
        public static void SetBackNavBar(Page page)
        {
            page.ToolbarItems.Add(new ToolbarItem(Strings.back,
                ImageFolder + "back_arrow.png",
                async () =>
                {
                    await GoToPrevPage(page);
                },
            ToolbarItemOrder.Primary
            ));

        }
        public static bool IsTablet()
        {
            var d = DependencyService.Get<ITextMeasuring>().ScreenSizeInInches();
            return d >= 6.1;
        }
        public static string GetErrorMessage(Constants.WebApiErrorCode code)
        {
            switch (code)
            {
                case Constants.WebApiErrorCode.INVALID_SESSION:
                    return string.Format(Strings.error_message_format, Strings.invalid_session);
                case Constants.WebApiErrorCode.NO_NET:
                    return string.Format(Strings.error_message_format, Strings.not_net);
                case Constants.WebApiErrorCode.NULL_ERROR:
                    return string.Format(Strings.error_message_format, "NULL ERROR");
                case Constants.WebApiErrorCode.TIME_OUT:
                    return string.Format(Strings.error_message_format, Strings.errot_request_timeout);
                case Constants.WebApiErrorCode.UNDEFINED:
                    return string.Format(Strings.error_message_format, "UNDEFINED");
                case Constants.WebApiErrorCode.WRONG_CREDENTIALS:
                    return string.Format(Strings.error_message_format, Strings.wrong_credentials);
                default:
                    return string.Format(Strings.error_message_format, "Unknown code " + code);
            }
        }
        public static void CheckWebResponse(WebServiceResponse response, Page page, bool goBackIfError)
        {
            switch (response.Code)
            {
                case Constants.WebApiErrorCode.SUCCESS:
                    return;
                case Constants.WebApiErrorCode.NO_NET:
                    ShowTemporayMessage(ToastType.Info, Strings.not_net);
                    break;
                default:
                    ShowTemporayMessage(ToastType.Error, GetErrorMessage(response.Code) + " " + response.Message);
                    break;
            }
            if (!goBackIfError || page == null) return;
            if (page.Navigation?.NavigationStack?.Count > 0)
            {
                try
                {
                    GoToPrevPage();
                }
                catch (Exception exc)
                {
                    ShowTemporayMessage(ToastType.Info, "Error: " + exc.Message);
                }
            }
        }
        public static string ImageFolder {
            get
            {
                var imageFolderPath = "";
				if (Device.RuntimePlatform == Device.UWP)
                {
                    imageFolderPath = "assets\\";
                }
                return imageFolderPath;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="layout"></param>
        /// <param name="text"></param>
        /// <param name="percentsOfWidth">0..1</param>
        public static Label SetCenterText(AbsoluteLayout layout, string text, double percentsOfWidth, TapGestureRecognizer tapSearch, TapGestureRecognizer tapRefresh, TapGestureRecognizer tapMenu, bool halfMenuPanelWidth)
        {
            //Color backColor = (Color)Application.Current.Resources["nav_bar_color"];
            var labelNum = new Label
            {
                Text = text,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                //BackgroundColor = backColor,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                TextColor = Color.White,
                LineBreakMode = LineBreakMode.NoWrap,
            };
            var panelLabel = new StackLayout();
            panelLabel.Children.Add(labelNum);
            layout.Children.Add(panelLabel);

            AbsoluteLayout.SetLayoutBounds(panelLabel, new Rectangle(0.5, 0, percentsOfWidth, 1));
            AbsoluteLayout.SetLayoutFlags(panelLabel, AbsoluteLayoutFlags.All);
            
            var imageFolder = Utils.ImageFolder;
            var imageCoef = Utils.IsTablet() ? 0.8 : 1.0;

            if (tapSearch != null)
            {
                var imSearch = new Image()
                {
                    Source = ImageSource.FromFile(imageFolder + "menu_2_icon.png"),
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    HeightRequest = CommonStatic.Instance.GetMediumHeight * imageCoef,
                    Aspect = Aspect.AspectFit ,
                    AutomationId = "FilterImageId"
                };
                var searchPanel = new StackLayout()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.End,
                    Padding = new Thickness(10, 0, 10, 0),
                    Orientation = StackOrientation.Horizontal,
                    //BackgroundColor = Color.Aqua
                };
                searchPanel.Children.Add(imSearch);
                searchPanel.GestureRecognizers.Add(tapSearch);

                //labelNum.GestureRecognizers.Add(tapSearch);

                var refreshAndSearchPanel = new StackLayout()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Padding = new Thickness(0, 0, 0, 0),
                    Orientation = StackOrientation.Horizontal,
                    Spacing = 0
                };
                if (Utils.IsTablet())
                {
                    refreshAndSearchPanel.Padding = new Thickness(0);
                    searchPanel.Padding = new Thickness(CommonStatic.Instance.GetMediumHeight * .4, 0,0,0);
                }

                if (tapRefresh != null)
                {
                    var refreshPanel = new StackLayout()
                    {
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalOptions = LayoutOptions.EndAndExpand,
                        Padding = new Thickness(0, 0, 10, 0),
                        Orientation = StackOrientation.Horizontal,
                    };

                    var imReresh = new Image()
                    {
                        Source = ImageSource.FromFile(imageFolder + "btn_refresh.png"),
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.End,
                        HeightRequest = CommonStatic.Instance.GetMediumHeight,
                        Aspect = Aspect.AspectFit
                    };
                    refreshPanel.Children.Add(imReresh);
                    imReresh.GestureRecognizers.Add(tapRefresh);
                    refreshAndSearchPanel.Children.Add(refreshPanel);
                }
                refreshAndSearchPanel.Children.Add(searchPanel);
                AbsoluteLayout.SetLayoutBounds(refreshAndSearchPanel, new Rectangle(1, 0, 0.5, 1));
                AbsoluteLayout.SetLayoutFlags(refreshAndSearchPanel, AbsoluteLayoutFlags.All);
                layout.Children.Add(refreshAndSearchPanel); // warning! order af adding in layout is important!

            }
            if (tapMenu != null)
            {
                // warning! order af adding in layout is important!
                var menuPanel = new StackLayout()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    Padding = new Thickness(10, 0, 0, 0),
                    Orientation = StackOrientation.Horizontal,
                };
                

                var imMenu = new Image()
                {
                    Source = ImageSource.FromFile(imageFolder + "menu.png"),
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    //HeightRequest = CommonStatic.Instance.GetMediumHeight,
                    Aspect = Aspect.AspectFit,
                    AutomationId = "SideMenuID"

                                   
                };

                if (Utils.IsTablet())
                {
                    imMenu.Source = ImageSource.FromFile(imageFolder + "menu_tablet.png");
                    imMenu.HeightRequest = CommonStatic.Instance.GetMediumHeight;
                    menuPanel.Padding = new Thickness(CommonStatic.Instance.GetMediumHeight * 0.4, 0);
                    if (halfMenuPanelWidth)
                    {
                        menuPanel.Padding = new Thickness(0, 0, CommonStatic.Instance.GetMediumHeight * 1.5, 0);
                        menuPanel.Margin = new Thickness(CommonStatic.Instance.GetMediumHeight * 1.4, 0, 0, 0);
                       
                        AbsoluteLayout.SetLayoutBounds(menuPanel, new Rectangle(0, 0, 1, 1));
                        AbsoluteLayout.SetLayoutFlags(menuPanel, AbsoluteLayoutFlags.All);

                    }
                    else
                    {
                        AbsoluteLayout.SetLayoutBounds(menuPanel, new Rectangle(0, 0, 1, 1));
                        AbsoluteLayout.SetLayoutFlags(menuPanel, AbsoluteLayoutFlags.All);

                    }
                }
                else
                {
                    AbsoluteLayout.SetLayoutBounds(menuPanel, new Rectangle(0, 0, 1, 1));
                    AbsoluteLayout.SetLayoutFlags(menuPanel, AbsoluteLayoutFlags.All);
                    if (Device.RuntimePlatform == Device.Android && !Utils.IsTablet())
                    {
                        imMenu.HeightRequest = CommonStatic.Instance.GetMediumHeight * 1.3;  //CA-52 Android version: The menu button is about 15 % too big
                        //GetMediumHeight is bit small. Full header:
                        //panelFlightListTop.HeightRequest = CommonStatic.Instance.MediumHeight * 2;
                        //layout.BackgroundColor = Color.Yellow;
                    }
                }
                menuPanel.Children.Add(imMenu);

                //menuPanel.BackgroundColor = Color.Green;
                menuPanel.GestureRecognizers.Add(tapMenu);
                layout.Children.Add(menuPanel);
            }
            return labelNum;
        }
        public static async void GoToPrevPage()
        {
            await GoToPrevPage(null);
        }
        public static async Task GoToPrevPage(Page currentPage)
        {
           // App.FlightDetailsOnResumePage = null;
           // App.FlightDetailsOnResume = null;

            if (md == null)
            {
                if (currentPage == null)
                {
                    ShowTemporayMessage(ToastType.Info, "No previous page available (empty page)");
                    return;
                }
                try
                {
                    var x = await currentPage.Navigation.PopModalAsync();
                    if (x == null)
                    {
                       ShowTemporayMessage(ToastType.Info, "No previous page available");
                    }
                    return;
                } catch (Exception exc)
                {
                    ShowTemporayMessage(ToastType.Error, "Error: " + exc.Message);
                    return;
                }
            }
            if (md.Detail.Navigation != null)
            {
                try
                {
                    Page x = await md.Detail.Navigation.PopAsync();
                    if (x == null)
                    {
                        ShowTemporayMessage(ToastType.Info, "No previous page available");
                    }
                    return;
                }
                catch (Exception exc)
                {
                    ShowTemporayMessage(ToastType.Error, "Error: " + exc.Message);
                    return;
                }
            }
            ShowTemporayMessage(ToastType.Info, "No previous page available");

        }
        public static void SetBackNavBarWithShareIcon(Page page, StackLayout panelTop, string flightNo , Action  callback , ref  Image shareImage)
        {
            SetBackNavBar(page, panelTop, flightNo, false , true , callback ,   ref shareImage);
        }
        public static void SetBackNavBar(Page page, StackLayout panelTop, string flightNo)
        {
            var image = new Image();
            SetBackNavBar(page, panelTop, flightNo, false , false , null ,  ref image  );
        }
        public static void SetBackNavBar(Page page, StackLayout panelTop, string flightNo, bool setTextOnly , bool shareImage , Action callback ,  ref Image shareImageValue)
        {
            Image shareIm = null;

            var backColor = (Color)Application.Current.Resources["nav_bar_color"];
            var layout = new AbsoluteLayout()
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            panelTop.BackgroundColor = backColor;
            panelTop.Orientation = StackOrientation.Horizontal;
            panelTop.Padding = new Thickness(5, 10, 0, 10);
            if (!setTextOnly)
            {
                var tap = new TapGestureRecognizer();
                tap.Tapped += async (object sender, EventArgs e) =>
                {
                    await GoToPrevPage(page);
                };
                var labelBack = new Label()
                {
                    Text = Strings.back,
                    FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                    TextColor = Color.White
                };

                var im = new Image()
                {
                    Source = ImageSource.FromFile(ImageFolder + "arrow_left.png"),
                    VerticalOptions = LayoutOptions.Center,
                    HeightRequest = CommonStatic.Instance.GetMediumHeight,
                    Aspect = Aspect.AspectFit,
                };
                
                if (IsTablet())
                {
                    im.HeightRequest = CommonStatic.Instance.GetMediumHeight * TabletMainPage.ImageCoef;
                }
                var st = new StackLayout()
                {
                    Orientation = StackOrientation.Horizontal,
                    Spacing = 10,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Padding = new Thickness(10,4,0,4)
                };
               
               
                st.Children.Add(im);
                st.Children.Add(labelBack);
                st.GestureRecognizers.Add(tap);
               
                AbsoluteLayout.SetLayoutBounds(st, new Rectangle(0, 0, -1, -1));
                AbsoluteLayout.SetLayoutFlags(st, AbsoluteLayoutFlags.None);

                layout.Children.Add(st);

                /*
                layout.Children.Add(im);
                layout.Children.Add(labelBack);
                */
            }
            var labelNum = new Label()
            {
                Text = flightNo,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium) * 1.2,
                //BackgroundColor = backColor,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                TextColor = Color.White
            };
            AbsoluteLayout.SetLayoutBounds(labelNum, new Rectangle(0.5, 0, 0.5, 1));
            AbsoluteLayout.SetLayoutFlags(labelNum, AbsoluteLayoutFlags.All);

            if (shareImage)
            {
                var shareTapped = new TapGestureRecognizer()
                {
                    NumberOfTapsRequired = 1
                };

                shareTapped.Tapped += (sender, args) =>
                {
                    callback?.Invoke();
                };
                shareIm = new Image()
                {
                    Source = ImageSource.FromFile(ImageFolder + "tablet_share_popover_icon.png"),
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    HeightRequest = CommonStatic.Instance.GetMediumHeight,
                    Aspect = Aspect.AspectFit,
                    Margin = new Thickness(0, 0, 5, 0),

                        
                };

                shareImageValue = shareIm;

                shareIm.GestureRecognizers.Add(shareTapped);


                AbsoluteLayout.SetLayoutBounds(shareIm, new Rectangle(1, 0, 0.5, 1));
                AbsoluteLayout.SetLayoutFlags(shareIm, AbsoluteLayoutFlags.All);

            }

            layout.Children.Add(labelNum);
            if (shareImage)
            {
                layout.Children.Add(shareIm);
            }
            panelTop.Children.Add(layout);

            
        }
        public static double DistanceTo(double lat1, double lon1, double lat2, double lon2 /*, char unit = 'K' */)
        {
            var rlat1 = Math.PI * lat1 / 180;
            var rlat2 = Math.PI * lat2 / 180;
            var theta = lon1 - lon2;
            var rtheta = Math.PI * theta / 180;
            var dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;
            /*
            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }*/

            return dist;
        }
        public static double Calc(double lat1,
                          double long1, double lat2, double long2)
        {
            /*
                The Haversine formula according to Dr. Math.
                http://mathforum.org/library/drmath/view/51879.html

                dlon = lon2 - lon1
                dlat = lat2 - lat1
                a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
                c = 2 * atan2(sqrt(a), sqrt(1-a)) 
                d = R * c

                Where
                    * dlon is the change in longitude
                    * dlat is the change in latitude
                    * c is the great circle distance in Radians.
                    * R is the radius of a spherical Earth.
                    * The locations of the two points in 
                        spherical coordinates (longitude and 
                        latitude) are lon1,lat1 and lon2, lat2.
            */
            var dDistance = Double.MinValue;
            var dLat1InRad = lat1 * (Math.PI / 180.0);
            var dLong1InRad = long1 * (Math.PI / 180.0);
            var dLat2InRad = lat2 * (Math.PI / 180.0);
            var dLong2InRad = long2 * (Math.PI / 180.0);

            var dLongitude = dLong2InRad - dLong1InRad;
            var dLatitude = dLat2InRad - dLat1InRad;

            // Intermediate result a.
            var a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                       Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) *
                       Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            // Intermediate result c (great circle distance in Radians).
            var c = 2.0 * Math.Asin(Math.Sqrt(a));

            // Distance.
            const Double kEarthRadiusMiles = 3956.0;
            //const Double kEarthRadiusKms = 6376.5;
            dDistance = kEarthRadiusMiles * c;

            return dDistance;
        }
    }
}
