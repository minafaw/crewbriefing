﻿using System;
using System.IO;
using Foundation;
using QuickLook;

namespace CrewBriefingForms.iOS
{
	public class MyQLPreviewControllerDataSource : QLPreviewControllerDataSource
	{
        string fileName , title;
        public MyQLPreviewControllerDataSource(string fileName , string title ){
            this.fileName = fileName;
            this.title = title;
		
            
        }
		public override nint PreviewItemCount(QLPreviewController controller)
		{
			return 1;
		}



        public override IQLPreviewItem GetPreviewItem(QLPreviewController controller, nint index)
        {
			var documents = NSBundle.MainBundle.BundlePath;
			var library = Path.Combine(documents, fileName);
			NSUrl url = NSUrl.FromFilename(library);
            return new QlItem(title, url);
        }

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			// where clean resource 
			
		}
	}
}
