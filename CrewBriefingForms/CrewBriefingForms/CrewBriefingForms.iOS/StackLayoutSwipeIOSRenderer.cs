﻿
using CrewBriefing.Standard.Controls;
using CrewBriefingForms.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(StackLayoutSwipe), typeof(StackLayoutSwipeIOSRenderer))]
namespace CrewBriefingForms.iOS
{
    public class StackLayoutSwipeIOSRenderer : ViewRenderer
    {
        //private UISwipeGestureRecognizer gestureSwipe;
        private UISwipeGestureRecognizer swipeRight;
        private UISwipeGestureRecognizer swipeLeft;
        private UITapGestureRecognizer tapRecognizer;
        private StackLayoutSwipe sls = null;

        public StackLayoutSwipeIOSRenderer()
        {
        
        }
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            /*
            gestureSwipe = new UISwipeGestureRecognizer();
            gestureSwipe.CancelsTouchesInView = true;
            gestureSwipe.AddTarget(() => HandleSwipe(gestureSwipe));
            this.AddGestureRecognizer(gestureSwipe);
            +*/
            swipeRight = new UISwipeGestureRecognizer(
                            () =>
                            {
                                if (sls != null)
                                {
                                    sls.SwipeRightCustom();
                                }
                            }
                        )
            {
                Direction = UISwipeGestureRecognizerDirection.Right,
            };
            swipeLeft= new UISwipeGestureRecognizer(
                            () =>
                            {
                                if (sls != null)
                                {
                                    sls.SwipeLeftCustom();
                                }
                            }
                        )
            {
                Direction = UISwipeGestureRecognizerDirection.Left,
            };

            /*
            tapRecognizer = new UITapGestureRecognizer(
                () =>
                {
                    if (sls != null)
                    {
                        sls.TapCustom();
                    }
                    }
              );
              */

            if (e.NewElement == null)
            {
                if (swipeRight != null)
                {
                    this.RemoveGestureRecognizer(swipeRight);
                    this.RemoveGestureRecognizer(swipeLeft);
                    //this.RemoveGestureRecognizer(tapRecognizer);
                }
            }
            if (e.OldElement == null)
            {
                if (e.NewElement != null)
                {
                    if (e.NewElement is StackLayoutSwipe && sls == null)
                    {
                        sls = e.NewElement as StackLayoutSwipe;
                        this.AddGestureRecognizer(swipeRight);
                        this.AddGestureRecognizer(swipeLeft);
                        //this.AddGestureRecognizer(tapRecognizer);
                    }
                }

            }
        }
        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);
            if (sls != null)
            {
                sls.TapCustom();
            }

        }

    }
}
