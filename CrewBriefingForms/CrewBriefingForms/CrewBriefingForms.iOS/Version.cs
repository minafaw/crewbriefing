using CB.Core.Common.Interfaces;
using CrewBriefingForms.iOS;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(VersionIOS))]
namespace CrewBriefingForms.iOS
{
    public class VersionIOS : IAppVersion
    {
        public static bool FormsInitialized = false;
        public string GetAppVersion()
        {
            var nd = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
            return nd.ToString();
        }

        public string GetAppVersionBuild()
        {
            var version = NSBundle.MainBundle.InfoDictionary["CFBundleShortVersionString"];
            var build = NSBundle.MainBundle.InfoDictionary["CFBundleVersion"];
            return version + "." + build;
        }

        public string GetDeviceName()
        {
            return UIDevice.CurrentDevice.Model;
        }
        public string GetOs()
        {
            return UIDevice.CurrentDevice.SystemVersion;
        }
        public int GetOsMajorVersion()
        {
            return 0;
        }
        public string MailEol()
        {
            return Environment.NewLine;
        }
    }
}