using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Threading;
using CrewBriefingForms.iOS;
using CrewBriefingForms.iOS.Helper;
using Foundation;
using UIKit;
using CB.Core.Common.Interfaces;
using static CB.Core.Common.Keys.Enums;
using CB.Core.Common;

[assembly: Dependency(typeof(ToastIos))]
namespace CrewBriefingForms.iOS
{
    public class ToastIos : IToast
    {
        public Task<int> ShowToast(ToastType type, string message, uint timeMsec)
        {
            var prefix = string.Empty;

            var showTime = 2.0;
            switch (type)
            {
                case ToastType.Error:
                    prefix = Resource.error.ToUpper();
                    break;
                case ToastType.Warning:
                    prefix = Resource.warning;
                    break;
                default:
                    showTime = 1.5;
                    break;
            }
            if (timeMsec > 0)
            {
                showTime = timeMsec / 1000.0;
            }
            ShowAlert(prefix, message, showTime);
            var task = Task<int>.Factory.StartNew(() => {
                Thread.Sleep(100);
                return 0;
            });
            return task;
        }

        public void ShowAlert(string title, string message, double seconds)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
            var alert = UIAlertController.Create(string.IsNullOrEmpty(title) ? null : title, message, UIAlertControllerStyle.Alert);

            NSTimer.CreateScheduledTimer(seconds, async (obj) =>
            {
                var dismissViewControllerAsync = alert?.DismissViewControllerAsync(true);
                if (dismissViewControllerAsync != null) await dismissViewControllerAsync;
            });
                var topViewController = ViewContollerUtils.GetVisibleViewController();
                (topViewController as UIAlertController)?.DismissViewController(true  , null);
                topViewController.PresentViewController(alert, true, null);
            });
        }
    }
}