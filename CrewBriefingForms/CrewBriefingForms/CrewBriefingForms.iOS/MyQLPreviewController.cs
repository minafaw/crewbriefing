﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrewBriefingForms.iOS.Extensions;
using Foundation;
using QuickLook;
using UIKit;

namespace CrewBriefingForms.iOS
{
	public class MyQLPreviewController : QLPreviewController
	{
		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			if (NavigationController != null && NavigationController.NavigationBar != null)
			{
				//		let naviButton = UIBarButtonItem(title: "naviButton", style: UIBarButtonItemStyle.plain, target: self, action: #selector(Tapped))

				//      // Left bar button does not appear
				//      self.navigationItem.leftBarButtonItem = naviButton

				//// Right bar button do appear
				//self.navigationItem.rightBarButtonItem = naviButton

				var leftNavButton = new UIBarButtonItem() { Title = "Done" };
				leftNavButton.Clicked += LeftNavButton_Clicked;   
				NavigationItem.LeftBarButtonItem = leftNavButton;

				NavigationController.NavigationBar.TintColor = UIColor.White;
				NavigationController.NavigationBar.TitleTextAttributes = new UIStringAttributes() { ForegroundColor = UIColor.White }; 
			}
				
		}

		private void LeftNavButton_Clicked(object sender, EventArgs e)
		{
			DismissModalViewController(true);
		}
	}
}