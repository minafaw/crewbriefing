﻿using System;
using CB.Core.CustomControls.Controls;
using CrewBriefingForms.iOS.CustomRender;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtLabel), typeof(ExtLabelRenderer))]
namespace CrewBriefingForms.iOS.CustomRender
{
    public class ExtLabelRenderer : LabelRenderer
    {
		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			
            if(Control == null){
				ExtLabel _extLabel = Element as ExtLabel;

				SetNativeControl(new ExtUILabel(_extLabel.Padding));
            }
            

            base.OnElementChanged(e);

		}
	}
}
