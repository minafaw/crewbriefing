﻿using CrewBriefingForms.Controls;
using CrewBriefingForms.iOS.CustomRender;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(WebViewZoom), typeof(WebViewCustomRenderer))]
namespace CrewBriefingForms.iOS.CustomRender
{
    public class WebViewCustomRenderer : ViewRenderer<WebViewZoom, UIWebView>
    {
        /*
        protected override void OnElementChanged(ElementChangedEventArgs<CustomWebView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                SetNativeControl(new UIWebView());
            }
            if (e.OldElement != null)
            {
                // Cleanup
            }
            if (e.NewElement != null)
            {
                var customWebView = Element as CustomWebView;
                string fileName = Path.Combine(NSBundle.MainBundle.BundlePath, string.Format("Content/{0}", WebUtility.UrlEncode(customWebView.Uri)));
                Control.LoadRequest(new NSUrlRequest(new NSUrl(fileName, false)));
                Control.ScalesPageToFit = true;
            }
        }
        */
        protected override void OnElementChanged(ElementChangedEventArgs<WebViewZoom> e)
        {
            base.OnElementChanged(e);
            
            if (Control == null)
            {
                SetNativeControl(new UIWebView());
            }
            if (e.OldElement != null)
            {
                // Cleanup
            }
            if (e.NewElement != null)
            {
                if (e.NewElement is WebViewZoom)
                {
                    //https://developer.xamarin.com/recipes/cross-platform/xamarin-forms/controls/display-pdf/
                    //https://developer.xamarin.com/recipes/ios/content_controls/scroll_view/zoom_a_scrollview/
                    Control.LoadRequest(new NSUrlRequest(new NSUrl((e.NewElement as WebViewZoom).FileNameWithPath, false)));

                    Control.ScalesPageToFit = true;

                }
            }
            
        }
    }
}
