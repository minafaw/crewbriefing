﻿using System;
using CB.Core.Dtos;
using CoreGraphics;
using CoreLocation;
using CrewBriefing.Standard;
using CrewBriefingForms;
using CrewBriefingForms.iOS.CustomRender;
using Foundation;
using MapKit;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.iOS;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRendererIOS))]
namespace CrewBriefingForms.iOS.CustomRender
{
    public class CustomMapRendererIOS : MapRenderer
    {
        private MKMapView _nativeMap;
        private FlightDto _flightInfo;
        private RoutePointWithIndex[] _routePoinstWithIndex;
        private int _markerScale = 1;
        UIImage _imageStart, _imageEnd, _imagePoint;
        UIView _customPinView;

        MKPolyline _routePointsLine;
        MKPolylineRenderer _polyLineRenderer;
        MKPolylineRenderer _geoDescpolyLineRenderer;
        MKGeodesicPolyline _geoDesicLine;
        private bool _isDrawn = false;

       
        /*
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/custom-renderer/map/customized-pin/
            base.OnElementChanged(e);
            if (e.OldElement != null)
            {
                nativeMap = null;
            }

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;

                flightInfo = formsMap.FlightInfo;
                try
                {
                    if (TextMeasuring.Xdpi > 0 && TextMeasuring.Ydpi > 0 && !double.IsInfinity(TextMeasuring.Xdpi) && !double.IsInfinity(TextMeasuring.Ydpi))
                    {
                        double dpi = TextMeasuring.Xdpi > TextMeasuring.Ydpi ? TextMeasuring.Xdpi : TextMeasuring.Ydpi;
                        int i = (int)Math.Round(dpi / 96);
                        if (i >= 1 && i <= 4)
                        {
                            markerScale = i;
                        }
                    }
                    else
                    {
                        markerScale = 1;
                    }
                }
                catch
                {
                    markerScale = 1;
                }
                ((MapView)Control).GetMapAsync(this);

            }
        }
*/
/*
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            
            
            if (e.PropertyName.Equals("VisibleRegion") && !isDrawn)
            {
                MyDrawMap();
                isDrawn = true;
            }
        }
        */

        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                var nativeMap = Control as MKMapView;
                if (nativeMap != null)
                {
                    nativeMap?.RemoveAnnotations(nativeMap.Annotations);
                    nativeMap?.RemoveOverlays(nativeMap?.Overlays);
                    nativeMap.GetViewForAnnotation = null;
                    nativeMap.OverlayRenderer = null;
                }
                _isDrawn = false;
                /*
                nativeMap.CalloutAccessoryControlTapped -= OnCalloutAccessoryControlTapped;
                nativeMap.DidSelectAnnotationView -= OnDidSelectAnnotationView;
                nativeMap.DidDeselectAnnotationView -= OnDidDeselectAnnotationView;
                */
            }

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                _nativeMap = Control as MKMapView;
                _flightInfo = formsMap.FlightInfo;
                try
                {
                    if (TextMeasuringIOS.Xdpi > 0 && TextMeasuringIOS.Ydpi > 0 && !double.IsInfinity(TextMeasuringIOS.Xdpi) && !double.IsInfinity(TextMeasuringIOS.Ydpi))
                    {
                        double dpi = TextMeasuringIOS.Xdpi > TextMeasuringIOS.Ydpi ? TextMeasuringIOS.Xdpi : TextMeasuringIOS.Ydpi;
                        int i = (int)Math.Round(dpi / 160);
                        if (i >= 1 && i <= 4)
                        {
                            _markerScale = i;
                        }
                        else if (i <= 1)
                        {
                            _markerScale = 1;
                        }
                        else
                        {
                            if (i > 4)
                            {
                                _markerScale = 4;
                            }
                        }
                    }
                    else
                    {
                        _markerScale = 1;
                    }
                }
                catch
                {
                    _markerScale = 1;
                }
                _imageStart = UIImage.FromFile("ICAO_symbol_84_blue_" + _markerScale.ToString() + ".png");
                _imageEnd = UIImage.FromFile("ICAO_symbol_84_orange_" + _markerScale.ToString() + ".png");
                _imagePoint = UIImage.FromFile("ICAO_symbol_121_on_request_blue_" + _markerScale.ToString() + ".png");
                if (_nativeMap != null)
                {
                    _nativeMap.GetViewForAnnotation = GetViewForAnnotation;
                    _nativeMap.OverlayRenderer = GetOverlayRenderer;
                }
                MyDrawMap();
            }
        }
        MKAnnotationView GetViewForAnnotation(MKMapView mapView, IMKAnnotation annotation)
        {
            MKAnnotationView annotationView = null;

            if (annotation is MKUserLocation)
                return null;

            var anno = annotation as CustomMKPointAnnotation; //MKPointAnnotation
            if (_routePoinstWithIndex == null || anno == null)
            {
                return null;
            }
            // Warning! GetCustomPin (get pin from array by anno) - works wrong. Probably parral calls are here - so need semaphors/critica sections/etc. 
            // we nned only image here, so we wiil use own CustomMKPointAnnotation class

            /*RoutePointWithIndex customPin = GetCustomPin(anno);
            if (customPin == null)
            {
                throw new Exception("Custom pin not found");
            } */
            // https://developer.xamarin.com/guides/ios/platform_features/ios_maps_walkthrough/
            //string _id = "crew_flight_point_" + customPin.Index.ToString();
            annotationView = mapView.DequeueReusableAnnotation("myPin"); // shows default marker?  _id
            if (annotationView == null)
            {
                annotationView = new MKAnnotationView(annotation, "myPin"); 
                // MKAnnotationView
                                                                            //annotationView.LeftCalloutAccessoryView = new UIImageView(UIImage.FromFile("ICAO_symbol_84_blue.png"));
                                                                            //annotationView.RightCalloutAccessoryView = UIButton.FromType(UIButtonType.DetailDisclosure);
                                                                            /*

                                                                            if (customPin.Index == 0 || customPin.Index == routePoinstWithIndex.Length - 1)
                                                                            {
                                                                                if (customPin.Index == 0)
                                                                                {
                                                                                    annotationView.Image = imageStart;
                                                                                }
                                                                                else
                                                                                {
                                                                                    annotationView.Image = imageEnd;
                                                                                }
                                                                            }
                                                                            else {
                                                                                annotationView.Image = imagePoint;
                                                                            } */
                //annotationView.Image = anno.Image;
                annotationView.CalloutOffset = new CGPoint(0, 0);

            }
            annotationView.Image = anno.Image;
            annotationView.CanShowCallout = true;

            return annotationView;
        }
        RoutePointWithIndex GetCustomPin(MKPointAnnotation annotation)
        {
            if (annotation == null)
            {
                return null;
            }
            double delta = 1e-6;
            if (_flightInfo != null && _routePoinstWithIndex != null)
            {
                Position position = new Position(annotation.Coordinate.Latitude, annotation.Coordinate.Longitude);
                for (int i = 0; i < _routePoinstWithIndex.Length; i++)
                {
                    RoutePointWithIndex p = _routePoinstWithIndex[i];
                    if (Math.Abs(annotation.Coordinate.Latitude - p.P.Lat) < delta && Math.Abs(annotation.Coordinate.Longitude - p.P.Lon) < delta)
                    {
                        return p;
                    }
                }
            }
            else
            {
                return null;
            }
            return null;
        }

        MKOverlayRenderer GetOverlayRenderer(MKMapView mapView, IMKOverlay overlay)
        {

            if (overlay.GetType() == typeof(MKPolyline))
            {
                //if(this.polyLineRenderer == null)
                //{
                _polyLineRenderer = new MKPolylineRenderer(overlay as MKPolyline);
                _polyLineRenderer.LineWidth = 1.0f; //1.0
                                                        //#0000E0  : 0, 0 , 224 : Blue
                this._polyLineRenderer.StrokeColor = UIColor.FromRGB(0, 0, 224);
                //this.polyLineRenderer.StrokeColor = UIColor.Yellow;
                this._polyLineRenderer.Alpha = 1.0f;
                //return polyLineRenderer;
                //}

                return this._polyLineRenderer;
            }
            else if (overlay.GetType() == typeof(MKGeodesicPolyline))
            {
                // if(this.geoDescpolyLineRenderer == null) {

                _geoDescpolyLineRenderer = new MKPolylineRenderer(overlay as MKPolyline);
                _geoDescpolyLineRenderer.LineWidth = 1.0f;
                NSNumber[] num = new NSNumber[] { 12, 8 };
                _geoDescpolyLineRenderer.LineDashPattern = num;
                //#E00000 : 224, 0 ,0  : red
                _geoDescpolyLineRenderer.StrokeColor = UIColor.FromRGB(224, 0, 0);
                //this.geoDescpolyLineRenderer.StrokeColor = UIColor.Black;
                _geoDescpolyLineRenderer.Alpha = 1.0f;
                // }

                return this._geoDescpolyLineRenderer;
            }
            else
            {
                return null;
            }
        }

        private void MyDrawMap()
        {

            if (_nativeMap == null)
            {
                return;
            }
            if (_flightInfo == null)
            {
                return;
            }

            else if (_flightInfo.RoutePoints == null)
            {
                return;
            }
            else if (_flightInfo.RoutePoints.Count == 0)
            {
                return;
            }
            //nativeMap.Clear();

            if (_flightInfo.RoutePoints.Count > 2)
            {
                int nLength = _flightInfo.RoutePoints.Count;
                _routePoinstWithIndex = new RoutePointWithIndex[nLength];

                CLLocationCoordinate2D[] geoDesicCordinates = new CLLocationCoordinate2D[2];
                geoDesicCordinates[0] = new CLLocationCoordinate2D(_flightInfo.RoutePoints[0].Lat, _flightInfo.RoutePoints[0].Lon);
                geoDesicCordinates[1] = new CLLocationCoordinate2D(_flightInfo.RoutePoints[nLength-1].Lat, _flightInfo.RoutePoints[nLength-1].Lon);

                _geoDesicLine = MKGeodesicPolyline.FromCoordinates(geoDesicCordinates);
                

                CLLocationCoordinate2D[] cordinates = new CLLocationCoordinate2D[nLength];

                CustomMKPointAnnotation[] _annotations = new CustomMKPointAnnotation[nLength];
                for (int i = 0; i < nLength; i++)
                {
                    RoutePointDto p = _flightInfo.RoutePoints[i];

                    UIImage _image = null;
                    if (i == 0 || i == _routePoinstWithIndex.Length - 1)
                    {
                        if (i == 0)
                        {
                            _image = _imageStart;
                        }
                        else
                        {
                            _image = _imageEnd;
                        }
                    }
                    else {
                        _image = _imagePoint;
                    }
                    


                    _routePoinstWithIndex[i] = new RoutePointWithIndex()
                    {
                        Index = i,
                        P = p,

                    };
                    string _title = string.Empty;
                    if (i == 0 || i == _flightInfo.RoutePoints.Count - 1)  //DEP and DEST
                    {
                        _title = i == 0 ? _flightInfo.Airports4ShowDep : _flightInfo.Airports4ShowDst;
                    }
                    else {
                        _title = p.IdEnt;
                    }

                    _annotations[i] = new CustomMKPointAnnotation()
                    {
                        Title = _title,
                        Coordinate = new CLLocationCoordinate2D(p.Lat, p.Lon),
                        Image = _image
                    };
                    

                    
                    cordinates[i] = _annotations[i].Coordinate;
                }
                BeginInvokeOnMainThread(() =>
                {
                    _nativeMap.AddAnnotations(_annotations);
                    _nativeMap.AddOverlay(_geoDesicLine);
                    _routePointsLine = MKPolyline.FromCoordinates(cordinates);
                    _nativeMap.AddOverlay(_routePointsLine);
                });
            }
        }

    }
    public class RoutePointWithIndex
    {
        public RoutePointDto P
        {
            get; set;
        }
        public int Index
        {
            get; set;
        }
    }
}
