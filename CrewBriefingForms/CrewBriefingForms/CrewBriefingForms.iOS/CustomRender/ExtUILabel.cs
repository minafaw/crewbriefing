﻿using System;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;

namespace CrewBriefingForms.iOS.CustomRender
{
	public class ExtUILabel : UILabel
    {
		UIEdgeInsets edgeInsets;
        public ExtUILabel(Thickness padding)
        {
			edgeInsets = new UIEdgeInsets((nfloat)padding.Top, (nfloat)padding.Left, (nfloat)padding.Bottom, (nfloat)padding.Right);
        }

		public override void DrawText(CGRect rect)
		{
			base.DrawText(edgeInsets.InsetRect( rect));
		}

		public override CGSize IntrinsicContentSize {
			get{
				var originalSize = base.IntrinsicContentSize;
				originalSize.Width += edgeInsets.Left + edgeInsets.Right;
				originalSize.Height += edgeInsets.Top + edgeInsets.Bottom;

				return originalSize;
			}
		}
	}
}
