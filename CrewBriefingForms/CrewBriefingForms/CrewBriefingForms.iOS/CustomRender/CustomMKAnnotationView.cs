﻿using MapKit;
using UIKit;

namespace CrewBriefingForms.iOS.CustomRender
{
	public class CustomMKAnnotationView : MKAnnotationView
	{
		public string Id { get; set; }

		public CustomMKAnnotationView(IMKAnnotation annotation, string id)
			: base(annotation, id)
		{
            Id = id;
		}
	}
    public class CustomMKPointAnnotation : MKPointAnnotation
    {
        public UIImage Image { get; set; }
    }
}
