﻿using System;
using CB.Core.CustomControls.Controls;
using CrewBriefingForms.iOS.CustomRender;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtEntry) , typeof(ExtEntryRender) ) ]
namespace CrewBriefingForms.iOS.CustomRender
{
    public class ExtEntryRender : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                // do whatever you want to the UITextField here!
              // Control.BackgroundColor = UIColor.FromRGB(204, 153, 255);
               // Control.BorderStyle = UITextBorderStyle.Line;
            }
        }
    }
}
