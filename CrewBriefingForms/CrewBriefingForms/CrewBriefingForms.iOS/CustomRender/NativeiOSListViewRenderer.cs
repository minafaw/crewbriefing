﻿using CB.Core.CustomControls.Controls;
using CrewBriefingForms.iOS.CustomRender;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtListView), typeof(NativeiOSListViewRenderer))]
namespace CrewBriefingForms.iOS.CustomRender
{
    public class NativeiOSListViewRenderer : ListViewRenderer
    {

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                // Unsubscribe
            }

            if (e.NewElement != null)
            {
                Control.SeparatorInset = UIEdgeInsets.Zero;
                Control.LayoutMargins = UIEdgeInsets.Zero;

                if (UIDevice.CurrentDevice.CheckSystemVersion(9, 0))
                    Control.CellLayoutMarginsFollowReadableWidth = false;
                
            }
        }
    }
}