using System;
using Xamarin.Forms;
using CrewBriefingForms.iOS;
using CB.Core.Common.Interfaces;

[assembly: Dependency(typeof(NetworkStatusIOS))]
namespace CrewBriefingForms.iOS
{
    public class NetworkStatusIOS : INetworkStatus
    {
        public bool IsInternet()
        {
            // done wast before
            return Reachability.IsHostReachable(Reachability.HostName);
        }
    }
}