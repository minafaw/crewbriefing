﻿using System;
using CoreGraphics;
using UIKit;
using Xamarin.Forms.Platform.iOS;

namespace CrewBriefingForms.iOS.Helper
{
    public class FormsViewToNativeiOS
    {
		public static UIView ConvertFormsToNative(Xamarin.Forms.View view, CGRect size)
		{
			var renderer = Platform.GetRenderer(view);

			renderer.NativeView.Frame = size;

			renderer.NativeView.AutoresizingMask = UIViewAutoresizing.All;
			renderer.NativeView.ContentMode = UIViewContentMode.ScaleToFill;

			renderer.Element.Layout(size.ToRectangle());

			var nativeView = renderer.NativeView;

			nativeView.SetNeedsLayout();

			return nativeView;
		}


		public static UIView ConvertFormsToNative(Xamarin.Forms.View view)
		{
			var renderer = Platform.GetRenderer(view);

			renderer.NativeView.AutoresizingMask = UIViewAutoresizing.All;
			renderer.NativeView.ContentMode = UIViewContentMode.ScaleToFill;
			var nativeView = renderer.NativeView;

			nativeView.SetNeedsLayout();

			return nativeView;
		}
    }
}
