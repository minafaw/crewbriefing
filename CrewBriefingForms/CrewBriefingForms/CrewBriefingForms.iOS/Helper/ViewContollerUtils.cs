﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace CrewBriefingForms.iOS.Helper
{
    internal class ViewContollerUtils
    {

        // Search recursively for the top most UIViewController
        // Source: http://stackoverflow.com/questions/41241508/xamarin-forms-warning-attempt-to-present-on-whose-view-is-not-in-the-window
        public static UIViewController GetVisibleViewController(UIViewController controller = null)
        {
            controller = controller ?? UIApplication.SharedApplication.KeyWindow.RootViewController;

            if (controller.PresentedViewController == null)
                return controller;

            var navigationController = controller.PresentedViewController as UINavigationController;
            if (navigationController != null)
            {
                return navigationController.VisibleViewController;
            }

            var barController = controller.PresentedViewController as UITabBarController;
            if (barController != null)
            {
                return barController.SelectedViewController;
            }

            return GetVisibleViewController(controller.PresentedViewController);
        }
    }
}
