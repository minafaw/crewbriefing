﻿using System;
using Foundation;
using QuickLook;

namespace CrewBriefingForms.iOS
{
	public class QlItem : QLPreviewItem
	{
		string title;
		NSUrl uri;

		public QlItem(string title, NSUrl uri)
		{
			this.title = title;
			this.uri = uri;
		}

		public override string ItemTitle
		{
			get { return title; }
		}

		public override NSUrl ItemUrl
		{
			get { return uri; }
		}
	}
}
