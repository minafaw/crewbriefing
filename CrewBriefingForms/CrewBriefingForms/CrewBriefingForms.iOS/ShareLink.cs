﻿using System;
using CB.Core.Common.Interfaces;
using CrewBriefingForms.iOS;
using Foundation;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(ShareLink))]
namespace CrewBriefingForms.iOS
{
    public class ShareLink : IShareLink
    {
        public bool CanOpenUri(string url)
        {
            return UIApplication.SharedApplication.CanOpenUrl(NSUrl.FromString(url));
        }

        public void OpenUri(string url)
        {
            UIApplication.SharedApplication.OpenUrl(new NSUrl(url));
        }
    }
}
