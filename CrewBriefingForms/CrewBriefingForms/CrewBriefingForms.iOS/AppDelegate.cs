﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Xamarin.Forms;
using HockeyApp.iOS;
using UserNotifications;
using Firebase.CloudMessaging;
using Firebase.Analytics;
using static System.Net.Mime.MediaTypeNames;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Push;
using CrewBriefing.Standard;
using CB.Core.Common.Interfaces;
using CrewBriefingForms.iOS.Extensions;

namespace CrewBriefingForms.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, IUNUserNotificationCenterDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {

            global::Xamarin.Forms.Forms.Init();

            #region Code for starting up the Xamarin Test Cloud Agent

            // Newer version of Visual Studio for Mac and Visual Studio provide the
            // ENABLE_TEST_CLOUD compiler directive to prevent the Calabash DLL from
            // being included in the released version of the application.
#if ENABLE_TEST_CLOUD
            Xamarin.Calabash.Start();
#endif
            #endregion


            var manager = BITHockeyManager.SharedHockeyManager;
            manager.Configure("703c1b3a1999473e805c308e2d6e699f");
            manager.StartManager();
            manager.Authenticator.AuthenticateInstallation(); // This line is obsolete in crash only builds

            UIColor tintColor = UIColor.White;
            UINavigationBar.Appearance.TintColor = tintColor;
			UINavigationBar.Appearance.BarTintColor = ColorExtension.FromHexString("#445C74");
			UINavigationBar.Appearance.TitleTextAttributes = new UIStringAttributes() { ForegroundColor = UIColor.White};
		

			Xamarin.FormsMaps.Init();
           

            Forms.ViewInitialized += (object sender, ViewInitializedEventArgs e) =>
            {
                // http://developer.xamarin.com/recipes/testcloud/set-accessibilityidentifier-ios/
                if (null != e.View.AutomationId)
                {
                    e.NativeView.AccessibilityIdentifier = e.View.AutomationId;
                }
            };
			Dictionary<Type, Type> MappedTyped = new Dictionary<Type, Type>();
			// key instance of class , value is the interface
			MappedTyped.Add(typeof(SaveAndLoad_IOS), typeof(ISaveAndLoad));

			LoadApplication(new App());

            // This should come before AppCenter.Start() is called
            Push.PushNotificationReceived += (sender, e) => {

                // Add the notification message and title to the message
                var summary = $"Push notification received:" +
                                    $"\n\tNotification title: {e.Title}" +
                                    $"\n\tMessage: {e.Message}";

                // If there is custom data associated with the notification,
                // print the entries
                if (e.CustomData != null)
                {
                    summary += "\n\tCustom data:\n";
                    foreach (var key in e.CustomData.Keys)
                    {
                        summary += $"\t\t{key} : {e.CustomData[key]}\n";
                    }
                }

                // Send the notification summary to debug output
                System.Diagnostics.Debug.WriteLine(summary);
            };


            AppCenter.Start("703c1b3a-1999-473e-805c-308e2d6e699f", typeof(Push) );

        

            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // iOS 10
                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, (granted, error) =>
                {
                    Console.WriteLine(granted);
                });

                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.Current.Delegate = this;
            }
            else
            {

                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);


                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }

            UIApplication.SharedApplication.RegisterForRemoteNotifications();



            Firebase.InstanceID.InstanceId.Notifications.ObserveTokenRefresh((sender, e) =>
            {
                var newToken = Firebase.InstanceID.InstanceId.SharedInstance.Token;
                // if you want to send notification per user, use this token
                System.Diagnostics.Debug.WriteLine(newToken);

                connectFCM();
            });

            Firebase.Core.App.Configure();

            return base.FinishedLaunching(app, options);
        }



        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {

            Push.RegisteredForRemoteNotifications(deviceToken);

#if DEBUG
            Messaging.SharedInstance.SetApnsToken(deviceToken, ApnsTokenType.Sandbox);
#endif
#if RELEASE
            Messaging.SharedInstance.SetApnsToken(deviceToken, ApnsTokenType.Production);
#endif



        }

        public override void DidEnterBackground(UIApplication uiApplication)
        {
            Messaging.SharedInstance.ShouldEstablishDirectChannel = false;
        }

        public override void OnActivated(UIApplication uiApplication)
        {
            connectFCM();
            base.OnActivated(uiApplication);

        }


        private void connectFCM()
        {
            Messaging.SharedInstance.Connect((error) =>
            {
                if (error == null)
                {
                    Messaging.SharedInstance.Subscribe("/topics/all");
                }
                System.Diagnostics.Debug.WriteLine(error != null ? "error occured" : "connect success");
            });
        }

    public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
    {
            Push.FailedToRegisterForRemoteNotifications(error);
            new UIAlertView("FailedToRegisterForRemoteNotifications", "FailedToRegisterForRemoteNotifications", null, "OK", null).Show();
            Console.WriteLine("Error AppDelegate push notification");
            new UIAlertView("Error For Push Notification", error.LocalizedDescription, null, "OK", null).Show();
    }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {

            //Debug.WriteLine(userInfo.ToString());
            NSObject inAppMessage;

            bool success = userInfo.TryGetValue(new NSString("inAppMessage"), out inAppMessage);

            if (success)
            {
                var alert = new UIAlertView("Got push notification", inAppMessage.ToString(), null, "OK", null);
                alert.Show();
            }
        
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, System.Action<UIBackgroundFetchResult> completionHandler)
        {
            var result = Push.DidReceiveRemoteNotification(userInfo);
            if (result)
            {
                completionHandler?.Invoke(UIBackgroundFetchResult.NewData);
            }
            else
            {
                completionHandler?.Invoke(UIBackgroundFetchResult.NoData);
            }
        }

        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification localNotification)
        {
            new UIAlertView(localNotification.AlertAction, localNotification.AlertBody, null, "OK", null).Show();
            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
        }

        [Export("userNotificationCenter:willPresentNotification:withCompletionHandler:")]
        public void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            // Do your magic to handle the notification data
            System.Console.WriteLine(notification.Request.Content.UserInfo);

            HandlePushNotification(notification.Request);

            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
        }

        // Receive data message on iOS 10 devices.
        public void ApplicationReceivedRemoteMessage(RemoteMessage remoteMessage)
        {
            Console.WriteLine(remoteMessage.AppData);
           
        }

        [Export("userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler:")]
        public void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            // Do your magic to handle the notification data
            System.Console.WriteLine(response.Notification.Request.Content.UserInfo);
            HandlePushNotification(response.Notification.Request);

        }

        private void HandlePushNotification(UNNotificationRequest request){

            if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
            {
                System.Diagnostics.Debug.WriteLine(request.Content.UserInfo);
               
                string title = "CrewBriefing";

                var app_title = request.Content.UserInfo["google.c.a.c_l"] as NSString;

                if(!string.IsNullOrEmpty(app_title) ){
                    title = app_title;
                }else if(!string.IsNullOrEmpty(request.Content.Title )){
                    title = request.Content.Title;
                }
                new UIAlertView(title, request.Content.Body , null, "OK", null).Show();

            }
            
        }

}
}
