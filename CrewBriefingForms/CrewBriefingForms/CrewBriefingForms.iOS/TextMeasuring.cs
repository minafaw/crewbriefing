﻿using Xamarin.Forms;
using System;
using CrewBriefingForms.iOS;
using Foundation;
using System.Drawing;
using UIKit;
using CoreGraphics;
using CB.Core.Common.Interfaces;

[assembly: Dependency(typeof(TextMeasuringIOS))]
namespace CrewBriefingForms.iOS
{
    public class TextMeasuringIOS : ITextMeasuring
    {
       
        private static int screenWidthPixels = 0;
        private static int screenHeightPixels = 0;

        public Xamarin.Forms.Size ScreenSizeInFormPixels()
        {
            //used to calculate CommonStatic.ScreenCoef
            screenWidthPixels = (int)UIKit.UIScreen.MainScreen.Bounds.Width;
            screenHeightPixels = (int)UIKit.UIScreen.MainScreen.Bounds.Height;
            return new Xamarin.Forms.Size(UIKit.UIScreen.MainScreen.Bounds.Width, UIKit.UIScreen.MainScreen.Bounds.Height);
        }
        private static double screenWidthInches = 0;
        private static double screenHeightInches = 0;

        private static double xdpi = 0, ydpi = 0;
        public static double Xdpi => xdpi;
	    public static double Ydpi => ydpi;

	    public Xamarin.Forms.Size ScreenDPI()
        {
            /*
            var device = AppleDevice.CurrentDevice;
            var display = device.Display;
            UIKit.UIScreen.MainScreen.Bounds.
              var inchX = display.WidthRequestInInches(1);
            var inchY = display.HeightRequestInInches(1);
            */
            xdpi = 160;
            ydpi = 160;
            return new Xamarin.Forms.Size(160, 160);

        }

        public double ScreenSizeInInches()
        {
            Xamarin.Forms.Size s = ScreenSizeInFormPixels();

            xdpi = 160;
            ydpi = 160;

            screenWidthInches = s.Width/ xdpi;
            screenHeightInches = s.Height / xdpi;

            var size = Math.Sqrt(Math.Pow(screenWidthInches, 2) +
                                    Math.Pow(screenHeightInches, 2));
            return size;
        }
        public static double GetNamedSizeDoubleForLabel(NamedSize size)
        {
            return Device.GetNamedSize(size, typeof(Label));
        }
        

        
        UILabel uiLabel = null;
        private Xamarin.Forms.Size calculateSize(string text, NamedSize fontSizeNamed)
        {
            CGSize length;

            uiLabel = new UILabel();
            uiLabel.Text = text;

            string fontName = "HelveticaNeue";
            float fontSize = (float)GetNamedSizeDoubleForLabel(fontSizeNamed);
            UIFont font = UIFont.FromName(fontName, fontSize);

            length = uiLabel.Text.StringSize(uiLabel.Font);
            return new Xamarin.Forms.Size(length.Width, length.Height);

            /*
            var nsText = new NSString(text);
            var boundSize = new SizeF((float)width, float.MaxValue);
            var options = NSStringDrawingOptions.UsesFontLeading |
                NSStringDrawingOptions.UsesLineFragmentOrigin;

            if (fontName == null)
            {
                fontName = "HelveticaNeue";
            }

            var attributes = new UIStringAttributes
            {
                Font = UIFont.FromName(fontName, (float)fontSize)
            };

            var sizeF = nsText.GetBoundingRect(boundSize, options, attributes, null).Size;

            return new Xamarin.Forms.Size((double)sizeF.Width, (double)sizeF.Height);*/
        }

		public double CalculateWidth(string text, NamedSize fontSize)
		{
			return calculateSize(text, fontSize).Width;
		}

		public double CalculateHeight(string text, NamedSize fontSize)
		{
			return calculateSize(text, fontSize).Height;
		}
	}
}
