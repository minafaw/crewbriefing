using System.IO;
using System.Threading.Tasks;
using Xamarin.Forms;
using System;
using CrewBriefingForms.iOS;
using Foundation;
using QuickLook;
using UIKit;
using CB.Core.Common.Interfaces;
using CrewBriefingForms.iOS.Extensions;

[assembly: Dependency(typeof(SaveAndLoad_IOS))]
namespace CrewBriefingForms.iOS
{
    public class SaveAndLoad_IOS: ISaveAndLoad
    {
        public static string AppDataFolder => Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

	    public async Task SaveFile(string fileName, byte[] data)
        {
            string filePath = Path.Combine(AppDataFolder, fileName);
            File.WriteAllBytes(filePath, data);
        }
        public async Task<byte[]> LoadFile(string fileName)
        {
            string filePath = Path.Combine(AppDataFolder, fileName);
            return  System.IO.File.ReadAllBytes(filePath);
        }
        public void RemoveFile(string fileName)
        {
            string filePath = Path.Combine(AppDataFolder, fileName);
            File.Delete(filePath);
        }
        public void RemoveAllFilesInLocalStorage(string fileNameMaskStart)
        {
            var documentsPath = AppDataFolder;
            foreach (var filePath in Directory.GetFiles(documentsPath))
            {
                if (!string.IsNullOrEmpty(fileNameMaskStart))
                {
                    if (!filePath.ToLower().StartsWith(fileNameMaskStart))
                    {
                        continue;
                    }
                }
                if (filePath.ToLower().EndsWith(".db3"))
                {
                    continue;
                }
                File.Delete(filePath);
            }

        }

        public async Task OpenFileWithDefaultOsViewer(string fileName, string title)
        {
            //SaveAndLoad_IOS files = new SaveAndLoad_IOS();
           // byte[] b = await files.LoadFile(fileName);

            HandleViewPdf(fileName, title);
        }

        private static void HandleViewPdf(string fileName, string title)
        {
            var localDocUrl = Path.Combine( AppDataFolder, fileName);
            // Utils.GoToPage(null, new PageViewIOS(fileName, false, title));

            Device.BeginInvokeOnMainThread(() =>
            {
				
				var previewController = new MyQLPreviewController
				{
                    DataSource = new MyQLPreviewControllerDataSource(localDocUrl, title) ,
				
				};
				UINavigationController navigationController = new UINavigationController(previewController)
				{
					//NavigationController.NavigationBar.TintColor = ColorExtension.FromHexString("#445C74")

				};


				UIApplication.SharedApplication.KeyWindow.RootViewController.PresentViewController(navigationController, true, null);

            });

        }

        public async Task<FileTextAndPath> LoadTextFile(string fileName)
        {
	        var result = new FileTextAndPath
	        {
		        FileName = fileName,
		        TextContent = System.IO.File.ReadAllText(fileName)
	        };


	        return result;

        }

        public string AppPath(string pathFile)
        {
            return  Path.Combine(NSBundle.MainBundle.BundlePath, pathFile);
        }

    }

}