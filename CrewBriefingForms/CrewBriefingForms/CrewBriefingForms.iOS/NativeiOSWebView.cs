﻿using System;
using System.Collections.Generic;
using System.Text;
using CrewBriefingForms.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ExtWebView), typeof(NativeiOSWebView))]
namespace CrewBriefingForms.iOS
{
    class NativeiOSWebView : WebViewRenderer
    {
    }
}
