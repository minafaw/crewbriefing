using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using Xamarin.Forms;
using CrewBriefingForms.Droid;
using System.Threading.Tasks;
using System.Threading;
using CB.Core.Common.Interfaces;
using static CB.Core.Common.Keys.Enums;

[assembly: Dependency(typeof(ToastDroid))]
namespace CrewBriefingForms.Droid
{
    public class ToastDroid : IToast
    {
        public Task<int> ShowToast(ToastType type, string message, uint msec)
        {
            string prefix = string.Empty;
            ToastLength showLen = ToastLength.Long;
            if (type == ToastType.Error)
            {
                prefix = CB.Core.Common.Resource.error.ToUpper() + ": ";
            }
            else if (type == ToastType.Warning)
            {
                prefix = CB.Core.Common.Resource.warning + ": ";
            }
            else
            {
                showLen = ToastLength.Short;
            }
            Toast.MakeText(Android.App.Application.Context,
                prefix + message,
                showLen).Show();
            var task = Task<int>.Factory.StartNew(() => {
                Thread.Sleep(100);
                return 0;
            });
            return task;
        }
    }
}