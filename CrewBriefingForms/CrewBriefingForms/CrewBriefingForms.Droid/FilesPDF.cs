using System.IO;
using CrewBriefingForms.Droid;
using System.Threading.Tasks;
using Xamarin.Forms;
using Android.Content.Res;
using CB.Core.Common.Interfaces;
using CrewBriefing.Standard;

[assembly: Dependency(typeof(SaveAndLoad_Droid))]
namespace CrewBriefingForms.Droid
{
	public class SaveAndLoad_Droid: ISaveAndLoad
    {
        public async Task SaveFile(string fileName, byte[] data)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, fileName);
            System.IO.File.WriteAllBytes(filePath, data);
        }
        public string AppPath(string pathFile)
        {
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            return Path.Combine(documentsPath, pathFile);
        }

        public async Task<byte[]> LoadFile(string fileName)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, fileName);
            return System.IO.File.ReadAllBytes(filePath);
        }
        public void RemoveFile(string fileName)
        {
            var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var filePath = Path.Combine(documentsPath, fileName);
            File.Delete(filePath);
        }
        public void RemoveAllFilesInLocalStorage(string fileNameMaskStart)
        {
            string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            foreach (string filePath in Directory.GetFiles(documentsPath))
            {
               
                if (!filePath.ToLower().EndsWith(".pdf"))
                {
                    continue;
                }
                File.Delete(filePath);
            }

        }

		public async Task<FileTextAndPath> LoadTextFile(string fileName)
        {
            AssetManager assets = Android.App.Application.Context.Assets;
            FileTextAndPath result = new FileTextAndPath();

            using (StreamReader sr = new StreamReader(assets.Open(fileName)))
            {
                result.TextContent = sr.ReadToEnd();
            }
            return result;
        }

		public async Task OpenFileWithDefaultOsViewer(string fileName, string title)
		{
			var documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			var filePath = Path.Combine(documentsPath, fileName);
			PdfModel pdfModel = new PdfModel() { FilePath = filePath, Title = title };
			await App.NavigationService.NavigateAsync(nameof(PdfViewPage), pdfModel);
			
		}
	}

}