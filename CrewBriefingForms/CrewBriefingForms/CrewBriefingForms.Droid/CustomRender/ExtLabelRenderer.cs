﻿using Android.Content;
using Android.Graphics;
using CB.Core.CustomControls.Controls;
using CrewBriefingForms.Droid.CustomRender;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ExtLabel), typeof(ExtLabelRenderer))]
namespace CrewBriefingForms.Droid.CustomRender
{
    public class ExtLabelRenderer : LabelRenderer
    {
        public ExtLabelRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);
            var extLabel = e.NewElement  as ExtLabel;
            if (Control != null && extLabel != null)
            {
                if ( extLabel.IsUnderlined)
                {
                    Control.PaintFlags = PaintFlags.UnderlineText;
                }

                if (extLabel.Padding != default(Thickness))
                {
                    Control.SetPadding((int)extLabel.Padding.Left, (int)extLabel.Padding.Top, (int)extLabel.Padding.Right, (int)extLabel.Padding.Bottom);
                }

            }
        }
    }
}