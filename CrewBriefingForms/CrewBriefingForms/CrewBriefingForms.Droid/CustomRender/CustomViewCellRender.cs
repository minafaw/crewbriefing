﻿using Android.Content;
using Android.Views;
using CB.Core.CustomControls.Controls;
using CrewBriefingForms.Droid.CustomRender;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(ExtViewCell), typeof(CustomViewCellRender))]
namespace CrewBriefingForms.Droid.CustomRender
{

    public class CustomViewCellRender : ViewCellRenderer
    {
        protected override View GetCellCore(Cell item, View convertView, ViewGroup parent, Context context)
        {
            var cell = base.GetCellCore(item, convertView, parent, context);


            var listView = parent as Android.Widget.ListView;

            if (listView != null && cell != null)
            {
                cell.SetBackgroundResource(Resource.Drawable.ViewCellBackground);
              
            }

            return cell;
        }
    }
}
