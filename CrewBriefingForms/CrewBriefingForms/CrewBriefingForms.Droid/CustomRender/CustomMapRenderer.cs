﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using CB.Core.Dtos;
using CrewBriefing.Standard;
using CrewBriefingForms.Droid.CustomRender;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.Android;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRendererDroid))]
namespace CrewBriefingForms.Droid.CustomRender
{
    public class CustomMapRendererDroid :MapRenderer
    {
        private FlightDto _flightInfo;
        private int _markerScale = 2;
        private bool isDrawn = false;

        public CustomMapRendererDroid(Context context) : base(context)
         {
         }

         protected override void OnMapReady(GoogleMap map)
         {
             base.OnMapReady(map);
			 isDrawn = false;


			 MyDrawMap();

         }

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            // https://developer.xamarin.com/guides/xamarin-forms/application-fundamentals/custom-renderer/map/customized-pin/
            base.OnElementChanged(e);
            if (e.OldElement != null)
            {
				var oldMap = (Control as MapView);
				oldMap.RemoveAllViews();
            }

            if (e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;


                _flightInfo = formsMap.FlightInfo;
                try
                {
					var sizeInInch = new TextMeasuring().ScreenSizeInInches();

					if (TextMeasuring.Xdpi > 0 && TextMeasuring.Ydpi > 0 && !double.IsInfinity(TextMeasuring.Xdpi) && !double.IsInfinity(TextMeasuring.Ydpi))
                    {
                        double dpi = TextMeasuring.Xdpi > TextMeasuring.Ydpi ? TextMeasuring.Xdpi : TextMeasuring.Ydpi;
                        int i = (int)Math.Round(dpi / 96);
                        if (i >= 1 && i <= 4)
                        {
                            _markerScale = i;
                        }
                        else if (i < 1)
                        {
                            _markerScale = 1;
                        }
                        else if (i > 4)
                        {
                            _markerScale = 4;
                        }

                    }
                    else
                    {
                        _markerScale = 1;
                    }
                }
                catch
                {
                    _markerScale = 1;
                }
                ((MapView)Control).GetMapAsync(this);

            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
               MyDrawMap();
              
       }  
        private void MyDrawMap()
        {

            if (NativeMap == null)
            {
                return;
            }
            if (_flightInfo == null)
            {
                return;
            }

            else if (_flightInfo.RoutePoints == null)
            {
                return;
            }
            else if (_flightInfo.RoutePoints.Count == 0)
            {
                return;
            }

			

			if (!isDrawn )
            {
				NativeMap?.Clear();

				PolylineOptions polylineOptionsGeodesic = new PolylineOptions();
                polylineOptionsGeodesic.InvokeColor(0x75E00000);
                polylineOptionsGeodesic.InvokeWidth(2);
                polylineOptionsGeodesic.Geodesic(true);

                PolylineOptions polylineOptionsFlight = new PolylineOptions();
                polylineOptionsFlight.InvokeColor(Android.Graphics.Color.ParseColor("#DD0000E0"));
                polylineOptionsFlight.InvokeWidth(3);
                polylineOptionsFlight.Geodesic(false);


                BitmapDescriptor descriptor = BitmapDescriptorFactory.FromAsset("ICAO_symbol_121_on_request_blue_"+ _markerScale.ToString() + ".png");
                BitmapDescriptor descriptorStart = BitmapDescriptorFactory.FromAsset("ICAO_symbol_84_blue_" + _markerScale.ToString() + ".png");
                BitmapDescriptor descriptorEnd = BitmapDescriptorFactory.FromAsset("ICAO_symbol_84_orange_" + _markerScale.ToString() + ".png");

                for (int i = 0; i < _flightInfo.RoutePoints.Count; i++)
                {
                    RoutePointDto p = _flightInfo.RoutePoints[i];

                    var marker = new MarkerOptions();
                    marker.SetPosition(new LatLng(p.Lat, p.Lon));
                    marker.Anchor(0.5f, 0.5f);
					
                    //? marker.SetSnippet(pin.Pin.Address);

                    if (i == 0 || i == _flightInfo.RoutePoints.Count - 1)  //DEP and DEST
                    {
                        marker.SetIcon(i == 0 ? descriptorStart : descriptorEnd);
                        
                        string _title = i == 0 ? _flightInfo.Airports4ShowDep : _flightInfo.Airports4ShowDst;
                        if (!string.IsNullOrEmpty(_title))
                        {
                            marker.SetTitle(_title);
                        }
                        polylineOptionsGeodesic.Add(new LatLng(p.Lat, p.Lon));
                    }
                    else {
                        marker.SetTitle(p.IdEnt);
                        marker.SetIcon(descriptor);
                    }
                    polylineOptionsFlight.Add(new LatLng(p.Lat, p.Lon));

                    NativeMap.AddMarker(marker);

                }
                NativeMap.AddPolyline(polylineOptionsGeodesic);
                NativeMap.AddPolyline(polylineOptionsFlight);

				isDrawn = true;
			}
        }

    }
}
