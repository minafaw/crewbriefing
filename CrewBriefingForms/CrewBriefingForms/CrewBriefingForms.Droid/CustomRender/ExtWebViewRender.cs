﻿
using Android.Content;
using Android.OS;
using Android.Webkit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using CrewBriefingForms.Droid.CustomRender;
using CrewBriefingForms.Droid.Helper;
using WebView = Xamarin.Forms.WebView;
using CB.Core.CustomControls.Controls;

[assembly: ExportRenderer(typeof(ExtWebView) , typeof(ExtWebViewRender))]
namespace CrewBriefingForms.Droid.CustomRender
{
    class ExtWebViewRender : WebViewRenderer
    {
        private readonly Context _context;

        public ExtWebViewRender(Context context) : base(context)
        {
            _context = context;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);


            if (Control == null)
            {
                var webView = new Android.Webkit.WebView(_context);
                webView.Settings.JavaScriptEnabled = true;
                SetNativeControl(webView);
            }
            if (e.NewElement != null)
            {
                Control.Settings.JavaScriptEnabled = true;
                Control.Settings.DomStorageEnabled = true;
                Control.SetWebViewClient(new CustomWebViewClient());
            }


        }

        public class CustomWebViewClient : WebViewClient
        {
            readonly string _javaScriptCode = "javascript:document.getElementById('versionCode').innerHTML = '" + "Version: " +
                                    VersionUtils.GetAppVersionBuild() + "';";

            public override void OnPageFinished(Android.Webkit.WebView webView, string url)
            {
                if (Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat)
                {
                    webView.EvaluateJavascript(_javaScriptCode, null);
                }
                else
                {
                    webView.LoadUrl(_javaScriptCode);
                }
            }

           
        }

        
    }
}