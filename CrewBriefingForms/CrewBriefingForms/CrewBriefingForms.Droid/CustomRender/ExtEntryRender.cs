﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Widget;
using CB.Core.CustomControls.Controls;
using CrewBriefingForms.Droid.CustomRender;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(ExtEntry), typeof(ExtEntryRender))]
namespace CrewBriefingForms.Droid.CustomRender
{
    public class ExtEntryRender : EntryRenderer
    {
        Context _context;
        public ExtEntryRender(Context context) : base(context)
        {
            _context = context;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
              //  Control.SetTextAppearance(_context , Resource.Style.ExtEditTextStyle);

                if(e.OldElement != null){
                    //ChangeCursorBG();
                    
                }
                
				var newView = e.NewElement as ExtEntry;
				if (newView != null)
                {
                  
                    Control.Background.SetColorFilter(Android.Graphics.Color.White  , PorterDuff.Mode.SrcIn);               

    				if (newView.HasBorder)
                    {
                        var shape = new ShapeDrawable(new Android.Graphics.Drawables.Shapes.RectShape());
    					shape.Paint.Color = newView.BorderColor.ToAndroid();
                        shape.Paint.SetStyle(Paint.Style.Stroke);
                        Control.Background = shape;
                    }
				}
            }

           
        }

        private void ChangeCursorBG(){
            IntPtr IntPtrtextViewClass = JNIEnv.FindClass(typeof(TextView));
            IntPtr mCursorDrawableResProperty = JNIEnv.GetFieldID(IntPtrtextViewClass, "mCursorDrawableRes", "I");
            JNIEnv.SetField(Control.Handle, mCursorDrawableResProperty, Resource.Drawable.WhiteCursorDrawable);
        }
    }
}
