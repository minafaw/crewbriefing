﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Xamarin.Forms;
using Android.Content;
using HockeyApp.Android;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using Microsoft.AppCenter.Push;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using CrewBriefing.Standard;
using System.Collections.Generic;
using CB.Core.Common.Interfaces;

namespace CrewBriefingForms.Droid
{
    [Activity( Label = "CrewBriefing", Icon = "@drawable/icon", MainLauncher = false , ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

         //   AppDomain.CurrentDomain.UnhandledException += CurrentDomainOnUnhandledException;
          // TaskScheduler.UnobservedTaskException += TaskSchedulerOnUnobservedTaskException;  

			ToolbarResource = Resource.Layout.Toolbar;
            global::Xamarin.Forms.Forms.SetFlags("FastRenderers_Experimental");
            global::Xamarin.Forms.Forms.Init(this, bundle);
            //see SpalshActivity
            Forms.SetTitleBarVisibility(this , AndroidTitleBarVisibility.Never);

            Xamarin.FormsMaps.Init(this, bundle);
           // FFImageLoading.Forms.Droid.CachedImageRenderer.Init(true);
            VersionDroid.FormsInitialized = true;

                Forms.ViewInitialized += (object sender, ViewInitializedEventArgs e) => {
                if (!string.IsNullOrWhiteSpace(e.View.AutomationId))
                {
                    e.NativeView.ContentDescription = e.View.AutomationId;
                }
            };

            Push.SetSenderId("945205565427");
            AppCenter.Start("a425fa12-1a32-49dd-b43f-9d357fbd6aa3", typeof(Push) , typeof(Analytics));


            var MappedTyped = new Dictionary<Type, Type>
            {
                // key instance of class , value is the interface
                { typeof(SaveAndLoad_Droid), typeof(ISaveAndLoad) }
            };

            LoadApplication(new App());
			// Register PdfViewPage
			App.NavigationService.Configure(nameof(PdfViewPage), typeof(PdfViewPage));
		}

          

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
        }
        protected override void OnResume()
        {
            base.OnResume();            
            CrashManager.Register(this, "a425fa121a3249ddb43f9d357fbd6aa3");
            // https://support.hockeyapp.net/kb/client-integration-cross-platform/how-to-integrate-hockeyapp-with-xamarin#integrate-sdk

        }

       
        private static void TaskSchedulerOnUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs unobservedTaskExceptionEventArgs)
        {
            var newExc = new Exception("TaskSchedulerOnUnobservedTaskException", unobservedTaskExceptionEventArgs.Exception);
            LogUnhandledException(newExc);
        }  

        private static void CurrentDomainOnUnhandledException(object sender, UnhandledExceptionEventArgs unhandledExceptionEventArgs)
        {
            var newExc = new Exception("CurrentDomainOnUnhandledException", unhandledExceptionEventArgs.ExceptionObject as Exception);
            LogUnhandledException(newExc);
        }  

        internal static void LogUnhandledException(Exception exception)
        {
            try
            {
                const string errorFileName = "Fatal.log";
                var libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal); // iOS: Environment.SpecialFolder.Resources
                var errorFilePath = Path.Combine(libraryPath, errorFileName);
                var errorMessage = String.Format("Time: {0}\r\nError: Unhandled Exception\r\n{1}",
                DateTime.Now, exception.ToString());
                File.WriteAllText(errorFilePath, errorMessage);

                // Log to Android Device Logging.
                Android.Util.Log.Error("Crash Report", errorMessage);
            }
            catch
            {
                // just suppress any error logging exceptions
            }
        }

        /// <summary>
        // If there is an unhandled exception, the exception information is diplayed 
        // on screen the next time the app is started (only in debug configuration)
        /// </summary>
        [Conditional("DEBUG")]
        private void DisplayCrashReport()
        {
            const string errorFilename = "Fatal.log";
            var libraryPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            var errorFilePath = Path.Combine(libraryPath, errorFilename);

            if (!File.Exists(errorFilePath))
            {
                return;
            }

            var errorText = File.ReadAllText(errorFilePath);
            new AlertDialog.Builder(this)
                .SetPositiveButton("Clear", (sender, args) =>
                {
                    File.Delete(errorFilePath);
                })
                .SetNegativeButton("Close", (sender, args) =>
                {
            // User pressed Close.
        })
                .SetMessage(errorText)
                .SetTitle("Crash Report")
                .Show();
        } 
 



    }
}

