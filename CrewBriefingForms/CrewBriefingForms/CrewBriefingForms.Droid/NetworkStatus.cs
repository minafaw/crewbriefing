using System;
using Xamarin.Forms;

using CrewBriefingForms.Droid;
using Android.Net;
using Android.Content;
using CB.Core.Common.Interfaces;

[assembly: Dependency(typeof(NetworkStatusDroid))]
namespace CrewBriefingForms.Droid
{
    public class NetworkStatusDroid : INetworkStatus
    {
        public bool IsInternet()
        {
            try
            {
                var context = Android.App.Application.Context.ApplicationContext;
                var connectivityManager = (ConnectivityManager)context.GetSystemService(Context.ConnectivityService);

                var activeConnection = connectivityManager.ActiveNetworkInfo;

                if ((activeConnection != null) && activeConnection.IsConnected)
                {
                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return true;
            }

            return false;
        }
    }
}