﻿using System;
using Android.App;
using Android.Content;
using Android.Util;
using CrewBriefing.Standard;
using Firebase.Messaging;
using Xamarin.Forms;

namespace CrewBriefingForms.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class FirebaseNotificationService : FirebaseMessagingService
    {
        const string TAG = "FirebaseNotificationService";

        public override void OnMessageReceived(RemoteMessage message)
        {
            Log.Debug(TAG, "From: " + message.From);

            // Pull message body out of the template
            var messageBody = message.GetNotification().Body;
            if (string.IsNullOrWhiteSpace(messageBody))
                return;

            Log.Debug(TAG, "Notification message body: " + messageBody);
            SendNotification(messageBody , message.GetNotification().Title);
            MessagingCenter.Send<App, string>((App)Xamarin.Forms.Application.Current, "PushNotification", messageBody);
        }

        void SendNotification(string messageBody , string messageTitle)
        {
            var PassedTitle = "CrewBriefing";
            if(!string.IsNullOrEmpty(messageTitle)){
                PassedTitle = messageTitle;  
            }
            var intent = new Intent(Android.App.Application.Context, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);
            var pendingIntent = PendingIntent.GetActivity(this, 0, intent, PendingIntentFlags.OneShot);

            var notificationBuilder = new Notification.Builder(this)
                .SetContentTitle(PassedTitle)
                .SetSmallIcon(Resource.Drawable.icon)
                .SetContentText(messageBody)
                .SetContentIntent(pendingIntent)
                .SetAutoCancel(true);

            var notificationManager = NotificationManager.FromContext(Android.App.Application.Context);
            notificationManager.Notify(0, notificationBuilder.Build());
        }
    }
}
