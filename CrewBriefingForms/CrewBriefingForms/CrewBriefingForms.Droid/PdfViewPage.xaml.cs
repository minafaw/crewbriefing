﻿using System;
using Artifex.MuPdf;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Forms.Platform.Android;
using CrewBriefingForms.Droid.Helper;
using CB.Core.CustomControls.NavigationService;

namespace CrewBriefingForms.Droid
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PdfViewPage  
    {
        private readonly PdfModel pdf;
        private MuPDFCore core; 
        public PdfViewPage(PdfModel pdf )
        {
            InitializeComponent();
			CustomNavigationPage.SetTitleColor(this, Color.White);
			Title = pdf.Title;
            this.pdf = pdf;
            CustomView(pdf);
        }

        private void CustomView(PdfModel pdf)
        {
            if (pdf != null && !string.IsNullOrEmpty(pdf.FilePath))
            {
                core = new MuPDFCore(Android.App.Application.Context, pdf.FilePath);

                var mDocView = new MuPDFReaderView(Android.App.Application.Context);
                var pdfAdapter = new MuPDFPageAdapter(Android.App.Application.Context, null, core);
                mDocView.SetAdapter(pdfAdapter);

                var pdfContentPage = new ContentView()
                {
                    Content = mDocView.ToView(),
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Padding = 0,
                    VerticalOptions = LayoutOptions.FillAndExpand
                };
				mainPanel.Children.Add( pdfContentPage);

            }
            

        }

		private void ToolbarItem_Clicked(object sender, EventArgs e)
		{
			FileUtils.OpenFile(pdf.FilePath);
		}
	}
}