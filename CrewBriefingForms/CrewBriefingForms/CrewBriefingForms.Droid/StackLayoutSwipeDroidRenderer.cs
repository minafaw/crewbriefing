
using Android.Content;
using Android.Views;
using CrewBriefing.Standard.Controls;
using CrewBriefingForms.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(StackLayoutSwipe), typeof(StackLayoutSwipeDroidRenderer))]
namespace CrewBriefingForms.Droid
{
    public class StackLayoutSwipeDroidRenderer : VisualElementRenderer<StackLayout>
    {
        private readonly CustomGestureListener _listener;
        private readonly GestureDetector _detector;

        public StackLayoutSwipeDroidRenderer(Context context) : base(context)
        {
            _listener = new CustomGestureListener();
            _detector = new GestureDetector(_listener);
        }
        protected override void OnElementChanged(ElementChangedEventArgs<StackLayout> e)
        {
            base.OnElementChanged(e);
            if (e.NewElement == null)
            {
                this.GenericMotion -= HandleGenericMotion;
                this.Touch -= HandleTouch;
            }

            if (e.OldElement == null && _listener.sls == null)
            {
                if (e.NewElement != null)
                {
                    if (e.NewElement is StackLayoutSwipe)
                    {
                        StackLayoutSwipe sls = e.NewElement as StackLayoutSwipe;

                        this.GenericMotion += HandleGenericMotion;
                        this.Touch += HandleTouch;
                        _listener.sls = sls;
                    }
                }
            }
        }
        void HandleGenericMotion(object sender, GenericMotionEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }
        void HandleTouch(object sender, TouchEventArgs e)
        {
            _detector.OnTouchEvent(e.Event);
        }

    }
}