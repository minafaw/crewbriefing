using Android.Views;
using CrewBriefing.Standard.Controls;

namespace CrewBriefingForms.Droid
{
	public class CustomGestureListener : GestureDetector.SimpleOnGestureListener
	{
        //public event EventHandler SwipeRightCustom;
        //public event EventHandler SwipeLeftCustom;
        //public ImageGestureRecognizer igr = null;
        //public ScrollViewGestureRecognizer svgr = null;
        public StackLayoutSwipe sls = null;
        public override void OnLongPress (MotionEvent e)
		{
			base.OnLongPress (e);
		}

		public override bool OnDoubleTap (MotionEvent e)
		{
			return base.OnDoubleTap (e);
		}

		public override bool OnDoubleTapEvent (MotionEvent e)
		{
			return base.OnDoubleTapEvent (e);
		}

		public override bool OnSingleTapUp (MotionEvent e)
		{
			return base.OnSingleTapUp (e);
		}

		public override bool OnDown (MotionEvent e)
		{
			return base.OnDown (e);
		}

		public override bool OnFling (MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
            if (e2.RawX < e1.RawX )
            {
                if (sls != null)
                {
                    sls.SwipeLeftCustom();
                }
                return true;
            }
            if (e1.RawX < e2.RawX)
            {
                if (sls != null)
                {
                    sls.SwipeRightCustom();
                }
                return true;
            }
            /*
            if (svgr != null)
            {
                svgr.TouchUpCustom(" fing ");
                return true;
            }
            */

            return base.OnFling (e1, e2, velocityX, velocityY);

		}

		public override bool OnScroll (MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
		{
            //http://stackoverflow.com/questions/2089552/android-how-to-detect-when-a-scroll-has-ended

            
            return base.OnScroll (e1, e2, distanceX, distanceY);
		} 

		public override void OnShowPress (MotionEvent e)
		{
			base.OnShowPress (e);
		}

		public override bool OnSingleTapConfirmed (MotionEvent e)
		{
			return base.OnSingleTapConfirmed (e);
		}
    }
}