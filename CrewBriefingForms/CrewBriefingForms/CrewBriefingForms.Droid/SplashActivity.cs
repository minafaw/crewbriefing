﻿using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Util;
using Android.Graphics;
using Android.Support.V7.Widget;
using Android.Widget;


namespace CrewBriefingForms.Droid
{
    [Activity(Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : AppCompatActivity
    {
        private static readonly string Tag = "X:" + typeof (SplashActivity).Name;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.SplashLayout);

            var textTypeFace = Typeface.CreateFromAsset(Assets, "FORMARegular.otf");
            var textTypeFace2 = Typeface.CreateFromAsset(Assets, "TitilliumWeb-SemiBold.ttf");

            var header = FindViewById<TextView>(Resource.Id.headerText);
            var bottom = FindViewById<TextView>(Resource.Id.bottomText);


            header.Typeface = textTypeFace;
            bottom.Typeface = textTypeFace2;


            Log.Debug(Tag, "SplashActivity.OnCreate");

        }

        // Launches the startup task
        protected override void OnResume()
        {
            base.OnResume();
            Task startupWork = new Task(() => { SimulateStartup(); });
            startupWork.Start();
        }

        // Prevent the back button from canceling the startup process
        public override void OnBackPressed() { }

        // Simulates background work that happens behind the splash screen
        private async void SimulateStartup ()
        {
            
            Log.Debug(Tag, "Performing some startup work that takes a bit of time.");
            await Task.Delay(5000); // Simulate a bit of startup work.
            Log.Debug(Tag, "Startup work is finished - starting MainActivity.");
            StartActivity(typeof(MainActivity)); // Application.Context
        }
    }
}