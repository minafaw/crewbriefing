﻿using System;
using System.IO;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Widget;

namespace CrewBriefingForms.Droid.Helper
{
    public static class FileUtils
    {

        public static void OpenFile(string filePath)
        {

            var bytes = File.ReadAllBytes(filePath);
            var folderPath = Build.VERSION.SdkInt >= BuildVersionCodes.M? System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) :global::Android.OS.Environment.ExternalStorageDirectory.Path;
            var externalPath = folderPath + "/report.pdf";
           
            File.WriteAllBytes(externalPath, bytes); // from android M it need run time permission to write in external path

            Java.IO.File file = new Java.IO.File(externalPath);
            file.SetReadable(true);
            String type = "application/pdf";
            try
            {
                var intent = new Intent(Intent.ActionView);
                intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask | ActivityFlags.GrantReadUriPermission);
                if (Build.VERSION.SdkInt >= BuildVersionCodes.M)
                {
                    var contentUri = Android.Support.V4.Content.FileProvider.GetUriForFile(Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity, "crewbriefing.droid.fileProvider", file);
                    intent.SetDataAndType(contentUri, type);
                }
                else
                {
                    intent.SetDataAndType(Android.Net.Uri.FromFile(file), type);
                }

                if (intent.ResolveActivity(Application.Context.PackageManager) != null)
                {
                    Plugin.CurrentActivity.CrossCurrentActivity.Current.Activity.StartActivity(intent);
                }
                else
                {
                    Toast.MakeText(Application.Context, "No Application to view Pdf", ToastLength.Long).Show();
                }

            }
            catch (System.Exception _exc)
            {
                var message = "No Application Available to View PDF Due to " + _exc.Message;
                Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
                Log.Info("PDF_Error", _exc.Message);
            }
        }


    }
}
