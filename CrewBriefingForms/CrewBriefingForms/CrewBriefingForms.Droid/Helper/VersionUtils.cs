﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;

namespace CrewBriefingForms.Droid.Helper
{
    public class VersionUtils
    {
        public static string GetAppVersion()
        {
            var context = Android.App.Application.Context;
            var version = context?.PackageManager.GetPackageInfo(context.PackageName, 0).VersionName;
            return version;
        }

        public static string GetAppVersionBuild()
        {
            var context = Android.App.Application.Context;
            var build = context?.PackageManager.GetPackageInfo(context.PackageName, 0).VersionCode;
            return GetAppVersion() + "." + build;
        }
    }
}