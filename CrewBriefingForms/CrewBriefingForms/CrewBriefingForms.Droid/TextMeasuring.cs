﻿using Xamarin.Forms;
using CrewBriefingForms.Droid;
using System;
using Android.Graphics;
using Android.Widget;
using Android.Content.Res;
using Android.Util;
using Android.Views;
using CB.Core.Common.Interfaces;

[assembly: Dependency(typeof(TextMeasuring))]
namespace CrewBriefingForms.Droid
{
    public class TextMeasuring : ITextMeasuring
    {
        
        private static int screenWidthPixels = 0;
        private static int screenHeightPixels = 0;

        public Xamarin.Forms.Size ScreenSizeInFormPixels()
        {
            //used to calculate CommonStatic.ScreenCoef

            var context = Android.App.Application.Context.ApplicationContext;
            var dm = context.Resources.DisplayMetrics;
            screenWidthPixels = dm.WidthPixels;
            screenHeightPixels = dm.HeightPixels;
            return new Xamarin.Forms.Size(dm.WidthPixels, dm.HeightPixels);
        }
        private static double screenWidthInches = 0;
        private static double screenHeightInches = 0;

        private static double xdpi, ydpi;
        public static double Xdpi => xdpi;

	    public static double Ydpi => ydpi;

	    public double ScreenSizeInInches()
        {
           
            var context = Android.App.Application.Context.ApplicationContext;
            var dm = context.Resources.DisplayMetrics;
            xdpi = dm.Xdpi;
            ydpi = dm.Ydpi;
            screenWidthInches = dm.WidthPixels / dm.Xdpi;
            screenHeightInches = dm.HeightPixels / dm.Ydpi;
            var size = Math.Sqrt(Math.Pow(screenWidthInches, 2) +
                                    Math.Pow(screenHeightInches, 2));
            return size;
        }

        private Rect calculateSize(string text, NamedSize fontSize)
        {
            Rect bounds = new Rect();
            TextView textView = new TextView(Android.App.Application.Context);
            double textSize = Device.GetNamedSize(fontSize, typeof(TextView)); // StackLayout?
            textView.TextSize = (float)textSize;
            textView.Paint.GetTextBounds(text, 0, text.Length, bounds);
            var length = bounds.Width();
            
            return new Rect(0, 0,
                (int)(Math.Round(bounds.Width() / Resources.System.DisplayMetrics.ScaledDensity)),
                (int)(Math.Round(bounds.Height() / Resources.System.DisplayMetrics.ScaledDensity)));
                /*
            TextView textView = new TextView(Forms.Context);
            //textView.Typeface = GetTypeface(fontName);
            textView.SetText(text, TextView.BufferType.Normal);

            //textView.SetTextSize(ComplexUnitType.Px, _fontSize);
            
            int widthMeasureSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec(
                0, MeasureSpecMode.Unspecified);
            int heightMeasureSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec(
                0, MeasureSpecMode.Unspecified);

            //textView.Measure(widthMeasureSpec, heightMeasureSpec); //?

            float _fontSize = (float)Device.GetNamedSize(fontSize, typeof(TextView));
           textView.SetTextSize(ComplexUnitType.Px, _fontSize);
           textView.Measure(widthMeasureSpec, heightMeasureSpec);

            return new Rect(0, 0,
                textView.MeasuredWidth,
                textView.MeasuredHeight);

            */
            /*
            Rect bounds = new Rect();
            TextView textView = new TextView(Forms.Context);
            textView.Paint.GetTextBounds(text, 0, text.Length, bounds);
            var length = bounds.Width();
            return length / Resources.System.DisplayMetrics.ScaledDensity;
            */
        }
        private Typeface textTypeface;
        private Typeface GetTypeface(string fontName)
        {
            if (fontName == null)
            {
                return Typeface.Default;
            }

            if (textTypeface == null)
            {
                textTypeface = Typeface.Create(fontName, TypefaceStyle.Normal);
            }

            return textTypeface;
        }

		public double CalculateWidth(string text, NamedSize fontSize)
		{
			return calculateSize(text, fontSize).Width();
		}

		public double CalculateHeight(string text, NamedSize fontSize)
		{
			return calculateSize(text, fontSize).Height();
		}
	}
}
