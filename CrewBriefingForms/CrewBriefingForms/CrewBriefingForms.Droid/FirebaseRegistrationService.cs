﻿using System;
using Android.App;
using Android.Util;
using Firebase.Iid;


namespace CrewBriefingForms.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class FirebaseRegistrationService : FirebaseInstanceIdService
    {
        const string TAG = "FirebaseRegistrationService";

        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
           Log.Debug (TAG, "Refreshed token: " + refreshedToken);

        }

       
    }
}
