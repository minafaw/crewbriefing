using Android.OS;
using Xamarin.Forms;
using CrewBriefingForms.Droid;
using CrewBriefingForms.Droid.Helper;
using CB.Core.Common.Interfaces;

[assembly: Dependency(typeof(VersionDroid))]
namespace CrewBriefingForms.Droid
{
    public class VersionDroid : IAppVersion
    {
        public static bool FormsInitialized = false;
        public string GetAppVersion()
        {
            return VersionUtils.GetAppVersion();
        }

        public string GetAppVersionBuild()
        {
            return VersionUtils.GetAppVersion();
        }

        public string GetDeviceName()
        {
            return Build.Model;
        }

        public string GetOs()
        {
            return "Android SDK: " + Build.VERSION.Sdk + " " + Build.VERSION.SdkInt;
        }

        public int GetOsMajorVersion()
        {
            return (int)Build.VERSION.SdkInt;
        }

        public string MailEol()
        {
            return System.Environment.NewLine;
        }
    }
}