﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CB.Core.Common.Keys;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;

namespace CB.Core.DAL
{
    public class FilterDbSettingsDal : Repository<FilterDbSettings>, IFilterDbSettingsRepository
	{
        public async Task InsertOrUpdateFilterSetting(FilterDbSettings filterDbSettings)
        {
	        await InsertOrUpdateAsync(filterDbSettings , true);
        }

	    public async Task<string> GetFilterSettingAsync(Enums.SettingType id , string currentUserName)
	    {
		    bool Predicate(FilterDbSettings filterDbSettings) => filterDbSettings.Id == (int) id && filterDbSettings.UserName.Equals(currentUserName);
		    var taskSettings = (await GetWithPredicate((Func<FilterDbSettings, bool>) Predicate)).FirstOrDefault();
		    return taskSettings == null ? string.Empty : taskSettings.Value;
	    }
		
	}
}
