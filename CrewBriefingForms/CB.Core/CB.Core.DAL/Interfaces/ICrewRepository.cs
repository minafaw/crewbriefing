﻿using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
	public interface ICrewRepository
	{
		Task SaveCrewDb(int flightId, IList<Crew> crews);
		Task SaveCrewDb(FlightListItem flightListItem, IList<Crew> crews);
		Task<IList<Crew>> GetCrews(int flightId);
	}
}
