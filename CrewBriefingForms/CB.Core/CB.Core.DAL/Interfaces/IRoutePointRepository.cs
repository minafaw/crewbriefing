﻿using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
	public interface IRoutePointRepository
	{
		Task SaveRoutesDb(int flightId, IList<RoutePoint> points);
		Task<IList<RoutePoint>> GetRoutePoints(int flightId);
	}
}
