﻿using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
	public interface IAirportRepository
	{
		Task InsertOrUpdateAirport(Airport value);
		Task InsertOrUpdateAirport(List<Airport> value);
		Task<Airport> GetAirportDb(string icao);
	}
}
