﻿using CB.Core.Common.Keys;
using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
    public  interface IDalServices
    {
		bool IsSessionExpired();
		bool DeleteAllDateBase();
	}
}
