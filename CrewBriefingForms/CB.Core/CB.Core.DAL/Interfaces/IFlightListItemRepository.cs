﻿using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
	public interface IFlightListItemRepository
	{
		Task UpdateFlights(List<FlightListItem> flight, bool updateFlag);
		bool FlightListIsValidAsync();
		bool IsFlightListNotEmpty();
		List<FlightListItem> GetAllFlightListItems();

		bool DeleteAllFlightTable();
	}
}
