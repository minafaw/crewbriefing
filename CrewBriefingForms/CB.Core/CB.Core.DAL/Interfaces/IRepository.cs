﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CB.Core.Entities;
using CB.Core.Entities.Base;
using Realms;

namespace CB.Core.DAL.Interfaces
{
    public interface IRepository<T> where T : RealmObject
    {
        #region   Async Operation 
        Task<bool> InsertOrUpdateAsync(T value , bool updateFlag = false);
		Task<bool> InsertOrUpdateAsyncWithChild(FlightListItem value , IList<Crew> child ,  bool updateFlag = false);
        Task<IEnumerable<T>> GetWithIncludeAsync(
            Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            params Expression<Func<T, object>>[] includes);
        bool InsertOrUpdateListAsync(List<T> list) ;
		Task<IList<T>> GetWithPredicate(Func<T, bool> predicate);

	    Task InsertCollections(IEnumerable<T> collections  , bool updateFlag = false);
	    
			#endregion

			#region Normal operation
			bool Remove(T value);
		bool RemoveTable(Type value);
	   // Task<bool> RemoveAll();
		List<T> ReadAll() ;
        T ReadOne(Guid id);
		IEnumerable<T> GetWithInclude(
			Expression<Func<T, bool>> predicate,
			Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
			params Expression<Func<T, object>>[] includes);
        #endregion

    }
}
