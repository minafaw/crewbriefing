﻿using CB.Core.Common.Keys;
using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
	public interface IPdfRepository
	{
		Task InsertOrUpdatePdf(DbPdf dbPdf);
		Task<string> GetStoredPdfPath(int flightId, Enums.PdfDocType docType);
		Task<string> GetStoredPdfPathMaskDelete(int flightId, Enums.PdfDocType docType);
		Task<(bool downloadedExists, DateTime? downloadDate)> CheckDocExists(Enums.PdfDocType docType, int passedItemId);
		Task<bool> DeletePdf(int flightId, Enums.PdfDocType docType);
		Task<List<PdfDocTypeWithDate>> GetStoredPdf(int flightId);
		Task<PdfDocTypeWithDate> GetStoredPdf(int flightId, Enums.PdfDocType docType);
	}
}
