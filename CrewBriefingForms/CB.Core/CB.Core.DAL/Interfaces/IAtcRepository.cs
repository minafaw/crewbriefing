﻿using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
	public interface IAtcRepository
	{
		Task InsertOrUpdateAtc(Atc atc);
		Task<Atc> GetAtc(int flightId);
	}
}
