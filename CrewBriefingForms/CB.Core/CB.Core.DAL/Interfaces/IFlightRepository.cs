﻿using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL.Interfaces
{
	public interface IFlightRepository
	{
		Task InsertOrUpdateFlight(Flight flight);
		Task<Flight> GetFilghtDb(int id);
		
	}
}
