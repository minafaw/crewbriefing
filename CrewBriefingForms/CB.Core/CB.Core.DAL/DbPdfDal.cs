﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CB.Core.Common.Keys;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;

namespace CB.Core.DAL
{
    public class DbPdfDal : Repository<DbPdf> , IPdfRepository
	{
		public async Task InsertOrUpdatePdf(DbPdf dbPdf)
		{
			await InsertOrUpdateAsync(dbPdf, true);
		}

		public async Task<string> GetStoredPdfPath(int flightId, Enums.PdfDocType docType)
		{
			bool Predicate(DbPdf pdf) => pdf.FlightId == flightId && pdf.DocType == (int)docType;
			var record = (await GetWithPredicate((Func<DbPdf, bool>)Predicate)).FirstOrDefault();
			return record == null ? string.Empty : record.FilePath;

		}

		public async Task <string> GetStoredPdfPathMaskDelete(int flightId, Enums.PdfDocType docType)
		{
			bool Predicate(DbPdf pdf) => pdf.FlightId == flightId && pdf.DocType == (int)docType;
			var record = (await GetWithPredicate((Func<DbPdf, bool>)Predicate)).FirstOrDefault();
			return record == null ? string.Empty : record.FilePathMaskDoDelete;
		}

		public async Task<bool> DeletePdf(int flightId, Enums.PdfDocType docType)
		{
			bool Predicate(DbPdf pdf) => pdf.FlightId == flightId && pdf.DocType == (int)docType;
			var record = (await GetWithPredicate((Func<DbPdf, bool>)Predicate)).FirstOrDefault();
			return record != null && Remove(record);
		}

		public async Task<List<PdfDocTypeWithDate>> GetStoredPdf(int flightId)
		{
			List<PdfDocTypeWithDate> list;
			try
			{
				list = new List<PdfDocTypeWithDate>();
				bool Predicate(DbPdf pdf) => pdf.FlightId == flightId;
				var records = await GetWithPredicate((Func<DbPdf, bool>)Predicate);

				foreach (var item in records)
				{
					list.Add(new PdfDocTypeWithDate { DocType = (Enums.PdfDocType)Enum.Parse(typeof(Enums.PdfDocType), item.DocType.ToString()), LocalDate = item.DownloadTime });
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e);
				throw;
			}
			return list;
		}
		public async Task<PdfDocTypeWithDate> GetStoredPdf(int flightId, Enums.PdfDocType docType)
		{

			bool Predicate(DbPdf pdf) => pdf.FlightId == flightId && pdf.DocType == (int)docType;
			var item = (await GetWithPredicate((Func<DbPdf, bool>)Predicate)).FirstOrDefault();
			if (item != null)
			{
				return new PdfDocTypeWithDate { DocType = (Enums.PdfDocType)Enum.Parse(typeof(Enums.PdfDocType), item.DocType.ToString()), LocalDate = item.DownloadTime };
			}

			return null;

		}
		public async Task<(bool downloadedExists, DateTime? downloadDate)> CheckDocExists(Enums.PdfDocType docType, int passedItemId)
		{
			var downloadedExists = false;
			DateTime? downloadDate = null;

			var item = await GetStoredPdf(passedItemId, docType);

			if (item != null)
			{
				downloadedExists = true;
				downloadDate = item.LocalDate.UtcDateTime;
			}

			return (downloadedExists, downloadDate);
		}
	}
}
