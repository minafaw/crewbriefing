﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;

namespace CB.Core.DAL
{
    public class CrewDal : Repository<Crew>, ICrewRepository
    {
		
		public async Task<IList<Crew>> GetCrews(int flightId)
		{
			bool PredicateCrew(Crew crew) => crew.FlightId == flightId;
			var crewLists = await GetWithPredicate(PredicateCrew);
			return crewLists;
		}

		public async Task SaveCrewDb(int flightId, IList<Crew> crews)
		{
			//await SemaphoreSlim.WaitAsync().ConfigureAwait(false);
			try
			{
				if (crews == null) return;

				foreach (var crew in crews)
				{
					crew.FlightId = flightId;
					await InsertOrUpdateAsync(crew);
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(nameof(crews) + " SaveCrewDb " + e);
				throw;
			}
			//finally
			//{
			// SemaphoreSlim.Release();
			//}
		}

		public async Task SaveCrewDb(FlightListItem flightListItem, IList<Crew> crews)
		{
			try
			{
				if (crews == null) return;

				foreach (var crew in crews)
				{
					//crew.flightListItem = flightListItem;
					await InsertOrUpdateAsync(crew);
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(nameof(crews) + " SaveCrewDb " + e);
				throw;
			}
		}
	}
}
