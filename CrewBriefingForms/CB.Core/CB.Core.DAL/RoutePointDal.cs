﻿using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL
{
	public class RoutePointDal : Repository<RoutePoint>, IRoutePointRepository
	{
		public async Task<IList<RoutePoint>> GetRoutePoints(int flightId)
		{
			bool PredicateReoute(RoutePoint routePoint) => routePoint.FlightId == flightId;
			var routePoints = await GetWithPredicate(PredicateReoute);
			return routePoints;
		}

		public async Task SaveRoutesDb(int flightId, IList<RoutePoint> points)
		{

			if (points == null) return;

			//await SemaphoreSlim.WaitAsync().ConfigureAwait(false);
			try
			{


				foreach (var r in points)
				{
					r.FlightId = flightId;
					await InsertOrUpdateAsync(r);
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine(e.Message);
				throw;
			}
			//finally
			//{
			// SemaphoreSlim.Release();
			//}
		}

	}
}
