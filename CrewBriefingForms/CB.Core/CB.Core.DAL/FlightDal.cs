﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;

namespace CB.Core.DAL
{
    public class FlightDal : Repository<Flight> , IFlightRepository
    {
		private readonly IRoutePointRepository routePointRepository;
		private readonly IAtcRepository atcRepository;
		private readonly ICrewRepository crewRepository;

		public FlightDal(IRoutePointRepository routePointRepository , IAtcRepository atcRepository, ICrewRepository crewRepository)
		{
			this.routePointRepository = routePointRepository;
			this.atcRepository = atcRepository;
			this.crewRepository = crewRepository;
		}

	   

		public async Task InsertOrUpdateFlight(Flight flight)
		{
			try
			{
				// save child records
				//TO be tested 
				await atcRepository.InsertOrUpdateAtc(flight.AtcData);
				await routePointRepository.SaveRoutesDb(flight.Id, flight.RoutePoints);
				await crewRepository.SaveCrewDb(flight.Id, flight.Crews);

				await InsertOrUpdateAsync(flight);

			}
			catch (Exception e)
			{
				Debug.WriteLine(e);
			}

		}
		public async Task<Flight> GetFilghtDb(int id)
		{
			Flight flightResult;
			try
			{
				bool PredicateFlight(Flight flight) => flight.Id == id;
				flightResult = (await GetWithPredicate(PredicateFlight)).FirstOrDefault();
				if (flightResult != null)
				{
					// TODO to be tested 
					var routePoints = await routePointRepository.GetRoutePoints(id);
					flightResult.RoutePoints = routePoints;

					var crews = await crewRepository.GetCrews(id);
					flightResult.Crews = crews;

					var atcData = await atcRepository.GetAtc(id);
					flightResult.AtcData = atcData;
				}
			}
			catch (Exception e)
			{
				Debug.WriteLine("DataBase " + " GetFilghtDb " + e);
				throw;
			}
			return flightResult;

		}
		
	}
}
