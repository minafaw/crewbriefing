﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using CB.Core.DAL.Interfaces;
using CB.Core.Entities;
using Realms;
using Xamarin.Forms;

namespace CB.Core.DAL.Repository
{
    public class Repository<T>: IRepository<T> where T : RealmObject
	{
		//Only allowing one concurrent connection/operation to database
		private static readonly SemaphoreSlim semaphore = new SemaphoreSlim(1, 1);

		public async Task<bool> InsertOrUpdateAsync(T value , bool updateFlg = false) 
		{
            try
            {
				await semaphore.WaitAsync();
				var _realm = Realm.GetInstance();
				// pass id and looking if its already exists to remove update flag 
				await _realm.WriteAsync(realm =>
                {
                    realm.Add(value , updateFlg);
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
			}
			finally
			{
				semaphore.Release();
			}
            return true;
        }

        public Task<IEnumerable<T>> GetWithIncludeAsync(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes)
		{
            throw new NotImplementedException();
        }

        public  bool InsertOrUpdateListAsync(System.Collections.Generic.List<T> list) 
		{
            if (list != null)
            {
				var _realm = Realm.GetInstance();
				foreach (var item in list)
                {
                    using (var transaction = _realm.BeginWrite())
                    {
                        _realm.Add(item, true);
                        transaction.Commit();
                    }
                }
                return true;
            }
            return false;
        }

	    public async Task<IList<T>> GetWithPredicate(Func<T,bool> predicate) 
	    {
		    try
		    {
				await semaphore.WaitAsync();
				var _realm = Realm.GetInstance();
				return _realm.All<T>().Where(predicate).ToList();
			}
		    catch (Exception e)
		    {
			    Console.WriteLine(e);
			    return null;
			}
			finally
			{
				 semaphore.Release();
			}
	    }

	    public bool Remove(T value) 
		{
            try
            {
				var _realm = Realm.GetInstance();
				_realm.Remove(value);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            return true;
        }

		public bool RemoveTable(Type value)
		{
			var _realm = Realm.GetInstance();
			try
			{
				using (var trans = _realm.BeginWrite())
				{
					_realm.RemoveAll<T>();
					trans.Commit();
					return true;
				}
				
			}
			catch(Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
			
		}

        public List<T> ReadAll() 
		{
            try
            {
				var _realm = Realm.GetInstance();
				_realm.Refresh();
				var items =  _realm.All<T>().ToList();
				return items;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }
        }


        public T ReadOne(Guid id)
		{
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetWithInclude(Expression<Func<T, bool>> predicate, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, params Expression<Func<T, object>>[] includes)
		{
            throw new NotImplementedException();
        }

		public async Task InsertCollections(IEnumerable<T> collections , bool update = false) 
		{
			
				try
				{
					var _realm = Realm.GetInstance();
				using (var transaction = _realm.BeginWrite())
				{
					foreach (var item in collections)
					{
						_realm.Add(item, true);
					}
					transaction.Commit();
				}
				}
				catch(Exception e)
				{
					System.Diagnostics.Debug.WriteLine(e);
				}
			

		}

		public async Task<bool> InsertOrUpdateAsyncWithChild(FlightListItem value, IList<Crew> child, bool updateFlag = false)
		{
			try
			{
				await semaphore.WaitAsync();
				var _realm = Realm.GetInstance();
				_realm.Write(() =>
				{
					// pass id and looking if its already exists to remove update flag 
					foreach (var item in child)
					{
						value.CrewItem.Add(item);
					}
					_realm.Add(value, updateFlag);
				});
				
				
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
				return false;
			}
			finally
			{
				semaphore.Release();
			}
			return true;
		}
	}
}
