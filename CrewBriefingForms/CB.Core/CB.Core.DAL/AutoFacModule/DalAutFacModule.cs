﻿using Autofac;
using CB.Core.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CB.Core.DAL.AutoFacModule
{
	public class DalAutFacModule : Module
	{

		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<DalServices>().As<IDalServices>();
			builder.RegisterType<AirportDal>().As<IAirportRepository>();
			builder.RegisterType<AtcDal>().As<IAtcRepository>();
			builder.RegisterType<CrewDal>().As<ICrewRepository>();
			builder.RegisterType<DbPdfDal>().As<IPdfRepository>();
			builder.RegisterType<DbSettingsDal>().As<ISettingRepository>();
			builder.RegisterType<FilterDbSettingsDal>().As<IFilterDbSettingsRepository>();
			builder.RegisterType<FlightDal>().As<IFlightRepository>();
			builder.RegisterType<FlightListItemDal>().As<IFlightListItemRepository>();
			builder.RegisterType<RoutePointDal>().As<IRoutePointRepository>();

		}
	}
}
