﻿using System;
using CB.Core.Common.Keys;

namespace CB.Core.DAL
{
	public class PdfDocTypeWithDate
	{
		public Enums.PdfDocType DocType { get; set; }
		public DateTimeOffset LocalDate { get; set; }
	}
}
