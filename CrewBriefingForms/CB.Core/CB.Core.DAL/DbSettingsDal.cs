﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CB.Core.Common.Keys;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;

namespace CB.Core.DAL
{
    public class DbSettingsDal : Repository<DbSettings>, ISettingRepository
    {
		public async Task InsertOrUpdateSetting(DbSettings value)
		{
			await InsertOrUpdateAsync(value, true);
		}
		public async Task<string> GetSettingAsync(Enums.SettingType id)
		{
			bool Predicate(DbSettings dbSettings) => dbSettings.Id == (int)id;
			var taskSettings = (await GetWithPredicate((Func<DbSettings, bool>)Predicate)).FirstOrDefault();
			return taskSettings == null ? string.Empty : taskSettings.Value;
		}
	}
}
