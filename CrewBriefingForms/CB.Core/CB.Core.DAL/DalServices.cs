﻿using System;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.DAL.Interfaces;
using CB.Core.Entities;
using Realms;

namespace CB.Core.DAL
{
	public class DalServices :  IDalServices
	{
	    public bool IsSessionExpired()
	    {
		    // var datestring = await GetSettingAsync(SettingType.SessionReceived);
		    var sessionDate = AppSetting.Instance.GetValueOrDefault(CommanConstants.SessionReceivedKey, DateTime.MinValue);
			var ts = DateTime.UtcNow - sessionDate;
			return ts.TotalMinutes > 30;   
	    }
		public bool DeleteAllDateBase()
		{
		
				try
				{
					var _realm = Realm.GetInstance();
					using (var trans = _realm.BeginWrite())
					{
						_realm.RemoveAll<FlightListItem>();
						_realm.RemoveAll<Airport>();
						_realm.RemoveAll<Atc>();
						_realm.RemoveAll<Crew>();
						_realm.RemoveAll<RoutePoint>();
						_realm.RemoveAll<Flight>();
						_realm.RemoveAll<DbPdf>();
						_realm.RemoveAll<DbPdf>();
					trans.Commit();
					}


				}
				catch (Exception e)
				{
					Console.WriteLine(e);
					return false;
				}
				return true;
		
		}
	}
}
