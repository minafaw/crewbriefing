﻿using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CB.Core.DAL
{
	public class AtcDal : Repository<Atc>, IAtcRepository
	{
		public async Task<Atc> GetAtc(int flightId)
		{
			bool PredicateAtc(Atc atc) => atc.FlightId == flightId;
			var atcData = (await GetWithPredicate(PredicateAtc)).FirstOrDefault();
			return atcData; 
		}

		public async Task InsertOrUpdateAtc(Atc atc)
		{
			await InsertOrUpdateAsync(atc , true);
		}
	}
}
