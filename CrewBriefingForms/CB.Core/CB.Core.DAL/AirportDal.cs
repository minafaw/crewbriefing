﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;

namespace CB.Core.DAL
{
    public class AirportDal : Repository<Airport> , IAirportRepository
    {
       
		public async Task InsertOrUpdateAirport(Airport value)
		{
			await InsertOrUpdateAsync(value, true);
		}

		public async Task InsertOrUpdateAirport(List<Airport> value)
		{
			await InsertCollections(value);
		}

		public async Task<Airport> GetAirportDb(string icao)
		{
			try
			{
				if (string.IsNullOrEmpty(icao))
				{
					return null;
				}
				bool Predicate(Airport airport) => airport.Icao.Equals(icao);
				return (await GetWithPredicate((Func<Airport, bool>)Predicate)).FirstOrDefault();
			}
			catch (Exception e)
			{
				Debug.WriteLine(e);
				throw;
			}
		}
	}
}
