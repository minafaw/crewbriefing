﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CB.Core.DAL.Interfaces;
using CB.Core.DAL.Repository;
using CB.Core.Entities;

namespace CB.Core.DAL
{
    public class FlightListItemDal : Repository<FlightListItem> , IFlightListItemRepository
    {
		private readonly ICrewRepository crewRepository;

		public FlightListItemDal(ICrewRepository crewRepository)
		{
			this.crewRepository = crewRepository;
		}
		public async Task UpdateFlights(List<FlightListItem> flights)
		{
			await InsertCollections(flights);
		}
		public bool IsFlightListNotEmpty()
		{
			return GetAllFlightListItems() != null || GetAllFlightListItems().Any();
		}

		public bool FlightListIsValidAsync()
		{
			var latestItem = GetAllFlightListItems().OrderByDescending(r => r.CreatedDate).FirstOrDefault();
			if (latestItem != null)
			{
				return (DateTime.UtcNow - latestItem.CreatedDate).TotalHours <= 24;
			}
			return false;
		}

		public List<FlightListItem> GetAllFlightListItems()
		{
			return ReadAll();
		}

		public bool DeleteAllFlightTable()
		{
			return RemoveTable(typeof(FlightListItem));
		}

		public async Task UpdateFlights(List<FlightListItem> flight,  bool updateFlag)
		{
			await InsertCollections(flight, updateFlag);
		}
	}
}
