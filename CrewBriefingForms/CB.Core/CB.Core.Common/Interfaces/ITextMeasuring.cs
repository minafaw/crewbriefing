﻿using Xamarin.Forms;

namespace CB.Core.Common.Interfaces
{
    public interface ITextMeasuring
    {
        double CalculateWidth(string text, NamedSize fontSize);
        double CalculateHeight(string text, NamedSize fontSize);
        double ScreenSizeInInches();
        Size ScreenSizeInFormPixels();
    }
}
