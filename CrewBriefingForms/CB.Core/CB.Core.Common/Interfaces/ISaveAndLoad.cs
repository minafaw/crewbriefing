﻿using System.Threading.Tasks;

namespace CB.Core.Common.Interfaces
{
    public interface ISaveAndLoad
    {
        Task SaveFile(string fileName, byte[] data);
        Task<byte[]> LoadFile(string fileName);
        Task<FileTextAndPath> LoadTextFile(string fileName);
        Task OpenFileWithDefaultOsViewer(string fileName, string title);
        void RemoveAllFilesInLocalStorage(string fileMaskStart);
        void RemoveFile(string filename);
        string AppPath(string pathFile); // iOS only
    }
    public class FileTextAndPath
    {
        public string FileName { get; set; }
        public string TextContent { get; set; }
    }
}
