﻿namespace CB.Core.Common.Interfaces
{
    public interface IAppVersion
    {
        string GetAppVersion();
        string GetAppVersionBuild();
        string GetDeviceName();
        string GetOs();
        string MailEol();
        int GetOsMajorVersion();
    }
}
