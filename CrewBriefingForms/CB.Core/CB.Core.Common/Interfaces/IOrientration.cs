﻿namespace CB.Core.Common.Interfaces
{
    public enum DeviceOrientations
    {
        Undefined,
        Landscape,
        Portrait
    }

    public interface IOrientation
    {
        DeviceOrientations GetOrientation();
        //https://developer.xamarin.com/guides/xamarin-forms/dependency-service/device-orientation/
    }
}
