﻿namespace CB.Core.Common.Interfaces
{
    public interface IShareLink
    {
        void OpenUri(string url); 
        bool CanOpenUri(string url); 
    }
}
