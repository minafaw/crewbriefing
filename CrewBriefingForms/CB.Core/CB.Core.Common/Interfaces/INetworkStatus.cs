﻿namespace CB.Core.Common.Interfaces
{
    public interface INetworkStatus
    {
        bool IsInternet();
    }
}
