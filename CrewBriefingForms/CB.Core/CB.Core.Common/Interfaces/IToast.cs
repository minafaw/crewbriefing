﻿using System.Threading.Tasks;
using CB.Core.Common.Keys;

namespace CB.Core.Common.Interfaces
{
    public interface IToast
    {
        Task<int> ShowToast(Enums.ToastType type, string message, uint timeMSec);
    }
}
