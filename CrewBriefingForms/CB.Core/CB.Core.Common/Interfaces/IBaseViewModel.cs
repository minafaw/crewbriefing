﻿using System.ComponentModel;

namespace CB.Core.Common.Interfaces
{
    internal interface IBaseViewModel
    {
        void OnAppearing();
        void OnDisappearing();
    }
}
