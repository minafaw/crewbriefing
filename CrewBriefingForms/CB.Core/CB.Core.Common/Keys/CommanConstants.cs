﻿using System;
using System.Globalization;

namespace CB.Core.Common.Keys

{
    public static class CommanConstants
    {
	    public static readonly string BaseUrl = "https://www.crewbriefing.aero/EFB/v1.7/EfbService.asmx";
		public static readonly string DefaultDateTimeFormat = "yyyyMMddHHmmss";
        public static readonly CultureInfo DefaultCultureProvider = CultureInfo.InvariantCulture;

        public static readonly string PartnerUserName = "TeoIntl";
        public static readonly string PartnerPassword = "V5N9K5YH7V";

        public static readonly string ClientId = "CB6173";
        public static readonly string ClientKey = "7dba38bbfffbf974b6f7976ece8f43f8";

        public static readonly string ApiKey = "62FA4115-90FB-4908-910D-DDDA4A94E433";
        public static readonly string AppName = "CB_MOBILE";

        //Timer is set to go off one time after 120 seconds
        public static readonly TimeSpan RequestTimeout = new TimeSpan(0, 0, 0, 120);

        public static readonly int PopOverWidthIpad = 289;
        public static readonly int PopOverHeightForIos6 = 640; //Because it's navigation bar height is small
        public static readonly int PopOverHeightForIos7 = 395;

        public static readonly int MainMenuHeightIpad = 430;
        public static readonly string ServiceUrlKey = "serviceURL";

        public static readonly string UserNameKey = "UserNameKey";
        public static readonly string PasswordKey = "PasswordKey";
        public static readonly string QuickLoginKey = "QuickLoginKey";
        public static readonly string IsLoginedInKey = "IsLoginedInKey";
        public static readonly string CurrentSessionKey = "CurrentSessionKey";
        public static readonly string SessionReceivedKey = "SessionReceivedKey";
		public static readonly string FlightListRetrievedKey = "FlightListRetrievedKey";
		

		// error messages 
		public static readonly string ErrorMsgFormat = "Error '{0}' occured.";
        public static readonly string InvalidSession = "Invalid Session";
        public static readonly string NoNet = "No Internet";
        public static readonly string NullError = "Null Error";
        public static readonly string RequestTimeOutError = "Web Request Time Out";
        public static readonly string UndefinedError = "UNDEFINED";
        public static readonly string WrongCredentials = "Wrong Credentials";
        public static readonly string UnknownError = "Unknown Code";
       
    }
}
