﻿namespace CB.Core.Common.Keys
{
    public class Enums
    {
        public enum SettingType
        {
            SessionReceived = 1,
            FlightListRetrieved = 2,
            TailFilter = 3,
            CrewFilter = 4,
            StdFilter1 = 5,
            StdFilter2 = 6,
            SortField = 7,
            SortType = 8,
            StdFilterOn = 9,
            DepFilter = 10,
            DestFilter = 11,
            FlightNoFilter = 12,
            SessionId = 23
        }
        //Will be used for DBPdf
        public enum PdfDocDownloadingState
        {
            NotDownloaded,
            Downloaded,
            Downloading,
            NotAvailable
        }

        //Will be used for Sort Filter configuration
        public enum SortFilterType
        {
            FlightName,
            Dep,
            Std
        }

        //Will be used for Sort Filter configuration
        public enum SortFilterValue
        {
            AtoZ,
            ZtoA,
            EarliestFirst, //For STD Option
            LatestFirst   //For STD Option
        }

        //Will be used for STD Filter configuration
        public enum Std
        {
            Yesterday = 1,
            Now = 2,
            NextSixHours = 3,
            NextTwelveHours = 4,
            Tomorrow = 5,
            OneWeek = 6,
            TwoWeeks = 7,
            OneMonth = 8,
            Any = 9
        }

        public enum ClassType
        {
            Tail,
            Crew,
            Std,
            Sort
        }

        public enum SwitchClassType
        {
            Tail = 0,
            Crew = 1,
            Std = 2
        }

        public enum CrewType
        {
            Cmd,
            Cop,
            Ca1,
            Ca2,
            Ca3,
            Ca4
        }
        public enum WebApiErrorCode
        {
            Success = -1,
            WrongCredentials = 1,
            Undefined = 2,
            NullError = 3,
            TimeOut = 4,
            NoNet = 5,
            InvalidSession = 6
        }
        public enum PdfDocType
        {
            Atc,       //0
            Logstring, //1
            Messages,  //2 
            NotaMs,     //3
            Wx,         //4
            WindT, //5
            Swx,  //6
            CrossSection, //7
            UploadedDocuments,  //8
            CombineDocuments,  //9
            Other  //10, not used yet
        }
        public enum ToastType
        {
            Info, Warning, Error
        }
    }
}
