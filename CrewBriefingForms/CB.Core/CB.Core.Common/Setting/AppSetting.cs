﻿using System;
using Xamarin.Forms;

namespace CB.Core.Common.Setting
{
	// singleton class for app setting
	public sealed class AppSetting : IAppSetting
	{
		private static readonly Lazy<AppSetting> _instance =
			new Lazy<AppSetting>(() => new AppSetting());

		public static AppSetting Instance = _instance.Value;

		private AppSetting()
		{
			
		}

		public T GetValueOrDefault<T>(string key, T defaultValue)
		{
			if (Application.Current.Properties.ContainsKey(key))
			{
				return (T)Application.Current.Properties[key];
			}
			return defaultValue;
		}

		public bool AddOrUpdateValue<T>(string key, T value)
		{
			try
			{
				Application.Current.Properties[key] = value;
				return true;
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.Message);
				return false;
			}

		}

		public bool Remove(string key)
		{
			try
			{
				Application.Current.Properties.Remove(key);
				return true;
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e);
				return false;
			}
			
		}

		public void Clear()
		{
			Application.Current.Properties.Clear();
		}

		public bool Contains(string key)
		{
			return Application.Current.Properties.ContainsKey(key);
		}
	}
}
