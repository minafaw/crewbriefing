﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using Xamarin.Forms;

namespace CB.Core.Common.HelperClasses
{
    public class ToastUtils
    {
        private static int _oMessage;
        public static void ShowTemporayMessage(Enums.ToastType toastType, string message)
        {
            ShowTemporayMessage(toastType, message, 0);
        }
        public static void ShowTemporayMessage( Enums.ToastType toastType, string message, uint timeMSec)
        {
            if (_oMessage != 0 && toastType != Enums.ToastType.Info)
            {
                return;
            }
            if (toastType != Enums.ToastType.Info)
            {
                _oMessage = 1;
            }
            //await page.DisplayAlert(title, message, "OK");
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DependencyService.Get<IToast>().ShowToast(toastType, message, timeMSec);
            });

            _oMessage = 0;

        }
    }
}
