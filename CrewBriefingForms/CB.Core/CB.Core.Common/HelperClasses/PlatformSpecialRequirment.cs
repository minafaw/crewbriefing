﻿using Xamarin.Forms;

namespace CB.Core.Common.HelperClasses
{
    public static class PlatformSpecialRequirment
    {
        // how height of text can be applie to grid's row in different OS
        public static double GetTextHightInGrid()
        {
            double coefHeight;
            switch (Device.RuntimePlatform)
            {
                case Device.Android:
                    coefHeight = 2;
                    break;
                case Device.iOS:
                    coefHeight = 1;
                    break;
                case Device.UWP:
                    coefHeight = 1.2;
                    break;
                default:
                    coefHeight = 1.0;
                    break;
                 
            }
            return coefHeight;
        }
    }
}
