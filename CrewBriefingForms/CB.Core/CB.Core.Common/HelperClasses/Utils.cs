﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Enums = CB.Core.Common.Keys.Enums;

namespace CB.Core.Common.HelperClasses
{
	
	public class Utils
    {
        public static MasterDetailPage md;
        public static bool ShowFilter { get; set; } 
        public static string SummaryFontFamily => SummaryFontFamilyBold;
        public static string SummaryFontFamilyBold
        {
            get
            {
                string fontName = null;
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        fontName = "MonospaceBold";
                            break;
                    case Device.Android:
                        fontName = @"MonospaceBold.ttf#Monospace";
                        break;
                    case Device.UWP:
                        fontName = @"/Assets/Fonts/MonospaceBold.ttf#Monospace";
                        break;
                }
                return fontName;
            }
        }

		public static string ImageFolder
		{
			get
			{
				var imageFolderPath = "";
				if (Device.RuntimePlatform == Device.UWP)
				{
					imageFolderPath = "assets\\";
				}
				return imageFolderPath;
			}
		}
       
        //public static void CheckWebResponse(WebServiceResponse response, Page page, bool goBackIfError)
        //{
        //    switch (response.Code)
        //    {
        //        case Enums.WebApiErrorCode.Success:
        //            return;
        //        case Enums.WebApiErrorCode.NoNet:
	       //         ToastUtils.ShowTemporayMessage(Enums.ToastType.Info, Resource.not_net);
        //            break;
        //        default:
	       //         ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, GetErrorMessage(response.Code) + " " + response.Message);
        //            break;
        //    }
        //    if (!goBackIfError || page == null) return;
        //    if (page.Navigation?.NavigationStack?.Count > 0)
        //    {
        //        try
        //        {
                   
        //        }
        //        catch (Exception exc)
        //        {
	       //         ToastUtils.ShowTemporayMessage(Enums.ToastType.Info, "Error: " + exc.Message);
        //        }
        //    }
        //}

	    //public static string GetErrorMessage(Enums.WebApiErrorCode code)
	    //{
		   // switch (code)
		   // {
			  //  case Enums.WebApiErrorCode.InvalidSession:
				 //   return string.Format(Resource.error_message_format, Resource.invalid_session);
			  //  case Enums.WebApiErrorCode.NoNet:
				 //   return string.Format(Resource.error_message_format, Resource.not_net);
			  //  case Enums.WebApiErrorCode.NullError:
				 //   return string.Format(Resource.error_message_format, "NULL ERROR");
			  //  case Enums.WebApiErrorCode.TimeOut:
				 //   return string.Format(Resource.error_message_format, Resource.errot_request_timeout);
			  //  case Enums.WebApiErrorCode.Undefined:
				 //   return string.Format(Resource.error_message_format, "UNDEFINED");
			  //  case Enums.WebApiErrorCode.WrongCredentials:
				 //   return string.Format(Resource.error_message_format, Resource.wrong_credentials);
			  //  default:
				 //   return string.Format(Resource.error_message_format, "Unknown code " + code);
		   // }
	    //}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="layout"></param>
		/// <param name="text"></param>
		/// <param name="percentsOfWidth">0..1</param>
		public static Label SetCenterText(AbsoluteLayout layout, string text, double percentsOfWidth, TapGestureRecognizer tapSearch, TapGestureRecognizer tapRefresh, TapGestureRecognizer tapMenu, bool halfMenuPanelWidth)
        {
            //Color backColor = (Color)Application.Current.Resources["nav_bar_color"];
            var labelNum = new Label()
            {
                Text = text,
                FontSize = CommonStatic.LabelFontSize(NamedSize.Medium),
                //BackgroundColor = backColor,
                HorizontalTextAlignment = TextAlignment.Center,
                VerticalTextAlignment = TextAlignment.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                TextColor = Color.White,
                LineBreakMode = LineBreakMode.NoWrap,
            };
            var panelLabel = new StackLayout();
            panelLabel.Children.Add(labelNum);
            layout.Children.Add(panelLabel);

            AbsoluteLayout.SetLayoutBounds(panelLabel, new Rectangle(0.5, 0, percentsOfWidth, 1));
            AbsoluteLayout.SetLayoutFlags(panelLabel, AbsoluteLayoutFlags.All);
            
            var imageFolder = Utils.ImageFolder;
            var imageCoef = Device.Idiom == TargetIdiom.Tablet ? 0.8 : 1.0;

            if (tapSearch != null)
            {
                var imSearch = new Image()
                {
                    Source = ImageSource.FromFile(imageFolder + "menu_2_icon.png"),
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.EndAndExpand,
                    HeightRequest = CommonStatic.Instance.GetMediumHeight * imageCoef,
                    Aspect = Aspect.AspectFit ,
                    AutomationId = "FilterImageId"
                };
                var searchPanel = new StackLayout()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.End,
                    Padding = new Thickness(10, 0, 10, 0),
                    Orientation = StackOrientation.Horizontal,
                    //BackgroundColor = Color.Aqua
                };
                searchPanel.Children.Add(imSearch);
                searchPanel.GestureRecognizers.Add(tapSearch);

                //labelNum.GestureRecognizers.Add(tapSearch);

                var refreshAndSearchPanel = new StackLayout()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Padding = new Thickness(0, 0, 0, 0),
                    Orientation = StackOrientation.Horizontal,
                    Spacing = 0
                };
                if (Device.Idiom == TargetIdiom.Tablet)
                {
                    refreshAndSearchPanel.Padding = new Thickness(0);
                    searchPanel.Padding = new Thickness(CommonStatic.Instance.GetMediumHeight * .4, 0,0,0);
                }

                if (tapRefresh != null)
                {
                    var refreshPanel = new StackLayout()
                    {
                        VerticalOptions = LayoutOptions.CenterAndExpand,
                        HorizontalOptions = LayoutOptions.EndAndExpand,
                        Padding = new Thickness(0, 0, 10, 0),
                        Orientation = StackOrientation.Horizontal,
                    };

                    var imReresh = new Image()
                    {
                        Source = ImageSource.FromFile(imageFolder + "btn_refresh.png"),
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.End,
                        HeightRequest = CommonStatic.Instance.GetMediumHeight,
                        Aspect = Aspect.AspectFit
                    };
                    refreshPanel.Children.Add(imReresh);
                    imReresh.GestureRecognizers.Add(tapRefresh);
                    refreshAndSearchPanel.Children.Add(refreshPanel);
                }
                refreshAndSearchPanel.Children.Add(searchPanel);
                AbsoluteLayout.SetLayoutBounds(refreshAndSearchPanel, new Rectangle(1, 0, 0.5, 1));
                AbsoluteLayout.SetLayoutFlags(refreshAndSearchPanel, AbsoluteLayoutFlags.All);
                layout.Children.Add(refreshAndSearchPanel); // warning! order af adding in layout is important!

            }
            if (tapMenu != null)
            {
                // warning! order af adding in layout is important!
                var menuPanel = new StackLayout()
                {
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    Padding = new Thickness(10, 0, 0, 0),
                    Orientation = StackOrientation.Horizontal,
                };
                

                var imMenu = new Image()
                {
                    Source = ImageSource.FromFile(imageFolder + "menu.png"),
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.StartAndExpand,
                    //HeightRequest = CommonStatic.Instance.GetMediumHeight,
                    Aspect = Aspect.AspectFit,
                    AutomationId = "SideMenuID"

                                   
                };

                if (Device.Idiom == TargetIdiom.Tablet)
                {
                    imMenu.Source = ImageSource.FromFile(imageFolder + "menu_tablet.png");
                    imMenu.HeightRequest = CommonStatic.Instance.GetMediumHeight;
                    menuPanel.Padding = new Thickness(CommonStatic.Instance.GetMediumHeight * 0.4, 0);
                    if (halfMenuPanelWidth)
                    {
                        menuPanel.Padding = new Thickness(0, 0, CommonStatic.Instance.GetMediumHeight * 1.5, 0);
                        menuPanel.Margin = new Thickness(CommonStatic.Instance.GetMediumHeight * 1.4, 0, 0, 0);
                       
                        AbsoluteLayout.SetLayoutBounds(menuPanel, new Rectangle(0, 0, 1, 1));
                        AbsoluteLayout.SetLayoutFlags(menuPanel, AbsoluteLayoutFlags.All);

                    }
                    else
                    {
                        AbsoluteLayout.SetLayoutBounds(menuPanel, new Rectangle(0, 0, 1, 1));
                        AbsoluteLayout.SetLayoutFlags(menuPanel, AbsoluteLayoutFlags.All);

                    }
                }
                else
                {
                    AbsoluteLayout.SetLayoutBounds(menuPanel, new Rectangle(0, 0, 1, 1));
                    AbsoluteLayout.SetLayoutFlags(menuPanel, AbsoluteLayoutFlags.All);
                    if (Device.RuntimePlatform == Device.Android && Device.Idiom != TargetIdiom.Tablet)
                    {
                        imMenu.HeightRequest = CommonStatic.Instance.GetMediumHeight * 1.3;  //CA-52 Android version: The menu button is about 15 % too big
                        //GetMediumHeight is bit small. Full header:
                        //panelFlightListTop.HeightRequest = CommonStatic.Instance.MediumHeight * 2;
                        //layout.BackgroundColor = Color.Yellow;
                    }
                }
                menuPanel.Children.Add(imMenu);

                //menuPanel.BackgroundColor = Color.Green;
                menuPanel.GestureRecognizers.Add(tapMenu);
                layout.Children.Add(menuPanel);
            }
            return labelNum;
        }
     
      
        public static double Calc(double lat1,
                          double long1, double lat2, double long2)
        {
            /*
                The Haversine formula according to Dr. Math.
                http://mathforum.org/library/drmath/view/51879.html

                dlon = lon2 - lon1
                dlat = lat2 - lat1
                a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
                c = 2 * atan2(sqrt(a), sqrt(1-a)) 
                d = R * c

                Where
                    * dlon is the change in longitude
                    * dlat is the change in latitude
                    * c is the great circle distance in Radians.
                    * R is the radius of a spherical Earth.
                    * The locations of the two points in 
                        spherical coordinates (longitude and 
                        latitude) are lon1,lat1 and lon2, lat2.
            */
            var dDistance = Double.MinValue;
            var dLat1InRad = lat1 * (Math.PI / 180.0);
            var dLong1InRad = long1 * (Math.PI / 180.0);
            var dLat2InRad = lat2 * (Math.PI / 180.0);
            var dLong2InRad = long2 * (Math.PI / 180.0);

            var dLongitude = dLong2InRad - dLong1InRad;
            var dLatitude = dLat2InRad - dLat1InRad;

            // Intermediate result a.
            var a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                       Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) *
                       Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            // Intermediate result c (great circle distance in Radians).
            var c = 2.0 * Math.Asin(Math.Sqrt(a));

            // Distance.
            const Double kEarthRadiusMiles = 3956.0;
            //const Double kEarthRadiusKms = 6376.5;
            dDistance = kEarthRadiusMiles * c;

            return dDistance;
        }
    }
}
