﻿using CB.Core.Common.Interfaces;
using Plugin.Connectivity;

namespace CB.Core.Common.HelperClasses
{
    public static class NetworkUtils 
    {
       
        public static bool IsInternet()
        {
            return CrossConnectivity.Current.IsConnected;
        }
    }
}
