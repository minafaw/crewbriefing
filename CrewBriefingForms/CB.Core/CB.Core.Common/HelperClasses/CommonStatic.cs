﻿using System;
using System.Collections.Generic;
using System.Globalization;
using CB.Core.Common.Interfaces;
using Xamarin.Forms;

namespace CB.Core.Common.HelperClasses
{
    public class CommonStatic
    {
        private static double _mediumHeight = 20;
        public static double SmallHeight = 10;
        private static double _picTailWidth = 50;
        private static double _dateTimeFormattedWidth = 50;
        public static bool ToastsInitialized = false;
        private static readonly Dictionary<NamedSize, double> LimitedLabelSizes = new Dictionary<NamedSize, double>();
		static Size sizeScreen = DependencyService.Get<ITextMeasuring>().ScreenSizeInFormPixels();
		public double ScreenWidth { get; }

		public CommonStatic()
        {
            ScreenWidth = sizeScreen.Width;
            ScreenCoef = sizeScreen.Width / 750.0; // 750x1333 screen size on origin design
            if (Device.Idiom == TargetIdiom.Tablet)
            {
                FlightListItemGridVertPadding = new Thickness(0);
                FlightListItemGridRightPadding = new Thickness(0);
                if (sizeScreen.Width > sizeScreen.Height)
                {
                    const double fontCoef = 0.8;
                    LimitedLabelSizes.Add(NamedSize.Small, LabelFontSize(NamedSize.Small) * fontCoef);
                    LimitedLabelSizes.Add(NamedSize.Medium, LabelFontSize(NamedSize.Medium) * fontCoef);
                    LimitedLabelSizes.Add(NamedSize.Large, LabelFontSize(NamedSize.Large) * fontCoef);
                }
            }
            else
            {
                FlightListItemGridVertPadding = new Thickness(0, 5);
                FlightListItemGridRightPadding = new Thickness(0); //CW-51

            }
            FlightListItemGridVertPadding = new Thickness(0, 0, 0, 0);
        }

        public Thickness FlightListItemGridRightPadding { get; }
        public Thickness FlightListItemGridVertPadding { get; }
        public double ScreenCoef { get; }
        public Thickness FlightListColumn2Padding { get; set; }
        public double MediumHeight
		{
			get => _mediumHeight;
			set => _mediumHeight = value;
		}

		public double PicTailWidth
        {
            get => _picTailWidth;
            set => _picTailWidth = value;
        }
        public double TabletPageHeaderHeight { get; set; }
        public double DateTimeFormattedWidth
		{
			get => _dateTimeFormattedWidth;
			set => _dateTimeFormattedWidth = value;
		}
		public double GetMediumHeight => MediumHeight;

        /// <summary>
        /// for use in Binding - staic methods does not work
        /// </summary>
        public double GetSmallHeight => SmallHeight;

        /// <summary>
        /// Header pf list with small rext
        /// </summary>
        public double GetSmallHeightHeader => SmallHeight;

        public static int FixSize4Show(double value)
        {
            return (int)Math.Round(value);
        }
        private static CommonStatic _instance;
        /// <summary>
        /// For using in Binding
        /// </summary>
        
        public static CommonStatic Instance => _instance ?? (_instance = new CommonStatic());

        public static double LabelFontSize(NamedSize value)
        {
            var d = Device.GetNamedSize(value, typeof(Label));
            if (!LimitedLabelSizes.ContainsKey(value)) return d;
            var dLimit = LimitedLabelSizes[value];
            if (d > dLimit)
            {
                d = dLimit;
            }
            return d;
        }
        public double MediumFontSize => LabelFontSize(NamedSize.Medium);

		private double GetSmallHeightDouble => GetSmallHeight * 2.0;
		public double GetDetailsHeight { get; set; }

        public static string GetPreparedStd(DateTime std)
        {
            //--------------------------------------------------------------
            //The STD is a combination as DDHHMM , E.g: 242014 -> 0814PM on 24th
            return std.ToString("ddHHmm");
        }
        public static string GetFormatedDateTime(DateTime date)
        {
            return date.ToString("dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
        }
    }
}
