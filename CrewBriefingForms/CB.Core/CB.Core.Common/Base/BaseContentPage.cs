﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;
using Xamarin.Forms.Xaml;

namespace CB.Core.Common.Base
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public class BaseContentPage : ContentPage
    {
        public BaseContentPage()
        {
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);
		}

        protected enum OrientationValue
        {
            Portrait,
            Landscape
        }

        private double _oldWidth, _oldHeight;

        protected virtual void OrientationChanged(OrientationValue orientation) { }

        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            // to be tested 
            if (!(Math.Abs(_oldWidth - width) > 0) && !(Math.Abs(_oldHeight - height) > 0)) return;
            OrientationChanged(width > height ? OrientationValue.Landscape : OrientationValue.Portrait);
            _oldWidth = width;
            _oldHeight = height;
        }

        protected override void OnAppearing()
        {
           
            var viewAware = BindingContext as BaseViewModel;
            if (viewAware != null)
            {
                viewAware.Navigation = Navigation;
                viewAware.OnAppearing();
            }

            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            var viewAware = BindingContext as BaseViewModel;
            viewAware?.OnDisappearing();
            base.OnDisappearing();
        }
    }
}
