﻿using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using GalaSoft.MvvmLight;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace CB.Core.Common.Base
{
    public class BaseViewModel : ViewModelBase, IBaseViewModel
    {
        public INavigation Navigation { get; set; }
        public virtual void OnAppearing() { }

        public virtual void OnDisappearing() { }

        public bool IsConnected => NetworkUtils.IsInternet();
	    public double SmallFontSize => CommonStatic.LabelFontSize(NamedSize.Small);

		public string Title { get; set; }

	}
}
