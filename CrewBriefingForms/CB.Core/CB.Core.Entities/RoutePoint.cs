﻿using System;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class RoutePoint : RealmObject, IEntity
	{
        public int Id { get; set; }
        public string IdEnt { get; set; }
        public int Fl { get; set; }
        public int Wind { get; set; }
        public int Vol { get; set; }
        public int Isa { get; set; }
        public int LegTime { get; set; }
        public double LegCourse { get; set; }
        public int LegDistance { get; set; }
        public int LegCat { get; set; }
        public string LegName { get; set; }
        public string LegAwy { get; set; }
        public double FuelUsed { get; set; }
        public int FuelFlow { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
        public int Variation { get; set; }
        public int AccDist { get; set; }
        public int AccTime { get; set; }
        public double MagCourse { get; set; }
        public int TrueAirSpeed { get; set; }
        public int GroundSpeed { get; set; }
        public double FuelRemaining { get; set; }
        public int DistRemaining { get; set; }
        public int TimeRemaining { get; set; }
        public double MinReqFuel { get; set; }
        public double FuelFlowPerEng { get; set; }
        public int Temperature { get; set; }
        public int Mora { get; set; }
        public double Frequency { get; set; }
        public int FlightId { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
