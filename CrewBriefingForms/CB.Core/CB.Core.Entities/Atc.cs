﻿using System;
using CB.Core.Entities.Interfaces;
using Realms;


namespace CB.Core.Entities
{
    public class Atc : RealmObject, IEntity
	{
        public string AtcId { get; set; }
        public string Atctoa { get; set; }
        public string AtcRule { get; set; }
        public string AtcType { get; set; }
        public string AtcNum { get; set; }
        public string AtcWake { get; set; }
        public string AtcEqui { get; set; }
        public string AtcSsr { get; set; }
        public string AtcDep { get; set; }
        public string AtcTime { get; set; }
        public string AtcSpeed { get; set; }
        public string AtcFl { get; set; }
        public string AtcRoute { get; set; }
        public string AtcDest { get; set; }
        public string AtcEet { get; set; }
        public string AtcAlt1 { get; set; }
        public string AtcAlt2 { get; set; }
        public string AtcInfo { get; set; }
        public string AtcEndu { get; set; }
        public string AtcPers { get; set; }
        public string AtcRadi { get; set; }
        public string AtcSurv { get; set; }
        public string AtcJack { get; set; }
        public string AtcDing { get; set; }
        public string AtcCap { get; set; }
        public string AtcCover { get; set; }
        public string AtcColo { get; set; }
        public string AtcAcco { get; set; }
        public string AtcRem { get; set; }
        public string Atcpic { get; set; }
        public string AtcCtot { get; set; }
        public int FlightId { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
