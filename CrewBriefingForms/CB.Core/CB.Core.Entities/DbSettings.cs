﻿using System;
using CB.Core.Common.Keys;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class DbSettings: RealmObject, IEntity
	{
        [PrimaryKey]
        public int Id { get; set; }
        public string Value { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
