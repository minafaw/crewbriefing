﻿using System;
using System.Collections.Generic;
using System.Linq;
using CB.Core.Common.HelperClasses;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
	public  class FlightListItem : RealmObject, IEntity
	{
		[PrimaryKey]
		public int ID { get; set; }
		public IList<Entities.Crew> CrewItem { get; }
		public string Alt1 { get; set; }
		public string Alt2 { get; set; }
		public int FPLID { get; set; }
		public string FlightlogID { get; set; }
		public string DEP { get; set; }
		public string DEST { get; set; }
		public DateTimeOffset STD { get; set; }
		public DateTimeOffset STA { get; set; }
		public string ACFTAIL { get; set; }
		public int ATCTime { get; set; }
		public string ATCCtot { get; set; }
		//public ushort ATCEET { get; set; }
		public string TOA { get; set; }
		public string ATCID { get; set; }
		public DateTimeOffset LastEdit { get; set; }
		public string DestAirportFull {  get; set; }
		public string DepAirportFull { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
