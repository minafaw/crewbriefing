﻿using Realms;

namespace CB.Core.Entities
{
    public class Taf : RealmObject
    {
        public string Type { get; set; }
        public string Text { get; set; }
    }
}
