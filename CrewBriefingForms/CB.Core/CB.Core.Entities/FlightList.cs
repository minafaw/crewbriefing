﻿using System;
using CB.Core.Common.HelperClasses;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class FlightList : RealmObject, IEntity
	{
        [Ignored]
        public Crew[] Crew { get; set; }
        public string Alt1 { get; set; }
        [Ignored]
        public object Alt2 { get; set; }
        [Realms.PrimaryKey]
        public int Id { get; set; }
        public int FplId { get; set; }
        public string FlightlogId { get; set; }
        public string Dep { get; set; }
        public string Dest { get; set; }
        public DateTimeOffset Std { get; set; }
        public DateTimeOffset Sta { get; set; }
        public string AcfTail { get; set; }
       // public ushort AtcTime { get; set; }
        public string AtcCtot { get; set; }
        //public ushort AtcEet { get; set; }
        public string Toa { get; set; }
        public string AtcId { get; set; }
        public DateTimeOffset LastEdit { get; set; }
        //public bool IsRecalculated
        //{
        //    get
        //    {
        //        //return this.ID % 2 != 0;
        //        if (string.IsNullOrEmpty(FlightlogId))
        //        {
        //            return false;
        //        }
        //        return FlightlogId.EndsWith("-R");
        //    }
        //}
        public string FirstListColumn
        {
            get
            {
                return GetFlightIdToShow(FlightlogId);
            }
        }
        //public Thickness FlightListItemGridTopPadding
        //{
        //    get
        //    {
        //        return CommonStatic.Instance.FlightListItemGridVertPadding;
        //    }
        //}
        //public Thickness FlightListItemGridRightPadding
        //{
        //    get
        //    {
        //        return CommonStatic.Instance.FlightListItemGridRightPadding;
        //    }
        //}

        public static string GetFlightIdToShow(string flightlogId)
        {
            if (string.IsNullOrEmpty(flightlogId))
            {
                return string.Empty;
            }
            var view01 = string.Empty;
            var flightId1 = flightlogId;
            var flightId = flightId1.IndexOf("-", StringComparison.Ordinal) != -1 ? flightId1.Split('-') : new[] { flightId1 };
            var firstBit = flightId[0];
            if (firstBit.IndexOf("(", StringComparison.Ordinal) != -1)
            {
                //Step 2
                var fLogIDbits = firstBit.Split('(');

                firstBit = fLogIDbits[0];
                var secondBit = fLogIDbits[1];
                view01 = secondBit.TrimEnd(')');
            }
            var view1 = firstBit;
            var result = view1;
            if (!string.IsNullOrEmpty(result) && !string.IsNullOrEmpty(view01))
            {
                result += "\n";
            }
            return result + view01;
        }
        public string DepDest
        {
            get
            {
                return (Dep ?? "") + "-" + (Dest ?? "");
            }
        }
        //public Thickness FlightListColumn2Padding
        //{
        //    get
        //    {
        //        return CommonStatic.Instance.FlightListColumn2Padding;
        //    }
        //}
        //public string PreparedStd
        //{
        //    get
        //    {
        //        return CommonStatic.GetPreparedStd(Std);
        //    }
        //}
        [Ignored]
        public string MarkImagePath { get; set; }
        [Ignored]
        public double MarkImageHeight
        {
            get
            {
                return CommonStatic.Instance.GetMediumHeight;
            }
        }
        [Ignored]
        public double MarkImageWidth
        {
            get
            { // list_arrow: 15x23, so W = 0.65 * H

                return CommonStatic.Instance.GetMediumHeight * 0.65;
            }
        }
        [Ignored]
        public double RowMarkHeight
        {
            get
            {
                return CommonStatic.Instance.GetMediumHeight * 2.5;
            }
        }
        [Ignored]
        public bool IsMarkerVisible
        {
            get
            {
                if (string.IsNullOrEmpty(MarkImagePath))
                {
                    return false;
                }
                return MarkImagePath.ToLower().Contains("vert_mark.png");
            }
        }

        [Ignored]
        public bool IsImageVisible
        {
            get
            {
                return !IsMarkerVisible;
            }
        }
        public string DestAirportFull
        {
            get; set;
        }
        public string DepAirportFull
        {
            get; set;
        }

        [Ignored]
        public bool IsSelected { get; set; }

		public DateTimeOffset CreatedDate { get; set; }
	}
}
