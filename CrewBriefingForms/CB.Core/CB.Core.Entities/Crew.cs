﻿using System;
using System.Linq;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class Crew : RealmObject, IEntity
	{
		
		public FlightListItem flightListItem { get; set; }
		public string Id { get; set; }
        public string CrewType { get; set; }        
        public string CrewName { get; set; }
        public string Initials { get; set; }
        public string Gsm { get; set; }
	    public int FlightId { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
