﻿namespace CB.Core.Entities
{
    public class MessageRecipient
    {
        public string RecipientType { get; set; }
        public string Recipient { get; set; }
    }
}
