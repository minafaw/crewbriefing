﻿using System;

namespace CB.Core.Entities
{
    public class Notam
    {
        public string Number;
        public string Text;
        public DateTime FromDate;
        public DateTime ToDate;
        public int FromLevel;
        public int ToLevel;
        public string Fir;
        public string QCode;
        public string ECode;
        public string Icao;
        public string UniformAbbreviation;
        public int Year;

    }
}
