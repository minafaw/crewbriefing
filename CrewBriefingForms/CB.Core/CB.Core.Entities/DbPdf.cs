﻿using System;
using CB.Core.Common.Keys;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class DbPdf : RealmObject, IEntity
	{
		[Realms.PrimaryKey]
		public string Id { get; set; } 
        // ID from DBFlight
        public int FlightId { get; set; }
        // Document Type
        public int DocType { get; set; }
        // Filename from ProxyClient PDF's Attribute
        [Required]
        public string FileName { set; get; }

        // State of Downloading for this file
       // public Enums.PdfDocDownloadingState DownloadingState { set; get; }
        public DateTimeOffset DownloadTime { get; set; }

        [Ignored]
        public string FilePath => FilePathMaskDoDelete + DownloadTime.ToString(CommanConstants.DefaultDateTimeFormat) + ".pdf";

	    [Ignored]
        public string FilePathMaskDoDelete => FlightId + "_" + (int)DocType + "_";

		public DateTimeOffset CreatedDate { get; set; }
	}
}
