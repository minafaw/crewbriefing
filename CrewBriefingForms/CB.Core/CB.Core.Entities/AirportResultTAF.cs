﻿using System;
using CB.Core.Entities.Base;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class AirportResultTaf : RealmObject, IEntity
	{
         string TypeField { get; set; }
         string TextField { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
