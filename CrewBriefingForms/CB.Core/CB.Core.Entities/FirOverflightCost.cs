﻿using System;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
   public class FirOverflightCost : RealmObject, IEntity
	{        
        public string Fir;
        public int Distance;
        public int Cost;
		public DateTimeOffset CreatedDate { get; set; }
	}
}
