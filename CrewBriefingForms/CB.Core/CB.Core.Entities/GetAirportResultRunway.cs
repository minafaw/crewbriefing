﻿using System;
using CB.Core.Entities.Base;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class GetAirportResultRunway : RealmObject, IEntity
	{
        public string NameField { get; set; }
        public string LengthField { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
