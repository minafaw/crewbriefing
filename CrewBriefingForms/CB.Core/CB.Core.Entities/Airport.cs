﻿using System;
using System.Collections.Generic;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class Airport : RealmObject, IEntity
	{
        public IList<GetAirportResultRunway> Runways { get;  }        
        [Required]
        public string Name { get; set; }
        [Required]
        public string Icao { get; set; }
        public string Iata { get; set; }        
        public string Country { get; set; }
        public AirportResultTaf Taf { get; set; }
        public string Metar { get; set; }
        public string Var { get; set; }
        public string Elev { get; set; }
        public string Lat { get; set; }
        public string Lon { get; set; }
        public string RunWl { get; set; }
        public string Fir { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
