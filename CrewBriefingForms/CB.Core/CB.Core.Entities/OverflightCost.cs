﻿namespace CB.Core.Entities
{
    public class OverflightCost
    {
        public FirOverflightCost[] Cost { get; set; }
        public string Currency { get; set; }
    }
}
