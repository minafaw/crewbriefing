﻿using System;

namespace CB.Core.Entities.Interfaces
{
    public interface IEntity
    {
	     DateTimeOffset CreatedDate { get; set; }
    } 
}
