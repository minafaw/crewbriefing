﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities.Base
{
    public class Entity : RealmObject, IEntity
    {
        public DateTimeOffset CreatedDate { get; set; }
    }
}
