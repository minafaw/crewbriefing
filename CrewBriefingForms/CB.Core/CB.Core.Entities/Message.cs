﻿using System;
using CB.Core.Entities.Base;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class Message: RealmObject, IEntity
	{
        public string Subject;
        public string Text;
        public string SentFrom;
        public System.DateTime SentTime;
        public System.DateTime ValidFrom;
        public System.DateTime ValidTo;
        public bool HighPriority;
        public MessageRecipient[] Recipients;
		public DateTimeOffset CreatedDate { get; set; }
	}
}
