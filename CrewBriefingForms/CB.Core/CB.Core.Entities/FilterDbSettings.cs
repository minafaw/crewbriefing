﻿using System;
using CB.Core.Common.Keys;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class FilterDbSettings : RealmObject, IEntity
	{
        [Indexed]
        public string UserName { get; set; }
        public int Id { get; set; }
        public string Value { get; set; }
        [PrimaryKey]
        public string PrimKey { get; set; }
		public DateTimeOffset CreatedDate { get; set; }
	}
}
