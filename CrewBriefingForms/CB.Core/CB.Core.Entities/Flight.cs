﻿using System;
using System.Collections.Generic;
using CB.Core.Common;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Keys;
using CB.Core.Entities.Interfaces;
using Realms;

namespace CB.Core.Entities
{
    public class Flight : RealmObject, IEntity
	{
        [PrimaryKey]
        public int Id { get; set; }
        public Message[] Messages;
        public OverflightCost OverflightCost;
        public string FlightLogId { get; set; }
        public string PpsName { get; set; }
        public string AcfTail { get; set; }
        public string Dep { get; set; }
        public string Dest { get; set; }
        public string Alt1 { get; set; }
        public string Alt2 { get; set; }
        public DateTimeOffset Std { get; set; }
        public int Pax { get; set; }
        public double Fuel { get; set; }
        public int Load { get; set; }
        public string ValidHrs { get; set; }
        public int MinFl { get; set; }
        public int MaxFl { get; set; }
        public string EropsAltApts { get; set; }
        public IList<string> AdequateApt { get;  }
        public IList<string> Fir { get;  }
        public IList<string> AltApts { get;  }
        public string Toa { get; set; }
        public string Fmdid { get; set; }
        public string DestStdAlt { get; set; }
        public string FuelComp { get; set; }
        public string TimeComp { get; set; }
        public string FuelCont { get; set; }
        public string TimeCont { get; set; }
        public string PctCont { get; set; }
        public string Fuelmin { get; set; }
        public string TimeMin { get; set; }
        public string FuelTaxi { get; set; }
        public string TimeTaxi { get; set; }
        public string FuelExtra { get; set; }
        public string TimeExtra { get; set; }
        public string FuelLdg { get; set; }
        public string TimeLdg { get; set; }
        public string Zfm { get; set; }
        public string Gcd { get; set; }
        public string Esad { get; set; }
        public string Gl { get; set; }
        public string FuelBias { get; set; }
        public DateTimeOffset Sta { get; set; }
        public int SchbLockTime { get; set; }
        public string Disp { get; set; }
        public DateTimeOffset LastEditDate { get; set; }
        public string FuelMinto { get; set; }
        public string TimeMinto { get; set; }
        public string Aramp { get; set; }
        public string TimeAct { get; set; }
        public string FuelAct { get; set; }
        public string DestEra { get; set; }
        public string TrafficLoad { get; set; }
        public string WeightUnit { get; set; }
        public string WindComponent { get; set; }
        public string CustomerDataPps { get; set; }
        public string CustomerDataScheduled { get; set; }
        public int Fl { get; set; }
        public int RouteDistNm { get; set; }
        public string RouteName { get; set; }
        public string RouteType { get; set; }
        public int EmptyWeight { get; set; }
        public int TotalDistance { get; set; }
        public int AltDist { get; set; }
        public int DestTime { get; set; }
        public int AltTime { get; set; }
        public int AltFuel { get; set; }
        public int HoldTime { get; set; }
        public int ReserveTime { get; set; }
        public int Cargo { get; set; }
        public double ActTow { get; set; }
        public double TripFuel { get; set; }
        public double HoldFuel { get; set; }
        public double Elw { get; set; }
        public string FuelPolicy { get; set; }
        public int Alt2Time { get; set; }
        public int Alt2Fuel { get; set; }
        public double MaxTom { get; set; }
        public double MaxLm { get; set; }
        public double MaxZfm { get; set; }
        public DateTimeOffset WeatherObsTime { get; set; }
        public DateTimeOffset WeatherPlanTime { get; set; }
        public string Mfci { get; set; }
        public string CruiseProfile { get; set; }
        public int TempTopOfClimb { get; set; }
        public string Climb { get; set; }
        public string Descend { get; set; }
        public string FuelPl { get; set; }
        public string DescendWind { get; set; }
        public string ClimbProfile { get; set; }
        public string DescendProfile { get; set; }
        public string HoldProfile { get; set; }
        public string StepClimbProfile { get; set; }
        public string FuelContDef { get; set; }
        public string FuelAltDef { get; set; }
        public string AmexsyStatus { get; set; }
        public int AvgTrack { get; set; }
        public Taf DepTaf;
        public string DepMetar { get; set; }
        public Notam[] DepNotam;
        public Taf DestTaf;
        public string DestMetar { get; set; }
        public Notam[] DestNotam;
        public Taf Alt1Taf;
        public Taf Alt2Taf;
        public string Alt1Metar { get; set; }
        public string Alt2Metar { get; set; }
        public Notam[] Alt1Notam;
        public Notam[] Alt2Notam;
        public IList<RoutePoint> RoutePoints;
        public IList<Crew> Crews;
        
        public Atc AtcData;
        
        public Notam[] AdequateNotam;
        public Notam[] FirNotam;
        public Notam[] AlternateNotam;
        //public AltAirport[] Airports;
        public string[] EnrouteAlternates;
        public RoutePoint[] Alt1Points;
        public RoutePoint[] Alt2Points;
        
        public string[] CustomerData;
        
        public string ToAlt { get; set; }
        public string DepIata { get; set; }
        public string DestIata { get; set; }
        public int FinalReserveMinutes { get; set; }
        public int FinalReserveFuel { get; set; }
        public int AddFuelMinutes { get; set; }
        public int AddFuel { get; set; }
        public string FlightSummary { get; set; }

		[Ignored]
		public string Airports4ShowDep
		{
			get; set;
		}
		[Ignored]
		public string Airports4ShowDst { get; set; }

		public DateTimeOffset CreatedDate { get; set; }
	}
}
