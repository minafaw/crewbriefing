﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.Models.Base;
using CB.Core.Models.RequestModels;
using CB.Core.Services.Interfaces;
using Xamarin.Forms;

namespace CB.Core.ViewModels
{
	public class LoginPageViewModel : BaseViewModel
	{
		private IApiServices _apiServices;

		public string UserNameTxt
		{
			get => _userNameTxt;
			set
			{
				_userNameTxt = value;
				RaisePropertyChanged();

			}
		}

		private string _passwordTxt;

		public string PasswordTxt
		{
			get { return _passwordTxt; }
			set
			{
				_passwordTxt = value;
				RaisePropertyChanged();
			}
		}

		private bool _loginBusy;

		public bool LoginBusy
		{
			get { return _loginBusy; }
			set { _loginBusy = value;
				RaisePropertyChanged();
			}
		}


		public string LogoImageSouce { get => Utils.ImageFolder + "icon_info.png"; }
		public double LogoHeight { get => CommonStatic.Instance.GetMediumHeight * coef; }
		public double LogoWidth { get => CommonStatic.Instance.GetMediumHeight * coef; }
		public double smallSize { get => CommonStatic.Instance.GetSmallHeight; }
		public double LabelFontSize
		{
			get => CommonStatic.LabelFontSize(NamedSize.Large) *
(Device.Idiom == TargetIdiom.Tablet ? 1.3 : 1.15);
		}

		double coef = 1.0; // iso & UWP 

		public LoginRequestModel LoginRequestModel { get; set; }
		public bool QuickLoginStatus
		{
			get => _quickLoginStatus;
			set
			{
				_quickLoginStatus = value;
				RaisePropertyChanged();
			}
		}
		public ICommand quickLoginCommand;
		private bool _quickLoginStatus;
		private string _userNameTxt;

		public LoginPageViewModel(IApiServices apiServices)
		{
			_apiServices = apiServices;
			quickLoginCommand = new Command(HandleQuickCommand);

			if (Device.RuntimePlatform == Device.Android)
				coef = 1.5;

			var quickLogin = AppSetting.Instance.GetValueOrDefault(CommanConstants.QuickLoginKey, false);

			if (quickLogin)
			{
				LoginRequestModel.CustomerUser = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.UserNameKey, null);
				LoginRequestModel.CustomerPassword = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.PasswordKey, null);
			}

		}

		private void HandleQuickCommand(object obj)
		{
			QuickLoginStatus = !QuickLoginStatus;

		}

		private async Task ProcessLoginButton(string username, string password)
		{
			if (string.IsNullOrWhiteSpace(username) || !username.Equals("AAADEMO"))
			{
				string errorMsg;
				if (!Validate(out errorMsg))
				{
					ToastUtils.ShowTemporayMessage(Enums.ToastType.Warning, errorMsg);
					return;
				}
			}
			LoginBusy = true;
			//SetBusy(true);

			var response = await _apiServices.Login<BaseResponseModel>(username, password);

			LoginBusy = false;
			//SetBusy(false);
			// that mean credential is wrong
			if (response.Message != null && response.Message.Contains("Credentials are not valid for"))
			{
				ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Invalid Username or Password");
				return;
			}

			if (response.Code == Enums.WebApiErrorCode.Success)
			{
				var oldUserName = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.UserNameKey, null);
				// TODO recreate the database if user changed
				//if (!string.IsNullOrEmpty(oldUserName) && !oldUserName.Equals(username))
				//{
				//    await DB_PCL.ReCreateDatabase(); //recreate
				//}


				//NavigationPage.SetHasNavigationBar(this, false);
				AppSetting.Instance.AddOrUpdateValue(CommanConstants.UserNameKey, username);
				AppSetting.Instance.AddOrUpdateValue(CommanConstants.PasswordKey, password);
				AppSetting.Instance.AddOrUpdateValue(CommanConstants.QuickLoginKey, QuickLoginStatus);

				if (Device.Idiom == TargetIdiom.Tablet)
				{
					await App.NavigationService.NavigateAsync(nameof(TabletMainPage));

				}
				else
				{
					await App.NavigationService.NavigateAsync(nameof(FlightListPage));
				}
			}

		}



		private bool Validate(out string errorMsg)
		{
			errorMsg = null;
			if (string.IsNullOrEmpty(UserNameTxt))
			{
				errorMsg = Common.Resource.username_empty_error;
				return false;
			}
			if (string.IsNullOrEmpty(PasswordTxt))
			{
				errorMsg = Common.Resource.password_empty_error;
				return false;
			}
			return true;
		}
	}
}
