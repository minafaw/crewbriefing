﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Resources;
using System.Threading.Tasks;
using System.Windows.Input;
using CB.Core.Common;
using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Interfaces;
using CB.Core.Common.Keys;
using CB.Core.DAL;
using CB.Core.Models;
using CB.Core.Models.Base;
using CB.Core.Services;
using Xamarin.Forms;
using Flight = CB.Core.Entities.Flight;

namespace CB.Core.ViewModels
{
    public class FlightDetailsViewModel : BaseViewModel
    {
		public ICommand PressItemCommand { get; private set; }
	    private readonly DbPdfDal _dbPdfDal;
	    private readonly FlightDal _flightDal;
	    private readonly AirportDal _airportDal;
		public FlightDetailsViewModel(int flightId)
        {
	        _flightDal = new FlightDal();
	        _dbPdfDal = new DbPdfDal();
	        _airportDal = new AirportDal();

			_myItems = new List<FlightDetialListItem>();

            PressItemCommand = new Command(async obj =>
            {
                await HandleItemPressAction(obj);
            });

            Device.BeginInvokeOnMainThread(async () =>
            {
                HeaderVisibility = true;
                await GetFlightDetialAsync(flightId);
                HeaderVisibility = false;
            });


            Task.Run(async () =>
            {
                await CreatelDetialList(flightId);
            });
        }

	    private async Task HandleItemPressAction(object obj)
		{
			var selectedItem = obj as FlightDetialListItem;
			if (selectedItem == null) return;

			if (selectedItem.Title.Equals(Resource.route_map)){
				
			}else if(selectedItem.Title.Equals(Resource.flight_summary)){
				//var summeryPage = new FlightSummaryPage(flightDetails.FlightSummary, FlightListItem.GetFlightIdToShow(flightDetails.FlightLogID));
				
			}
			if (selectedItem.DocType == null) return;
            var saveLoad = DependencyService.Get<ISaveAndLoad>();
			var storedPath =  _dbPdfDal.GetStoredPdfPath(selectedItem.FlightId, (Enums.PdfDocType)selectedItem.DocType);
            if (!string.IsNullOrEmpty(storedPath))
            {
                try
                {
					await saveLoad.OpenFileWithDefaultOsViewer(storedPath, selectedItem.Title);
                }
                catch (Exception exc)
                {
                    ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Error open file. " + exc.Message);
                }
            }
        }

        private bool _headerVisibility;
        public bool HeaderVisibility
        {
            get => _headerVisibility;
            set
            {
                _headerVisibility = value;
                RaisePropertyChanged();
            }
        }
        private Flight _flightDetial;
        public Flight FlightDetial
        {
            get => _flightDetial;
            set
            {
                _flightDetial = value;
                RaisePropertyChanged();
            }
        }
     
        private List<FlightDetialListItem> _myItems ;
        public List<FlightDetialListItem> MyItems
        {
            get => _myItems;
            set
            {
                _myItems = value;
                RaisePropertyChanged();
            } 
        }
        
        private async Task GetFlightDetialAsync(int flightId)
        {
            // check if flight exists in database
            FlightDetial =  _flightDal.GetFilghtDb(flightId);
            // if not get the data from webservice 
            if (FlightDetial == null)
            {
                //FlightDetial = await GeFlightFromWs(flightId);
            }

            if (FlightDetial == null )
            {
                // if flight detial still null 
                return;
            }

                var i = 0;
                foreach (var icao in new[] { FlightDetial.Dep, FlightDetial.Dest })
                {
                    if (string.IsNullOrEmpty(icao)) continue;

                    var airport =  _airportDal.GetAirportDb(icao);
                    if (airport == null)
                    {
                        var ar = new GetAirportRequest
                        {
						   // SessionID = App.SessionId,
                            ICAO = icao
                        };
					//TODO fix this part
                       // var responseAirport = await EdfService.GetAirport(ar);
                        var responseAirport = new BaseResponseModel();
                        if (responseAirport.Code == Enums.WebApiErrorCode.Success)
                        {
                            //airport = EdfService.ParseAirport(responseAirport.Message);
                            await _airportDal.InsertOrUpdateAirport(airport);
                        }
                        else
                        {
                            ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Error receive Airport data for '" + icao + "'");
                        }

                    }
                    if (airport != null)
                    {
                        var extraString = "";
                        if (Device.RuntimePlatform == Device.UWP)
                            extraString = Environment.NewLine;
                        var airportName = (airport.Name ?? string.Empty).Replace("/", "/ " + extraString);
                        if (i == 0)
                        {
                            FlightDetial.Airports4ShowDep = airportName;
                            if (Device.RuntimePlatform == Device.Android || Device.RuntimePlatform == Device.iOS)
                            {
							   // FlightDetial.DepDstLineBreakMode = LineBreakMode.WordWrap;
                            }
                        }
                        else
                        {
                            FlightDetial.Airports4ShowDst = airportName;
                        }
                    }

                    i++;
                }
                // TODO Refine this part 
                //if (_parentPage is TabletMainPage)
                //{
                //    (_parentPage as TabletMainPage)?.ShowMapOrSummary(flightDetails, false);
                //    _firstLoad = true;
                //}
            

            
        }
        //private async Task<Flight> GeFlightFromWs(int flightId)
        //{
        //    Flight flightDetails;
        //    var checkSessionOk = await Utils.CheckSessionAndRefreshIfRequired(true);
        //    if (!checkSessionOk || IsConnected)
        //    {
        //        //await this.Navigation.PopModalAsync();
        //        if (Device.Idiom == TargetIdiom.Tablet)
        //        {
        //            //clear binding context 
        //            // show error toast 
        //            //gridDetails.BindingContext = null;
        //            //await DrawDocList(null, passedItemId);
        //        }
        //        else
        //        {
        //           // Utils.GoToPrevPage();
        //        }

        //        return null;
        //    }
        //    var response = await EdfService.GetFlight(CreateFlightRequest(flightId));
        //    if (response.Code == Enums.WebApiErrorCode.Success)
        //    {
        //        flightDetails = EdfService.ParseFlight(response.Message);
        //        if (flightDetails != null)
        //        {
        //            await _flightDal.InsertOrUpdateFlight(flightDetails);
        //        }
        //    }
        //    else // no details in DB and can't get from Inet - go back
        //    {
        //        Utils.CheckWebResponse(response, null, true);
        //        return null;
        //    }
        //    return flightDetails;
        //}
        private FlightRequest CreateFlightRequest(int passedItemId)
        {
            var fr = new FlightRequest()
            {
               // SessionID = App.SessionId,
                FlightID = passedItemId,
                Messages = false,
                Metar = false,
                Notams = false,
                Taf = false
            };
            return fr;
        }
        private async Task CreatelDetialList(int passedItemId)
        {
            var assembly = typeof(Resource).GetTypeInfo().Assembly;
            var manager = new ResourceManager("CrewBriefingForms.Strings", assembly);

            var items = new List<FlightDetialListItem>();

            var routeItem = new FlightDetialListItem
            {
                Title = Resource.route_map,
				Image1Path = Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false

            };
            var summeryItem = new FlightDetialListItem
            {
                Title = Resource.flight_summary,
                Image1Path = Utils.ImageFolder + "route_map_icon.png",
                ItemDownloaded = false

            };
            items.Add(routeItem);
            items.Add(summeryItem);

            //foreach (var itemDocType in DocsOrder)
            //{
            //    var result =  CheckDocExists(itemDocType, passedItemId);
            //    bool downloadedExists = result.Item1;
            //    DateTime? downloadDate = result.Item2;

            //    var item = new FlightDetialListItem
            //    {
            //        Title = manager.GetString("doc_title_" + itemDocType),
            //        Image1Path = Utils.ImageFolder + (downloadedExists ? "redownload_doc.png" : "icon_download.png"),
            //        DownloadDate = downloadDate,
            //        ItemDownloaded = downloadedExists,
            //        DocType = itemDocType,
            //        FlightId = passedItemId
            //    };
            //    if (itemDocType == Enums.PdfDocType.UploadedDocuments)
            //    {
            //        item.Title = downloadedExists
            //            ? Resource.doc_title_UploadedDocumentsShort
            //            : Resource.doc_title_UploadedDocuments;
            //    }
            //    if (itemDocType == Enums.PdfDocType.CombineDocuments)
            //    {
            //        item.Title = downloadedExists
            //            ? Resource.doc_title_CombineDocumentsShort
            //            : Resource.doc_title_CombineDocuments;
            //    }
            //    items.Add(item);
            //}

            MyItems =  items; 
        }
        private (bool downloadedExists, DateTime? downloadDate) CheckDocExists(Enums.PdfDocType docType, int passedItemId)
        {
            var downloadedExists = false;
            DateTime? downloadDate = null;
            // var storedDocuments = new List<PdfDocTypeWithDate>();
            var storedDocuments =  _dbPdfDal.GetStoredPdf(passedItemId);
            foreach (var pdd in storedDocuments)
            {
                if (pdd.DocType == docType)
                {
                    downloadedExists = true;
                    //downloadDate = pdd.LocalDate;
                }
            }
            return (downloadedExists, downloadDate);
        }
    }
}
