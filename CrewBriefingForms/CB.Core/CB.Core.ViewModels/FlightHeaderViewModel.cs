﻿using CB.Core.Common.Base;
using Xamarin.Forms;

namespace CB.Core.ViewModels
{
	public class FlightHeaderViewModel : BaseModel
    {
		public ImageSource AirPlaneImage => ImageSource.FromFile("aeroplane.png");
    }
}
