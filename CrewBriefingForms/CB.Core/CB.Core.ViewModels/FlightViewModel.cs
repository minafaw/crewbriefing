﻿using System;
using System.Collections.Generic;
using CB.Core.Common.Base;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Keys;
using CB.Core.Models;
using CB.Core.Services;
using Xamarin.Forms;

namespace CB.Core.ViewModels
{
	public class FlightViewModel : BaseViewModel
    {

		/// <remarks/>
        public ATC ATCData;
		public string DEST;
		/// <remarks/>
        public System.DateTime STA { get; set; }
		/// <remarks/>
        public System.DateTime LastEditDate { get; set; }
		/// <remarks/>
        public List<Crew> Crews;
		/// <remarks/>
        public string ACFTAIL { get; set; }

		public string UploadeDate4Show => CommonStatic.GetFormatedDateTime(this.LastEditDate);

	    public string CrewCMD4Show => GetCrew4Show(Enums.CrewType.Cmd);

	    public string CrewCop4Show => GetCrew4Show(Enums.CrewType.Cop);

	    private string GetCrew4Show(Enums.CrewType crewType)
        {
            if (Crews == null)
            {
                return string.Empty;
            }
            foreach (var c in Crews)
            {
                if (c.CrewType == crewType.ToString())
                {
                    return c.Initials;
                }
            }
            return "****";

        }

		// height of image in details - trick for binding
        public double AeroplaneImageHeight
        {
            get
            {

                double result = Device.Idiom == TargetIdiom.Tablet ?
                     CommonStatic.Instance.MediumHeight * 0.8 :
                    39 * CommonStatic.Instance.ScreenCoef;
                if (Device.RuntimePlatform == Device.Android)
                {
                    result *= .4;
                }
                return result;
            }
        }

		public double AeroplaneImageWidth => AeroplaneImageHeight * 7.4;

	    public string ETA4Show
        {
            get
            {
				if (ATCData == null || string.IsNullOrEmpty(ATCData.ATCEET) || ATCData.ATCEET.Length < 4)
                {
                    return string.Empty;
                }
               
                return ATCData.ATCEET.Substring(0, 2) + ":" + ATCData.ATCEET.Substring(2, 2);
            }
        }

		public string STA4Show
        {
            get
            {
                var staValue = STA;

                if (DateTime.Compare(staValue, DateTime.MinValue) == 0)
                {
                    return "No STA";
                }
                return CommonStatic.GetPreparedStd(staValue);
            }
        }



        public FlightViewModel()
        {
        }
    }
}
