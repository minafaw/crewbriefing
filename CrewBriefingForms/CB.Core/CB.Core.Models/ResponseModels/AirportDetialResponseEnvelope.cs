﻿using CB.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CB.Core.Models.ResponseModels
{
	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class AirportDetialResponseEnvelope : BaseResponseModel
    {
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public AirportDetialBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class AirportDetialBody
	{
		[XmlElement(ElementName = "GetAirportResponse", Namespace = "http://crewbriefing.com/")]
		public GetAirportResponse GetAirportResponse { get; set; }
	}

	//[XmlRoot(ElementName = "Runway", Namespace = "http://crewbriefing.com/")]
	//public class Runway
	//{
	//	[XmlElement(ElementName = "name", Namespace = "http://crewbriefing.com/")]
	//	public string Name { get; set; }
	//	[XmlElement(ElementName = "Length", Namespace = "http://crewbriefing.com/")]
	//	public string Length { get; set; }
	//}

	//[XmlRoot(ElementName = "Runways", Namespace = "http://crewbriefing.com/")]
	//public class Runways
	//{
	//	[XmlElement(ElementName = "Runway", Namespace = "http://crewbriefing.com/")]
	//	public List<Runway> Runway { get; set; }
	//}

	//[XmlRoot(ElementName = "TAF", Namespace = "http://crewbriefing.com/")]
	//public class TAF
	//{
	//	[XmlElement(ElementName = "Type", Namespace = "http://crewbriefing.com/")]
	//	public string Type { get; set; }
	//	[XmlElement(ElementName = "Text", Namespace = "http://crewbriefing.com/")]
	//	public string Text { get; set; }
	//}

	[XmlRoot(ElementName = "GetAirportResult", Namespace = "http://crewbriefing.com/")]
	public class GetAirportResult
	{
		[XmlElement(ElementName = "Runways", Namespace = "http://crewbriefing.com/")]
		public Runways Runways { get; set; }
		[XmlElement(ElementName = "Name", Namespace = "http://crewbriefing.com/")]
		public string Name { get; set; }
		[XmlElement(ElementName = "ICAO", Namespace = "http://crewbriefing.com/")]
		public string ICAO { get; set; }
		[XmlElement(ElementName = "IATA", Namespace = "http://crewbriefing.com/")]
		public string IATA { get; set; }
		[XmlElement(ElementName = "TAF", Namespace = "http://crewbriefing.com/")]
		public TAF TAF { get; set; }
		[XmlElement(ElementName = "Metar", Namespace = "http://crewbriefing.com/")]
		public string Metar { get; set; }
		[XmlElement(ElementName = "VAR", Namespace = "http://crewbriefing.com/")]
		public string VAR { get; set; }
		[XmlElement(ElementName = "ELEV", Namespace = "http://crewbriefing.com/")]
		public string ELEV { get; set; }
		[XmlElement(ElementName = "LAT", Namespace = "http://crewbriefing.com/")]
		public string LAT { get; set; }
		[XmlElement(ElementName = "LON", Namespace = "http://crewbriefing.com/")]
		public string LON { get; set; }
		[XmlElement(ElementName = "RunWL", Namespace = "http://crewbriefing.com/")]
		public string RunWL { get; set; }
		[XmlElement(ElementName = "FIR", Namespace = "http://crewbriefing.com/")]
		public string FIR { get; set; }
	}

	[XmlRoot(ElementName = "GetAirportResponse", Namespace = "http://crewbriefing.com/")]
	public class GetAirportResponse
	{
		[XmlElement(ElementName = "GetAirportResult", Namespace = "http://crewbriefing.com/")]
		public GetAirportResult GetAirportResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}
}
