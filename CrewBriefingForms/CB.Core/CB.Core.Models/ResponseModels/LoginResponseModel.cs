﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Models.Base;

namespace CB.Core.Models.ResponseModels
{
    public class LoginResponseModel : BaseResponseModel
    {
        public string GetUserFilteredSessionIDResult { get; set; }
    }

}
