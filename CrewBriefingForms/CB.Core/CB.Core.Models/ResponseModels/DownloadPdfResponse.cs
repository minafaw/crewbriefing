﻿using CB.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CB.Core.Models.ResponseModels
{
     class DownloadPdfResponse
    {
    }

	//public class GetPDF_Result
	//{
	//	public string FileName { get; set; }
	//	public string BArray { get; set; }
	//	public Responce Responce { get; set; }
	//}

	#region Logs Pdf
	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class LogsPdfEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public LogsBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class LogsBody
	{
		[XmlElement(ElementName = "GetPDF_LogstringResponse", Namespace = "http://crewbriefing.com/")]
		public GetPDF_LogstringResponse GetPDF_LogstringResponse { get; set; }
	}

	[XmlRoot(ElementName = "GetPDF_LogstringResponse", Namespace = "http://crewbriefing.com/")]
	public class GetPDF_LogstringResponse
	{
		[XmlElement(ElementName = "GetPDF_LogstringResult", Namespace = "http://crewbriefing.com/")]
		public GetPDF_Result GetPDF_LogstringResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}

	
	public class GetPDF_Result
	{
		[XmlElement(ElementName = "FileName", Namespace = "http://crewbriefing.com/")]
		public string FileName { get; set; }
		[XmlElement(ElementName = "bArray", Namespace = "http://crewbriefing.com/")]
		public byte[] BArray { get; set; }
		[XmlElement(ElementName = "Responce", Namespace = "http://crewbriefing.com/")]
		public Responce Responce { get; set; }
	}

	#endregion

	#region Messages Pdf
	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class MessagesPdfEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public MessagesBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class MessagesBody
	{
		[XmlElement(ElementName = "GetPDF_MessagesResponse", Namespace = "http://crewbriefing.com/")]
		public GetPDF_MessagesResponse GetPDF_MessagesResponse { get; set; }
	}

	[XmlRoot(ElementName = "GetPDF_MessagesResponse", Namespace = "http://crewbriefing.com/")]
	public class GetPDF_MessagesResponse
	{
		[XmlElement(ElementName = "GetPDF_MessagesResult", Namespace = "http://crewbriefing.com/")]
		public GetPDF_Result GetPDF_MessagesResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}
	
	#endregion

	#region Wx Pdf
	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class WxPdfEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public WxBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class WxBody
	{
		[XmlElement(ElementName = "GetPDF_WXResponse", Namespace = "http://crewbriefing.com/")]
		public GetPDF_WXResponse GetPDF_WXResponse { get; set; }
	}

	[XmlRoot(ElementName = "GetPDF_WXResponse", Namespace = "http://crewbriefing.com/")]
	public class GetPDF_WXResponse
	{
		[XmlElement(ElementName = "GetPDF_WXResult", Namespace = "http://crewbriefing.com/")]
		public GetPDF_Result GetPDF_WXResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}




	#endregion

	#region NotAm Pdf
	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class NotamsEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public NotamsBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class NotamsBody
	{
		[XmlElement(ElementName = "GetPDF_NOTAMsResponse", Namespace = "http://crewbriefing.com/")]
		public GetPDF_NOTAMsResponse GetPDF_NOTAMsResponse { get; set; }
	}

	[XmlRoot(ElementName = "GetPDF_NOTAMsResponse", Namespace = "http://crewbriefing.com/")]
	public class GetPDF_NOTAMsResponse
	{
		[XmlElement(ElementName = "GetPDF_NOTAMsResult", Namespace = "http://crewbriefing.com/")]
		public GetPDF_Result GetPDF_NOTAMsResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}
	#endregion


	#region AtcPdf

	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class AtcEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public AtcBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class AtcBody
	{
		[XmlElement(ElementName = "GetPDF_ATCResponse", Namespace = "http://crewbriefing.com/")]
		public GetPDF_ATCResponse GetPDF_ATCResponse { get; set; }
	}

	[XmlRoot(ElementName = "GetPDF_ATCResponse", Namespace = "http://crewbriefing.com/")]
	public class GetPDF_ATCResponse
	{
		[XmlElement(ElementName = "GetPDF_ATCResult", Namespace = "http://crewbriefing.com/")]
		public GetPDF_Result GetPDF_ATCResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}
	#endregion

	//WindT / Swx / Cross Section
	#region PdfDoucments
	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class DocumentEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public DocBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class DocBody 
	{
		[XmlElement(ElementName = "GetFlightDocumentResponse", Namespace = "http://crewbriefing.com/")]
		public GetFlightDocumentResponse GetFlightDocumentResponse { get; set; }
	}

	[XmlRoot(ElementName = "GetFlightDocumentResponse", Namespace = "http://crewbriefing.com/")]
	public class GetFlightDocumentResponse
	{
		[XmlElement(ElementName = "GetFlightDocumentResult", Namespace = "http://crewbriefing.com/")]
		public GetFlightDocumentResult GetFlightDocumentResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}

	[XmlRoot(ElementName = "GetFlightDocumentResult", Namespace = "http://crewbriefing.com/")]
	public class GetFlightDocumentResult
	{
		[XmlElement(ElementName = "Meta", Namespace = "http://crewbriefing.com/")]
		public Meta Meta { get; set; }
		[XmlElement(ElementName = "ByteArray", Namespace = "http://crewbriefing.com/")]
		public byte[] ByteArray { get; set; }
		[XmlElement(ElementName = "Response", Namespace = "http://crewbriefing.com/")]
		public Responce Response { get; set; }
	}

	[XmlRoot(ElementName = "Meta", Namespace = "http://crewbriefing.com/")]
	public class Meta
	{
		[XmlElement(ElementName = "Title", Namespace = "http://crewbriefing.com/")]
		public string Title { get; set; }
		[XmlElement(ElementName = "ValidFrom", Namespace = "http://crewbriefing.com/")]
		public string ValidFrom { get; set; }
		[XmlElement(ElementName = "ValidTo", Namespace = "http://crewbriefing.com/")]
		public string ValidTo { get; set; }
		[XmlElement(ElementName = "FlightId", Namespace = "http://crewbriefing.com/")]
		public string FlightId { get; set; }
		[XmlElement(ElementName = "Identifier", Namespace = "http://crewbriefing.com/")]
		public string Identifier { get; set; }
		[XmlElement(ElementName = "Type", Namespace = "http://crewbriefing.com/")]
		public string Type { get; set; }
		[XmlElement(ElementName = "Category", Namespace = "http://crewbriefing.com/")]
		public string Category { get; set; }
		[XmlElement(ElementName = "Info1", Namespace = "http://crewbriefing.com/")]
		public string Info1 { get; set; }
		[XmlElement(ElementName = "Info2", Namespace = "http://crewbriefing.com/")]
		public string Info2 { get; set; }
		[XmlElement(ElementName = "Created", Namespace = "http://crewbriefing.com/")]
		public string Created { get; set; }
		[XmlElement(ElementName = "LastModified", Namespace = "http://crewbriefing.com/")]
		public string LastModified { get; set; }
		[XmlElement(ElementName = "BasedOnForecast", Namespace = "http://crewbriefing.com/")]
		public string BasedOnForecast { get; set; }
	}
	#endregion


	#region Combined / uploaded

	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class FullPdfEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public FullBody Body { get; set; }
		[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Soap { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
		[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsd { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class FullBody
	{
		[XmlElement(ElementName = "GetPDF_FullPackageResponse", Namespace = "http://crewbriefing.com/")]
		public GetPDF_FullPackageResponse GetPDF_FullPackageResponse { get; set; }
	}

	[XmlRoot(ElementName = "GetPDF_FullPackageResponse", Namespace = "http://crewbriefing.com/")]
	public class GetPDF_FullPackageResponse
	{
		[XmlElement(ElementName = "GetPDF_FullPackageResult", Namespace = "http://crewbriefing.com/")]
		public GetPDF_Result GetPDF_FullPackageResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}
	#endregion

}
