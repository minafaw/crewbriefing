﻿using CB.Core.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CB.Core.Models.ResponseModels
{
	

		[XmlRoot(ElementName = "FIROverflightCost", Namespace = "http://crewbriefing.com/")]
		public class FIROverflightCost
		{
			[XmlElement(ElementName = "FIR", Namespace = "http://crewbriefing.com/")]
			public string FIR { get; set; }
			[XmlElement(ElementName = "Distance", Namespace = "http://crewbriefing.com/")]
			public string Distance { get; set; }
			[XmlElement(ElementName = "Cost", Namespace = "http://crewbriefing.com/")]
			public string Cost { get; set; }
		}

		[XmlRoot(ElementName = "Cost", Namespace = "http://crewbriefing.com/")]
		public class Cost
		{
			[XmlElement(ElementName = "FIROverflightCost", Namespace = "http://crewbriefing.com/")]
			public List<FIROverflightCost> FIROverflightCost { get; set; }
		}

		[XmlRoot(ElementName = "OverflightCost", Namespace = "http://crewbriefing.com/")]
		public class OverflightCost
		{
			[XmlElement(ElementName = "Cost", Namespace = "http://crewbriefing.com/")]
			public Cost Cost { get; set; }
			[XmlElement(ElementName = "Currency", Namespace = "http://crewbriefing.com/")]
			public string Currency { get; set; }
			[XmlElement(ElementName = "TotalOverflightCost", Namespace = "http://crewbriefing.com/")]
			public string TotalOverflightCost { get; set; }
			[XmlElement(ElementName = "TotalTerminalCost", Namespace = "http://crewbriefing.com/")]
			public string TotalTerminalCost { get; set; }
		}

		[XmlRoot(ElementName = "AdequateApt", Namespace = "http://crewbriefing.com/")]
		public class AdequateApt
		{
			[XmlElement(ElementName = "string", Namespace = "http://crewbriefing.com/")]
			public List<string> String { get; set; }
		}

		[XmlRoot(ElementName = "FIR", Namespace = "http://crewbriefing.com/")]
		public class FIR
		{
			[XmlElement(ElementName = "string", Namespace = "http://crewbriefing.com/")]
			public List<string> String { get; set; }
		}

		[XmlRoot(ElementName = "AltApts", Namespace = "http://crewbriefing.com/")]
		public class AltApts
		{
			[XmlElement(ElementName = "string", Namespace = "http://crewbriefing.com/")]
			public List<string> String { get; set; }
		}

		[XmlRoot(ElementName = "Holding", Namespace = "http://crewbriefing.com/")]
		public class Holding
		{
			[XmlElement(ElementName = "Fuel", Namespace = "http://crewbriefing.com/")]
			public string Fuel { get; set; }
			[XmlElement(ElementName = "Minutes", Namespace = "http://crewbriefing.com/")]
			public string Minutes { get; set; }
			[XmlElement(ElementName = "Profile", Namespace = "http://crewbriefing.com/")]
			public string Profile { get; set; }
			[XmlElement(ElementName = "Specification", Namespace = "http://crewbriefing.com/")]
			public string Specification { get; set; }
			[XmlElement(ElementName = "FuelFlowType", Namespace = "http://crewbriefing.com/")]
			public string FuelFlowType { get; set; }
		}

		[XmlRoot(ElementName = "RoutePoint", Namespace = "http://crewbriefing.com/")]
		public class RoutePoint
		{
			[XmlElement(ElementName = "ID", Namespace = "http://crewbriefing.com/")]
			public string ID { get; set; }
			[XmlElement(ElementName = "IDENT", Namespace = "http://crewbriefing.com/")]
			public string IDENT { get; set; }
			[XmlElement(ElementName = "FL", Namespace = "http://crewbriefing.com/")]
			public string FL { get; set; }
			[XmlElement(ElementName = "Wind", Namespace = "http://crewbriefing.com/")]
			public string Wind { get; set; }
			[XmlElement(ElementName = "Vol", Namespace = "http://crewbriefing.com/")]
			public string Vol { get; set; }
			[XmlElement(ElementName = "ISA", Namespace = "http://crewbriefing.com/")]
			public string ISA { get; set; }
			[XmlElement(ElementName = "LegTime", Namespace = "http://crewbriefing.com/")]
			public string LegTime { get; set; }
			[XmlElement(ElementName = "LegCourse", Namespace = "http://crewbriefing.com/")]
			public string LegCourse { get; set; }
			[XmlElement(ElementName = "LegDistance", Namespace = "http://crewbriefing.com/")]
			public string LegDistance { get; set; }
			[XmlElement(ElementName = "LegCAT", Namespace = "http://crewbriefing.com/")]
			public string LegCAT { get; set; }
			[XmlElement(ElementName = "LegName", Namespace = "http://crewbriefing.com/")]
			public string LegName { get; set; }
			[XmlElement(ElementName = "LegAWY", Namespace = "http://crewbriefing.com/")]
			public string LegAWY { get; set; }
			[XmlElement(ElementName = "FuelUsed", Namespace = "http://crewbriefing.com/")]
			public string FuelUsed { get; set; }
			[XmlElement(ElementName = "FuelFlow", Namespace = "http://crewbriefing.com/")]
			public string FuelFlow { get; set; }
			[XmlElement(ElementName = "LAT", Namespace = "http://crewbriefing.com/")]
			public double LAT { get; set; }
			[XmlElement(ElementName = "LON", Namespace = "http://crewbriefing.com/")]
			public double LON { get; set; }
			[XmlElement(ElementName = "VARIATION", Namespace = "http://crewbriefing.com/")]
			public string VARIATION { get; set; }
			[XmlElement(ElementName = "ACCDIST", Namespace = "http://crewbriefing.com/")]
			public int ACCDIST { get; set; }
			[XmlElement(ElementName = "ACCTIME", Namespace = "http://crewbriefing.com/")]
			public string ACCTIME { get; set; }
			[XmlElement(ElementName = "MagCourse", Namespace = "http://crewbriefing.com/")]
			public string MagCourse { get; set; }
			[XmlElement(ElementName = "TrueAirSpeed", Namespace = "http://crewbriefing.com/")]
			public string TrueAirSpeed { get; set; }
			[XmlElement(ElementName = "GroundSpeed", Namespace = "http://crewbriefing.com/")]
			public string GroundSpeed { get; set; }
			[XmlElement(ElementName = "FuelRemaining", Namespace = "http://crewbriefing.com/")]
			public string FuelRemaining { get; set; }
			[XmlElement(ElementName = "DistRemaining", Namespace = "http://crewbriefing.com/")]
			public int DistRemaining { get; set; }
			[XmlElement(ElementName = "TimeRemaining", Namespace = "http://crewbriefing.com/")]
			public string TimeRemaining { get; set; }
			[XmlElement(ElementName = "MinReqFuel", Namespace = "http://crewbriefing.com/")]
			public string MinReqFuel { get; set; }
			[XmlElement(ElementName = "FuelFlowPerEng", Namespace = "http://crewbriefing.com/")]
			public string FuelFlowPerEng { get; set; }
			[XmlElement(ElementName = "Temperature", Namespace = "http://crewbriefing.com/")]
			public string Temperature { get; set; }
			[XmlElement(ElementName = "MORA", Namespace = "http://crewbriefing.com/")]
			public string MORA { get; set; }
			[XmlElement(ElementName = "Frequency", Namespace = "http://crewbriefing.com/")]
			public string Frequency { get; set; }
			[XmlElement(ElementName = "WindComponent", Namespace = "http://crewbriefing.com/")]
			public string WindComponent { get; set; }
			[XmlElement(ElementName = "MinimumEnrouteAltitude", Namespace = "http://crewbriefing.com/")]
			public string MinimumEnrouteAltitude { get; set; }
			[XmlElement(ElementName = "Eco", Namespace = "http://crewbriefing.com/")]
			public Eco Eco { get; set; }
		}

		[XmlRoot(ElementName = "Eco", Namespace = "http://crewbriefing.com/")]
		public class Eco
		{
			[XmlElement(ElementName = "OptSpeedFL", Namespace = "http://crewbriefing.com/")]
			public string OptSpeedFL { get; set; }
			[XmlElement(ElementName = "SpeedGain", Namespace = "http://crewbriefing.com/")]
			public string SpeedGain { get; set; }
			[XmlElement(ElementName = "OptEcoFL", Namespace = "http://crewbriefing.com/")]
			public string OptEcoFL { get; set; }
			[XmlElement(ElementName = "MoneyGain", Namespace = "http://crewbriefing.com/")]
			public string MoneyGain { get; set; }
			[XmlElement(ElementName = "OptFuelFL", Namespace = "http://crewbriefing.com/")]
			public string OptFuelFL { get; set; }
			[XmlElement(ElementName = "FuelGain", Namespace = "http://crewbriefing.com/")]
			public string FuelGain { get; set; }
		}

		[XmlRoot(ElementName = "RoutePoints", Namespace = "http://crewbriefing.com/")]
		public class RoutePoints
		{
			[XmlElement(ElementName = "RoutePoint", Namespace = "http://crewbriefing.com/")]
			public List<RoutePoint> RoutePoint { get; set; }
		}


		[XmlRoot(ElementName = "Crews", Namespace = "http://crewbriefing.com/")]
		public class Crews
		{
			[XmlElement(ElementName = "Crew", Namespace = "http://crewbriefing.com/")]
			public List<Crew> Crew { get; set; }
		}

		[XmlRoot(ElementName = "Responce", Namespace = "http://crewbriefing.com/")]
		public class Responce
		{
			[XmlElement(ElementName = "Succeed", Namespace = "http://crewbriefing.com/")]
			public bool Succeed { get; set; }
		}

		[XmlRoot(ElementName = "ATCData", Namespace = "http://crewbriefing.com/")]
		public class ATCData
		{
			[XmlElement(ElementName = "ATCID", Namespace = "http://crewbriefing.com/")]
			public string ATCID { get; set; }
			[XmlElement(ElementName = "ATCTOA", Namespace = "http://crewbriefing.com/")]
			public string ATCTOA { get; set; }
			[XmlElement(ElementName = "ATCRule", Namespace = "http://crewbriefing.com/")]
			public string ATCRule { get; set; }
			[XmlElement(ElementName = "ATCType", Namespace = "http://crewbriefing.com/")]
			public string ATCType { get; set; }
			[XmlElement(ElementName = "ATCNum", Namespace = "http://crewbriefing.com/")]
			public string ATCNum { get; set; }
			[XmlElement(ElementName = "ATCWake", Namespace = "http://crewbriefing.com/")]
			public string ATCWake { get; set; }
			[XmlElement(ElementName = "ATCEqui", Namespace = "http://crewbriefing.com/")]
			public string ATCEqui { get; set; }
			[XmlElement(ElementName = "ATCSSR", Namespace = "http://crewbriefing.com/")]
			public string ATCSSR { get; set; }
			[XmlElement(ElementName = "ATCDep", Namespace = "http://crewbriefing.com/")]
			public string ATCDep { get; set; }
			[XmlElement(ElementName = "ATCTime", Namespace = "http://crewbriefing.com/")]
			public string ATCTime { get; set; }
			[XmlElement(ElementName = "ATCSpeed", Namespace = "http://crewbriefing.com/")]
			public string ATCSpeed { get; set; }
			[XmlElement(ElementName = "ATCFL", Namespace = "http://crewbriefing.com/")]
			public string ATCFL { get; set; }
			[XmlElement(ElementName = "ATCRoute", Namespace = "http://crewbriefing.com/")]
			public string ATCRoute { get; set; }
			[XmlElement(ElementName = "ATCDest", Namespace = "http://crewbriefing.com/")]
			public string ATCDest { get; set; }
			[XmlElement(ElementName = "ATCEET", Namespace = "http://crewbriefing.com/")]
			public string ATCEET { get; set; }
			[XmlElement(ElementName = "ATCAlt1", Namespace = "http://crewbriefing.com/")]
			public string ATCAlt1 { get; set; }
			[XmlElement(ElementName = "ATCAlt2", Namespace = "http://crewbriefing.com/")]
			public string ATCAlt2 { get; set; }
			[XmlElement(ElementName = "ATCInfo", Namespace = "http://crewbriefing.com/")]
			public string ATCInfo { get; set; }
			[XmlElement(ElementName = "ATCEndu", Namespace = "http://crewbriefing.com/")]
			public string ATCEndu { get; set; }
			[XmlElement(ElementName = "ATCPers", Namespace = "http://crewbriefing.com/")]
			public string ATCPers { get; set; }
			[XmlElement(ElementName = "ATCRadi", Namespace = "http://crewbriefing.com/")]
			public string ATCRadi { get; set; }
			[XmlElement(ElementName = "ATCSurv", Namespace = "http://crewbriefing.com/")]
			public string ATCSurv { get; set; }
			[XmlElement(ElementName = "ATCJack", Namespace = "http://crewbriefing.com/")]
			public string ATCJack { get; set; }
			[XmlElement(ElementName = "ATCDing", Namespace = "http://crewbriefing.com/")]
			public string ATCDing { get; set; }
			[XmlElement(ElementName = "ATCCap", Namespace = "http://crewbriefing.com/")]
			public string ATCCap { get; set; }
			[XmlElement(ElementName = "ATCCover", Namespace = "http://crewbriefing.com/")]
			public string ATCCover { get; set; }
			[XmlElement(ElementName = "ATCColo", Namespace = "http://crewbriefing.com/")]
			public string ATCColo { get; set; }
			[XmlElement(ElementName = "ATCAcco", Namespace = "http://crewbriefing.com/")]
			public string ATCAcco { get; set; }
			[XmlElement(ElementName = "ATCRem", Namespace = "http://crewbriefing.com/")]
			public string ATCRem { get; set; }
			[XmlElement(ElementName = "ATCPIC", Namespace = "http://crewbriefing.com/")]
			public string ATCPIC { get; set; }
			[XmlElement(ElementName = "ATCCtot", Namespace = "http://crewbriefing.com/")]
			public string ATCCtot { get; set; }
		}

		[XmlRoot(ElementName = "NextLeg", Namespace = "http://crewbriefing.com/")]
		public class NextLeg
		{
			[XmlElement(ElementName = "STD", Namespace = "http://crewbriefing.com/")]
			public string STD { get; set; }
			[XmlElement(ElementName = "DEP", Namespace = "http://crewbriefing.com/")]
			public string DEP { get; set; }
			[XmlElement(ElementName = "DEST", Namespace = "http://crewbriefing.com/")]
			public string DEST { get; set; }
			[XmlElement(ElementName = "MINFUEL", Namespace = "http://crewbriefing.com/")]
			public string MINFUEL { get; set; }
		}

		[XmlRoot(ElementName = "FlightLevel", Namespace = "http://crewbriefing.com/")]
		public class FlightLevel
		{
			[XmlElement(ElementName = "Level", Namespace = "http://crewbriefing.com/")]
			public string Level { get; set; }
			[XmlElement(ElementName = "Cost", Namespace = "http://crewbriefing.com/")]
			public string Cost { get; set; }
			[XmlElement(ElementName = "WC", Namespace = "http://crewbriefing.com/")]
			public string WC { get; set; }
			[XmlElement(ElementName = "TimeNCruise", Namespace = "http://crewbriefing.com/")]
			public string TimeNCruise { get; set; }
			[XmlElement(ElementName = "FuelNCruise", Namespace = "http://crewbriefing.com/")]
			public string FuelNCruise { get; set; }
			[XmlElement(ElementName = "TimeProfile2", Namespace = "http://crewbriefing.com/")]
			public string TimeProfile2 { get; set; }
			[XmlElement(ElementName = "FuelProfile2", Namespace = "http://crewbriefing.com/")]
			public string FuelProfile2 { get; set; }
			[XmlElement(ElementName = "TimeProfile3", Namespace = "http://crewbriefing.com/")]
			public string TimeProfile3 { get; set; }
			[XmlElement(ElementName = "FuelProfile3", Namespace = "http://crewbriefing.com/")]
			public string FuelProfile3 { get; set; }
			[XmlElement(ElementName = "FuelLower", Namespace = "http://crewbriefing.com/")]
			public string FuelLower { get; set; }
			[XmlElement(ElementName = "CostDiff", Namespace = "http://crewbriefing.com/")]
			public string CostDiff { get; set; }
		}

		[XmlRoot(ElementName = "OptFlightLevels", Namespace = "http://crewbriefing.com/")]
		public class OptFlightLevels
		{
			[XmlElement(ElementName = "FlightLevel", Namespace = "http://crewbriefing.com/")]
			public List<FlightLevel> FlightLevel { get; set; }
		}

		[XmlRoot(ElementName = "Frequency", Namespace = "http://crewbriefing.com/")]
		public class Frequency
		{
			[XmlElement(ElementName = "Value", Namespace = "http://crewbriefing.com/")]
			public string Value { get; set; }
			[XmlElement(ElementName = "Information", Namespace = "http://crewbriefing.com/")]
			public string Information { get; set; }
		}

		[XmlRoot(ElementName = "Frequencies", Namespace = "http://crewbriefing.com/")]
		public class Frequencies
		{
			[XmlElement(ElementName = "Frequency", Namespace = "http://crewbriefing.com/")]
			public List<Frequency> Frequency { get; set; }
		}

		[XmlRoot(ElementName = "AltAirport", Namespace = "http://crewbriefing.com/")]
		public class AltAirport
		{
			[XmlElement(ElementName = "Type", Namespace = "http://crewbriefing.com/")]
			public string Type { get; set; }
			[XmlElement(ElementName = "Icao", Namespace = "http://crewbriefing.com/")]
			public string Icao { get; set; }
			[XmlElement(ElementName = "Freq", Namespace = "http://crewbriefing.com/")]
			public string Freq { get; set; }
			[XmlElement(ElementName = "Freq2", Namespace = "http://crewbriefing.com/")]
			public string Freq2 { get; set; }
			[XmlElement(ElementName = "Info", Namespace = "http://crewbriefing.com/")]
			public string Info { get; set; }
			[XmlElement(ElementName = "Info2", Namespace = "http://crewbriefing.com/")]
			public string Info2 { get; set; }
			[XmlElement(ElementName = "Dist", Namespace = "http://crewbriefing.com/")]
			public string Dist { get; set; }
			[XmlElement(ElementName = "Time", Namespace = "http://crewbriefing.com/")]
			public string Time { get; set; }
			[XmlElement(ElementName = "Fuel", Namespace = "http://crewbriefing.com/")]
			public string Fuel { get; set; }
			[XmlElement(ElementName = "MAGCURS", Namespace = "http://crewbriefing.com/")]
			public string MAGCURS { get; set; }
			[XmlElement(ElementName = "ATC", Namespace = "http://crewbriefing.com/")]
			public string ATC { get; set; }
			[XmlElement(ElementName = "Lat", Namespace = "http://crewbriefing.com/")]
			public string Lat { get; set; }
			[XmlElement(ElementName = "Long", Namespace = "http://crewbriefing.com/")]
			public string Long { get; set; }
			[XmlElement(ElementName = "Rwyl", Namespace = "http://crewbriefing.com/")]
			public string Rwyl { get; set; }
			[XmlElement(ElementName = "Elevation", Namespace = "http://crewbriefing.com/")]
			public string Elevation { get; set; }
			[XmlElement(ElementName = "Name", Namespace = "http://crewbriefing.com/")]
			public string Name { get; set; }
			[XmlElement(ElementName = "Iata", Namespace = "http://crewbriefing.com/")]
			public string Iata { get; set; }
			[XmlElement(ElementName = "Category", Namespace = "http://crewbriefing.com/")]
			public string Category { get; set; }
			[XmlElement(ElementName = "Frequencies", Namespace = "http://crewbriefing.com/")]
			public Frequencies Frequencies { get; set; }
		}

		[XmlRoot(ElementName = "Airports", Namespace = "http://crewbriefing.com/")]
		public class AltAirports
		{
			[XmlElement(ElementName = "AltAirport", Namespace = "http://crewbriefing.com/")]
			public List<AltAirport> AltAirport { get; set; }
		}

		[XmlRoot(ElementName = "Alt1Points", Namespace = "http://crewbriefing.com/")]
		public class Alt1Points
		{
			[XmlElement(ElementName = "RoutePoint", Namespace = "http://crewbriefing.com/")]
			public List<RoutePoint> RoutePoint { get; set; }
		}

		[XmlRoot(ElementName = "AlternateAirport", Namespace = "http://crewbriefing.com/")]
		public class AlternateAirport
		{
			[XmlElement(ElementName = "ICAO", Namespace = "http://crewbriefing.com/")]
			public string ICAO { get; set; }
			[XmlElement(ElementName = "IATA", Namespace = "http://crewbriefing.com/")]
			public string IATA { get; set; }
			[XmlElement(ElementName = "Name", Namespace = "http://crewbriefing.com/")]
			public string Name { get; set; }
			[XmlElement(ElementName = "Wind", Namespace = "http://crewbriefing.com/")]
			public string Wind { get; set; }
			[XmlElement(ElementName = "WindVelocity", Namespace = "http://crewbriefing.com/")]
			public string WindVelocity { get; set; }
			[XmlElement(ElementName = "FlightLevel", Namespace = "http://crewbriefing.com/")]
			public string FlightLevel { get; set; }
			[XmlElement(ElementName = "Distance", Namespace = "http://crewbriefing.com/")]
			public string Distance { get; set; }
			[XmlElement(ElementName = "Course", Namespace = "http://crewbriefing.com/")]
			public string Course { get; set; }
			[XmlElement(ElementName = "Time", Namespace = "http://crewbriefing.com/")]
			public string Time { get; set; }
			[XmlElement(ElementName = "Fuel", Namespace = "http://crewbriefing.com/")]
			public string Fuel { get; set; }
			[XmlElement(ElementName = "BlockTime", Namespace = "http://crewbriefing.com/")]
			public string BlockTime { get; set; }
			[XmlElement(ElementName = "BlockFuel", Namespace = "http://crewbriefing.com/")]
			public string BlockFuel { get; set; }
			[XmlElement(ElementName = "ERA", Namespace = "http://crewbriefing.com/")]
			public string ERA { get; set; }
		}

		[XmlRoot(ElementName = "StdAlternates", Namespace = "http://crewbriefing.com/")]
		public class StdAlternates
		{
			[XmlElement(ElementName = "AlternateAirport", Namespace = "http://crewbriefing.com/")]
			public List<AlternateAirport> AlternateAirport { get; set; }
		}

		[XmlRoot(ElementName = "CustomerData", Namespace = "http://crewbriefing.com/")]
		public class CustomerData
		{
			[XmlElement(ElementName = "string", Namespace = "http://crewbriefing.com/")]
			public string String { get; set; }
		}

		[XmlRoot(ElementName = "RouteStrings", Namespace = "http://crewbriefing.com/")]
		public class RouteStrings
		{
			[XmlElement(ElementName = "ToDest", Namespace = "http://crewbriefing.com/")]
			public string ToDest { get; set; }
			[XmlElement(ElementName = "ToAlt1", Namespace = "http://crewbriefing.com/")]
			public string ToAlt1 { get; set; }
			[XmlElement(ElementName = "ToAlt2", Namespace = "http://crewbriefing.com/")]
			public string ToAlt2 { get; set; }
		}

		[XmlRoot(ElementName = "EtopsInformation", Namespace = "http://crewbriefing.com/")]
		public class EtopsInformation
		{
			[XmlElement(ElementName = "RuleTimeUsed", Namespace = "http://crewbriefing.com/")]
			public string RuleTimeUsed { get; set; }
			[XmlElement(ElementName = "IcingPercentage", Namespace = "http://crewbriefing.com/")]
			public string IcingPercentage { get; set; }
		}

		[XmlRoot(ElementName = "GetFlightResult", Namespace = "http://crewbriefing.com/")]
		public class GetFlightResult
		{
			[XmlElement(ElementName = "OverflightCost", Namespace = "http://crewbriefing.com/")]
			public OverflightCost OverflightCost { get; set; }
			[XmlElement(ElementName = "FlightLogID", Namespace = "http://crewbriefing.com/")]
			public string FlightLogID { get; set; }
			[XmlElement(ElementName = "ID", Namespace = "http://crewbriefing.com/")]
			public int ID { get; set; }
			[XmlElement(ElementName = "PPSName", Namespace = "http://crewbriefing.com/")]
			public string PPSName { get; set; }
			[XmlElement(ElementName = "ACFTAIL", Namespace = "http://crewbriefing.com/")]
			public string ACFTAIL { get; set; }
			[XmlElement(ElementName = "DEP", Namespace = "http://crewbriefing.com/")]
			public string DEP { get; set; }
			[XmlElement(ElementName = "DEST", Namespace = "http://crewbriefing.com/")]
			public string DEST { get; set; }
			[XmlElement(ElementName = "ALT1", Namespace = "http://crewbriefing.com/")]
			public string ALT1 { get; set; }
			[XmlElement(ElementName = "ALT2", Namespace = "http://crewbriefing.com/")]
			public string ALT2 { get; set; }
			[XmlElement(ElementName = "STD", Namespace = "http://crewbriefing.com/")]
			public DateTime STD { get; set; }
			[XmlElement(ElementName = "PAX", Namespace = "http://crewbriefing.com/")]
			public string PAX { get; set; }
			[XmlElement(ElementName = "FUEL", Namespace = "http://crewbriefing.com/")]
			public string FUEL { get; set; }
			[XmlElement(ElementName = "LOAD", Namespace = "http://crewbriefing.com/")]
			public string LOAD { get; set; }
			[XmlElement(ElementName = "ValidHrs", Namespace = "http://crewbriefing.com/")]
			public string ValidHrs { get; set; }
			[XmlElement(ElementName = "MinFL", Namespace = "http://crewbriefing.com/")]
			public string MinFL { get; set; }
			[XmlElement(ElementName = "MaxFL", Namespace = "http://crewbriefing.com/")]
			public string MaxFL { get; set; }
			[XmlElement(ElementName = "EROPSAltApts", Namespace = "http://crewbriefing.com/")]
			public string EROPSAltApts { get; set; }
			[XmlElement(ElementName = "AdequateApt", Namespace = "http://crewbriefing.com/")]
			public AdequateApt AdequateApt { get; set; }
			[XmlElement(ElementName = "FIR", Namespace = "http://crewbriefing.com/")]
			public FIR FIR { get; set; }
			[XmlElement(ElementName = "AltApts", Namespace = "http://crewbriefing.com/")]
			public AltApts AltApts { get; set; }
			[XmlElement(ElementName = "TOA", Namespace = "http://crewbriefing.com/")]
			public string TOA { get; set; }
			[XmlElement(ElementName = "FMDID", Namespace = "http://crewbriefing.com/")]
			public string FMDID { get; set; }
			[XmlElement(ElementName = "DESTSTDALT", Namespace = "http://crewbriefing.com/")]
			public string DESTSTDALT { get; set; }
			[XmlElement(ElementName = "FUELCOMP", Namespace = "http://crewbriefing.com/")]
			public string FUELCOMP { get; set; }
			[XmlElement(ElementName = "TIMECOMP", Namespace = "http://crewbriefing.com/")]
			public string TIMECOMP { get; set; }
			[XmlElement(ElementName = "FUELCONT", Namespace = "http://crewbriefing.com/")]
			public string FUELCONT { get; set; }
			[XmlElement(ElementName = "TIMECONT", Namespace = "http://crewbriefing.com/")]
			public string TIMECONT { get; set; }
			[XmlElement(ElementName = "PCTCONT", Namespace = "http://crewbriefing.com/")]
			public string PCTCONT { get; set; }
			[XmlElement(ElementName = "FUELMIN", Namespace = "http://crewbriefing.com/")]
			public string FUELMIN { get; set; }
			[XmlElement(ElementName = "TIMEMIN", Namespace = "http://crewbriefing.com/")]
			public string TIMEMIN { get; set; }
			[XmlElement(ElementName = "FUELTAXI", Namespace = "http://crewbriefing.com/")]
			public string FUELTAXI { get; set; }
			[XmlElement(ElementName = "TIMETAXI", Namespace = "http://crewbriefing.com/")]
			public string TIMETAXI { get; set; }
			[XmlElement(ElementName = "FUELEXTRA", Namespace = "http://crewbriefing.com/")]
			public string FUELEXTRA { get; set; }
			[XmlElement(ElementName = "TIMEEXTRA", Namespace = "http://crewbriefing.com/")]
			public string TIMEEXTRA { get; set; }
			[XmlElement(ElementName = "FUELLDG", Namespace = "http://crewbriefing.com/")]
			public string FUELLDG { get; set; }
			[XmlElement(ElementName = "TIMELDG", Namespace = "http://crewbriefing.com/")]
			public string TIMELDG { get; set; }
			[XmlElement(ElementName = "ZFM", Namespace = "http://crewbriefing.com/")]
			public string ZFM { get; set; }
			[XmlElement(ElementName = "GCD", Namespace = "http://crewbriefing.com/")]
			public string GCD { get; set; }
			[XmlElement(ElementName = "ESAD", Namespace = "http://crewbriefing.com/")]
			public string ESAD { get; set; }
			[XmlElement(ElementName = "GL", Namespace = "http://crewbriefing.com/")]
			public string GL { get; set; }
			[XmlElement(ElementName = "FUELBIAS", Namespace = "http://crewbriefing.com/")]
			public string FUELBIAS { get; set; }
			[XmlElement(ElementName = "STA", Namespace = "http://crewbriefing.com/")]
			public DateTime STA { get; set; }
			[XmlElement(ElementName = "ETA", Namespace = "http://crewbriefing.com/")]
			public string ETA { get; set; }
			[XmlElement(ElementName = "SCHBLOCKTIME", Namespace = "http://crewbriefing.com/")]
			public string SCHBLOCKTIME { get; set; }
			[XmlElement(ElementName = "DISP", Namespace = "http://crewbriefing.com/")]
			public string DISP { get; set; }
			[XmlElement(ElementName = "LastEditDate", Namespace = "http://crewbriefing.com/")]
			public DateTime LastEditDate { get; set; }
			[XmlElement(ElementName = "FUELMINTO", Namespace = "http://crewbriefing.com/")]
			public string FUELMINTO { get; set; }
			[XmlElement(ElementName = "TIMEMINTO", Namespace = "http://crewbriefing.com/")]
			public string TIMEMINTO { get; set; }
			[XmlElement(ElementName = "ARAMP", Namespace = "http://crewbriefing.com/")]
			public string ARAMP { get; set; }
			[XmlElement(ElementName = "TIMEACT", Namespace = "http://crewbriefing.com/")]
			public string TIMEACT { get; set; }
			[XmlElement(ElementName = "FUELACT", Namespace = "http://crewbriefing.com/")]
			public string FUELACT { get; set; }
			[XmlElement(ElementName = "DestERA", Namespace = "http://crewbriefing.com/")]
			public string DestERA { get; set; }
			[XmlElement(ElementName = "TrafficLoad", Namespace = "http://crewbriefing.com/")]
			public string TrafficLoad { get; set; }
			[XmlElement(ElementName = "WeightUnit", Namespace = "http://crewbriefing.com/")]
			public string WeightUnit { get; set; }
			[XmlElement(ElementName = "WindComponent", Namespace = "http://crewbriefing.com/")]
			public string WindComponent { get; set; }
			[XmlElement(ElementName = "CustomerDataPPS", Namespace = "http://crewbriefing.com/")]
			public string CustomerDataPPS { get; set; }
			[XmlElement(ElementName = "CustomerDataScheduled", Namespace = "http://crewbriefing.com/")]
			public string CustomerDataScheduled { get; set; }
			[XmlElement(ElementName = "Fl", Namespace = "http://crewbriefing.com/")]
			public string Fl { get; set; }
			[XmlElement(ElementName = "RouteDistNM", Namespace = "http://crewbriefing.com/")]
			public string RouteDistNM { get; set; }
			[XmlElement(ElementName = "RouteName", Namespace = "http://crewbriefing.com/")]
			public string RouteName { get; set; }
			[XmlElement(ElementName = "RouteRemark", Namespace = "http://crewbriefing.com/")]
			public string RouteRemark { get; set; }
			[XmlElement(ElementName = "EmptyWeight", Namespace = "http://crewbriefing.com/")]
			public string EmptyWeight { get; set; }
			[XmlElement(ElementName = "TotalDistance", Namespace = "http://crewbriefing.com/")]
			public string TotalDistance { get; set; }
			[XmlElement(ElementName = "AltDist", Namespace = "http://crewbriefing.com/")]
			public int AltDist { get; set; }
			[XmlElement(ElementName = "DestTime", Namespace = "http://crewbriefing.com/")]
			public string DestTime { get; set; }
			[XmlElement(ElementName = "AltTime", Namespace = "http://crewbriefing.com/")]
			public string AltTime { get; set; }
			[XmlElement(ElementName = "AltFuel", Namespace = "http://crewbriefing.com/")]
			public int AltFuel { get; set; }
			[XmlElement(ElementName = "HoldTime", Namespace = "http://crewbriefing.com/")]
			public string HoldTime { get; set; }
			[XmlElement(ElementName = "ReserveTime", Namespace = "http://crewbriefing.com/")]
			public string ReserveTime { get; set; }
			[XmlElement(ElementName = "Cargo", Namespace = "http://crewbriefing.com/")]
			public string Cargo { get; set; }
			[XmlElement(ElementName = "ActTOW", Namespace = "http://crewbriefing.com/")]
            public double ActTOW { get; set; }
			[XmlElement(ElementName = "TripFuel", Namespace = "http://crewbriefing.com/")]
			public string TripFuel { get; set; }
			[XmlElement(ElementName = "HoldFuel", Namespace = "http://crewbriefing.com/")]
			public string HoldFuel { get; set; }
			[XmlElement(ElementName = "Holding", Namespace = "http://crewbriefing.com/")]
			public Holding Holding { get; set; }
			[XmlElement(ElementName = "Elw", Namespace = "http://crewbriefing.com/")]
			public string Elw { get; set; }
			[XmlElement(ElementName = "FuelPolicy", Namespace = "http://crewbriefing.com/")]
			public string FuelPolicy { get; set; }
			[XmlElement(ElementName = "Alt2Time", Namespace = "http://crewbriefing.com/")]
			public int Alt2Time { get; set; }
			[XmlElement(ElementName = "Alt2Fuel", Namespace = "http://crewbriefing.com/")]
			public int Alt2Fuel { get; set; }
			[XmlElement(ElementName = "MaxTOM", Namespace = "http://crewbriefing.com/")]
			public string MaxTOM { get; set; }
			[XmlElement(ElementName = "MaxLM", Namespace = "http://crewbriefing.com/")]
			public string MaxLM { get; set; }
			[XmlElement(ElementName = "MaxZFM", Namespace = "http://crewbriefing.com/")]
			public string MaxZFM { get; set; }
			[XmlElement(ElementName = "WeatherObsTime", Namespace = "http://crewbriefing.com/")]
			public string WeatherObsTime { get; set; }
			[XmlElement(ElementName = "WeatherPlanTime", Namespace = "http://crewbriefing.com/")]
			public string WeatherPlanTime { get; set; }
			[XmlElement(ElementName = "MFCI", Namespace = "http://crewbriefing.com/")]
			public string MFCI { get; set; }
			[XmlElement(ElementName = "CruiseProfile", Namespace = "http://crewbriefing.com/")]
			public string CruiseProfile { get; set; }
			[XmlElement(ElementName = "TempTopOfClimb", Namespace = "http://crewbriefing.com/")]
			public string TempTopOfClimb { get; set; }
			[XmlElement(ElementName = "Climb", Namespace = "http://crewbriefing.com/")]
			public string Climb { get; set; }
			[XmlElement(ElementName = "Descend", Namespace = "http://crewbriefing.com/")]
			public string Descend { get; set; }
			[XmlElement(ElementName = "FuelPL", Namespace = "http://crewbriefing.com/")]
			public string FuelPL { get; set; }
			[XmlElement(ElementName = "DescendWind", Namespace = "http://crewbriefing.com/")]
			public string DescendWind { get; set; }
			[XmlElement(ElementName = "ClimbProfile", Namespace = "http://crewbriefing.com/")]
			public string ClimbProfile { get; set; }
			[XmlElement(ElementName = "DescendProfile", Namespace = "http://crewbriefing.com/")]
			public string DescendProfile { get; set; }
			[XmlElement(ElementName = "HoldProfile", Namespace = "http://crewbriefing.com/")]
			public string HoldProfile { get; set; }
			[XmlElement(ElementName = "StepClimbProfile", Namespace = "http://crewbriefing.com/")]
			public string StepClimbProfile { get; set; }
			[XmlElement(ElementName = "FuelContDef", Namespace = "http://crewbriefing.com/")]
			public string FuelContDef { get; set; }
			[XmlElement(ElementName = "FuelAltDef", Namespace = "http://crewbriefing.com/")]
			public string FuelAltDef { get; set; }
			[XmlElement(ElementName = "AmexsyStatus", Namespace = "http://crewbriefing.com/")]
			public string AmexsyStatus { get; set; }
			[XmlElement(ElementName = "AvgTrack", Namespace = "http://crewbriefing.com/")]
			public string AvgTrack { get; set; }
			[XmlElement(ElementName = "DEPTAF", Namespace = "http://crewbriefing.com/")]
			public string DEPTAF { get; set; }
			[XmlElement(ElementName = "DESTTAF", Namespace = "http://crewbriefing.com/")]
			public string DESTTAF { get; set; }
			[XmlElement(ElementName = "ALT1TAF", Namespace = "http://crewbriefing.com/")]
			public string ALT1TAF { get; set; }
			[XmlElement(ElementName = "ALT2TAF", Namespace = "http://crewbriefing.com/")]
			public string ALT2TAF { get; set; }
			[XmlElement(ElementName = "RoutePoints", Namespace = "http://crewbriefing.com/")]
			public RoutePoints RoutePoints { get; set; }
			[XmlElement(ElementName = "Crews", Namespace = "http://crewbriefing.com/")]
			public Crews Crews { get; set; }
			[XmlElement(ElementName = "Responce", Namespace = "http://crewbriefing.com/")]
			public Responce Responce { get; set; }
			[XmlElement(ElementName = "ATCData", Namespace = "http://crewbriefing.com/")]
			public ATCData ATCData { get; set; }
			[XmlElement(ElementName = "NextLeg", Namespace = "http://crewbriefing.com/")]
			public NextLeg NextLeg { get; set; }
			[XmlElement(ElementName = "OptFlightLevels", Namespace = "http://crewbriefing.com/")]
			public OptFlightLevels OptFlightLevels { get; set; }
			[XmlElement(ElementName = "Airports", Namespace = "http://crewbriefing.com/")]
			public AltAirports Airports { get; set; }
			[XmlElement(ElementName = "EnrouteAlternates", Namespace = "http://crewbriefing.com/")]
			public string EnrouteAlternates { get; set; }
			[XmlElement(ElementName = "Alt1Points", Namespace = "http://crewbriefing.com/")]
			public Alt1Points Alt1Points { get; set; }
			[XmlElement(ElementName = "StdAlternates", Namespace = "http://crewbriefing.com/")]
			public StdAlternates StdAlternates { get; set; }
			[XmlElement(ElementName = "CustomerData", Namespace = "http://crewbriefing.com/")]
			public CustomerData CustomerData { get; set; }
			[XmlElement(ElementName = "TOALT", Namespace = "http://crewbriefing.com/")]
			public string TOALT { get; set; }
			[XmlElement(ElementName = "RouteStrings", Namespace = "http://crewbriefing.com/")]
			public RouteStrings RouteStrings { get; set; }
			[XmlElement(ElementName = "DEPIATA", Namespace = "http://crewbriefing.com/")]
			public string DEPIATA { get; set; }
			[XmlElement(ElementName = "DESTIATA", Namespace = "http://crewbriefing.com/")]
			public string DESTIATA { get; set; }
			[XmlElement(ElementName = "FinalReserveMinutes", Namespace = "http://crewbriefing.com/")]
			public string FinalReserveMinutes { get; set; }
			[XmlElement(ElementName = "FinalReserveFuel", Namespace = "http://crewbriefing.com/")]
			public string FinalReserveFuel { get; set; }
			[XmlElement(ElementName = "AddFuelMinutes", Namespace = "http://crewbriefing.com/")]
			public int AddFuelMinutes { get; set; }
			[XmlElement(ElementName = "AddFuel", Namespace = "http://crewbriefing.com/")]
            public int AddFuel { get; set; }
			[XmlElement(ElementName = "ExternalFlightId", Namespace = "http://crewbriefing.com/")]
			public string ExternalFlightId { get; set; }
			[XmlElement(ElementName = "FlightSummary", Namespace = "http://crewbriefing.com/")]
			public string FlightSummary { get; set; }
			[XmlElement(ElementName = "PassThroughValues", Namespace = "http://crewbriefing.com/")]
			public string PassThroughValues { get; set; }
			[XmlElement(ElementName = "EtopsInformation", Namespace = "http://crewbriefing.com/")]
			public EtopsInformation EtopsInformation { get; set; }
		}

		[XmlRoot(ElementName = "GetFlightResponse", Namespace = "http://crewbriefing.com/")]
		public class GetFlightResponse
		{
			[XmlElement(ElementName = "GetFlightResult", Namespace = "http://crewbriefing.com/")]
			public GetFlightResult GetFlightResult { get; set; }
			[XmlAttribute(AttributeName = "xmlns")]
			public string Xmlns { get; set; }
		}

		[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public class FlightResponseBody
	{
			[XmlElement(ElementName = "GetFlightResponse", Namespace = "http://crewbriefing.com/")]
			public GetFlightResponse GetFlightResponse { get; set; }
		}

		[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public class FlightResponseEnvelope : BaseResponseModel
		{
			[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
			public FlightResponseBody Body { get; set; }
			[XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
			public string Soap { get; set; }
			[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
			public string Xsi { get; set; }
			[XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
			public string Xsd { get; set; }
		}

	}


