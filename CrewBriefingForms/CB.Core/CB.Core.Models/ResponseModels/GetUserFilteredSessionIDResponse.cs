﻿using CB.Core.Models.Base;
using System.Xml.Serialization;

namespace CB.Core.Models.ResponseModels
{
	[XmlRoot(ElementName = "GetUserFilteredSessionIDResponse", Namespace = "http://crewbriefing.com/")]
	public class GetUserFilteredSessionIDResponse 
	{
		[XmlElement(ElementName = "GetUserFilteredSessionIDResult", Namespace = "http://crewbriefing.com/")]
		public string GetUserFilteredSessionIDResult { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public string Xmlns { get; set; }
	}

	[XmlRoot(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class LoginBody
	{
		[XmlElement(ElementName = "GetUserFilteredSessionIDResponse", Namespace = "http://crewbriefing.com/")]
		public GetUserFilteredSessionIDResponse GetUserFilteredSessionIDResponse { get; set; }
	}

	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public class LoginResponseEnvelope : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public LoginBody Body { get; set; }
	
	}
}
