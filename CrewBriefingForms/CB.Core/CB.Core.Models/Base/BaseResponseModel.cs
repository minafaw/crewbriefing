﻿using System;
using System.Collections.Generic;
using System.Text;
using CB.Core.Common.Keys;

namespace CB.Core.Models.Base
{
    public class BaseResponseModel
    {
	    public Enums.WebApiErrorCode Code { get; set; }
	    public string Message { get; set; }
	}
}
