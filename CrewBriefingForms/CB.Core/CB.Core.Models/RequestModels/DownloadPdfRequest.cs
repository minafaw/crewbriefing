﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CB.Core.Models.RequestModels
{
    class DownloadPdfRequest
    {
    }

	public class GetPDF_LogstringRequest
	{
		public string SessionID { get; set; }
		public int FlightID { get; set; }
	}

	public class GetPDF_MessagesRequest
	{
		public string SessionID { get; set; }
		public int FlightID { get; set; }
		public bool importantOnly { get; set; }
	}

	public class GetFlightDocumentRequest
	{
		public string SessionID { get; set; }
		public string documentIdentifier { get; set; }
	}

	public class GetPDF_WXRequest
	{
		public string SessionID { get; set; }
		public int FlightID { get; set; }
		public bool halfSizePages { get; set; }
	}
}
