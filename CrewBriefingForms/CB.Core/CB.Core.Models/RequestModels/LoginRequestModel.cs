﻿using CB.Core.Common.Keys;
using CB.Core.Models.Base;

namespace CB.Core.Models.RequestModels
{
    public class LoginRequestModel : BaseRequestModel
    {
        public string PartnerUser { get; set; } = CommanConstants.PartnerUserName;
        public string PartnerPassword { get; set; } = CommanConstants.PartnerPassword;
        public string CustomerUser { get; set; }
        public string CustomerPassword { get; set; }
    }
}
