﻿using System.Xml.Serialization;

namespace CB.Core.Models
{
    [XmlRoot(Namespace = "http://crewbriefing.com/")]
    public class FlightRequest : AuthentifiedRequest
    {
        public string SessionID { get; set; }
        public int FlightID { get; set; }
        public bool Taf { get; set; }
        public bool Metar { get; set; }
        public bool Notams { get; set; }
        public bool Messages { get; set; }
    }

    
    /// <remarks/>
    [XmlType(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeBody
    {

        //private GetFlightResponse getFlightResponseField;

        ///// <remarks/>
        //[XmlElement(Namespace = "http://crewbriefing.com/")]
        //public GetFlightResponse GetFlightResponse
        //{
        //    get
        //    {
        //        return getFlightResponseField;
        //    }
        //    set
        //    {
        //        getFlightResponseField = value;
        //    }
        //}
    }

    //[XmlType(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    //[XmlRoot(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    //public partial class GetFlightResponse
    //{
    //    private Flight getFlightResultField;

    //    /// <remarks/>
    //    public Flight GetFlightResult
    //    {
    //        get
    //        {
    //            return this.getFlightResultField;
    //        }
    //        set
    //        {
    //            this.getFlightResultField = value;
    //        }
    //    }
    //}



}
