﻿using CB.Core.Models.Base;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CB.Core.Models
{
	[XmlRoot(ElementName = "Envelope", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
	public partial class FlightListSearchResponseRoot : BaseResponseModel
	{
		[XmlElement(ElementName = "Body", Namespace = "http://www.w3.org/2003/05/soap-envelope")]
		public FlightEnvelopeBody Body { get; set; }
	}

	public class FlightEnvelopeBody : BaseResponseModel
	{
		[XmlElement(ElementName = "GetFlightListSearchResponse", Namespace = "http://crewbriefing.com/")]
		public GetFlightListSearchResponse GetFlightListSearchResponse { get; set; }
	}

	public partial class GetFlightListSearchResponse
    {
		[XmlElement(ElementName = "GetFlightListSearchResult", Namespace = "http://crewbriefing.com/")]

		public GetFlightListSearchResult GetFlightListSearchResult { get; set; }
	}

    [XmlType(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetFlightListSearchResult
	{
        [XmlArrayItem("FlightListItem", IsNullable = false)]
        public List<FlightListItem> Items { get; set; }
        public Responce Response { get; set; }
        public bool MoreFlightsToGet { get; set; }
    }

    [XmlType(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class Responce
	{
        public bool Succeed { get; set; }
    }

}
