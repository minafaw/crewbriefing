﻿using System.Xml.Serialization;

namespace CB.Core.Models
{
    [XmlRoot(Namespace = "http://crewbriefing.com/")]
    public abstract class AuthentifiedRequest
    {
    }

    [XmlInclude(typeof(FlightSearchRequest))]
    public class WebServiceAuthentifidRequestDataWrapper
    {
        public string SessionID { get; set; }
        public string ChangedAfter { get; set; }

        // [XmlElement(ElementName = "request")]
        // public Models.AuthentifiedRequest data { get; set; }
    }


}
