using System.Xml.Serialization;

namespace CB.Core.Models
{
	[XmlRoot(ElementName = "Crew", Namespace = "http://crewbriefing.com/")]
	public class Crew
    {
		[XmlElement(ElementName = "ID", Namespace = "http://crewbriefing.com/")]
		public string Id { get; set; }
		[XmlElement(ElementName = "CrewType", Namespace = "http://crewbriefing.com/")]
		public string CrewType { get; set; }
		[XmlElement(ElementName = "CrewName", Namespace = "http://crewbriefing.com/")]
		public string CrewName { get; set; }
		[XmlElement(ElementName = "Initials", Namespace = "http://crewbriefing.com/")]
		public string Initials { get; set; }
		[XmlElement(ElementName = "GSM", Namespace = "http://crewbriefing.com/")]
		public string Gsm { get; set; }
		

    }
}