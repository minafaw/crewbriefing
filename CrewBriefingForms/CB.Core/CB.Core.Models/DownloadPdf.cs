﻿using CB.Core.Models.ResponseModels;
using System;

namespace CB.Core.Models
{
   
    
    public class GetPDF_FullPackageRequest
    {
        public string SessionID { get; set; }
        public int FlightID { get; set; }
        public bool Messages { get; set; }
        public bool FlightLog { get; set; }
        public bool WX { get; set; }
        public bool Notams { get; set; }
        public bool ATC { get; set; }
        public bool Charts { get; set; }
        public bool Uploaded { get; set; }
    }

    // -- GetMessages

    
    
    //GetPDF_ATC
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public class EnvelopeATC
    {
        public EnvelopeATCBody Body { get; set; }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeATCBody
    {
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_ATCResponse GetPDF_ATCResponse { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetPDF_ATCResponse
    {
        public GetPDF_Result GetPDF_ATCResult { get; set; }
    }
    //--GetFlightDocument
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public partial class EnvelopeFlightDoc
    {

		/// <remarks/>
		public EnvelopeBodyFlightDoc Body { get; set; }
	}

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public partial class EnvelopeBodyFlightDoc
    {

		/// <remarks/>
		[System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
		public GetFlightDocumentResponse GetFlightDocumentResponse { get; set; }
	}

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetFlightDocumentResponse
    {

		/// <remarks/>
		public GetFlightDocumentResponseGetFlightDocumentResult GetFlightDocumentResult { get; set; }
	}

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetFlightDocumentResponseGetFlightDocumentResult
    {

		//private Response responseField;

		/// <remarks/>
		public GetFlightDocumentResponseGetFlightDocumentResultMeta Meta { get; set; }

		/// <remarks/>
		public byte[] ByteArray { get; set; }

		
	}

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class GetFlightDocumentResponseGetFlightDocumentResultMeta
    {

		/// <remarks/>
		public string Title { get; set; }

		/// <remarks/>
		public System.DateTime ValidFrom { get; set; }

		/// <remarks/>
		public System.DateTime ValidTo { get; set; }

		/// <remarks/>
		public uint FlightId { get; set; }

		/// <remarks/>
		public string Identifier { get; set; }

		/// <remarks/>
		public string Type { get; set; }

		/// <remarks/>
		public string Category { get; set; }

		/// <remarks/>
		public string Info1 { get; set; }

		/// <remarks/>
		public object Info2 { get; set; }

		/// <remarks/>
		public System.DateTime Created { get; set; }

		/// <remarks/>
		public System.DateTime BasedOnForecast { get; set; }
	}
    //GetPDF_FullPackage
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.w3.org/2003/05/soap-envelope", IsNullable = false, ElementName = "Envelope")]
    public class EnvelopeFullPackage
    {
        public EnvelopeFullPackageBody Body { get; set; }
    }
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.w3.org/2003/05/soap-envelope")]
    public class EnvelopeFullPackageBody
    {
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://crewbriefing.com/")]
        public GetPDF_FullPackageResponse GetPDF_FullPackageResponse { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://crewbriefing.com/", IsNullable = false)]
    public partial class GetPDF_FullPackageResponse
    {
        public GetPDF_Result GetPDF_FullPackageResult { get; set; }
    }


}
