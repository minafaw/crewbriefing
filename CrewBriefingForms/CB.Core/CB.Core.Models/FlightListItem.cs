using System;
using System.Collections.Generic;
using System.ComponentModel;
using CB.Core.Common.HelperClasses;
using CB.Core.Models.Base;

namespace CB.Core.Models
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://crewbriefing.com/")]
    public partial class FlightListItem : BaseResponseModel
    {
        [System.Xml.Serialization.XmlArrayItemAttribute("Crew", IsNullable = false)]
        public List<Crew> Crew { get; set; }
        public string Alt1 { get; set; }
        public string Alt2 { get; set; }
        public int ID { get; set; }
        public int FPLID { get; set; }
    
        public string FlightlogID { get; set; }
  
        public string DEP { get; set; }
       
        public string DEST { get; set; }

        public System.DateTime STD { get; set; }
       
        public System.DateTime STA { get; set; }
     
        public string ACFTAIL { get; set; }
       
        public ushort ATCTime { get; set; }
    
        public string ATCCtot { get; set; }
       
        public ushort ATCEET { get; set; }
    
        public string TOA { get; set; }
       
        public string ATCID { get; set; }
      
        public System.DateTime LastEdit { get; set; }        
        //public string Dep_Dest => (DEP ?? "") + "-" + (DEST ?? "");
       //public string PreparedStd => CommonStatic.GetPreparedStd(STD);
	    public string DestAirportFull { get;set; }
        public string DepAirportFull { get; set;}
       
    }
}