﻿using CB.Core.Models.ResponseModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace CB.Core.Models
{
	[XmlType(Namespace = "http://crewbriefing.com/")]
	[XmlInclude(typeof(ATCData))]
	public partial class ATC
	{

		public string ATCID { get; set; }
		public string ATCTOA { get; set; }
		public string ATCRule { get; set; }
		public string ATCType { get; set; }
		public string ATCNum { get; set; }
		public string ATCWake { get; set; }
		public string ATCEqui { get; set; }
		public string ATCSSR { get; set; }
		public string ATCDep { get; set; }
		public string ATCTime { get; set; }
		public string ATCSpeed { get; set; }
		public string ATCFL { get; set; }
		public string ATCRoute { get; set; }
		public string ATCDest { get; set; }
		public string ATCEET { get; set; }
		public string ATCAlt1 { get; set; }
		public string ATCAlt2 { get; set; }
		public string ATCInfo { get; set; }
		public string ATCEndu { get; set; }
		public string ATCPers { get; set; }
		public string ATCRadi { get; set; }
		public string ATCSurv { get; set; }
		public string ATCJack { get; set; }
		public string ATCDing { get; set; }
		public string ATCCap { get; set; }
		public string ATCCover { get; set; }		
		public string ATCColo { get; set; }
		public string ATCAcco { get; set; }
		public string ATCRem { get; set; }
		public string ATCPIC { get; set; }
		public string ATCCtot { get; set; }
		public int FlightID { get; set; }
	}

}
