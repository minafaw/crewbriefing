﻿using System;
using System.Collections;
using System.Collections.Generic;
using CB.Core.CustomControls.Helper;
using Xamarin.Forms;

namespace CB.Core.CustomControls.Controls
{
    public class BindableRadioGroup : StackLayout
    {
        public List<CustomRadioButton> Rads;

        public BindableRadioGroup()
        {
            Rads = new List<CustomRadioButton>();
        }

        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create(nameof(RadioTextColor), typeof(Color), typeof(BindableRadioGroup), Color.Black);

        public static readonly BindableProperty RadioHintColorProperty =
            BindableProperty.Create(nameof(RadioHintColor), typeof(Color), typeof(BindableRadioGroup), Color.Black);

        public static readonly BindableProperty RadioNormalColorProperty =
            BindableProperty.Create(nameof(RadioNormalColor), typeof(Color), typeof(BindableRadioGroup), Color.Black);

        public static BindableProperty ItemsSourceProperty =
            BindableProperty.Create(nameof(ItemsSource), typeof(IEnumerable) , typeof(BindableRadioGroup) ,default(IEnumerable) , propertyChanged: OnItemsSourceChanged);

        public static BindableProperty SelectedIndexProperty =
            BindableProperty.Create(nameof(SelectedIndex), typeof(int), typeof(BindableRadioGroup), default(int), propertyChanged: OnSelectedIndexChanged , defaultBindingMode:BindingMode.TwoWay);

        public IEnumerable ItemsSource
        {
            get => (IEnumerable)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

       
        public int SelectedIndex
        {
            get => (int)GetValue(SelectedIndexProperty);
            set => SetValue(SelectedIndexProperty, value);
        }

        public Color RadioTextColor
        {
            get => (Color)GetValue(TextColorProperty);
            set => SetValue(TextColorProperty, value);
        }

        public Color RadioHintColor
        {
            get => (Color)GetValue(RadioHintColorProperty);
            set => SetValue(RadioHintColorProperty, value);
        }

        public Color RadioNormalColor
        {
            get => (Color)GetValue(RadioNormalColorProperty);

            set => SetValue(RadioNormalColorProperty, value);
        }

        public static readonly BindableProperty HandleArabicProperty = BindableProperty.Create(nameof(HandleArabic), typeof(bool), typeof(ExtLabel), false);

        public bool HandleArabic
        {
            get => (bool)GetValue(HandleArabicProperty);
            set => SetValue(HandleArabicProperty, value);
        }


      
        public event EventHandler<int> CheckedChanged;

        private static void OnItemsSourceChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var radButtons = bindable as BindableRadioGroup;

            if (radButtons != null)
            {
                radButtons.Rads.Clear();
                radButtons.Children.Clear();
                if (newvalue != null && newvalue is IEnumerable)
                {
              
                    int radIndex = 0;
                    foreach (var item in (IEnumerable)newvalue)
                    {
                        var rad = new CustomRadioButton
                        {
                            ButtonText = item.ToString(),
                            ButtonId = radIndex,
                            HandleArabic = radButtons.HandleArabic,
                            RadioHintColor = radButtons.RadioHintColor,
                            TextColor = radButtons.RadioTextColor,
                            RadioNormalColor = radButtons.RadioNormalColor
                        };
                        rad.CheckedChanged += radButtons.OnCheckedChanged;
                        radButtons.Rads.Add(rad);         
                        radButtons.Children.Add(rad);
                        radIndex++;
                    }
                }
            }
        }

        private void OnCheckedChanged(object sender, EventArgs<bool> e)
        {
           
            if (e.Value == false) return;

            var selectedRad = sender as CustomRadioButton;

            foreach (var rad in Rads)
            {
                if(selectedRad != null && !selectedRad.Id.Equals(rad.Id))
                {
                    rad.Checked = false;
                }
                else
                {
                    if(CheckedChanged != null){
                        CheckedChanged.Invoke(sender, rad.ButtonId);
                        SelectedIndex = rad.ButtonId;
                    }
                    	 
                   
                }
            }

        }

        private static void OnSelectedIndexChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            if ((int)newvalue == -1) return;

            var bindableRadioGroup = bindable as BindableRadioGroup;


            if (bindableRadioGroup != null)
                foreach (var rad in bindableRadioGroup.Rads)
                {
                    if (rad.ButtonId == bindableRadioGroup.SelectedIndex)
                    {
                        rad.Checked = true;
                    }
                }
        }
    
    }
}
