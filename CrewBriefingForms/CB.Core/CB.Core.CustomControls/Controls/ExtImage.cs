﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace CB.Core.CustomControls.Controls
{
    public class ExtImage : Image
    {
		public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ExtImage), null, propertyChanged: (bo, o, n) => ((ExtImage)bo).OnCommandChanged());

	    public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ExtImage), null,
		    propertyChanged: (bindable, oldvalue, newvalue) => ((ExtImage)bindable).CommandCanExecuteChanged(bindable, EventArgs.Empty));

	    public ICommand Command
	    {
		    get => (ICommand)GetValue(CommandProperty);
		    set => SetValue(CommandProperty, value);
	    }

	    bool IsEnabledCore
	    {
		    set => SetValueCore(IsEnabledProperty, value);
	    }

	    public object CommandParameter
	    {
		    get => GetValue(CommandParameterProperty);
		    set => SetValue(CommandParameterProperty, value);
	    }

	    private void OnCommandChanged()
	    {

		    if (Command != null)
		    {
			    var tapGesture = new TapGestureRecognizer()
			    {
				    NumberOfTapsRequired = 1,
				    Command = Command
			    };
			    GestureRecognizers.Add(tapGesture);
		    }

		    if (Command != null)
		    {
			    Command.CanExecuteChanged += CommandCanExecuteChanged;
			    CommandCanExecuteChanged(this, EventArgs.Empty);
		    }
		    else
			    IsEnabledCore = true;
	    }

	    void CommandCanExecuteChanged(object sender, EventArgs eventArgs)
	    {
		    ICommand cmd = Command;
		    if (cmd != null)
			    IsEnabledCore = cmd.CanExecute(CommandParameter);
	    }
	}
}
