﻿using Xamarin.Forms;

namespace CB.Core.CustomControls.Controls
{
    public class ExtButton : Button
    {
        public static readonly BindableProperty PaddingProperty =
            BindableProperty.Create(nameof(Padding), typeof(Thickness), typeof(ExtButton), new Thickness(0));

        public Thickness Padding
        {
            get => (Thickness)GetValue(PaddingProperty);
            set => SetValue(PaddingProperty, value);
        }

        public static readonly BindableProperty HandleArabicProperty = BindableProperty.Create(nameof(HandleArabic), typeof(bool), typeof(ExtLabel), false);

        public bool HandleArabic
        {
            get => (bool)GetValue(HandleArabicProperty);
            set => SetValue(HandleArabicProperty, value);
        }


    }
}
