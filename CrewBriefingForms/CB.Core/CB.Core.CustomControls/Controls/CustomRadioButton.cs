﻿using System;
using CB.Core.CustomControls.Helper;
using Xamarin.Forms;

namespace CB.Core.CustomControls.Controls
{
    public class CustomRadioButton : View
    {
        public static readonly BindableProperty CheckedProperty =
            BindableProperty.Create(nameof(Checked),typeof(bool),typeof(CustomRadioButton),false);

        /// <summary>
        /// The default text property.
        /// </summary>
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(ButtonText) ,typeof(string) , typeof(CustomRadioButton),string.Empty);

        public static readonly BindableProperty RadioHintColorProperty =
            BindableProperty.Create(nameof(RadioHintColor), typeof(Color), typeof(CustomRadioButton), Color.Black);

        public static readonly BindableProperty RadioNormalColorProperty =
            BindableProperty.Create(nameof(RadioNormalColor), typeof(Color), typeof(CustomRadioButton), Color.Black);

        public static readonly BindableProperty HandleArabicProperty = BindableProperty.Create(nameof(HandleArabic), typeof(bool), typeof(ExtLabel), false);

        public bool HandleArabic
        {
            get { return (bool)GetValue(HandleArabicProperty); }
            set { SetValue(HandleArabicProperty, value); }
        }

        /// <summary>
        /// The checked changed event.
        /// </summary>
        public event EventHandler<EventArgs<bool>> CheckedChanged;


        /// <summary>
        /// Identifies the TextColor bindable property.
        /// </summary>
        /// 
        /// <remarks/>
        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(CustomRadioButton), Color.Black);

        /// <summary>
        /// Gets or sets a value indicating whether the control is checked.
        /// </summary>
        /// <value>The checked state.</value>
        public bool Checked
        {
            get
            {
                return (bool)GetValue(CheckedProperty);
            }

            set
            {
                SetValue(CheckedProperty, value);
                var eventHandler = this.CheckedChanged;
                var passedEvents = new EventArgs<bool>(value);
                eventHandler?.Invoke(this , passedEvents);

            }
        }

        public string ButtonText
        {
            get
            {
                return (string)GetValue(TextProperty) ;
            }

            set
            {
                this.SetValue(TextProperty, value);
            }
        }

        public Color TextColor
        {
            get
            {
                return (Color)GetValue(TextColorProperty);
            }

            set
            {
                this.SetValue(TextColorProperty, value);
            }
        }

        public Color RadioHintColor
        {
            get
            {
                return (Color)GetValue(RadioHintColorProperty);
            }

            set
            {
                this.SetValue(RadioHintColorProperty, value);
            }
        }

        public Color RadioNormalColor
        {
            get
            {
                return (Color)GetValue(RadioNormalColorProperty);
            }

            set
            {
                this.SetValue(RadioNormalColorProperty, value);
            }
        }



        public int ButtonId { get; set; }
   
        
       
    }

  
}
