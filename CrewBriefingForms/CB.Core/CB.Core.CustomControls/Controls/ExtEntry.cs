﻿using Xamarin.Forms;

namespace CB.Core.CustomControls.Controls
{
    public class ExtEntry : Entry
    {
        public static readonly BindableProperty EntryBackgroundProperty =
            BindableProperty.Create(nameof(EntryBackground),
                typeof(Color), typeof(ExtEntry), Color.White);
        public Color EntryBackground
        {
            get => (Color)GetValue(EntryBackgroundProperty);
            set => SetValue(EntryBackgroundProperty, value);
        }

        public static readonly BindableProperty HasUnderLineProperty =
            BindableProperty.Create(nameof(HasUnderLine), typeof(bool), typeof(ExtEntry), false);

        public bool HasUnderLine
        {
            get => (bool)GetValue(HasUnderLineProperty);
            set => SetValue(HasUnderLineProperty, value);
        }


        public static readonly BindableProperty HasBorderProperty =
            BindableProperty.Create(nameof(HasBorder), typeof(bool), typeof(ExtEntry), false);

        public bool HasBorder
        {
            get => (bool)GetValue(HasBorderProperty);
            set => SetValue(HasBorderProperty, value);
        }


        public static readonly BindableProperty BorderColorProperty =
            BindableProperty.Create(nameof(BorderColor), typeof(Color), typeof(ExtEntry), Color.Transparent);

        public Color BorderColor
        {
            get => (Color)GetValue(BorderColorProperty);
            set => SetValue(HasBorderProperty, value);
        }

        public static readonly BindableProperty PaddingProperty =
            BindableProperty.Create(nameof(Padding), typeof(Thickness), typeof(ExtEntry), default(Thickness));

        public Thickness Padding
        {
            get => (Thickness)GetValue(PaddingProperty);
            set => SetValue(PaddingProperty, value);
        }


        public static readonly BindableProperty ImageProperty =
            BindableProperty.Create(nameof(Image), typeof(string), typeof(ExtEntry), string.Empty);

        public static readonly BindableProperty LineColorProperty =
            BindableProperty.Create(nameof(LineColor), typeof(Color), typeof(ExtEntry), Color.White);

        public static readonly BindableProperty ImageHeightProperty =
            BindableProperty.Create(nameof(ImageHeight), typeof(int), typeof(ExtEntry), 40);

        public static readonly BindableProperty ImageWidthProperty =
            BindableProperty.Create(nameof(ImageWidth), typeof(int), typeof(ExtEntry), 40);

        public static readonly BindableProperty ImageAlignmentProperty =
            BindableProperty.Create(nameof(ImageAlignment), typeof(ImageAlignment), typeof(ExtEntry), ImageAlignment.Left);

        public Color LineColor
        {
            get => (Color)GetValue(LineColorProperty);
            set => SetValue(LineColorProperty, value);
        }

        public int ImageWidth
        {
            get => (int)GetValue(ImageWidthProperty);
            set => SetValue(ImageWidthProperty, value);
        }

        public int ImageHeight
        {
            get => (int)GetValue(ImageHeightProperty);
            set => SetValue(ImageHeightProperty, value);
        }

        public string Image
        {
            get => (string)GetValue(ImageProperty);
            set => SetValue(ImageProperty, value);
        }

        public ImageAlignment ImageAlignment
        {
            get => (ImageAlignment)GetValue(ImageAlignmentProperty);
            set => SetValue(ImageAlignmentProperty, value);
        }

    }
    public enum ImageAlignment
    {
        Left,
        Right
    }
}
