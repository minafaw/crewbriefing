﻿using CB.Core.CustomControls.Helper;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace CB.Core.CustomControls.Controls
{
    public class ExtLabel : Label 
    {
        public static readonly BindableProperty IsUnderlinedProperty = BindableProperty.Create(nameof(IsUnderlined), typeof(bool), typeof(ExtLabel), false);

        public bool IsUnderlined
        {
            get => (bool)GetValue(IsUnderlinedProperty);
            set => SetValue(IsUnderlinedProperty, value);
        }

        // set default value to handle RTL 
        public static readonly BindableProperty HandleArabicProperty = BindableProperty.Create(nameof(HandleArabic), typeof(bool), typeof(ExtLabel), false);

        public bool HandleArabic
        {
            get => (bool)GetValue(HandleArabicProperty);
            set => SetValue(HandleArabicProperty, value);
        }

        public static readonly BindableProperty DefaultEnglishFontProperty = BindableProperty.Create(nameof(DefaultEnglishFont), typeof(string), typeof(ExtLabel), ControlsConstants.DefaultEnglishFont);

        public string DefaultEnglishFont
        {
            get => (string)GetValue(DefaultEnglishFontProperty);
            set => SetValue(DefaultEnglishFontProperty, value);
        }


        public static readonly BindableProperty DefaultArabicFontProperty = BindableProperty.Create(nameof(DefaultArabicFont), typeof(string), typeof(ExtLabel), ControlsConstants.DefaultArabicFont);

        public string DefaultArabicFont 
        {
            get => (string)GetValue(DefaultArabicFontProperty);
            set => SetValue(DefaultArabicFontProperty, value);
        }

        public static readonly BindableProperty PaddingProperty =
            BindableProperty.Create(nameof(Padding), typeof(Thickness), typeof(ExtLabel), new Thickness(0));

        public Thickness Padding
        {
            get => (Thickness)GetValue(PaddingProperty);
            set => SetValue(PaddingProperty, value);
        }

		public static readonly BindableProperty CommandProperty = BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ExtLabel), null, propertyChanged: (bo, o, n) => ((ExtLabel)bo).OnCommandChanged());

		public static readonly BindableProperty CommandParameterProperty = BindableProperty.Create(nameof(CommandParameter), typeof(object), typeof(ExtLabel), null,
			propertyChanged: (bindable, oldvalue, newvalue) => ((ExtLabel)bindable).CommandCanExecuteChanged(bindable, EventArgs.Empty));

		public ICommand Command
		{
			get => (ICommand)GetValue(CommandProperty);
			set => SetValue(CommandProperty, value);
		}
		public object CommandParameter
		{
			get => GetValue(CommandParameterProperty);
			set => SetValue(CommandParameterProperty, value);
		}
		bool IsEnabledCore
		{
			set => SetValueCore(IsEnabledProperty, value);
		}

		private void OnCommandChanged()
		{

			if (Command != null)
			{
				var tapGesture = new TapGestureRecognizer()
				{
					NumberOfTapsRequired = 1,
					Command = Command
				};
				GestureRecognizers.Add(tapGesture);
			}

			if (Command != null)
			{
				Command.CanExecuteChanged += CommandCanExecuteChanged;
				CommandCanExecuteChanged(this, EventArgs.Empty);
			}
			else
				IsEnabledCore = true;
		}

		void CommandCanExecuteChanged(object sender, EventArgs eventArgs)
		{
			ICommand cmd = Command;
			if (cmd != null)
				IsEnabledCore = cmd.CanExecute(CommandParameter);
		}


	}
}
