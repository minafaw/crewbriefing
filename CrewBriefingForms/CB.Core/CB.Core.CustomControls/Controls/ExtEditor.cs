﻿using Xamarin.Forms;

namespace CB.Core.CustomControls.Controls
{
    public class ExtEditor : Editor
    {
       

        public static readonly BindableProperty HasUnderLineProperty =
            BindableProperty.Create(nameof(HasUnderLine), typeof(bool), typeof(ExtEntry), false);

        public bool HasUnderLine
        {
            get => (bool)GetValue(HasUnderLineProperty);
            set => SetValue(HasUnderLineProperty, value);
        }

       

    }
}

