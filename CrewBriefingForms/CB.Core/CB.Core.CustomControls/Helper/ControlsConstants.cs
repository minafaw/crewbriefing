﻿namespace CB.Core.CustomControls.Helper
{
    public class ControlsConstants
    {
        public static string DefaultEnglishFont = "Sansation_Regular.ttf";
        public static string DefaultArabicFont = "QSWArabic-Regular.otf";
    }
}
