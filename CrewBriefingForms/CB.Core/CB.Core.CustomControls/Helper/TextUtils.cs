﻿namespace CB.Core.CustomControls.Helper
{
    public class TextUtils
    {
        public static bool IsArabic(string text)
        {
            char[] chars = text.ToCharArray();
            foreach (var archar in chars)
            {
                if (archar >= 0x600 && archar <= 0x6ff) return true;
                if (archar >= 0x750 && archar <= 0x77f) return true;
                if (archar >= 0xfb50 && archar <= 0xfc3f) return true;
                if (archar >= 0xfe70 && archar <= 0xfefc) return true;
            }
            return false;
        }
    }
}
