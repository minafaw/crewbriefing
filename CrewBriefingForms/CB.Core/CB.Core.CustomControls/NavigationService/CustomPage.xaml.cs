﻿using Xamarin.Forms;

namespace CB.Core.CustomControls.NavigationService
{
    public partial class CustomPage : ContentPage
    {
		public static readonly BindableProperty FormattedTitleProperty = BindableProperty.Create(nameof(FormattedTitle), typeof(FormattedString), typeof(CustomPage), null);

        public FormattedString FormattedTitle
        {
            get => (FormattedString)GetValue(FormattedTitleProperty);
	        set => SetValue(FormattedTitleProperty, value);
        }

        public static readonly BindableProperty FormattedSubtitleProperty = BindableProperty.Create(nameof(FormattedSubtitle), typeof(FormattedString), typeof(CustomPage), null);

        public FormattedString FormattedSubtitle
        {
            get => (FormattedString)GetValue(FormattedSubtitleProperty);
	        set => SetValue(FormattedSubtitleProperty, value);
        }

        public static readonly BindableProperty SubtitleProperty = BindableProperty.Create(nameof(Subtitle), typeof(string), typeof(CustomPage), null);


        public string Subtitle
        {
            get => (string)GetValue(SubtitleProperty);
	        set => SetValue(SubtitleProperty, value);
        }

        public CustomPage()
        {
            InitializeComponent();
        }
    }
}
