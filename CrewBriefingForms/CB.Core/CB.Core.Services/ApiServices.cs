﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CB.Core.Common;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Keys;
using CB.Core.Common.Setting;
using CB.Core.DAL;
using CB.Core.DAL.Interfaces;
using CB.Core.Models;
using CB.Core.Models.Base;
using CB.Core.Models.RequestModels;
using CB.Core.Models.ResponseModels;
using CB.Core.Services.HelperClasses;
using CB.Core.Services.Interfaces;
using CB.Core.Services.Keys;
using Xamarin.Forms.Internals;

namespace CB.Core.Services
{
	[Preserve(AllMembers = true)]
	public class ApiServices : IApiServices
    {
	    public readonly IApiManager ApiManager;
		private readonly IDalServices _dalServices;
        private static string _soapUrl = "https://www.crewbriefing.aero/EFB/v1.7/EfbService.asmx";

		public ApiServices(IApiManager apiManager , IDalServices dalServices )
        {
            ApiManager = apiManager;
			_dalServices = dalServices;
        }

		// TODO find best place to call it 
		public async Task Intialize()
		{
			 await FetchUrlAsync();	
		}

        public async Task<bool> FetchUrlAsync()
        {
	        var xmlData = string.Format(Resource.data_fetch_url_request + "{0} {1} {2}" , ServiceConstants.ApiKey, ServiceConstants.AppName, ServiceConstants.AppVersion);
            var response = await ApiManager.PostDataAsync<BaseResponseModel>( ServiceConstants.SoapUrl, xmlData);
            if (!response.Equals( default(BaseResponseModel)))
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(EnvelopeFetchUrl));
                    using (var reader = new StringReader(response.Message))
                    {
                        var resultWrapper = (EnvelopeFetchUrl)serializer.Deserialize(reader);
	                    if (string.IsNullOrEmpty(resultWrapper.Body.ResolveServiceSingleCustomerResponse.Services
		                    .ServiceInfo.Url)) return false;
	                    
	                    _soapUrl = resultWrapper.Body.ResolveServiceSingleCustomerResponse.Services.ServiceInfo.Url;
	                    return true;
                    }
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.WriteLine(exc.Message);
                    ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, "Error parse: FetchUrl " + response.Message);
                    return false;
                }
            }
	        ToastUtils.ShowTemporayMessage(Enums.ToastType.Error, response.Message);
            return false;
        }

		// we assum that user already logged in and need to check if token is not expired and if its here we need to refresh it 
		public async Task<bool> CheckSessionAndRefreshIfRequired()
		{
			if (_dalServices.IsSessionExpired())
			{
				string username = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.UserNameKey, null) , 
					   password = AppSetting.Instance.GetValueOrDefault<string>(CommanConstants.PasswordKey, null);
				

				if(!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password)){
					var result = await Login(username, password);
					if(result.Code == Enums.WebApiErrorCode.Success)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			return true;
		}

		public async Task<LoginResponseEnvelope> GetSessionIdAsync(string username, string password)
        {
           
            var logindata = new LoginRequestModel
            {
                PartnerUser = CommanConstants.PartnerUserName,
                PartnerPassword = CommanConstants.PartnerPassword,
                CustomerPassword = password,
                CustomerUser = username
            };
            var wrp = new WebServiceLoginBodyWrapper { LoginData = logindata };
            var rootWrapper = new WebServiceLoginRootWrapper { Body = wrp };

            var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
            return await ApiManager.PostDataAsync<LoginResponseEnvelope>(_soapUrl  , xmlData );
        }

        public async Task<LoginResponseEnvelope> Login(string userName, string password)
	    {
           
            LoginResponseEnvelope sessIdResponse = await GetSessionIdAsync(userName, password);
			
				var sessionId = sessIdResponse?.Body?.GetUserFilteredSessionIDResponse?.GetUserFilteredSessionIDResult;
				if (!string.IsNullOrEmpty(sessionId) )
				{
					if( !sessionId.Contains("Credentials are not valid for"))
					{
						sessIdResponse.Code = Enums.WebApiErrorCode.Success;
						AppSetting.Instance.AddOrUpdateValue(CommanConstants.CurrentSessionKey, sessionId);
						AppSetting.Instance.AddOrUpdateValue(CommanConstants.SessionReceivedKey, DateTime.UtcNow);
					}
					else
					{
						sessIdResponse.Code = Enums.WebApiErrorCode.WrongCredentials;
						sessIdResponse.Message = "Invalid Username or Password";
					}
				
				}
				else
				{
                        if(sessIdResponse == null){
                            sessIdResponse = new LoginResponseEnvelope();
                        }
					sessIdResponse.Code = Enums.WebApiErrorCode.Undefined;
					sessIdResponse.Message = "Can not retrieve SessionID";
				}
            return sessIdResponse;
		}

	    public async Task<FlightListSearchResponseRoot> GetFlightList(FlightSearchRequest request, string sessionId)
	    {
			var wrp = new WebServiceGetFlightListSearchBodyWrapper()
		    {
			    method =
				    new WebServiceAuthentifidRequestDataWrapper()
				    {
					    SessionID = sessionId,
					    ChangedAfter = request.ChangedAfter
				    }
		    };
		    var rootWrapper = new WebServiceGetFlightListSearchRootWrapper { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<FlightListSearchResponseRoot>( _soapUrl , xmlData);
		}

	     public async Task<FlightResponseEnvelope> GetFlight(FlightRequest request)
	    {
			var wrp = new WebServiceGetFlightBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetFlightRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<FlightResponseEnvelope>( _soapUrl , xmlData);
		}

	    public async Task<LogsPdfEnvelope> GetPDF_Logstring(GetPDF_LogstringRequest request)
	    {
			var wrp = new WebServiceGetPDF_LogstringBodyWrapper()
			{
				method = request
			};
			var rootWrapper = new WebServiceGetPDF_LogstringRootWrapper() { Body = wrp };

			var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
			return await ApiManager.PostDataAsync<LogsPdfEnvelope>( _soapUrl , xmlData);
		}

		public async Task<T> GetPDF_Messages<T>(GetPDF_MessagesRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetPDF_MessagesBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetPDF_MessagesRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<T>( _soapUrl , xmlData);
		}

	    public async Task<T> GetPDF_WX<T>(GetPDF_WXRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetPDF_WXBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetPDF_WXRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<T>( _soapUrl, xmlData);
		}

	    public async Task<T> GetPDF_NOTAMs<T>(GetPDF_WXRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetPDF_NOTAMsBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetPDF_NOTAMsRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<T>( _soapUrl , xmlData);
		}

	    public async Task<T> GetPDF_ATC<T>(GetPDF_LogstringRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetPDF_ATCBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetPDF_ATCRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
			return await ApiManager.PostDataAsync<T>( _soapUrl , xmlData);
		}

	    public async Task<T> GetFlightDoc<T>(GetFlightDocumentRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetFlightDocumentBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetFlightDocumentRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<T>(_soapUrl , xmlData);
		}

	    public async Task<T> GetPDF_FullPackage<T>(GetPDF_FullPackageRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetPDF_FullPackageBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetPDF_FullPackageRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<T>( _soapUrl , xmlData);
		}

	    public async Task<T> GetAirport<T>(GetAirportRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetAirportBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetAirportRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<T>(_soapUrl , xmlData);
		}

	    public async Task<T> GetAirportList<T>(GetAirportListRequest request) where T : BaseResponseModel
	    {
			var wrp = new WebServiceGetAirportListBodyWrapper()
		    {
			    method = request
		    };
		    var rootWrapper = new WebServiceGetAirportListRootWrapper() { Body = wrp };

		    var xmlData = ServiceHelper.SerializeForWebService(rootWrapper);
		    return await ApiManager.PostDataAsync<T>( _soapUrl , xmlData);
		}
    }
}
