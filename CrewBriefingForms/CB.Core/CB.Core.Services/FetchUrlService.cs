﻿using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CB.Core.Common;
using CB.Core.Common.HelperClasses;
using CB.Core.Common.Keys;
using CB.Core.Models;
using CB.Core.Models.Base;
using CB.Core.Services.HelperClasses;
using CB.Core.Services.Keys;
using static CB.Core.Common.Keys.Enums;

namespace CB.Core.Services
{
    public class FetchUrlService
    {
        
        public static async Task<string> FetchUrl()
        {
            var result = string.Empty;
            var clientName = string.Empty;

            var xmlData = string.Format(Resource.data_fetch_url_request, ServiceConstants.ApiKey, ServiceConstants.AppName, ServiceConstants.AppVersion);
            var response = await GetDataFromWebService(xmlData);
            if (response.Code == Enums.WebApiErrorCode.Success)
            {
                try
                {
                    var serializer = new XmlSerializer(typeof(EnvelopeFetchUrl));
                    using (var reader = new StringReader(response.Message))
                    {
                        var resultWrapper = (EnvelopeFetchUrl)serializer.Deserialize(reader);
                        return resultWrapper.Body.ResolveServiceSingleCustomerResponse.Services.ServiceInfo.Url;
                    }
                }
                catch (Exception exc)
                {
                    System.Diagnostics.Debug.WriteLine(exc.Message);
                    ToastUtils.ShowTemporayMessage(ToastType.Error, "Error parse: FetchUrl " + response.Message);
                    return string.Empty;
                }
            }else {
                ToastUtils.ShowTemporayMessage(ToastType.Error, response.Message);
                return string.Empty;
            }
        }

        // 120 sec timeout
        public static async Task<BaseResponseModel> GetDataFromWebService(string requestXmltData)
        {

            var client = new HttpClient
            {
                Timeout = CommanConstants.RequestTimeout
            };

            var request = new HttpRequestMessage(HttpMethod.Post, ServiceConstants.SoapUrl)
            {
                Content = new StringContent(requestXmltData,
                    Encoding.UTF8,
                    "text/xml")
            };

            request.Headers.Add("SOAPAction", "http://www.airsupport.dk/urlfetcher/IAutomaticEndpointResolverService/ResolveServices");

            var result = new BaseResponseModel { Code = WebApiErrorCode.NullError };
            var startRequest = DateTime.Now;


            try
            {
                var response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    result.Message = await response.Content.ReadAsStringAsync();
                    result.Code = WebApiErrorCode.Success;
                    return result;
                }
                var err = await response.Content.ReadAsStringAsync();
               ServiceHelper.ParseErrorMessage(err, ref result);
                return result;

            }
            catch (System.Net.WebException webExeption)
            {
                string errorMessage;
                if (webExeption.Response != null)
                {
                    try
                    {
                        using (var stream = webExeption.Response.GetResponseStream())
                        {
                            using (var responseStream = new StreamReader(stream))
                            {
                                errorMessage = await responseStream.ReadToEndAsync();
                            }
                            ServiceHelper.ParseErrorMessage(errorMessage, ref result);
                            return result;
                        }
                    }
                    catch
                    {
                    }
                }
                result.Message = "Unkonwn error";

            }
            catch (Exception exc)
            {
                var ts = DateTime.Now - startRequest;
                if (ts.TotalMilliseconds >= CommanConstants.RequestTimeout.TotalMilliseconds)
                {
                    result.Code = Enums.WebApiErrorCode.TimeOut;
                    result.Message = Resource.errot_request_timeout;
                }
                else {
                    result.Message = exc.Message;
                }
                result.Message = exc.Message;
            }
            return result;
        }

    }
    /// <remarks/>
    [XmlType(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    [XmlRoot(Namespace = "http://schemas.xmlsoap.org/soap/envelope/", IsNullable = false, ElementName = "Envelope")]
    public partial class EnvelopeFetchUrl
    {
        /// <remarks/>
        public EnvelopeBody Body { get; set; }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true, Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class EnvelopeBody
    {

        private ResolveServiceSingleCustomerResponse resolveServiceSingleCustomerResponseField;

        /// <remarks/>
        [XmlElement(Namespace = "http://www.airsupport.dk/urlfetcher")]
        public ResolveServiceSingleCustomerResponse ResolveServiceSingleCustomerResponse
        {
            get
            {
                return this.resolveServiceSingleCustomerResponseField;
            }
            set
            {
                this.resolveServiceSingleCustomerResponseField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true, Namespace = "http://www.airsupport.dk/urlfetcher")]
    [XmlRoot(Namespace = "http://www.airsupport.dk/urlfetcher", IsNullable = false)]
    public partial class ResolveServiceSingleCustomerResponse
    {

        private ResolveServiceSingleCustomerResponseServices servicesField;

        /// <remarks/>
        public ResolveServiceSingleCustomerResponseServices Services
        {
            get
            {
                return this.servicesField;
            }
            set
            {
                this.servicesField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true, Namespace = "http://www.airsupport.dk/urlfetcher")]
    public partial class ResolveServiceSingleCustomerResponseServices
    {

        private ResolveServiceSingleCustomerResponseServicesServiceInfo serviceInfoField;

        /// <remarks/>
        public ResolveServiceSingleCustomerResponseServicesServiceInfo ServiceInfo
        {
            get
            {
                return this.serviceInfoField;
            }
            set
            {
                this.serviceInfoField = value;
            }
        }
    }

    /// <remarks/>
    [XmlType(AnonymousType = true, Namespace = "http://www.airsupport.dk/urlfetcher")]
    public partial class ResolveServiceSingleCustomerResponseServicesServiceInfo
    {

        private string nameField;

        private string versionField;

        private string urlField;

        private object propertiesField;

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string Version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }

        /// <remarks/>
        public string Url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }

        /// <remarks/>
        public object Properties
        {
            get
            {
                return this.propertiesField;
            }
            set
            {
                this.propertiesField = value;
            }
        }
    }



}
