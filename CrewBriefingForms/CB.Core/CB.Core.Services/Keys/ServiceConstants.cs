﻿using System;
namespace CB.Core.Services.Keys
{
    public static class ServiceConstants
    {
        public static readonly string SoapUrl = "http://services.airsupport.dk/AutomaticEndpointResolverService/Service.svc";		    
        public static readonly string ApiKey = "3A3F7419-A92A-496D-AD7E-657F2E6BCD12";
        public static readonly string AppName = "CB_MOBILE_NEXTGEN";
        public static readonly string AppVersion = "1.8.0.0";

    }
}
