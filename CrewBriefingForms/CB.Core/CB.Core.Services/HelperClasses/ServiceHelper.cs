﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using CB.Core.Common.Keys;
using CB.Core.Models.Base;

namespace CB.Core.Services.HelperClasses
{
    public static class ServiceHelper
    {
        public static string GetErrorMessage(Enums.WebApiErrorCode code)
        {
            string parsedMessage;
            switch (code)
            {
                case Enums.WebApiErrorCode.InvalidSession:
                    parsedMessage =  CommanConstants.InvalidSession;
                    break;
                case Enums.WebApiErrorCode.NoNet:
                    parsedMessage= CommanConstants.NoNet;
                    break;
                case Enums.WebApiErrorCode.NullError:
                    parsedMessage = CommanConstants.NullError;
                    break;
                case Enums.WebApiErrorCode.TimeOut:
                    parsedMessage = CommanConstants.RequestTimeOutError;
                    break;
                case Enums.WebApiErrorCode.Undefined:
                    parsedMessage = CommanConstants.UndefinedError;
                    break;
                case Enums.WebApiErrorCode.WrongCredentials:
                    parsedMessage = CommanConstants.WrongCredentials;
                    break;
                case Enums.WebApiErrorCode.Success:
                    // it never should be success 
                    parsedMessage = CommanConstants.UndefinedError + code;
                    break;
                default:
                    parsedMessage = CommanConstants.UndefinedError + code;
                    break;
            }

            return string.Format(CommanConstants.ErrorMsgFormat, parsedMessage);
        }

		public static string ParseSessionIdResponse(string xmlResponse)
		{
			var serializer = new XmlSerializer(typeof(WebServiceLoginResponseRootWrapper));

			using (TextReader reader = new StringReader(xmlResponse))
			{
				var result = (WebServiceLoginResponseRootWrapper)serializer.Deserialize(reader);
				return result.Body.ResponseData.GetUserFilteredSessionIDResult;
			}

		}

		public static void ParseErrorMessage(string errorMessage, ref BaseResponseModel result)
        {
	        if (string.IsNullOrEmpty(errorMessage)) return;
	        var lowerErrorMsg = errorMessage.ToLower();
	        switch (lowerErrorMsg)
	        {
		        case string a when a.Contains("credentials are not valid"):
		        {
			        result.Message = "Invalid Username or Password";
			        result.Code = Enums.WebApiErrorCode.WrongCredentials;
			        break;
		        }

		        case string a when a.Contains("web service timeout") || a.Contains("aborted") ||
		                           a.Contains("NameResolutionFailure"):
		        {
			        result.Message = "Please check your internet connection and try again.";
			        result.Code = Enums.WebApiErrorCode.TimeOut;
			        break;
		        }
		        case string a when a.Contains("uploaded documents not available"):
		        {
			        result.Message = "Uploaded documents not available";
			        result.Code = Enums.WebApiErrorCode.TimeOut;
			        break;
		        }
		        case string a when a.Contains("invalid sessionid"):
		        {
			        result.Message = "Invalid session";
			        result.Code = Enums.WebApiErrorCode.InvalidSession;
			        break;
		        }
		        default:
		        {
			        result.Message = "Something went wrong. Please try again.";
			        result.Code = Enums.WebApiErrorCode.Undefined;
			        break;
		        }

	        }
        }


        public static string SerializeForWebService(Object value)
        {
            var xsSubmit = new XmlSerializer(value.GetType());

            var ns = new XmlSerializerNamespaces();

            ns.Add("soap", "http://www.w3.org/2003/05/soap-envelope");
            ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            ns.Add("xsd", "http://www.w3.org/2001/XMLSchema");
            //ns.Add("", "http://crewbriefing.com/");

            var ms = new MemoryStream();
            var sww = new StreamWriter(ms);
            var settings = new XmlWriterSettings { Encoding = Encoding.UTF8 };
            using (var writer = XmlWriter.Create(sww, settings))
            {
                xsSubmit.Serialize(writer, value, ns);
                writer.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                using (var sr = new StreamReader(ms, Encoding.UTF8))
                {
                    return sr.ReadToEnd();
                }
                //xmlData = sww.ToString(); // Your XML
            }
        }

    }
}
