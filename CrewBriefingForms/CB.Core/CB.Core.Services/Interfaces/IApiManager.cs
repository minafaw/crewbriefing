﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CB.Core.Models.Base;

namespace CB.Core.Services.Interfaces
{
    public interface IApiManager
    {
        Task<T> GetDataAsync<T>(string url, Dictionary<string,string> httpHeaders = null) where T:BaseResponseModel;
	    Task<T> PostDataAsync<T>(string url, object body, Dictionary<string, string> httpHeaders = null) where T : BaseResponseModel;
    }
}
