﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CB.Core.Models;
using CB.Core.Models.Base;
using CB.Core.Models.RequestModels;
using CB.Core.Models.ResponseModels;

namespace CB.Core.Services.Interfaces
{
    public interface IApiServices
    {
       // Task<bool> FetchUrlAsync();
		Task Intialize();

		Task<LoginResponseEnvelope> GetSessionIdAsync(string username, string password);

		Task<bool> CheckSessionAndRefreshIfRequired();

        Task<LoginResponseEnvelope> Login(string userName, string password) ;
	    Task<FlightListSearchResponseRoot> GetFlightList(FlightSearchRequest request, string sessionId);
	    Task<FlightResponseEnvelope> GetFlight(FlightRequest request);
	    Task<LogsPdfEnvelope> GetPDF_Logstring(GetPDF_LogstringRequest request);
	    Task<T> GetPDF_Messages<T>(GetPDF_MessagesRequest request) where T : BaseResponseModel;
	    Task<T> GetPDF_WX<T>(GetPDF_WXRequest request) where T : BaseResponseModel;
	    Task<T> GetPDF_NOTAMs<T>(GetPDF_WXRequest request) where T : BaseResponseModel;
	    Task<T> GetPDF_ATC<T>(GetPDF_LogstringRequest request) where T : BaseResponseModel;
	    Task<T> GetFlightDoc<T>(GetFlightDocumentRequest request) where T : BaseResponseModel;
	    Task<T> GetPDF_FullPackage<T>(GetPDF_FullPackageRequest request) where T : BaseResponseModel;
	    Task<T> GetAirport<T>(GetAirportRequest request) where T : BaseResponseModel;
	    Task<T> GetAirportList<T>(GetAirportListRequest request) where T : BaseResponseModel;

    }
}
