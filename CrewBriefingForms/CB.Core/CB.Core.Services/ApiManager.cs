﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CB.Core.Common;
using CB.Core.Common.Keys;
using CB.Core.Models.Base;
using CB.Core.Services.Interfaces;
using Newtonsoft.Json;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;

namespace CB.Core.Services
{
    public class ApiManager : IApiManager
    {

        readonly IConnectivity _connectivity = CrossConnectivity.Current;
        public bool IsConnected { get; set; }

	    public ApiManager()
	    {
		    IsConnected = _connectivity.IsConnected;
			_connectivity.ConnectivityChanged += OnConnectivityChanged;
	    }

		private void OnConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
		{
			IsConnected = e.IsConnected;
		}

        public async Task<T> GetDataAsync<T>(string url , Dictionary<string, string> httpHeaders ) where T :BaseResponseModel
        {
            var responseModel = new BaseResponseModel();
            if (IsConnected)
            {
                responseModel.Code = Enums.WebApiErrorCode.NoNet;
                return (T)responseModel;
            }

            var client = new HttpClient { Timeout = CommanConstants.RequestTimeout };
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            if (httpHeaders != null){
                foreach(var header in httpHeaders){
                    request.Headers.TryAddWithoutValidation (header.Key , header.Value);  
                }
                  
            } 
            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode) return default(T);
            try
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return default(T);
            }
        }

        public async Task<T> PostDataAsync<T>(string url, object body , Dictionary<string, string> httpHeaders ) where T : BaseResponseModel
		{
            var responseModel = new BaseResponseModel();
			if (IsConnected)
			{
				return await MakeWebRequest<T>(url, body, httpHeaders);
			}
	        responseModel.Code = Enums.WebApiErrorCode.NoNet;
	        return (T)responseModel;
        }

	   // public async Task<T> PostListDataAsync<T>(string url, object body, Dictionary<string, string> httpHeaders = null) where T : IEnumerable<BaseResponseModel>
	  //  {
			//var responseModel = new List<BaseResponseModel>();
			//if (IsConnected)
			//{
			//	return await MakeWebRequest<T>(url, body, httpHeaders);
			//}
		 //   responseModel[0].Code = Enums.WebApiErrorCode.NoNet;
		 //   return (T) (IEnumerable<BaseResponseModel>)responseModel;
	  //  }

	    private static async Task<T> MakeWebRequest<T>(string url, object body, Dictionary<string, string> httpHeaders)
	    {
		    var client = new HttpClient {Timeout = CommanConstants.RequestTimeout};
		    var request = new HttpRequestMessage(HttpMethod.Post, url)
		    {
			    //TODO parse object to string
			    Content = new StringContent(body.ToString(), Encoding.UTF8, "application/soap+xml")
		    };

		    if (httpHeaders != null)
		    {
			    foreach (var header in httpHeaders)
			    {
				    request.Headers.TryAddWithoutValidation(header.Key, header.Value);
			    }
		    }
            try
            {
		    var response = await client.SendAsync(request);

		    if (!response.IsSuccessStatusCode) return default(T);
		    
			    var result = response.Content.ReadAsStringAsync().Result;

				var serializer = new XmlSerializer(typeof(T));

				using (TextReader reader = new StringReader(result))
				{
					var resultWrapper = (T)serializer.Deserialize(reader);
					System.Diagnostics.Debug.WriteLine("SucessedWithResult " + resultWrapper);
					return resultWrapper;
				}
		    }
		    catch (Exception e)
		    {
			    Console.WriteLine(e);
			    return default(T);
		    }
	    }
    }
}
